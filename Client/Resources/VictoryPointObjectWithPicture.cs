﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client.Resources
{
    class VictoryPointObjectWithPicture : Panel
    {
        public int victoryPoints { get; private set; }

        public VictoryPointObjectWithPicture(String picture, int pointsValue)
        {
            victoryPoints = pointsValue;

            BackColor = Color.Transparent;
            BackgroundImage = Image.FromFile(picture);
            BackgroundImageLayout = ImageLayout.Stretch;
        }
    }
}
