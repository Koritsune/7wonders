﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client.Resources
{
    public class PNLDebtTokens : Panel
    {
        Player player;
        List<VictoryPointObjectWithPicture> debtTokens = new List<VictoryPointObjectWithPicture>();
        Size buttonSize;

        public event DebtTokenReceivedHandler DebtTokenReceived;

        public delegate void DebtTokenReceivedHandler(object source, DebtTokenReceivedEvent e);

        public class DebtTokenReceivedEvent : EventArgs
        {
            private string EventInfo;
            public DebtTokenReceivedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public PNLDebtTokens(Player player_)
        {
            if (player_.getPlayerRotation() == Player.PlayerRotation.None || player_.getPlayerRotation() == Player.PlayerRotation.Clockwise180)
            {
                buttonSize = new Size(Height, Height);
            }
            else
            {
                buttonSize = new Size(Height / 8, Height / 8);
            }

            BackColor = Color.Transparent;
            player = player_;
            this.Resize += resize;
        }
        private delegate void addDebtD(int amount);

        public void addDebt(int amount)
        {
            if (this.InvokeRequired)
            {
                addDebtD d = new addDebtD(addDebt);
                this.Invoke(d, new object[] { amount });
            }
            else
            {
                for (int count2 = 0; count2 < amount; count2++)
                {
                    VictoryPointObjectWithPicture debtToken = new VictoryPointObjectWithPicture(Constants.ICONS + "Debt1.PNG", 1);

                    debtToken.Size = buttonSize;

                    if (player.getPlayerRotation() == Player.PlayerRotation.None || player.getPlayerRotation() == Player.PlayerRotation.Clockwise180)
                    {
                        debtToken.Location = new Point(buttonSize.Width * debtTokens.Count, 0);
                    }
                    else
                    {
                        debtToken.Location = new Point(0, buttonSize.Height * debtTokens.Count);
                    }

                    Controls.Add(debtToken);

                    debtTokens.Add(debtToken);

                    if (canReduce())
                    {
                        int reductionGoal = debtTokens.Count - 5;
                        int count = debtTokens.Count - 5;

                        while (debtTokens.Count != reductionGoal)
                        {
                            Controls.Remove(debtTokens[count]);
                            debtTokens.RemoveAt(count);
                        }

                        VictoryPointObjectWithPicture debtToken5 = new VictoryPointObjectWithPicture(Constants.ICONS + "Debt5.PNG", 5);

                        debtToken.Size = buttonSize;
                        if (player.getPlayerRotation() == Player.PlayerRotation.None || player.getPlayerRotation() == Player.PlayerRotation.Clockwise180)
                        {
                            debtToken5.Location = new Point(buttonSize.Width * debtTokens.Count, 0);
                        }
                        else
                        {
                            debtToken5.Location = new Point(0, buttonSize.Height * debtTokens.Count);
                        }
                        debtTokens.Add(debtToken5);
                        Controls.Add(debtToken5);
                    }
                }

                if (DebtTokenReceived != null)
                {
                    DebtTokenReceived(this, new DebtTokenReceivedEvent(""));
                }
            }
        }
        private void resize(object source, EventArgs e)
        {
            if (player.getPlayerRotation() == Player.PlayerRotation.None || player.getPlayerRotation() == Player.PlayerRotation.Clockwise180)
            {
                buttonSize = new Size(Height, Height);
            }
            else
            {
                buttonSize = new Size(Height / 8, Height / 8);
            }

            if (player.getPlayerRotation() == Player.PlayerRotation.None || player.getPlayerRotation() == Player.PlayerRotation.Clockwise180)
            {
                for (int count = 0; count < debtTokens.Count; count++)
                {
                    debtTokens[count].Size = buttonSize;
                    debtTokens[count].Location = new Point(buttonSize.Width * count, 0);
                }
            }
            else
            {
                for (int count = 0; count < debtTokens.Count; count++)
                {
                    debtTokens[count].Size = buttonSize;
                    debtTokens[count].Location = new Point(0, buttonSize.Height * count);
                }
            }
        }
        private bool canReduce()
        {
            int count = 0;

            foreach (VictoryPointObjectWithPicture item in debtTokens)
            {
                if (item.victoryPoints == 1)
                {
                    count++;
                }
            }

            return count >= 5;
        }
        public int getVictoryPoints()
        {
            int points = 0;

            foreach (VictoryPointObjectWithPicture item in debtTokens)
            {
                points += item.victoryPoints;
            }

            return points;
        }
    }
}
