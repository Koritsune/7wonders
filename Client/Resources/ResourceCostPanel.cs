﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client.Resources
{
    public class ResourceCostPanel : Panel
    {
        List<ResourceToggle> cost = new List<ResourceToggle>();
        Card cardToCheck;
        public Player.PlayerRotation rotation { get; set; }
        Player player;
        Resource goldReduction = new Resource();
        public bool canPlayCard { get; private set; }
        Resource missingResources;
        Resource emptyResource = new Resource();
        Form1 form;
        WonderStage wonderStageToPlay;

        int allowedVariance = 0;

        public event CardPurchaseEventHandler cardPurchase;
        public event CardCancelHandler cardCancel;

        public delegate void CardCancelHandler(object source, CardCancelEvent e);

        public class CardCancelEvent : EventArgs
        {
            private string EventInfo;
            public CardCancelEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public delegate void CardPurchaseEventHandler(object source, CardPurchaseEvent e);

        public class CardPurchaseEvent : EventArgs
        {
            private string EventInfo;
            public CardPurchaseEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public ResourceCostPanel(Player player_, Form1 form_)
        {
            canPlayCard = false;
            rotation = Player.PlayerRotation.None;
            player = player_;
            //player.ResourceAdded += updateCanPlayCard;
            this.Resize += resizeMe;
            form = form_;
        }
        private void resizeMe(Object source, EventArgs e)
        {
            Size newSize = new Size(Height, Height);

            for (int count = 0; count < cost.Count; count++)
            {
                cost[count].Location = new Point(newSize.Width * count, 0);
                cost[count].Size = newSize;
            }
        }
        public void checkCardCost(Card cardToCheck_)
        {
            wonderStageToPlay = null;
            canPlayCard = true;

            cardToCheck = cardToCheck_;

            missingResources = player.resources - cardToCheck.cost;
            
            goldReduction.gold = -1 * cardToCheck.cost.gold;
            player.addResource(goldReduction);

            if (player.resources.gold < 0)
            {
                canPlayCard = false;
            }

            missingResources.gold = 0;

            if (missingResources.loom < 0)
            {
                canPlayCard = false;
                for (int count = missingResources.loom; count < 0; count++)
                {
                    ResourceToggle toggle = new ResourceToggle(ResourceToggle.Type.Loom,null,false);
                    toggle.Size = new Size(Height,Height);
                    toggle.Location = new Point(Height * cost.Count,0);
                    Controls.Add(toggle);
                    cost.Add(toggle);
                }
            }
            else
            {
                missingResources.loom = 0;
            }

            if (missingResources.papyrus < 0)
            {
                canPlayCard = false;
                for (int count = missingResources.papyrus; count < 0; count++)
                {
                    ResourceToggle toggle = new ResourceToggle(ResourceToggle.Type.Papyrus,null,false);
                    toggle.Size = new Size(Height, Height);
                    toggle.Location = new Point(Height * cost.Count, 0);
                    Controls.Add(toggle);
                    cost.Add(toggle);
                }
            }
            else
            {
                missingResources.papyrus = 0;
            }

            if (missingResources.glassworks < 0)
            {
                canPlayCard = false;
                for (int count = missingResources.glassworks; count < 0; count++)
                {
                    ResourceToggle toggle = new ResourceToggle(ResourceToggle.Type.Glass,null,false);
                    toggle.Size = new Size(Height, Height);
                    toggle.Location = new Point(Height * cost.Count, 0);
                    Controls.Add(toggle);
                    cost.Add(toggle);
                }
            }
            else
            {
                missingResources.glassworks = 0;
            }

            if (missingResources.clay < 0)
            {
                canPlayCard = false;
                for (int count = missingResources.clay; count < 0; count++)
                {
                    ResourceToggle toggle = new ResourceToggle(ResourceToggle.Type.Clay,null,false);
                    toggle.Size = new Size(Height, Height);
                    toggle.Location = new Point(Height * cost.Count, 0);
                    Controls.Add(toggle);
                    cost.Add(toggle);
                }
            }
            else
            {
                missingResources.clay = 0;
            }

            if (missingResources.ore < 0)
            {
                canPlayCard = false;
                for (int count = missingResources.ore; count < 0; count++)
                {
                    ResourceToggle toggle = new ResourceToggle(ResourceToggle.Type.Ore, null,false);
                    toggle.Size = new Size(Height, Height);
                    toggle.Location = new Point(Height * cost.Count, 0);
                    Controls.Add(toggle);
                    cost.Add(toggle);
                }
            }
            else
            {
                missingResources.ore = 0;
            }

            if (missingResources.silver < 0)
            {
                canPlayCard = false;
                for (int count = missingResources.silver; count < 0; count++)
                {
                    ResourceToggle toggle = new ResourceToggle(ResourceToggle.Type.Stone,null,false);
                    toggle.Size = new Size(Height, Height);
                    toggle.Location = new Point(Height * cost.Count, 0);
                    Controls.Add(toggle);
                    cost.Add(toggle);
                }
            }
            else
            {
                missingResources.silver = 0;
            }

            if (missingResources.wood < 0)
            {
                canPlayCard = false;
                for (int count = missingResources.wood; count < 0; count++)
                {
                    ResourceToggle toggle = new ResourceToggle(ResourceToggle.Type.Wood,null,false);
                    toggle.Size = new Size(Height, Height);
                    toggle.Location = new Point(Height * cost.Count, 0);
                    Controls.Add(toggle);
                    cost.Add(toggle);
                }
            }
            else
            {
                missingResources.wood = 0;
            }

            missingResources.gold = 0;

            missingResources.glassworks *= -1;
            missingResources.loom *= -1;
            missingResources.papyrus *= -1;

            missingResources.clay *= -1;
            missingResources.ore *= -1;
            missingResources.silver *= -1;
            missingResources.wood *= -1;

            if (missingResources.sumResources() - allowedVariance <= 0 && player.resources.gold >= 0)
            {
                canPlayCard = true;
            }
        }
        public void clear()
        {
            canPlayCard = false;

            while (cost.Count != 0)
            {
                Controls.Remove(cost[0]);
                cost.RemoveAt(0);
            }

            goldReduction.gold = -1 * goldReduction.gold;
            player.addResource(goldReduction);

            goldReduction.gold = 0;
            missingResources = new Resource();

            if (cardCancel != null)
            {
                cardCancel(this, new CardCancelEvent("Card was purchase canceled"));
            }

        }
        public void purchase()
        {
            if (goldReduction.gold < 0)
            {
                form.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "COIN" + Constants.DELIM_FIELD + player.getUsername() + Constants.DELIM_FIELD + goldReduction.gold);
            }

            if (cardPurchase != null)
            {
                cardPurchase(this, new CardPurchaseEvent("Card was purchased"));
            }

            cardToCheck = new Card(Constants.PICTURES + "Age I\\Back",player);
        }
        public bool needs(Resource resource)
        {
            return missingResources.contains(resource);
        }
        public void add(Resource resource)
        {
            foreach (ResourceToggle item in cost)
            {
                if (item.generates == resource && !item.active)
                {
                    item.active = true;
                    missingResources -= resource;

                    if (missingResources == emptyResource || missingResources.sumResources() - allowedVariance <= 0)
                    {
                        canPlayCard = true;
                    }

                    updateCanPlayCard();
                    break;
                }
            }
        }
        public void remove(Resource resource)
        {
            foreach (ResourceToggle item in cost)
            {
                if (item.generates == resource && item.active)
                {
                    canPlayCard = false;
                    missingResources += resource;
                    item.active = false;
                }
            }
        }
        private void updateCanPlayCard()
        {
            if (player.resources.gold < 0)
            {
                canPlayCard = false;
            }
        }
        public void update()
        {
            int oldVariance = allowedVariance;

            clear();
            if (cardToCheck != null)
            {
                checkCardCost(cardToCheck);
            }
            else
            {
                checkWonderCost(wonderStageToPlay);
            }

            allowedVariance = oldVariance;
            updateCanPlayCard();
        }
        public void checkWonderCost(WonderStage wonderStage)
        {
            try
            {
                cardToCheck = null;
                canPlayCard = true;
                wonderStageToPlay = wonderStage;

                missingResources = player.resources - wonderStage.cost;

                goldReduction.gold = -1 * wonderStage.cost.gold;
                player.addResource(goldReduction);

                if (player.resources.gold < 0)
                {
                    canPlayCard = false;
                }

                missingResources.gold = 0;

                if (missingResources.loom < 0)
                {
                    canPlayCard = false;
                    for (int count = missingResources.loom; count < 0; count++)
                    {
                        ResourceToggle toggle = new ResourceToggle(ResourceToggle.Type.Loom, null, false);
                        toggle.Size = new Size(Height, Height);
                        toggle.Location = new Point(Height * cost.Count, 0);
                        Controls.Add(toggle);
                        cost.Add(toggle);
                    }
                }
                else
                {
                    missingResources.loom = 0;
                }

                if (missingResources.papyrus < 0)
                {
                    canPlayCard = false;
                    for (int count = missingResources.papyrus; count < 0; count++)
                    {
                        ResourceToggle toggle = new ResourceToggle(ResourceToggle.Type.Papyrus, null, false);
                        toggle.Size = new Size(Height, Height);
                        toggle.Location = new Point(Height * cost.Count, 0);
                        Controls.Add(toggle);
                        cost.Add(toggle);
                    }
                }
                else
                {
                    missingResources.papyrus = 0;
                }

                if (missingResources.glassworks < 0)
                {
                    canPlayCard = false;
                    for (int count = missingResources.glassworks; count < 0; count++)
                    {
                        ResourceToggle toggle = new ResourceToggle(ResourceToggle.Type.Glass, null, false);
                        toggle.Size = new Size(Height, Height);
                        toggle.Location = new Point(Height * cost.Count, 0);
                        Controls.Add(toggle);
                        cost.Add(toggle);
                    }
                }
                else
                {
                    missingResources.glassworks = 0;
                }

                if (missingResources.clay < 0)
                {
                    canPlayCard = false;
                    for (int count = missingResources.clay; count < 0; count++)
                    {
                        ResourceToggle toggle = new ResourceToggle(ResourceToggle.Type.Clay, null, false);
                        toggle.Size = new Size(Height, Height);
                        toggle.Location = new Point(Height * cost.Count, 0);
                        Controls.Add(toggle);
                        cost.Add(toggle);
                    }
                }
                else
                {
                    missingResources.clay = 0;
                }

                if (missingResources.ore < 0)
                {
                    canPlayCard = false;
                    for (int count = missingResources.ore; count < 0; count++)
                    {
                        ResourceToggle toggle = new ResourceToggle(ResourceToggle.Type.Ore, null, false);
                        toggle.Size = new Size(Height, Height);
                        toggle.Location = new Point(Height * cost.Count, 0);
                        Controls.Add(toggle);
                        cost.Add(toggle);
                    }
                }
                else
                {
                    missingResources.ore = 0;
                }

                if (missingResources.silver < 0)
                {
                    canPlayCard = false;
                    for (int count = missingResources.silver; count < 0; count++)
                    {
                        ResourceToggle toggle = new ResourceToggle(ResourceToggle.Type.Stone, null, false);
                        toggle.Size = new Size(Height, Height);
                        toggle.Location = new Point(Height * cost.Count, 0);
                        Controls.Add(toggle);
                        cost.Add(toggle);
                    }
                }
                else
                {
                    missingResources.silver = 0;
                }

                if (missingResources.wood < 0)
                {
                    canPlayCard = false;
                    for (int count = missingResources.wood; count < 0; count++)
                    {
                        ResourceToggle toggle = new ResourceToggle(ResourceToggle.Type.Wood, null, false);
                        toggle.Size = new Size(Height, Height);
                        toggle.Location = new Point(Height * cost.Count, 0);
                        Controls.Add(toggle);
                        cost.Add(toggle);
                    }
                }
                else
                {
                    missingResources.wood = 0;
                }

                missingResources.gold = 0;

                missingResources.glassworks *= -1;
                missingResources.loom *= -1;
                missingResources.papyrus *= -1;

                missingResources.clay *= -1;
                missingResources.ore *= -1;
                missingResources.silver *= -1;
                missingResources.wood *= -1;

                if (missingResources.sumResources() - allowedVariance <= 0 && player.resources.gold >= 0)
                {
                    canPlayCard = true;
                }
            }
            catch (NullReferenceException e)
            {
                canPlayCard = false;
            }
        }
        public void increaseVariance()
        {
            allowedVariance++;
        }
        public void decreaseVariance()
        {
            allowedVariance--;

            if (allowedVariance < 0)
            {
                allowedVariance = 0;
            }
        }
    }
}
