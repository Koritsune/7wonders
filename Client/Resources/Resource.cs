﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Client.Resources
{
    public class Resource
    {
        public int gold {get; set;}

        public int glassworks  {get; set;}
        public int loom  {get; set;}
        public int papyrus  {get; set;}

        public int clay  {get; set;}
        public int ore  {get; set;}
        public int silver  {get; set;}
        public int wood  {get; set;}

        public static Resource glassResource = new Resource(0,1,0,0,0,0,0,0);
        public static Resource loomResource = new Resource(0,0,1,0,0,0,0,0);
        public static Resource papyrusResource = new Resource(0,0,0,1,0,0,0,0);

        public static Resource clayResource = new Resource(0,0,0,0,1,0,0,0);
        public static Resource oreResource = new Resource(0,0,0,0,0,1,0,0);
        public static Resource stoneResource = new Resource(0,0,0,0,0,0,1,0);
        public static Resource woodResource = new Resource(0,0,0,0,0,0,0,1);

        public Resource()
        {
            gold = 0;

            glassworks = 0;
            loom = 0;
            papyrus = 0;

            clay = 0;
            ore = 0;
            silver = 0;
            wood = 0;
        }
        public Resource(int gold_, int glass_, int loom_, int papyrus_, int clay_, int ore_, int silver_, int wood_)
        {
            gold = gold_;

            glassworks = glass_;
            loom = loom_;
            papyrus = papyrus_;

            clay = clay_;
            ore = ore_;
            silver = silver_;
            wood = wood_;
        }
        public static Resource operator +(Resource r1, Resource r2)
        {
            Resource result = new Resource();

            result.gold = r1.gold + r2.gold;
            
            result.glassworks = r1.glassworks + r2.glassworks;
            result.loom = r1.loom + r2.loom;
            result.papyrus = r1.papyrus + r2.papyrus;

            result.clay = r1.clay + r2.clay;
            result.ore = r1.ore + r2.ore;
            result.silver = r1.silver + r2.silver;
            result.wood = r1.wood + r2.wood;

            return result;
        }
        public int this[int index]
        {
            get
            {
                if (index == 0)
                {
                    return this.gold;
                }
                else if (index == 1)
                {
                    return this.glassworks;
                }
                else if (index == 2)
                {
                    return this.loom;
                }
                else if (index == 3)
                {
                    return this.papyrus;
                }
                else if (index == 4)
                {
                    return this.clay;
                }
                else if (index == 5)
                {
                    return this.ore;
                }
                else if (index == 6)
                {
                    return this.silver;
                }
                else
                {
                    return this.wood;
                }
            }
            set
            {
                if (index == 0)
                {
                    this.gold = value;
                }
                else if (index == 1)
                {
                    this.glassworks = value;
                }
                else if (index == 2)
                {
                    this.loom = value;
                }
                else if (index == 3)
                {
                    this.papyrus = value;
                }
                else if (index == 4)
                {
                    this.clay = value;
                }
                else if (index == 5)
                {
                    this.ore = value;
                }
                else if (index == 6)
                {
                    this.silver = value;
                }
                else
                {
                    this.wood = value;
                }
            }
        }
        public static Resource operator-(Resource r1, Resource r2)
        {
            Resource result = new Resource();

            result.gold = r1.gold - r2.gold;

            result.loom = r1.loom - r2.loom;
            result.glassworks = r1.glassworks - r2.glassworks;
            result.papyrus = r1.papyrus - r2.papyrus;

            result.clay = r1.clay - r2.clay;
            result.ore = r1.ore - r2.ore;
            result.silver = r1.silver - r2.silver;
            result.wood = r1.wood - r2.wood;

            return result;
        }

        public static Resource operator *(Resource r1, int mult)
        {
            Resource ans = new Resource();

            ans.gold = r1.gold * mult;

            ans.loom = r1.loom * mult;
            ans.glassworks = r1.glassworks * mult;
            ans.papyrus = r1.papyrus * mult;

            ans.clay = r1.clay * mult;
            ans.silver = r1.silver * mult;
            ans.wood = r1.wood * mult;
            ans.ore = r1.ore * mult;

            return ans;
        }
        public static bool operator!=(Resource r1, Resource r2)
        {
            return !(r1 == r2);
        }
        public static bool operator==(Resource r1, Resource r2)
        {
            try
            {
                if (r1.gold != r2.gold)
                {
                    return false;
                }

                if (r1.loom != r2.loom)
                {
                    return false;
                }
                if (r1.glassworks != r2.glassworks)
                {
                    return false;
                }
                if (r1.papyrus != r2.papyrus)
                {
                    return false;
                }

                if (r1.clay != r2.clay)
                {
                    return false;
                }
                if (r1.ore != r2.ore)
                {
                    return false;
                }
                if (r1.silver != r2.silver)
                {
                    return false;
                }
                if (r1.wood != r2.wood)
                {
                    return false;
                }
            }
#pragma warning disable 0168
            catch (NullReferenceException e)
            {
                try
                {
                    int temp = r1.gold;
                    return false;
                }
                catch(NullReferenceException ex)
                {
                     try
                     {
                            int temp = r2.gold;
                            return false;
                     }
                     catch(NullReferenceException exx)
                     {
                            return true;
                     }
                }
            }
#pragma warning restore 0168
            return true;
        }
        public int sumResources()
        {
            int sum = 0;

            for (int count = 0; count < 8; count++)
            {
                sum += this[count];
            }

            return sum;
        }
        public bool contains(Resource r1)
        {
            Resource temp = this - r1;

            if (temp.gold < 0)
            {
                return false;
            }

            if (temp.loom < 0)
            {
                return false;
            }
            if (temp.glassworks < 0)
            {
                return false;
            }
            if (temp.papyrus < 0)
            {
                return false;
            }

            if (temp.clay < 0)
            {
                return false;
            }
            if (temp.silver < 0)
            {
                return false;
            }
            if (temp.ore < 0)
            {
                return false;
            }
            if (temp.wood < 0)
            {
                return false;
            }

            return true;
        }
        public String generateDescription()
        {
            String desc = "";

            if (gold > 0)
            {
                desc += gold + " gold";
            }

            if (glassworks > 0)
            {
                desc = addComma(desc);
                desc += glassworks + " glass";
            }

            if (loom > 0)
            {
                desc = addComma(desc);
                desc += loom + " loom";
            }

            if (papyrus > 0)
            {
                desc = addComma(desc);
                desc += papyrus + " papyrus";
            }

            if (clay > 0)
            {
                desc = addComma(desc);
                desc += clay + " clay";
            }

            if (ore > 0)
            {
                desc = addComma(desc);
                desc += ore + " ore";
            }

            if (silver > 0)
            {
                desc = addComma(desc);
                desc += silver + " stone";
            }

            if (wood > 0)
            {
                desc = addComma(desc);
                desc += wood + " wood";
            }

            return desc;
        }

        private String addComma(String desc)
        {
            if (desc.Length > 0)
            {
                desc += ",";
            }

            return desc;
        }

        public static Resource buildResourceFromXML(XmlReader xmlReader)
        {
            Resources.Resource resource = new Resources.Resource();
#pragma warning disable 0168
            try
            {
                int gold = Convert.ToInt32(xmlReader.GetAttribute("Gold"));
                resource.gold += gold;
            }
            catch (SystemException e)
            {

            }

            try
            {
                int glass = Convert.ToInt32(xmlReader.GetAttribute("Glass"));
                resource.glassworks += glass;
            }
            catch (SystemException e)
            {

            }

            try
            {
                int loom = Convert.ToInt32(xmlReader.GetAttribute("Loom"));
                resource.loom += loom;
            }
            catch (SystemException e)
            {

            }

            try
            {
                int papyrus = Convert.ToInt32(xmlReader.GetAttribute("Papyrus"));
                resource.papyrus += papyrus;
            }
            catch (SystemException e)
            {

            }

            try
            {
                int clay = Convert.ToInt32(xmlReader.GetAttribute("Clay"));
                resource.clay += clay;
            }
            catch (SystemException e)
            {

            }

            try
            {
                int ore = Convert.ToInt32(xmlReader.GetAttribute("Ore"));
                resource.ore += ore;
            }
            catch (SystemException e)
            {

            }

            try
            {
                int silver = Convert.ToInt32(xmlReader.GetAttribute("Silver"));
                resource.silver += silver;
            }
            catch (SystemException e)
            {

            }

            try
            {
                int wood = Convert.ToInt32(xmlReader.GetAttribute("Wood"));
                resource.wood += wood;
            }
            catch (SystemException e)
            {

            }
#pragma warning restore 0168
            return resource;
        }
    }
}
