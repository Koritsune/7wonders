﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Client.Resources
{
    public class PNLArmyVictoryTokens : Panel
    {
        Player player;
        List<VictoryPointObjectWithPicture> armyPointTokens = new List<VictoryPointObjectWithPicture>();
        Size buttonSize;

        public event DefeatTokenReceivedHandler DefeatTokenReceived;

        public delegate void DefeatTokenReceivedHandler(object source, VictoryTokenReceivedEvent e);

        public class DefeatTokenReceivedEvent : EventArgs
        {
            private string EventInfo;
            public DefeatTokenReceivedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public event VictoryTokenReceivedHandler VictoryTokenReceived;

        public delegate void VictoryTokenReceivedHandler(object source, VictoryTokenReceivedEvent e);

        public class VictoryTokenReceivedEvent : EventArgs
        {
            private string EventInfo;
            public VictoryTokenReceivedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public PNLArmyVictoryTokens(Player player_)
        {
            if (player_.getPlayerRotation() == Player.PlayerRotation.None || player_.getPlayerRotation() == Player.PlayerRotation.Clockwise180)
            {
                buttonSize = new Size(Height, Height);
            }
            else
            {
                buttonSize = new Size(Height / 8, Height / 8);
            }

            BackColor = Color.Transparent;
            player = player_;
            this.Resize += resize;
        }

        private delegate void addPointsD(int points);

        public void addPoints(int points)
        {
            if (this.InvokeRequired)
            {
                addPointsD d = new addPointsD(addPoints);
                this.Invoke(d, new object[] { points });
            }
            else
            {
                
                VictoryPointObjectWithPicture armyPointToken = new VictoryPointObjectWithPicture(Constants.ICONS + "Military-1.PNG", -1); ;
                if (points == -1)
                {
                    if (DefeatTokenReceived != null)
                    {
                        DefeatTokenReceived(this, new VictoryTokenReceivedEvent("Defeat Token Received."));
                    }
                }
                else
                {

                    if (VictoryTokenReceived != null)
                    {
                        VictoryTokenReceived(this, new VictoryTokenReceivedEvent("Received Victory Token."));
                    }

                    if (points == 1)
                    {
                        armyPointToken = new VictoryPointObjectWithPicture(Constants.ICONS + "Military1.PNG", 1);
                    }
                    else if (points == 3)
                    {
                        armyPointToken = new VictoryPointObjectWithPicture(Constants.ICONS + "Military3.PNG", 3);
                    }
                    else if (points == 5)
                    {
                        armyPointToken = new VictoryPointObjectWithPicture(Constants.ICONS + "Military5.PNG", 5);
                    }
                }

                if (player.getPlayerRotation() == Player.PlayerRotation.None || player.getPlayerRotation() == Player.PlayerRotation.Clockwise180)
                {
                    armyPointToken.Size = buttonSize;
                    armyPointToken.Location = new Point(buttonSize.Width * armyPointTokens.Count, 0);
                }
                else
                {
                    armyPointToken.Size = buttonSize;
                    armyPointToken.Location = new Point(0,buttonSize.Height * armyPointTokens.Count);
                }

                armyPointTokens.Add(armyPointToken);
                Controls.Add(armyPointToken);
                
            }
        }
        private void resize(object source, EventArgs e)
        {
            if (player.getPlayerRotation() == Player.PlayerRotation.None || player.getPlayerRotation() == Player.PlayerRotation.Clockwise180)
            {
                buttonSize = new Size(Height, Height);
            }
            else
            {
                buttonSize = new Size(Height / 8, Height / 8);
            }

            if (player.getPlayerRotation() == Player.PlayerRotation.None || player.getPlayerRotation() == Player.PlayerRotation.Clockwise180)
            {
                for (int count = 0; count < armyPointTokens.Count; count++)
                {
                    armyPointTokens[count].Size = buttonSize;
                    armyPointTokens[count].Location = new Point(buttonSize.Width * count, 0);
                }
            }
            else
            {
                for (int count = 0; count < armyPointTokens.Count; count++)
                {
                    armyPointTokens[count].Size = buttonSize;
                    armyPointTokens[count].Location = new Point(0, buttonSize.Height * count);
                }
            }
        }
        public int getVictoryPoints()
        {
            int points = 0;

            foreach (VictoryPointObjectWithPicture item in armyPointTokens)
            {
                points += item.victoryPoints;
            }

            return points;
        }
        public int getNbVictoryTokens()
        {
            int nb = 0;

            foreach (VictoryPointObjectWithPicture item in armyPointTokens)
            {
                if (item.victoryPoints > 0)
                {
                    nb ++;
                }
            }

            return nb; 
        }
        public int getNbDefeatTokens()
        {
            int nb = 0;

            foreach (VictoryPointObjectWithPicture item in armyPointTokens)
            {
                if (item.victoryPoints < 0)
                {
                    nb++;
                }
            }

            return nb;
        }
    }
}
