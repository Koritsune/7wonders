﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Resources
{
    public class Trade
    {
        public ResourceToggle resourceButton { get; private set; }
        public int goldCost { get; set; }

        public Player owner { get; private set; }
        Game game;

        public event TradeCancelHandler TradeCancel;
        public delegate void TradeCancelHandler(object source, TradeCancelEvent e);

        public class TradeCancelEvent : EventArgs
        {
            private string EventInfo;
            public TradeCancelEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public event TradeCompleteHandler TradeComplete;
        public delegate void TradeCompleteHandler(object source, TradeEvent e);

        public class TradeEvent : EventArgs
        {
            private string EventInfo;
            public TradeEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public Trade(ResourceToggle resourceButton_, Game game_, Player player_)
        {
            resourceButton = resourceButton_;
            goldCost = 2;
            game = game_;
            owner = player_;
        }
        public void completeTrade(int id)
        {
            if (!resourceButton.owner.getUsername().Equals(game.getConfig().getUsername()))
            {
                game.sendMessage("GAME" + Constants.DELIM_FIELD + "TRADE" + Constants.DELIM_FIELD + id + Constants.DELIM_FIELD + goldCost + Constants.DELIM_FIELD + game.getConfig().getUsername() + Constants.DELIM_FIELD + owner.getUsername() + Constants.DELIM_FIELD + resourceButton.generates.generateDescription());
            }

            if (TradeComplete != null)
            {
                TradeComplete(this, new TradeEvent("Trade Completed."));
            }
        }
        public void cancelTrade()
        {
            Resource goldRefund = new Resource();
            goldRefund.gold = goldCost;
            game.getPlayers()[0].addResource(goldRefund);
            goldRefund.gold *= -1;

            if (owner != game.getPlayers()[0])
            {
                resourceButton.owner.addResource(goldRefund);
            }

            resourceButton.active = true;

            if (TradeCancel != null){
                TradeCancel(this, new TradeCancelEvent("Trade Canceled.")) ;
            }
        }
    }
}
