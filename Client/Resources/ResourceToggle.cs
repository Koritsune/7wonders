﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client.Resources
{
    public class ResourceToggle : Panel
    {
        private bool active_back;
        public bool active {get{
                                return this.active_back;
        } set{
                                       this.active_back = value;
                                        resourcePicture.BackgroundImage = Image.FromFile((active_back?activePicture:inactivePicture));

                                        if (ValueChange != null)
                                        {
                                            ValueChange(this, new ValueChangeEvent("Value was Clicked"));
                                        }
                                    }}

        String activePicture = "";
        String inactivePicture = "";

        Panel resourcePicture = new Panel();
        public Player owner {get; private set;}

        public Resource generates {get; private set;}

        public event ButtonClickHandler ButtonClick;
        public delegate void ButtonClickHandler(object source, ButtonClickEvent e);

        public class ButtonClickEvent : EventArgs
        {
            private string EventInfo;
            public ButtonClickEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public event ValueChangeHandler ValueChange;
        public delegate void ValueChangeHandler(object source, ValueChangeEvent e);

        public class ValueChangeEvent : EventArgs
        {
            private string EventInfo;
            public ValueChangeEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public enum Type
        {
            Glass,
            Loom,
            Papyrus,
            Clay,
            Ore,
            Stone,
            Wood
        }

        public ResourceToggle(Type resourceType, Player owner_, bool active_ = true)
        {
            owner = owner_;
            generates = new Resource();
            this.Resize += resizeMe;

            this.BackColor = Color.Transparent;
            resourcePicture.BackColor = Color.Transparent;
            resourcePicture.BackgroundImageLayout = ImageLayout.Stretch;
            Controls.Add(resourcePicture);

            resourcePicture.MouseClick += raiseClickEvent;

            switch (resourceType)
            {
                case Type.Loom:
                    {
                        activePicture = Constants.ICONS + "Loom.PNG";
                        inactivePicture = Constants.ICONS + "LoomInactive.PNG";
                        generates.loom = 1;
                        break;
                    }
                case Type.Papyrus:
                    {
                        activePicture = Constants.ICONS + "Papyrus.PNG";
                        inactivePicture = Constants.ICONS + "PapyrusInactive.PNG";
                        generates.papyrus = 1;
                        break;
                    }
                case Type.Glass:
                    {
                        activePicture = Constants.ICONS + "Glass.PNG";
                        inactivePicture = Constants.ICONS + "GlassInactive.PNG";
                        generates.glassworks = 1;
                        break;
                    }
                case Type.Clay:
                    {
                        activePicture = Constants.ICONS + "Clay.PNG";
                        inactivePicture = Constants.ICONS + "ClayInactive.PNG";
                        generates.clay = 1;
                        break;
                    }
                case Type.Ore:
                    {
                        activePicture = Constants.ICONS + "Ore.PNG";
                        inactivePicture = Constants.ICONS + "OreInactive.PNG";
                        generates.ore = 1;
                        break;
                    }
                case Type.Stone:
                    {
                        activePicture = Constants.ICONS + "Stone.PNG";
                        inactivePicture = Constants.ICONS + "StoneInactive.PNG";
                        generates.silver = 1;
                        break;
                    }
                case Type.Wood:
                    {
                        activePicture = Constants.ICONS + "Wood.PNG";
                        inactivePicture = Constants.ICONS + "WoodInactive.PNG";
                        generates.wood = 1;
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            active = active_;

            if (active)
            {
                resourcePicture.BackgroundImage = Image.FromFile(activePicture);
            }
            else
            {
                resourcePicture.BackgroundImage = Image.FromFile(inactivePicture);
            }
        }

        private void resizeMe(object source, EventArgs e)
        {
            resourcePicture.Size = new Size(Height, Width);
        }

        private void raiseClickEvent(object source, EventArgs e)
        {
            if (ButtonClick != null)
            {
                ButtonClick(this, new ButtonClickEvent("Button was Clicked"));
            }
        }

        public void setActive(bool ans)
        {
            active_back = ans;
            resourcePicture.BackgroundImage = Image.FromFile((active_back ? activePicture : inactivePicture));
        }
    }
}
