﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Resources
{
    public class TradeManager
    {
        Game game;
        public List<Trade> trades {get; private set;}
        public List<CardEffects.TradeEffect> tradeEffects = new List<CardEffects.TradeEffect>();
        ResourceCostPanel PNLCost;

        public TradeManager(Game game_, ResourceCostPanel PNLCost_)
        {
            trades = new List<Trade>();
            game = game_;
            PNLCost = PNLCost_;

            PNLCost.cardPurchase += purchaseTrades;
            PNLCost.cardCancel += purchaseRollback;
        }
        public void trade(object sender, EventArgs e)
        {
            ResourceToggle resourceToggle = (ResourceToggle)sender;
            
            if (PNLCost.needs(resourceToggle.generates) && resourceToggle.active)
            {
                Trade trade = new Trade(resourceToggle, game, resourceToggle.owner);
                foreach (CardEffects.TradeEffect item in tradeEffects)
                {
                    item.applyRebate(trade);
                }

                trades.Add(trade);

                Resource goldTradeCost = new Resource();

                if (resourceToggle.owner == game.getPlayers()[0])
                {
                    goldTradeCost.gold = 1;
                    trade.goldCost = 1;
                }
                else
                {
                    goldTradeCost.gold = trade.goldCost;
                }

                if (resourceToggle.owner != game.getPlayers()[0])
                {
                    resourceToggle.owner.addResource(goldTradeCost);
                }

                goldTradeCost.gold = -1 * trade.goldCost;
                game.getPlayers()[0].addResource(goldTradeCost);

                PNLCost.add(resourceToggle.generates);

                resourceToggle.active = false;
            }
            else if (!resourceToggle.active)
            {
                for(int count = 0; count < trades.Count; count ++)
                {
                    Trade item = trades[count];

                    if (item.resourceButton == resourceToggle)
                    {
                        PNLCost.remove(item.resourceButton.generates);
                        trades.Remove(item);
                        item.cancelTrade();
                        break;
                    }
                }
                resourceToggle.active = true;
            }

        }

        private void purchaseRollback(object source, EventArgs e)
        {
            foreach (Trade item in trades)
            {
                item.cancelTrade();
            }
            while (trades.Count != 0)
            {
                trades.RemoveAt(0);
            }
        }

        private void purchaseTrades(object source, EventArgs e)
        {
            int tradeID = 0;

            foreach (Trade item in trades)
            {
                item.completeTrade(tradeID);
                tradeID++;
            }
        }

        public void removeTrade(ResourceToggle resourceToggle)
        {
            for (int count = 0; count < trades.Count; count++)
            {
                if (trades[count].resourceButton == resourceToggle)
                {
                    trades[count].cancelTrade();
                    trades.RemoveAt(count);
                    break;
                }
            }
        }
    }
}
