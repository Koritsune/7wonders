﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class Deck<T>
    {
        List<T> cards;
        Random rnd;

        public Deck(List<T> cards_, Random rnd_){
            cards = cards_;
            rnd = rnd_;
        }
        public void shuffle()
        {
            int swap1 = 0;
            int swap2 = 0;

            for (int count = 0; count < Constants.SHUFFLES; count++)
            {
                swap1 = rnd.Next(0, cards.Count);
                swap2 = rnd.Next(0, cards.Count);
                Swap(swap1, swap2);
            }
        }
        private void Swap(int swap1, int swap2)
        {
            T temp;
            temp = cards[swap1];
            cards[swap1] = cards[swap2];
            cards[swap2] = temp;
        }
        public T deal()
        {
            T card = cards[0];
            cards.RemoveAt(0);
            return card;
        }
        public bool isEmpty()
        {
            return cards.Count == 0;
        }
    }
}
