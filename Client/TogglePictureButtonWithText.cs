﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client
{
    class TogglePictureButtonWithText : TogglePicutreButton
    {
        Label LBLText = new Label();

        public TogglePictureButtonWithText(String active, String inactive)
        {
            activePicture = active;
            inactivePicture = inactive;

            BackColor = Color.Transparent;
            BackgroundImage = Image.FromFile(inactivePicture);
            BackgroundImageLayout = ImageLayout.Stretch;

            //this.MouseClick += toggleActive;

            LBLText.Size = new Size(Width, 20);
            LBLText.Location = new Point(0, (Height - 20)/2);
            LBLText.TextAlign = ContentAlignment.MiddleCenter;
            LBLText.BackColor = Color.Transparent;
            Controls.Add(LBLText);
            LBLText.MouseClick += mouseClick;
            this.MouseClick += mouseClick;
        }
        public override void setSize(int width, int height){
            Size = new Size(width, height);
            LBLText.Size = new Size(Width, 20);
            LBLText.Location = new Point( 0, (Height - 20) / 2);
        }
        public void setFontSize(int size)
        {
            LBLText.Font = new Font(LBLText.Font.FontFamily.Name, size);
        }
        public void setTextColor(Color color)
        {
            LBLText.ForeColor = color;
        }
        public void setText(String ans){
            LBLText.Text = ans;
        }
        protected void mouseClick(object sender, EventArgs e)
        {
            mouseClick();
        }
        protected void mouseClick()
        {
            if (isActive())
            {
                raiseValueChangedEvent();
            }
            else
            {
                toggleActive();
            }
        }
    }
}
