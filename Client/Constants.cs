﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    static class Constants
    {
        public const double VERSION = 1.3;
        public const String WONDERS_FOLDER = "Pictures\\Wonders\\";
        public const String WONDERS_XML = "Wonders.XML";
        public const String PICTURES = "Pictures\\";
        public const String ICONS = PICTURES + "Icons\\";
        public const String BLACK_TRANSPARENT = ICONS + "BlackTransparent.PNG";
        public const String WHITE_TRANSPARENT = ICONS + "WhiteTransparent.PNG";

        public const String Ready = ICONS + "Ready.PNG";
        public const String Disconnected = ICONS + "Disconnected.PNG";
        public const String Connected = ICONS + "Connected.PNG";
        //public String APPDATA_FOLDER = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\7Wonders";
        public const String CONFIGURATION = "Config.XML";
        public const int MAX_PLAYERS = 7;
        public const int MAX_PLAYERS_CITIES = 8;
        public const int WONDERS = 14;
        public const int FONT_SIZE = 12;
        public const int ICON_WIDTH = 100;
        public const int ICON_HEIGHT = 100;

        public const char DELIM_STREAM = (char)28;//Convert.ToChar(28);
        public const char DELIM_FIELD = (char)29;//Convert.ToChar(29);

        public const int TIMER_INTERVAL = 100;
        public const int NETWORK_INTERVAL = 1000;
        public const int NETWORK_RECONNECT_TIMEOUT = 30;
        public const int NETWORK_RECONNECT_TIMEOUT_SERVER = 30;

        public const int MAX_USERNAME_LENGTH = 12;

        public const String LEADERS_ENABLED = ICONS + "LEADERS_LOGO.png";
        public const String LEADERS_DISABLED = ICONS + "LEADERS_LOGO_GRAY.png";
        public const String CITIES_ENABLED = ICONS + "CITIES_LOGO.png";
        public const String CITIES_DISABLED = ICONS + "CITIES_LOGO_GRAY.png";

        public const int SHUFFLES = 80;
        public const int MAX_COLORS = 9;

        public const int TRADE_RATE = 2;
        public const int MAX_PRIO = 8;

        public const int LEADERS_TO_DEAL = 4;
    }
}
