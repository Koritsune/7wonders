﻿using System;
using System.Windows.Forms;
using System.Windows.Markup;

public partial class TransparentTextBox : RichTextBox
{
    bool singleLine = true;

    public TransparentTextBox()
    {
        MaxLength = 0;
        this.KeyDown += noNewLine;
        //AcceptReturn = false;
    }
    protected override CreateParams CreateParams
    {
        get
        {
            CreateParams parms = base.CreateParams;
            parms.ExStyle |= 0x20;  // Turn on WS_EX_TRANSPARENT
            return parms;
        }
    }
    private void noNewLine(object sender, KeyEventArgs e) {
        //KeyPressEventArgs key = (KeyPressEventArgs)e;
        if (singleLine)
        {
            if (e.KeyCode == Keys.Return)
            {
                ////Console.WriteLine("enter detected");
                e.Handled = true;// SuppressKeyPress = true;
            }
        }
    }

    public void setSingleLine(bool ans) {
        singleLine = ans;
    }
}