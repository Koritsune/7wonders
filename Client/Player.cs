﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;

namespace Client
{
    public class Player:Panel
    {
        String Username;
        String display;

        public Resources.TradeManager tradeManager {get; private set;}
        Wonder wonder;
        public Form1 form_ { get; private set; }

        public Player leftNeighbor { get; set; }
        public Player rightNeighbor { get; set; }
        bool ready = false;
        private bool buildgingWonderBack = false;
        public bool reflectLoss {get; set;}
        bool chainMade = false;
        public int scienceChoice { get; set; }
        public int scienceCopy { get; set; }
        public bool buildingWonder
        {
            get 
            {
                return buildgingWonderBack;
            }
            set 
            {
                buildgingWonderBack = value;

                if (BuildingWonderChanged != null)
                {
                    BuildingWonderChanged(this, new BuildingWonderChangedEvent("Building Wonder Event."));
                }
            }
        }

        PlayerRotation rotation;
        TransparentLabel LBLUsername = new TransparentLabel();

        List<Card> hand = new List<Card>();
        Point[] handCardLocations = new Point[] { new Point(0, 100), new Point(81, 100), new Point(162, 100), new Point(243, 100), new Point(324, 100), new Point(405, 100), new Point(486, 100), new Point(567, 100) };
        Size cardSize = new Size(80, 128);

        public List<List<Card>> playedCards {get; private set;}
        public Resources.Resource resources { get; private set; }

        TogglePictureGroupBox cardTypeSelection = new TogglePictureGroupBox();
        List<TogglePicutreButton> buttonToggleList = new List<TogglePicutreButton>();

        TogglePictureGroupBox TPGBCardviewing = new TogglePictureGroupBox();
        List<TogglePictureButtonWithText> TPGBWTToggleViewingButtons = new List<TogglePictureButtonWithText>();

        String delayedPassHandMessage;

        List<int> cardviewingIndex = new List<int>();
        public int[] SCIENCE = new int[] { 0, 0, 0 };
        public Resources.ResourceCostPanel PNLCost { get; private set; }

        public List<Resources.ResourceToggle> resourceToggleList {get; set;}
        public int armySize { get; private set;}

        public bool[] produces_back;
        public bool[] produces { get { return produces_back; } private set { produces_back = value; } }

        bool DoublePlay_back = false;

        public bool DoublePlay
        { get 
            {
                return DoublePlay_back;
            } 
          set 
            {
                DoublePlay_back = value;
                if (DoublePlay_back)
                {
                    form_.getGame().serverConnection.playManager.increaseDoublePlays();
                }
            } 
        }

        public Resources.PNLDebtTokens PNLDebtToken {get; private set;}
        public Resources.PNLArmyVictoryTokens PNLArmyVictoryToken { get; private set; }

        public event BuildingWonderChangedHandler BuildingWonderChanged;

        public delegate void BuildingWonderChangedHandler(object source, BuildingWonderChangedEvent e);

        public class BuildingWonderChangedEvent : EventArgs
        {
            private string EventInfo;
            public BuildingWonderChangedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public event TradeMadeHandler TradeMade;

        public delegate void TradeMadeHandler(object source, TradeMadeEvent e);

        public class TradeMadeEvent : EventArgs
        {
            private Player tradePartnerUsername;
            private int gold;
            private String desc;

            public TradeMadeEvent(Player tradePartnerUsername_, int gold_, String desc_)
            {
                tradePartnerUsername = tradePartnerUsername_;
                gold = gold_;
                desc = desc_;
            }
            public Player GetTradePartner()
            {
                return tradePartnerUsername;
            }
            public int getGold()
            {
                return gold;
            }
            public String getDesc()
            {
                return desc;
            }
        }

        public event ChainBuildHandler ChainBuild;

        public delegate void ChainBuildHandler(object source, ChainBuildEvent e);

        public class ChainBuildEvent : EventArgs
        {
            private string EventInfo;
            public ChainBuildEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public event GoldReceivedHandler GoldReceived;

        public delegate void GoldReceivedHandler(object source, GoldReceivedEvent e);

        public class GoldReceivedEvent : EventArgs
        {
            private string EventInfo;
            public GoldReceivedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public event ResourceGenerationChangedEventHandler ResourceGenerationChanged;
        public delegate void ResourceGenerationChangedEventHandler(object source, ResourceGenerationChangedEvent e);
        public event WonderLoadedHandler WonderLoaded;

        public delegate void WonderLoadedHandler(object source, WonderLoadedEvent e);

        public class WonderLoadedEvent : EventArgs
        {
            private string EventInfo;
            public WonderLoadedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }
        public class ResourceGenerationChangedEvent : EventArgs
        {
            private string EventInfo;
            public ResourceGenerationChangedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public CardPlayedHandler[] cardPlayedHandler = new CardPlayedHandler[Constants.MAX_COLORS + 1];

        public delegate void CardPlayedHandler(Card cardPlayed);

        public event CardSelectetHandler CardSelected;

        public delegate void CardSelectetHandler(object source, CardSelectedEvent e);

        public class CardSelectedEvent : EventArgs
        {
            private string EventInfo;
            public CardSelectedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public event ResourceAddedHandler ResourceAdded;

        public delegate void ResourceAddedHandler(object source, ResourceAddedEvent e);

        public class ResourceAddedEvent : EventArgs
        {
            private string EventInfo;
            public ResourceAddedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public event ArmyIncreaseHandler ArmySizeIncrease;

        public delegate void ArmyIncreaseHandler(object source, ArmyIncreaseEvent e);

        public class ArmyIncreaseEvent : EventArgs
        {
            private string EventInfo;
            public ArmyIncreaseEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public enum PlayerRotation
        {
            None = 0,
            Clockwise90 = 1,
            Clockwise180 = 2,
            ClockWise270 = 3,
        }

        public Player(Form1 form) {
            scienceChoice = 0;
            scienceCopy = 0;
            reflectLoss = false;
            playedCards = new List<List<Card>>();
            PNLDebtToken = new Resources.PNLDebtTokens(this);
            Controls.Add(PNLDebtToken);
            PNLDebtToken.Visible = false;

            PNLArmyVictoryToken = new Resources.PNLArmyVictoryTokens(this);
            Controls.Add(PNLArmyVictoryToken);
            PNLArmyVictoryToken.Visible = false;

            produces_back = new bool[] { false, false, false, false, false, false, false };
            this.DoubleBuffered = true;
            resourceToggleList = new List<Resources.ResourceToggle>();
            for (int count = 0; count < Constants.MAX_COLORS; count++)
            {
                playedCards.Add(new List<Card>());
                cardviewingIndex.Add(0);
            }

            playedCards.Add(new List<Card>());
            cardviewingIndex.Add(0);
            resources = new Resources.Resource();

            form_ = form;
            //Controls.Add(LBLUsername);

            TPGBCardviewing.Location = new Point(0, 0);
            TPGBCardviewing.Size = new Size(210, 100);
            TPGBCardviewing.singleSelectable = true;
            TPGBCardviewing.BackColor = Color.Transparent;
            TPGBCardviewing.Text = "View Cards of Type";

            for (int count = 0; count <= Constants.MAX_COLORS; count++)
            {
                TogglePictureButtonWithText toggleButton = new TogglePictureButtonWithText(Constants.ICONS + "cardType" + count + "active.png", Constants.ICONS + "cardType" + count + "inactive.png");
                toggleButton.Location = new Point(count % 5 * 40 + 5, count / 5 * 40 + 16);
                toggleButton.setSize(40, 40);
                toggleButton.setActive(false);
                toggleButton.setText("0");
                cardPlayedHandler[count] += updateNbOfCards;
                toggleButton.valueChange += toggleView;
                //Console.WriteLine("Calculated Point: (" + toggleButton.Location.X + ", " + toggleButton.Location.Y + ")");
                TPGBWTToggleViewingButtons.Add(toggleButton);
                TPGBCardviewing.add(toggleButton);
            }
            TPGBWTToggleViewingButtons[0].setActive(true);
            //change text color voer black button to white so it is easily visible
            TPGBWTToggleViewingButtons[8].setTextColor(Color.White);
            TPGBWTToggleViewingButtons[0].setTextColor(Color.White);
            TPGBWTToggleViewingButtons[6].setTextColor(Color.White);
            TPGBWTToggleViewingButtons[3].setTextColor(Color.White);
            TPGBWTToggleViewingButtons[1].setTextColor(Color.White);
            Controls.Add(TPGBCardviewing);
            armySize = 0;

            PNLCost = new Resources.ResourceCostPanel(this,form);
            tradeManager = new Resources.TradeManager(form.getGame(), PNLCost);

            form.getPNLServer().getPNLGame().AllWondersLoaded += attachCoinArmyListener;
            form.getGame().ServerConnectionUpdated += attachResetChain;
        }

        public void setUsername(String ans){
            ////Console.WriteLine("Username change: " + ans);
            Username = ans;
            BackColor = Color.Transparent;
            //BackColor = Color.Purple;

            if (ans != form_.getConfig().getUsername())
            {
                LBLUsername.setText(ans);
                LBLUsername.setFontSize(10);
                LBLUsername.setSize(100, 20);
                LBLUsername.BackgroundImageLayout = ImageLayout.Stretch;
                LBLUsername.BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
                LBLUsername.BackColor = Color.Transparent;
                LBLUsername.ForeColor = Color.White;
                LBLUsername.Location = new Point(0, 0);
                LBLUsername.Visible = true;
            }
            else
            {
                LBLUsername.Visible = false;
            }

        }
        
        public String getUsername() {
            return Username;
        }
        
        public String getDisplay() {
            return display;
        }
        
        public void setDisplay(String ans) {
            //Console.WriteLine("Display change: " + ans);
            display = ans;
        }
        public bool isReady()
        {
            return ready;
        }
        public void setReady(bool ans)
        {
            ready = ans;
        }
        public void loadWonder(String wonder_, char side)
        {
            List<Wonder> wonders = form_.getGame().getWonders();

            for (int count = 0; count < wonders.Count; count++)
            {
                if (wonders[count].getName().Equals(wonder_))
                {
                    wonder = wonders[count];
                    //Console.WriteLine("User: " + Username + " Attached to wonder: " + wonder.getName());
                    break;
                }
            }

            wonder.load(side, this);
            setSize(Width, Height - 20, Username.Equals(form_.getConfig().getUsername()));
            wonder.Location = new Point(0, Convert.ToInt32(Height * 0.16));

            if (rotation.Equals(PlayerRotation.None))
            {
                wonder.Location = new Point(0, 133);
            }
            
            wonder.setRotation(rotation);
            Controls.Add(wonder);
            wonder.Controls.Add(LBLUsername);

            BackColor = Color.Transparent;
            if (form_.getConfig().getUsername().Equals(Username))
            {
                wonder.Controls.Add(PNLCost);
            }

            wonder.wonderManager.DelayedBuildMessageSent += delayedPassHandMessageCall;

            if (WonderLoaded != null)
            {
                WonderLoaded(this, new WonderLoadedEvent("Wonder Loaded."));
            }

        }
        public void setSize(int width, int height, bool ownPlayer = false)
        {

            this.Size = new Size(width, height);
            TPGBCardviewing.Size = new Size(width, Convert.ToInt32(height * 0.2));

            if (rotation == PlayerRotation.Clockwise90 || rotation == PlayerRotation.ClockWise270)
            {
                //location stays (0,0)
                TPGBCardviewing.Location = new Point(0, 0);
                Size toggleSize = new Size((width - 10) / 5, (width - 10) / 8);
                for (int count = 0; count < TPGBWTToggleViewingButtons.Count; count ++)
                {
                    TPGBWTToggleViewingButtons[count].setSize(toggleSize.Width,toggleSize.Height);
                    TPGBWTToggleViewingButtons[count].Location = new Point(count % 5 * toggleSize.Width + 5, count / 5 * toggleSize.Height + 16);
                }
            }
            else if (rotation == PlayerRotation.None)
            {
                PNLCost.Size = new Size(Convert.ToInt32(width * 0.8), Convert.ToInt32(height * 0.1));
                PNLCost.Location = new Point(Convert.ToInt32(width * 0.24), 0);
                TPGBCardviewing.Size = new Size(Convert.ToInt32(width * 0.8), Convert.ToInt32(height * 0.11 + 20));
                //location must be changed
                TPGBCardviewing.Location = new Point(Convert.ToInt32(width * 0.1), 0);

                Size toggleSize = new Size(Convert.ToInt32((width * 0.8 - 10) / 10), Convert.ToInt32(height * 0.1));
                for (int count = 0; count < TPGBWTToggleViewingButtons.Count; count++)
                {
                    TPGBWTToggleViewingButtons[count].setSize(toggleSize.Width, toggleSize.Height);
                    TPGBWTToggleViewingButtons[count].Location = new Point(count * toggleSize.Width + 5, 16);
                }

            }
            else //rotation is 180
            {
                //location must be changed
                TPGBCardviewing.Size = new Size(Convert.ToInt32(width * 0.8), Convert.ToInt32(height * 0.11 + 20));
                //location must be changed
                TPGBCardviewing.Location = new Point(Convert.ToInt32(Width * 0.1) , Convert.ToInt32(Height * 0.8));

                Size toggleSize = new Size(Convert.ToInt32((width * 0.8 - 10) / 10), Convert.ToInt32(height * 0.1));
                for (int count = 0; count < TPGBWTToggleViewingButtons.Count; count++)
                {
                    TPGBWTToggleViewingButtons[count].setSize(toggleSize.Width, toggleSize.Height);
                    TPGBWTToggleViewingButtons[count].Location = new Point(count * toggleSize.Width + 5, 16);
                }
            }
            try
            {
                if (!Username.Equals(form_.getConfig().getUsername()) && rotation != PlayerRotation.Clockwise180)
                {
                    if (rotation == PlayerRotation.Clockwise90)
                    {
                        //wonder.Location = new Point( 0, Convert.ToInt32(height * 0.05));
                        wonder.Location = new Point(Convert.ToInt32(width * 0.05), Convert.ToInt32(height * 0.2));
                    }
                    else
                    {
                        wonder.Location = new Point(Convert.ToInt32(width * 0.35), Convert.ToInt32(height * 0.2));
                    }
                    //LBLUsername.Location = new Point(wonder.Location.X, wonder.Location.Y - 20);
                    wonder.setSize(Convert.ToInt32(width * 0.6), Convert.ToInt32(height * 0.8));
                    LBLUsername.setSize(wonder.Size.Width, LBLUsername.Size.Height);
                }
                else if (rotation == PlayerRotation.None)
                {
                    wonder.Location = new Point(0, Convert.ToInt32(height / 3.233));
                    wonder.setSize(width, Convert.ToInt32(height * 0.6));
                }
                else
                {
                    wonder.Location = new Point(0, Convert.ToInt32(height * 0.08));
                    wonder.setSize(width, Convert.ToInt32(height * 0.6));
                }

                int cardWidth = width / 8;
                int cardHeight = Convert.ToInt32(cardWidth / 2 * 4);

                //resize all cards in hand

                for (int count = 0; count < hand.Count; count++)
                {
                    hand[count].setSize(cardWidth, cardHeight);
                    hand[count].Location = new Point((cardWidth) * count, Convert.ToInt32((wonder.Size.Height - 32) * 0.18));
                }

                //resize all played cards

                for (int count = 0; count < playedCards.Count; count++)
                {
                    for (int count2 = 0; count2 < playedCards[count].Count; count2++)
                    {
                        setCardSizeLocation(playedCards[count][count2],count2);
                    }
                }
            }
            #pragma warning disable 0168
            catch (NullReferenceException e)
            {

            }
            #pragma warning restore 0168
            try
            {
                //resize PNLDebt and PNLWar
                if (rotation == PlayerRotation.None)
                {
                    //PNLDebtToken.Location = new Point(0, 0);
                    PNLDebtToken.Location = new Point(0, TPGBCardviewing.Size.Height);
                    PNLDebtToken.Size = new Size(width, wonder.Location.Y - TPGBCardviewing.Height);

                    PNLArmyVictoryToken.Location = PNLDebtToken.Location;
                    PNLArmyVictoryToken.Size = PNLDebtToken.Size;

                }
                else if (rotation == PlayerRotation.Clockwise90)
                {
                    PNLDebtToken.Location = new Point(wonder.Size.Width + wonder.Location.X, wonder.Location.Y);
                    PNLDebtToken.Size = new Size(width - PNLDebtToken.Location.X, height - TPGBCardviewing.Size.Height);

                    PNLArmyVictoryToken.Location = PNLDebtToken.Location;
                    PNLArmyVictoryToken.Size = PNLDebtToken.Size;
                }
                else if (rotation == PlayerRotation.Clockwise180)
                {
                    PNLDebtToken.Location = new Point(0, wonder.Size.Height);
                    PNLDebtToken.Size = new Size(width, TPGBCardviewing.Location.Y - wonder.Size.Height);

                    PNLArmyVictoryToken.Location = PNLDebtToken.Location;
                    PNLArmyVictoryToken.Size = PNLDebtToken.Size;
                }
                else //rotation 270
                {
                    PNLDebtToken.Location = new Point(0, wonder.Location.Y);
                    PNLDebtToken.Size = new Size(wonder.Location.X, wonder.Size.Height);

                    PNLArmyVictoryToken.Location = PNLDebtToken.Location;
                    PNLArmyVictoryToken.Size = PNLDebtToken.Size;
                }
            }
#pragma warning disable 0168
            catch (NullReferenceException e)
            {
#pragma warning restore 0168

            }

        }
        public void setRotation(PlayerRotation ans){

            rotation = ans;
            try
            {
                wonder.setRotation(ans);
            }
            #pragma warning disable 0168
            catch (NullReferenceException ex)
            {

            }
            #pragma warning restore 0168
        }

        delegate void clearHandD();

        public void clearHand()
        {
            if (this.InvokeRequired)
            {
                clearHandD d = new clearHandD(clearHand);
                this.Invoke(d, new object[] { });
            }
            else
            {
                for (int count = 0; count < hand.Count; count++)
                {
                    wonder.Controls.Remove(hand[count]);
                }
                hand.Clear();
            }
        }
        
        delegate void addHandD(String location);

        public void addCard(String location)
        {
            if (this.InvokeRequired)
            {
                addHandD d = new addHandD(addCard);
                this.Invoke(d, new object[] { location });
            }
            else
            {
                while (wonder == null) ;

                Card card = new Card(location,this);

                hand.Add(card);

                int cardWidth = Width / 8;
                int cardHeight = Convert.ToInt32(cardWidth / 2 * 4);


                card.setSize(cardWidth, cardHeight);
                card.Location = new Point((cardWidth) * (hand.Count - 1), Convert.ToInt32((wonder.Size.Height - 32 ) * 0.18));

                if (rotation == PlayerRotation.Clockwise180)
                {
                    card.rotateCard(rotation);
                    card.Location = new Point((cardWidth) * (hand.Count - 1), Convert.ToInt32((wonder.Size.Height - 32) * 0.36));
                }

                card.CardClick += cardClick;
                card.CardPlay += playCard;
                card.CardBurn += burnCard;
                card.CardWonder += wonderCard;

                wonder.Controls.Add(card);
                card.BringToFront();
            }
        }
        private void cardClick (object sender, EventArgs e){
            Card card = (Card)sender;

            buildingWonder = false;
            for (int count = 0; count < hand.Count; count++)
            {
                if (hand[count] != card)
                {
                    hand[count].hideButtons();
                }
            }

            form_.getPNLServer().getPNLGame().setDescription(card.getDesc());
            PNLCost.clear();

            if (form_.getGame().getAge() != Game.GameAge.LeaderRoster)
            {
                PNLCost.checkCardCost(card);
            }

            if (CardSelected != null)
            {
                CardSelected(card, new CardSelectedEvent("Card Selected."));
            }
        }
        public Wonder getWonder()
        {
            return wonder;
        }
        public void playCard(object sender, EventArgs e)
        {
            PNLArmyVictoryToken.Visible = false;
            PNLDebtToken.Visible = false;

            Card card = (Card)sender;

            if (PNLCost.canPlayCard && (!hasPlayedCard(card.name) || buildingWonder) && (form_.getGame().getAge() == Game.GameAge.AgeI || form_.getGame().getAge() == Game.GameAge.AgeII || form_.getGame().getAge() == Game.GameAge.AgeIII))
            {
                Game.GameAge age = form_.getGame().getAge();

                
                //Card.CardColor color = card.getCardColor();

                //this line will be handled when receiving the card has been played form the server
                //playedCards[(int)color].Add(card);
                //card.play();
                //card.setSize(120, 192);
                //card.Location = new Point(0, 0);
                //Controls.Add(card);
                PNLCost.purchase();
                PNLCost.clear();
                hand.Remove(card);

                if (hand.Count == 1)
                {
                    form_.getGame().serverConnection.playManager.lastCardLocation = hand[0].cardLocation;
                }

                wonder.Controls.Remove(card);

                String leftUser = leftNeighbor.getUsername();
                String rightUser = rightNeighbor.getUsername();
                String cardsToSend = "";

                for (int count = 0; count < hand.Count; count++)
                {
                    cardsToSend += hand[count].getLocation() + Constants.DELIM_FIELD;
                }

                String receiver = "";

                if (age == Game.GameAge.AgeI || age == Game.GameAge.AgeIII)
                {
                    receiver = leftUser;
                }
                else if (age == Game.GameAge.LeaderRoster || age == Game.GameAge.AgeII)
                {
                    receiver = rightUser;
                }

                if (buildingWonder)
                {
                    wonder.wonderManager.sendBuildMessage();
                }
                else
                {
                    if (!card.moreInfoNeeded)
                    {
                        form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "PLAYCARD" + Constants.DELIM_FIELD + card.getLocation());
                    }
                    else
                    {
                        delayedPassHandMessage = "GAME" + Constants.DELIM_FIELD + "PASSHAND" + Constants.DELIM_FIELD + (int)age + Constants.DELIM_FIELD + receiver + Constants.DELIM_FIELD + hand.Count + Constants.DELIM_FIELD + cardsToSend;
                        card.MoreInfoReceived += delayedPassHandMessageCall;
                        card.getMoreInfo();
                    }
                }

                //form_.getGame().getPlayers();
                int handCount = hand.Count;

                clearHand();

                if (!buildingWonder)
                {
                    if (!card.moreInfoNeeded)
                    {
                        form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "PASSHAND" + Constants.DELIM_FIELD + (int)age + Constants.DELIM_FIELD + receiver + Constants.DELIM_FIELD + handCount + Constants.DELIM_FIELD + cardsToSend);
                    }
                }
                else if (!wonder.wonderManager.getNextWonderStage().moreInfoNeeded && buildingWonder)
                {
                    form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "PASSHAND" + Constants.DELIM_FIELD + (int)age + Constants.DELIM_FIELD + receiver + Constants.DELIM_FIELD + handCount + Constants.DELIM_FIELD + cardsToSend);
                }
                else
                {
                    delayedPassHandMessage = "GAME" + Constants.DELIM_FIELD + "PASSHAND" + Constants.DELIM_FIELD + (int)age + Constants.DELIM_FIELD + receiver + Constants.DELIM_FIELD + handCount + Constants.DELIM_FIELD + cardsToSend;
                }
            }
            else if (PNLCost.canPlayCard && (form_.getGame().getAge() == Game.GameAge.LeaderHire1 || form_.getGame().getAge() == Game.GameAge.LeaderHire2 || form_.getGame().getAge() == Game.GameAge.LeaderHire3))
            {
                PNLCost.purchase();
                PNLCost.clear();

                String rightUser = rightNeighbor.getUsername();
                if (buildingWonder)
                {
                    wonder.wonderManager.sendBuildMessage();

                    if (wonder.wonderManager.getNextWonderStage().moreInfoNeeded)
                    {
                        delayedPassHandMessage = "GAME" + Constants.DELIM_FIELD + "PASSHAND" + Constants.DELIM_FIELD + (int)form_.getGame().getAge() + Constants.DELIM_FIELD + rightUser + Constants.DELIM_FIELD + 0 + Constants.DELIM_FIELD;
                    }
                    else
                    {
                        form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "PASSHAND" + Constants.DELIM_FIELD + (int)form_.getGame().getAge() + Constants.DELIM_FIELD + rightUser + Constants.DELIM_FIELD + 0 + Constants.DELIM_FIELD);
                    }
                    
                }
                else {
                    form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "LEADERHIRE" + Constants.DELIM_FIELD + card.getLocation());
                    form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "PASSHAND" + Constants.DELIM_FIELD + (int)form_.getGame().getAge() + Constants.DELIM_FIELD + rightUser + Constants.DELIM_FIELD + 0 + Constants.DELIM_FIELD);
                }
                
                
            }
            else if (form_.getGame().getAge() == Game.GameAge.LeaderRoster)
            {
                PNLCost.clear();
                hand.Remove(card);
                wonder.Controls.Remove(card);

                String rightUser = rightNeighbor.getUsername();
                String cardsToSend = "";

                int handCount = hand.Count;

                for (int count = 0; count < hand.Count; count++)
                {
                    cardsToSend += hand[count].getLocation() + Constants.DELIM_FIELD;
                }

                card.color = Card.CardColor.Roster;
                playCard(card);

                form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "PASSHAND" + Constants.DELIM_FIELD + (int)form_.getGame().getAge() + Constants.DELIM_FIELD + rightUser + Constants.DELIM_FIELD + handCount + Constants.DELIM_FIELD + cardsToSend);
                form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "ROSTER" + Constants.DELIM_FIELD);
            }
        }
        delegate void playCardD(String card);

        public void playCard(String card)
        {
            if (this.InvokeRequired)
            {
                playCardD d = new playCardD(playCard);
                this.Invoke(d, new object[] { card });
            }
            else
            {
                Card cardToPlay = new Card(card, this);

                if (rotation == PlayerRotation.Clockwise180)
                {
                    cardToPlay.rotateCard(rotation);
                }

                setCardsVisible(playedCards[TPGBCardviewing.getCurrentActiveIndex()], false);
                TPGBWTToggleViewingButtons[TPGBCardviewing.getCurrentActiveIndex()].setActive(false);

                setCardsVisible(playedCards[(int)cardToPlay.getCardColor()], true);
                TPGBCardviewing.activateIndex((int)cardToPlay.getCardColor());

                playedCards[(int)cardToPlay.getCardColor()].Add(cardToPlay);
                setCardSizeLocation(cardToPlay, playedCards[(int)cardToPlay.getCardColor()].Count);
                cardToPlay.play();
                Controls.Add(cardToPlay);
                cardToPlay.BringToFront();

                wonder.BringToFront();
                

                //make sure its the last set of cards which are visible and not the first so players see the newly added cards
                int cardVisibleMax = getCardVisibleMax();
                if (playedCards[(int)cardToPlay.getCardColor()].Count > cardVisibleMax){
                    for (int count = 0; count < cardVisibleMax; count++)
                    {
                        playedCards[(int)cardToPlay.getCardColor()][count].Visible = false;
                    }
                    
                    cardviewingIndex[(int)cardToPlay.getCardColor()] = playedCards[(int)cardToPlay.getCardColor()].Count / cardVisibleMax;
                    int startingVisibleIndex = cardviewingIndex[(int)cardToPlay.getCardColor()] * cardVisibleMax;

                    for (int count = startingVisibleIndex; count < startingVisibleIndex + cardVisibleMax && count < playedCards[(int)cardToPlay.getCardColor()].Count; count++)
                    {
                        playedCards[(int)cardToPlay.getCardColor()][count].Visible = true;
                    }
                }

                //raise corresponding cardPlayEvent

                if (cardPlayedHandler[(int)cardToPlay.getCardColor()] != null)
                {
                    cardPlayedHandler[(int)cardToPlay.getCardColor()](cardToPlay);
                }
            }
        }
        delegate void playCardDC(Card card);

        public void playCard(Card cardToPlay)
        {
            if (this.InvokeRequired)
            {
                playCardDC d = new playCardDC(playCard);
                this.Invoke(d, new object[] { cardToPlay });
            }
            else
            {
                //Card cardToPlay = new Card(card, this);

                if (rotation == PlayerRotation.Clockwise180)
                {
                    cardToPlay.rotateCard(rotation);
                }

                setCardsVisible(playedCards[TPGBCardviewing.getCurrentActiveIndex()], false);
                TPGBWTToggleViewingButtons[TPGBCardviewing.getCurrentActiveIndex()].setActive(false);

                setCardsVisible(playedCards[(int)cardToPlay.getCardColor()], true);
                TPGBCardviewing.activateIndex((int)cardToPlay.getCardColor());

                setCardSizeLocation(cardToPlay, playedCards[(int)cardToPlay.getCardColor()].Count);


                cardToPlay.play();
                playedCards[(int)cardToPlay.getCardColor()].Add(cardToPlay);
                
                Controls.Add(cardToPlay);
                cardToPlay.BringToFront();

                wonder.BringToFront();
                

                //make sure its the last set of cards which are visible and not the first so players see the newly added cards
                int cardVisibleMax = getCardVisibleMax();
                if (playedCards[(int)cardToPlay.getCardColor()].Count > cardVisibleMax)
                {
                    for (int count = 0; count < cardVisibleMax; count++)
                    {
                        playedCards[(int)cardToPlay.getCardColor()][count].Visible = false;
                    }

                    cardviewingIndex[(int)cardToPlay.getCardColor()] = playedCards[(int)cardToPlay.getCardColor()].Count / cardVisibleMax;
                    int startingVisibleIndex = cardviewingIndex[(int)cardToPlay.getCardColor()] * cardVisibleMax;

                    for (int count = startingVisibleIndex; count < startingVisibleIndex + cardVisibleMax && count < playedCards[(int)cardToPlay.getCardColor()].Count; count++)
                    {
                        playedCards[(int)cardToPlay.getCardColor()][count].Visible = true;
                    }
                }

                //raise corresponding cardPlayEvent

                if (cardPlayedHandler[(int)cardToPlay.getCardColor()] != null)
                {
                    cardPlayedHandler[(int)cardToPlay.getCardColor()](cardToPlay);
                }
            }
        }
        public void setCardSizeLocation(Card card, int colorPlayedIndex)
        {
            Size playCardSize = new Size(Width/5, Width/3);
            Point location = new Point(0, 0);

            //int colorPlayedindex = 0;

            //colorPlayedindex = playedCards[(int)card.getCardColor()].Count;
            ////Console.WriteLine("Card count: " + colorPlayedIndex);

            switch (rotation)
            {
                case PlayerRotation.None:
                    {
                        location.X = colorPlayedIndex % 5 * playCardSize.Width;
                        location.Y = Convert.ToInt32(TPGBCardviewing.Size.Height);

                        if (colorPlayedIndex / 5 == cardviewingIndex[(int)card.getCardColor()] / 5 && TPGBCardviewing.getCurrentActiveIndex() == (int)card.getCardColor())
                        {
                            card.Visible = true;
                        }
                        else
                        {
                            card.Visible = false;
                        }

                        break;
                    }
                case PlayerRotation.Clockwise90:
                    {
                        playCardSize.Width = Convert.ToInt32(Width * 0.35);
                        playCardSize.Height = Convert.ToInt32(Height * 0.35);
                        location.X = wonder.Size.Width + wonder.Location.X;
                        location.Y = Convert.ToInt32(colorPlayedIndex%8 * playCardSize.Height * 0.2) + wonder.Location.Y;

                        if (colorPlayedIndex / 8 == cardviewingIndex[(int)card.getCardColor()] / 8)
                        {
                            card.Visible = true;
                        }
                        else
                        {
                            card.Visible = false;
                        }

                        break;
                    }
                case PlayerRotation.ClockWise270:
                    {
                        playCardSize.Width = Convert.ToInt32(Width * 0.35);
                        playCardSize.Height = Convert.ToInt32(Height * 0.35);
                        location.X = 0;
                        location.Y = Convert.ToInt32(colorPlayedIndex%8 * playCardSize.Height * 0.2) + wonder.Location.Y;

                        if (colorPlayedIndex / 8 == cardviewingIndex[(int)card.getCardColor()] / 8)
                        {
                            card.Visible = true;
                        }
                        else
                        {
                            card.Visible = false;
                        }

                        break;
                    }
                case PlayerRotation.Clockwise180:
                    {
                        playCardSize.Width = Width / 4;
                        playCardSize.Height = playCardSize.Width * 5 / 3;

                        location.X = colorPlayedIndex % 5 * playCardSize.Width;
                        location.Y = Convert.ToInt32(TPGBCardviewing.Location.Y - playCardSize.Height);

                        //Console.WriteLine("Calculated Point: (" + location.X + ", " + location.Y + ")");
                        if (colorPlayedIndex / 4 == cardviewingIndex[(int)card.getCardColor()] / 4)
                        {
                            card.Visible = true;
                        }
                        else
                        {
                            card.Visible = false;
                        }

                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            card.setSize(playCardSize.Width, playCardSize.Height);
            card.Location = location;
        }
        public PlayerRotation getPlayerRotation()
        {
            return rotation;
        }
        private void toggleView(object sender, EventArgs e)
        {
            if (PNLDebtToken.Visible)
            {
                PNLDebtToken.Visible = false;
            }
            else if (PNLArmyVictoryToken.Visible)
            {
                PNLArmyVictoryToken.Visible = false;
            }
            else
            {
                TogglePicutreButton toggleButton = (TogglePicutreButton)sender;

                setCardsVisible(playedCards[toggleButton.getID()], toggleButton.isActive());
            }
        }
        private void setCardsVisible(List<Card> buttonList, bool visible)
        {
            if (!visible)
            {
                foreach (Card item in buttonList)
                {
                    item.Visible = false;
                }

                if (buttonList.Count > 0)
                {
                    Card.CardColor cardColorType = buttonList[0].getCardColor();
                    cardviewingIndex[(int)cardColorType] = 0;
                }
            }
            else
            {
                if (buttonList.Count > 0){

                int cardVisibleMax = getCardVisibleMax();
                Card.CardColor cardColorType = buttonList[0].getCardColor();
                int firstCurrentIndex = cardviewingIndex[(int)cardColorType] * cardVisibleMax + (cardviewingIndex[(int)cardColorType] == 0 ? 0 : -1);
                bool isCurrentIndexAlreadyShowing = buttonList[firstCurrentIndex].Visible;

                    if (isCurrentIndexAlreadyShowing){

                        for (int count = firstCurrentIndex; count < firstCurrentIndex + cardVisibleMax && count < buttonList.Count; count ++ )
                        {
                            buttonList[count].Visible = false;
                        }

                        if (firstCurrentIndex + cardVisibleMax >= buttonList.Count)
                        {
                            cardviewingIndex[(int)cardColorType] = 0;
                        }
                        else
                        {
                            cardviewingIndex[(int)cardColorType] ++;
                        }
                    }

                    for (int count = cardviewingIndex[(int)cardColorType] * cardVisibleMax; count < (cardviewingIndex[(int)cardColorType] + 1) * cardVisibleMax && count < buttonList.Count; count++)
                    {
                        buttonList[count].Visible = true;
                    }

                }
            }
        }
        public int getCardVisibleMax()
        {
            if (rotation == PlayerRotation.None)
            {
                return 5;
            }
            else if (rotation == PlayerRotation.Clockwise90 || rotation == PlayerRotation.ClockWise270)
            {
                return 8;
            }
            else
            {
                return 4;
            }
        }
        private void updateNbOfCards(Card cardPlayed)
        {
            TPGBWTToggleViewingButtons[(int)cardPlayed.getCardColor()].setText("" + playedCards[(int)cardPlayed.getCardColor()].Count);
        }
        public void addResource(Resources.Resource resourceToAdd)
        {
            resources += resourceToAdd;

            if (ResourceAdded != null)
            {
                ResourceAdded(resourceToAdd, new ResourceAddedEvent("Resources added."));
            }
        }
        public void increaseArmy(int value)
        {
            armySize += value;

            if (ArmySizeIncrease != null)
            {
                ArmySizeIncrease(this, new ArmyIncreaseEvent("Army Size Increased."));
            }
        }
        public void setProduces(int resourceIndex, bool ans = true)
        {
            if (produces_back[resourceIndex] != ans)
            {
                produces_back[resourceIndex] = ans;

                if (ResourceGenerationChanged != null)
                {
                    ResourceGenerationChanged(this, new ResourceGenerationChangedEvent("Resource Generation changed"));
                }
            }
        }
        private void burnCard(object source, EventArgs e)
        {
            if ( form_.getGame().getAge() != Game.GameAge.LeaderRoster)
            {
                PNLCost.clear();
                Card cardToBurn = (Card)source;
                hand.Remove(cardToBurn);
                wonder.Controls.Remove(cardToBurn);

                if (hand.Count == 1)
                {
                    form_.getGame().serverConnection.playManager.lastCardLocation = hand[0].cardLocation;
                }

                Game.GameAge age = form_.getGame().getAge();
                Card.CardColor color = cardToBurn.getCardColor();

                String leftUser = leftNeighbor.getUsername();
                String rightUser = rightNeighbor.getUsername();
                String cardsToSend = "";

                for (int count = 0; count < hand.Count; count++)
                {
                    cardsToSend += hand[count].getLocation() + Constants.DELIM_FIELD;
                }

                String receiver = Username;

                if (age == Game.GameAge.AgeI || age == Game.GameAge.AgeIII)
                {
                    receiver = leftUser;
                }
                else if (age == Game.GameAge.LeaderRoster || age == Game.GameAge.AgeII)
                {
                    receiver = rightUser;
                }

                if (age == Game.GameAge.LeaderHire1 || age == Game.GameAge.LeaderHire2 || age == Game.GameAge.LeaderHire3)
                {
                    form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "REMOVELEADERROSTER" + Constants.DELIM_FIELD + Username);
                    removeKnownLeader(cardToBurn.cardLocation);
                }
           
                int handCount = hand.Count;

                clearHand();
                form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "PASSHAND" + Constants.DELIM_FIELD + (int)age + Constants.DELIM_FIELD + receiver + Constants.DELIM_FIELD + handCount + Constants.DELIM_FIELD + cardsToSend);

                form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "BURN" + Constants.DELIM_FIELD + cardToBurn.cardLocation);
            }
        }
        private void wonderCard(object source, EventArgs e)
        {
            if ( form_.getGame().getAge() != Game.GameAge.LeaderRoster)
            {
                WonderStage wonderStage = wonder.wonderManager.getNextWonderStage();

                if (wonderStage != null)
                {
                    PNLCost.clear();
                    form_.getPNLServer().getPNLGame().setDescription(wonder.wonderManager.getDesc());
                    PNLCost.checkWonderCost(wonderStage);
                    buildingWonder = true;
                }
            }
        }
        private void showDebt(object source, EventArgs e)
        {
            PNLDebtToken.Visible = !PNLDebtToken.Visible;
            PNLDebtToken.BringToFront();
        }
        public void attachCoinArmyListener(object source, EventArgs e)
        {
            if (wonder != null)
            {
                wonder.PBCoin.ButtonClick += showDebt;
                wonder.PBArmy.ButtonClick += showArmy;
            }
        }
        private void showArmy(object source, EventArgs e)
        {
            PNLArmyVictoryToken.Visible = !PNLArmyVictoryToken.Visible;
            PNLArmyVictoryToken.BringToFront();
        }

        private delegate void removeUnknownLeaderD();
        public void removeUnknownLeader()
        {
            if (this.InvokeRequired)
            {
                removeUnknownLeaderD d = new removeUnknownLeaderD(removeUnknownLeader);
                this.Invoke(d,new object[]{});

            }
            else
            {
                Card cardToRemove = playedCards[(int)Card.CardColor.Roster][playedCards[(int)Card.CardColor.Roster].Count - 1];
                Controls.Remove(cardToRemove);
             
                playedCards[(int)Card.CardColor.Roster].Remove(cardToRemove);
                cardPlayedHandler[(int)Card.CardColor.Roster](cardToRemove);
            }
        }
        private delegate void removeKnownLeaderD(String leader);
        public void removeKnownLeader(String leader)
        {
            if (this.InvokeRequired)
            {
                removeKnownLeaderD d = new removeKnownLeaderD(removeKnownLeader);
                this.Invoke(d, new object[] { leader});

            }
            else
            {
                Card cardToRemove = null;

                for(int count = 0; count < playedCards[(int)Card.CardColor.Roster].Count; count++)
                {
                    if (playedCards[(int)Card.CardColor.Roster][count].cardLocation.Equals(leader))
                    {
                        cardToRemove = playedCards[(int)Card.CardColor.Roster][count];
                        playedCards[(int)Card.CardColor.Roster].Remove(cardToRemove);
                        break;
                    }
                }

                Controls.Remove(cardToRemove);
                cardPlayedHandler[(int)Card.CardColor.Roster](cardToRemove);

                repostitionLeaders();
            }
        }
        private void repostitionLeaders()
        {
            Size cardSize = playedCards[(int)Card.CardColor.Roster][0].Size;

            for (int count = 0; count < playedCards[(int)Card.CardColor.Roster].Count; count++)
            {
                Point newPoint = new Point(cardSize.Width * count, playedCards[(int)Card.CardColor.Roster][count].Location.Y);
                playedCards[(int)Card.CardColor.Roster][count].Location = newPoint;
            }
        }
        private delegate void removeKnownHiredLeaderD(String leader);
        public void removeKnownHiredLeader(String leader)
        {
            if (this.InvokeRequired)
            {
                removeKnownHiredLeaderD d = new removeKnownHiredLeaderD(removeKnownHiredLeader);
                this.Invoke(d, new object[] { leader });

            }
            else
            {
                Card cardToRemove = null;

                for (int count = 0; count < playedCards[(int)Card.CardColor.White].Count; count++)
                {
                    if (playedCards[(int)Card.CardColor.White][count].cardLocation.Equals(leader))
                    {
                        cardToRemove = playedCards[(int)Card.CardColor.White][count];
                        playedCards[(int)Card.CardColor.White].Remove(cardToRemove);
                        break;
                    }
                }

                cardToRemove.removeCardAbililities();
                Controls.Remove(cardToRemove);
                cardPlayedHandler[(int)Card.CardColor.White](cardToRemove);

                repostitionHiredLeaders();
            }
        }
        private void repostitionHiredLeaders()
        {
            Size cardSize = playedCards[(int)Card.CardColor.Roster][0].Size;

            for (int count = 0; count < playedCards[(int)Card.CardColor.White].Count; count++)
            {
                Point newPoint = new Point(cardSize.Width * count, playedCards[(int)Card.CardColor.White][count].Location.Y);
                playedCards[(int)Card.CardColor.White][count].Location = newPoint;
            }
        }
        private void delayedPassHandMessageCall(object sender, EventArgs e)
        {
            //Console.WriteLine(delayedPassHandMessage);
            form_.getGame().sendMessage(delayedPassHandMessage);
        }
        public bool hasPlayedCard(String cardName)
        {
            foreach (List<Card> item in playedCards)
            {
                foreach (Card cardToCheck in item)
                {
                    if (cardName.Equals(cardToCheck.name))
                    {
                        //Console.WriteLine("card has been played.");
                        return true;
                    }
                }
            }

            return false;
        }
        public void raiseCardSelected(Card card) 
        {
            if (CardSelected != null)
            {
                CardSelected(card, new CardSelectedEvent("Card Selected."));
            }
        }
        public void raiseChainBuildEvent()
        {
            if (!chainMade)
            {
                if (ChainBuild != null)
                {
                    ChainBuild(this, new ChainBuildEvent("Chain Build."));
                }
            }
        }
        public void raiseGoldReceivedEvent()
        {
            if (GoldReceived != null)
            {
                GoldReceived(this, new GoldReceivedEvent("Gold Received."));
            }
        }
        public void registerTrade(Player user, int trade, String desc)
        {
            if (TradeMade != null)
            {
                TradeMade(this, new TradeMadeEvent(user,trade,desc));
            }
        }
        public List<Card> getHand()
        {
            return hand;
        }
        private void resetChain(object source, EventArgs e)
        {
            chainMade = false;
        }
        private void attachResetChain(object source, EventArgs e)
        {
            form_.getGame().serverConnection.playManager.NewTurn += resetChain;
        }
        public delegate void getScienceChoicesD();
        public void getScienceChoices()
        {
            if (this.InvokeRequired)
            {
                getScienceChoicesD d = new getScienceChoicesD(getScienceChoices);
                this.Invoke(d, new object[] { });
            }
            else
            {
                if (scienceCopy + scienceChoice > 0)
                {
                    CardEffects.ScienceEndOfGamePNL pnl = new CardEffects.ScienceEndOfGamePNL(this);
                    pnl.Size = new Size(370, 350);
                    pnl.Location = new Point((this.form_.getPNLServer().getPNLGame().getRightSectionXPoint() - pnl.Size.Width) / 2, (this.form_.Size.Height - pnl.Size.Height) / 2);
                    form_.getPNLServer().getPNLGame().Controls.Add(pnl);
                    pnl.BringToFront();
                }
                else
                {
                    form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "SCIENCECHOICE" + Constants.DELIM_FIELD + Username + Constants.DELIM_FIELD + "0" + Constants.DELIM_FIELD + "0" + Constants.DELIM_FIELD + "0");
                }
            }
        }
        public int getVictoryPointsForColor(Card.CardColor color)
        {
            int points = 0;

            foreach (Card item in playedCards[(int)color])
            {
                points += item.getVictoryPoint();
            }

            return points;
        }
    }
}
