﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client
{
    class TransparentLabel : Panel
    {
        Label LBLText = new Label();

        public TransparentLabel(){
            LBLText.BackColor = Color.Transparent;
            BackColor = Color.Transparent;
            Controls.Add(LBLText);
        }
        public void setSize(int width, int height) {
            LBLText.Size = new Size(width, height);
            Size = new Size(width, height);
        }
        public void setFontSize(int size) {
            LBLText.Font = new Font(LBLText.Font.FontFamily.Name, size);
        }
        public void setText(String ans) {
            LBLText.Text = ans;
        }
        public String getText() {
            return LBLText.Text;
        }
        public Label getLabel()
        {
            return LBLText;
        }
    }
}
