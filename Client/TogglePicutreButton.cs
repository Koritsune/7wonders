﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client
{
    class TogglePicutreButton: Panel
    {
        bool active = false;
        bool enable = true;
        bool autoToggle;

        protected String activePicture;
        protected String inactivePicture;

        int ID = 0;

        public event ToggleClickHandler ToggleClick;
        public delegate void ToggleClickHandler(object source, ToggleClickEvent e);

        public class ToggleClickEvent : EventArgs
        {
            private string EventInfo;
            public ToggleClickEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public event ValueChangeHandler valueChange;
        public delegate void ValueChangeHandler(object source, ValueChangeEvent e);

        public class ValueChangeEvent : EventArgs
        {
            private string EventInfo;
            public ValueChangeEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        protected TogglePicutreButton()
        {

        }
        public TogglePicutreButton(String active, String inactive, bool autotoggle_ = true) {
            activePicture = active;
            inactivePicture = inactive;

            BackColor = Color.Transparent;
            BackgroundImage = Image.FromFile(inactivePicture);
            BackgroundImageLayout = ImageLayout.Stretch;

            autoToggle = autotoggle_;
            this.MouseClick += toggleActive;
        }
        public void setActive(bool ans)
        {
            active = ans;

            if (active)
            {
                BackgroundImage = Image.FromFile(activePicture);
            }
            else
            {
                BackgroundImage = Image.FromFile(inactivePicture);
            }

        }
        private void toggleActive(object sender, EventArgs e)
        {
            if (enable)
            {
                if (autoToggle)
                {
                    active = !active;

                    if (active)
                    {
                        BackgroundImage = Image.FromFile(activePicture);
                    }
                    else
                    {
                        BackgroundImage = Image.FromFile(inactivePicture);
                    }
                }

                raiseValueChangedEvent();
                raiseToggleClickEvent();
            }
        }
        public void toggleActive() {
            if (enable)
            {
                active = !active;

                if (active)
                {
                    BackgroundImage = Image.FromFile(activePicture);
                }
                else
                {
                    BackgroundImage = Image.FromFile(inactivePicture);
                }

                raiseValueChangedEvent();
            }
        }
        public bool isActive()
        {
            return active;
        }
        public virtual void setSize(int width, int height) {

            this.Size = new Size(width, height);
        }
        public void setEnable(bool ans)
        {
            enable = ans;
        }
        public String getActivePicutre()
        {
            return activePicture;
        }
        public void setID(int ans)
        {
            ID = ans;
        }
        public int getID()
        {
            return ID;
        }
        protected void raiseValueChangedEvent()
        {
            if (valueChange != null)
            {
                valueChange(this, new ValueChangeEvent("Value of ToggleGroupBox changed."));
            }
        }
        protected void raiseToggleClickEvent()
        {
            if (ToggleClick != null)
            {
                ToggleClick(this, new ToggleClickEvent("Toggle Button was clicked"));
            }
        }
    }
}
