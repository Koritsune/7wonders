﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;

namespace Client
{
    public class SocketKeepAlive
    {
        Timer timerStatusUpdate = new Timer();
        Timer timerStatusPoll = new Timer();
        Socket handler;
        bool AliveBuffer = true;
        bool Alive = true;
        public event SocketDiedHandler onSocketDied;
        public event SocketAliveHandler onSocketAlive;

        Server server;

        //First we have to define a delegate that acts as a signature for the
        //function that is ultimately called when the event is triggered.
        //You will notice that the second parameter is of MyEventArgs type.
        //This object will contain information about the triggered event.
        public delegate void SocketAliveHandler(object source, SocketAliveEvent e);

        //This is a class which describes the event to the class that recieves it.
        //An EventArgs class must always derive from System.EventArgs.
        public class SocketAliveEvent : EventArgs
        {
            private string EventInfo;
            public SocketAliveEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }
        //First we have to define a delegate that acts as a signature for the
        //function that is ultimately called when the event is triggered.
        //You will notice that the second parameter is of MyEventArgs type.
        //This object will contain information about the triggered event.
        public delegate void SocketDiedHandler(object source, SocketDiedEvent e);

        //This is a class which describes the event to the class that recieves it.
        //An EventArgs class must always derive from System.EventArgs.
        public class SocketDiedEvent : EventArgs
        {
            private string EventInfo;
            public SocketDiedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }
        public SocketKeepAlive(Socket ans, Server server_) {
            ////Console.WriteLine("Object Made");
            server = server_;
            handler = ans;
            timerStatusUpdate.Interval = Constants.NETWORK_INTERVAL;
            timerStatusUpdate.Tick += checkStatus;
            timerStatusPoll.Interval = Constants.NETWORK_INTERVAL/2;
            timerStatusPoll.Tick += pollStatus;
        }
        public void keepAlive() {
            AliveBuffer = true;
        }
        private void checkStatus(object sender, EventArgs e) {
            if (AliveBuffer && !Alive)
            {
                Alive = true;
                AliveBuffer = false;

                if (onSocketAlive != null) { 
                    onSocketAlive(this, new SocketAliveEvent("Socket changed from dead to alive"));
                    //timerStatusPoll.Tick += pollStatus;
                }
            }
            else if (!AliveBuffer && Alive){
                Alive = false;
                if (onSocketDied!= null){
                    onSocketDied(this,new SocketDiedEvent("Socket changed form Alive to dead"));
                    //timerStatusPoll.Tick -= pollStatus;
                }
            }
            AliveBuffer = false;
        }
        private void pollStatus(object sender, EventArgs e) {
            ////Console.WriteLine("Sending ACK");
            Server.sendSingleMessage(handler, "ACKREQUEST" + Constants.DELIM_FIELD);
        }
        public bool isAlive() {
            return Alive;
        }
        public void start() {
            ////Console.WriteLine("Start Ticking");
            timerStatusPoll.Start();
            timerStatusUpdate.Start();
        }
        public void stop() {
            timerStatusPoll.Stop();
            timerStatusUpdate.Stop();
        }
    }
}
