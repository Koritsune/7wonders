﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;

namespace Client
{
    public class ServerConnection
    {
        Thread thread;
        
        String username;
        Socket handler;
        Game game;
        CommandQeue commandsReader;
        SocketKeepAlive socketKeepAlive;
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        int timeout = Constants.NETWORK_RECONNECT_TIMEOUT;
        bool started = false;
        bool connected = false;
        bool firstDrawComplete = false;
        public bool lockMessage { get; set; }
#pragma warning disable 0414
        bool wonderloaded = false;
#pragma warning restore 0414

        Thread wonderthread;
        Thread receiveHandThread;

        List<String> processedCommands = new List<string>();

        public CardPlayManager.PlayManager playManager {get; private set;}
        public ServerConnection(Game game_, Socket handler_, String username_, CommandQeue commandsReader_)
        {
            handler = handler_;
            username = username_;
            game = game_;
            commandsReader = commandsReader_;
            socketKeepAlive = new SocketKeepAlive(handler,game.getForm().getPNLMainContainer().getServer());
            socketKeepAlive.onSocketAlive += regainedConnection;
            socketKeepAlive.onSocketDied += lostConnection;
            timer.Interval = Constants.NETWORK_INTERVAL;
            timer.Tick += disconnect;
            thread = new Thread(() => listen(this));
            lockMessage = false;

            playManager = new CardPlayManager.PlayManager(game);
        }
        public Game getGame()
        {
            return game;
        }
        public Socket getHandler()
        {
            return handler;
        }
        public String getUsername()
        {
            return username;
        }
        public CommandQeue getCommandsReader() {
            return commandsReader;
        }
        private void listen(ServerConnection serverConnection) {
            ////Console.WriteLine("Listening to server.");
            ServerConnection serverConnection_ = serverConnection;
            Form1 form_ = serverConnection_.getGame().getForm();
            String data = "";

            CommandQeue commandsReader = serverConnection.getCommandsReader();
            
            //SocketKeepAlive socketKeepAlive = new SocketKeepAlive(handler);
            //socketKeepAlive.start();

            while (started)
            {
                data = commandsReader.next();
                String command = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));

                switch (command)
                {
                    case "USER":
                        {
                            game.sendMessage("CONF" + Constants.DELIM_FIELD + data);
                            data = data.Substring(command.Length + 1);
                            String username = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                            String display = data.Substring(username.Length + 1);
                            form_.getGame().addPlayer(username, display);
                            break;
                        }
                    case "LEAVE":
                        {
                            game.sendMessage("CONF" + Constants.DELIM_FIELD + data);
                            data = data.Substring(command.Length + 1);
                            //Console.WriteLine("USER: " + data + " is leaving.");
                            form_.getGame().removePlayer(data);
                            break;
                        }
                    case "KICK": 
                        {
                            game.sendMessage("CONF" + Constants.DELIM_FIELD + data);
                            data = data.Substring(command.Length + 1);
                            if (data.Equals(username))
                            {
                                form_.getPNLMainContainer().showPopUp("Kicked from Game", form_.getkickReasonGenerator().getReason());
                                form_.getGame().clearGame();
                            }
                            else {
                                form_.getGame().removePlayer(data);
                            }
                            break;
                        }
                    case "SERVEREND":
                        {
                            form_.getPNLMainContainer().showPopUp("Connection Lost", "The Server closed the game, please reconnect later once the host starts again; or try to connect to another host.");
                            form_.getGame().clearGame();
                            break;
                        }
                    case "ACKREQUEST":
                        {
                            Server.sendSingleMessage(handler,"ACKRESPONSE" + Constants.DELIM_FIELD);
                            break;
                        }
                    case "ACKRESPONSE":
                        {
                            socketKeepAlive.keepAlive();
                            break;
                        }
                    case "LOST":
                        {
                            game.sendMessage("CONF" + Constants.DELIM_FIELD + data);
                            String username = data.Substring(command.Length + 1);
                            ////Console.WriteLine("Lost player: " + username);
                            form_.getGame().lostConnection(username);
                            break;
                        }
                    case "FOUND":
                        {
                            game.sendMessage("CONF" + Constants.DELIM_FIELD + data);
                            String username = data.Substring(command.Length + 1);
                            form_.getGame().regainedConnection(username);
                            break;
                        }
                    case "MSG":
                        {
                            game.sendMessage("CONF" + Constants.DELIM_FIELD + data);
                            data = data.Substring(command.Length + 1);
                            String sender = data.Substring(0,data.IndexOf(Constants.DELIM_FIELD));
                            String content = data.Substring(sender.Length + 1);
                            
                            form_.getGame().receiveMessage(sender,content);
                            break;
                        }
                    case "SYS":
                        {
                            game.sendMessage("CONF" + Constants.DELIM_FIELD + data);
                            data = data.Substring(command.Length + 1);

                            form_.getGame().receiveSystemMessage(data);
                            break;
                        }
                    case "EXIT":
                        {
                            break;
                        }
                    case "OPTIONS": 
                        {
                            game.sendMessage("CONF" + Constants.DELIM_FIELD + data);

                            data = data.Substring(command.Length + 1);
                            String option = data.Substring(0,data.IndexOf(Constants.DELIM_FIELD));

                            data = data.Substring(option.Length + 1);
                            form_.getGame().sendMessage("NOTREADY" + Constants.DELIM_FIELD + form_.getConfig().getUsername());

                            switch (option)
                            { 
                                case "TIME":
                                    {
                                        int time = Convert.ToInt32(data);

                                        form_.getGame().setTurnTime(time);
                                        form_.getPNLServer().changeTime(time);

                                        form_.getGame().receiveSystemMessage("Time changed to: " + time);
                                        break;
                                    }
                                case "AB": 
                                    {
                                        int index = Convert.ToInt32(data);

                                        form_.getGame().setABChoicePlayer(index == 1);
                                        form_.getPNLServer().changeChoiceAB(index);

                                        if (index == 1)
                                        {
                                            form_.getGame().receiveSystemMessage("Side A or B: Player's choice");
                                        }
                                        else
                                        {
                                            form_.getGame().receiveSystemMessage("Side A or B: Random");
                                        }
                                        break;
                                    }
                                case "EXP":
                                    {
                                        String index = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                        String status = data.Substring(index.Length + 1);

                                        if (index.Equals("0"))
                                        {
                                            //Expansion is in regards to Leaders
                                            bool statusb = status.Equals("1");

                                            form_.getGame().receiveSystemMessage("Expansion Leaders: " + (statusb ? "Enabled" : "Disabled"));
                                            form_.getGame().setLeaders(statusb);


                                        }
                                        else if (index.Equals("1"))
                                        {
                                            //Expansion is in regards to Cities
                                            bool statusb = status.Equals("1");

                                            form_.getGame().receiveSystemMessage("Expansion Cities: " + (statusb ? "Enabled" : "Disabled"));
                                            form_.getGame().setCities(statusb);
                                        }
                                        else if (index.Equals("2"))
                                        {
                                            //Expansion is in regards to Babel
                                        }
                                        form_.getPNLServer().changeExpansionStatus(Convert.ToInt32(index), status.Equals("1"));
                                        break;
                                    }
                                case "WONDER":
                                    {
                                        String index = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                        int indexValue = Convert.ToInt32(index);
                                        String status = data.Substring(index.Length + 1);
                                        bool wonderstatus = status.Equals("1");

                                        form_.getGame().getWonders()[indexValue].setActive(wonderstatus);
                                        form_.getPNLServer().getPNLWonder().setWonderStatus(indexValue, wonderstatus);

                                        form_.getGame().receiveSystemMessage("Wonder : " + form_.getGame().getConfig().getWonders()[indexValue].getName() + (wonderstatus ? " Enabled" : " Disabled"));
                                        break;
                                    }
                                default:
                                    {
                                        //Console.WriteLine("Unrecognized option: " + option);
                                        break;
                                    }
                            }
                            break;
                        }
                    case "READY":
                        {
                            String user = data.Substring(command.Length + 1);
                            form_.getGame().ready(user);
                            break;
                        }
                    case "NOTREADY":
                        {
                            String user = data.Substring(command.Length + 1);
                            form_.getGame().notready(user);
                            break;
                        }
                    case "START":
                        {
                            //Console.WriteLine("Start.");
                            form_.getPNLServer().startGame();
                            break;
                        }
                    case "CANCEL":
                        {
                            form_.getPNLServer().stopGame();
                            break;
                        }
                    case "GAME":
                        {
                            data = data.Substring(command.Length + 1);

                            String turnSentString = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                            int turnSent = Convert.ToInt32(turnSentString);

                            if (turnSent != game.turn || processedCommands.Contains(command))
                            {
                                Server.sendSingleMessage(handler, "CONF" + Constants.DELIM_FIELD + "GAME" + Constants.DELIM_FIELD + data);
                                break;
                            }
                            else
                            {
                                Server.sendSingleMessage(handler, "CONF" + Constants.DELIM_FIELD + "GAME" + Constants.DELIM_FIELD + data);
                                processedCommands.Add(data);
                            }

                            data = data.Substring(turnSentString.Length + 1);
                            command = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));

                            switch (command)
                            {
                                case "WONDER":
                                    {
                                        ////Console.WriteLine("WONDER commnad received.");
                                        String wonder = data.Substring(command.Length + 1);
                                        //choice is launched on a thread because otherwise the listener thread will be lock and become unable to respond to heartbeat requests
                                        wonderthread = new Thread(() => chooseWonderSide(wonder));
                                        wonderthread.Start();
                                        break;
                                    }
                                case "WONDERCHOICE":
                                    {
                                        data = data.Substring(command.Length + 1);
                                        String wonderUsername = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                        data = data.Substring(wonderUsername.Length + 1);
                                        String wonder = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                        data = data.Substring(wonder.Length + 1);
                                        char side = data[0];

                                        form_.getPNLServer().getPNLGame().loadWonder(wonderUsername, wonder, side);

                                        if (wonderUsername.Equals(username))
                                        {
                                            wonderloaded = true;
                                        }
                                        //Thread wonderload = new Thread(()=>form_.getPNLServer().getPNLGame().loadWonder(wonderUsername, wonder, side));
                                        //wonderload.Start();

                                        break;
                                    }
                                case "PLAYERORDER":
                                    {
                                        data = data.Substring(command.Length + 1);
                                        //Console.WriteLine("PLAYERORDER received: " + data.Replace(Constants.DELIM_FIELD, '#'));

                                        List<String> playerOrder = new List<String>();
                                        
                                        for (int count = 0; count < game.getnbPlayers(); count++)
                                        {
                                            String user = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));

                                            if (count < game.getnbPlayers() - 1)
                                            {
                                                data = data.Substring(user.Length + 1);
                                            }

                                            playerOrder.Add(user);
                                        }

                                        game.setPlayerOrder(playerOrder);
                                        form_.getPNLServer().getPNLGame().init();
                                        break;
                                    }
                                case "CARDS":
                                    {
                                        data = data.Substring(command.Length + 1);

                                        int nb = Convert.ToInt32(data.Substring(0, data.IndexOf(Constants.DELIM_FIELD)));

                                        data = data.Substring(data.IndexOf(Constants.DELIM_FIELD) + 1);
                                        //Console.WriteLine("Cards: " + data.Replace(Constants.DELIM_FIELD, '#'));

                                        String cardsToReceive = data.Substring(0);

                                        if (!firstDrawComplete)
                                        {
                                            firstDrawComplete = true;
                                            receiveHandThread = new Thread(() => receiveCards(nb, cardsToReceive, form_));
                                            receiveHandThread.Start();
                                        }
                                        else
                                        {
                                            playManager.setHandBuffer(nb, cardsToReceive);
                                        }

                                        break;
                                    }
                                case "CARDPLAYED":
                                    {
                                        data = data.Substring(command.Length + 1);
                                        String user = data;

                                        form_.getPNLServer().getPNLGame().cardPlayed(user);
                                        break;
                                    }
                                case "COIN":
                                    {
                                        data = data.Substring(command.Length + 1);
                                        String user = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));                                        
                                        int goldVariance = Convert.ToInt32(data.Substring(user.Length + 1));
                                        //Console.WriteLine("Gold Variance: " + goldVariance);
                                        Resources.Resource goldToAdd = new Resources.Resource();
                                        goldToAdd.gold = goldVariance;

                                        Player playerAffected = form_.getPNLServer().getPNLGame().getPlayer(user);
                                        playerAffected.addResource(goldToAdd);

                                        break;
                                    }
                                case "TRADE":
                                    {
                                        
                                        data = data.Substring(command.Length + 1);
                                        data = data.Substring(data.IndexOf(Constants.DELIM_FIELD) + 1); //remove id field

                                        int gold = Convert.ToInt32(data.Substring(0, data.IndexOf(Constants.DELIM_FIELD)));
                                        data = data.Substring(data.IndexOf(Constants.DELIM_FIELD) + 1);
                                        String sender = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                        data = data.Substring(sender.Length + 1);
                                        String receiver = data.Substring(0,data.IndexOf(Constants.DELIM_FIELD));
                                        String desc = data.Substring(receiver.Length + 1);

                                        Player playerTradeSender = form_.getPNLServer().getPNLGame().getPlayer(sender);
                                        Player playerTradeReceiver = form_.getPNLServer().getPNLGame().getPlayer(receiver);

                                        Resources.Resource goldTradeIncome = new Resources.Resource();
                                        goldTradeIncome.gold = gold;
                                        playerTradeReceiver.addResource(goldTradeIncome);

                                        Resources.Resource goldTradeDeposit = new Resources.Resource();
                                        goldTradeDeposit.gold = gold * -1;
                                        playerTradeSender.addResource(goldTradeDeposit);

                                        playerTradeSender.registerTrade(playerTradeReceiver,gold, desc);

                                        break;
                                    }
                                case "PLAYCARD":
                                    {
                                        //Console.WriteLine("Card played info: " + data.Replace(Constants.DELIM_FIELD, '#'));
                                        data = data.Substring(command.Length + 1);
                                        String card = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                        data = data.Substring(card.Length + 1);
                                        String user = "";
                                        String moreInfo = "";

                                        try
                                        {
                                            user = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                            data = data.Substring(user.Length + 1);
                                            moreInfo = data;
                                        }
#pragma warning disable 0168
                                        catch (ArgumentOutOfRangeException e)
                                        {
#pragma warning restore 0168
                                            user = data;
                                        }

                                        if (game.getAge() == Game.GameAge.LeaderRoster) 
                                        {
                                            CardPlayManager.PlayCard cardToPlay = new CardPlayManager.PlayLeaderRoster(game.getForm(), user, card);
                                            playManager.addCardToPlay(cardToPlay);
                                        }
                                        else
                                        {
                                            
                                            CardPlayManager.PlayCard cardToPlay = new CardPlayManager.PlayCard(game.getForm(), user, card, moreInfo);
                                            playManager.addCardToPlay(cardToPlay);
                                        }
                                        
                                        //form_.getPNLServer().getPNLGame().playCard(card, user);
                                        break;
                                    }
                                case "BURN":
                                    {
                                        data = data.Substring(command.Length + 1);
                                        String user = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                        String cardLocation = data.Substring(user.Length + 1);

                                        CardPlayManager.PlayBurn cardToPlay = new CardPlayManager.PlayBurn(game,user,cardLocation);
                                        playManager.addCardToPlay(cardToPlay);
                                        break;
                                    }
                                case "DEBT":
                                    {
                                        data = data.Substring(command.Length + 1);
                                        String user = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                        int amount = Convert.ToInt32(data.Substring(user.Length + 1));

                                        Player player = game.getForm().getPNLServer().getPNLGame().getPlayer(user);
                                        player.PNLDebtToken.addDebt(amount);
                                        break;
                                    }
                                case "WAR":
                                    {
                                        data = data.Substring(command.Length + 1);
                                        String user = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                        int amount = Convert.ToInt32(data.Substring(user.Length + 1));

                                        Player player = game.getForm().getPNLServer().getPNLGame().getPlayer(user);
                                        player.PNLArmyVictoryToken.addPoints(amount);
                                        break;
                                    }
                                case "LEADERHIRE":
                                    {
                                        data = data.Substring(command.Length + 1);
                                        String card = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                        data = data.Substring(card.Length + 1);
                                        String user = data;

                                        CardPlayManager.PlayLeaderHire leaderHire = new CardPlayManager.PlayLeaderHire(form_, user, card);
                                        playManager.addCardToPlay(leaderHire);
                                        break;
                                    }
                                case "WONDERSTAGE":
                                    {
                                        data = data.Substring(command.Length + 1);
                                        String user = "";

                                        try
                                        {
                                            user = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                            data = data.Substring(user.Length + 1);
                                        }
#pragma warning disable 0168
                                        catch (ArgumentOutOfRangeException e)
                                        {
                                            user = data;
                                        }
#pragma warning restore 0168
                                        CardPlayManager.PlayWonder wonderPlay = new CardPlayManager.PlayWonder(form_, user, data);
                                        playManager.addCardToPlay(wonderPlay);
                                        break;
                                    }
                                case "GRAVEADD":
                                    {
                                        data = data.Substring(command.Length + 1);
                                        //Console.WriteLine("Added the following to the graveyard: " + data);
                                        game.Graveyard.Add(data);
                                        game.raiseUpdatedGraveyard();
                                        break;
                                    }
                                case "GRAVEDEL":
                                    {
                                        data = data.Substring(command.Length + 1);
                                        game.Graveyard.Remove(data);
                                        game.raiseUpdatedGraveyard();
                                        break;
                                    }
                                case "CARDRES":
                                    {
                                        data = data.Substring(command.Length + 1);

                                        String user = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                        data = data.Substring(user.Length + 1);
                                        String cardLocation = "";
                                        String moreInfo = "";
                                        Player player = form_.getPNLServer().getPNLGame().getPlayer(user);

                                        try
                                        {
                                            cardLocation = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                            data = data.Substring(data.IndexOf(Constants.DELIM_FIELD));
                                            moreInfo = data;
                                        }
#pragma warning disable 0168
                                        catch (ArgumentOutOfRangeException e)
                                        {
#pragma warning restore 0168
                                            cardLocation = data;
                                        }

                                        Card cardToPlay = new Card(cardLocation, player);

                                        if (cardToPlay.moreInfoNeeded)
                                        {
                                            foreach (CardEffects.CardEffect item in cardToPlay.immediateEffects)
                                            {
                                                item.receiveMoreInfo(moreInfo);
                                            }
                                        }


                                        //cardToPlay.play();
                                        cardToPlay.CardPlayed += unlockPlayManager;
                                        form_.getPNLServer().getPNLGame().playCard(cardToPlay, player);
                                        game.Graveyard.Remove(cardLocation);

                                        
                                        break;
                                    }
                                case "UNLOCK":
                                    {
                                        playManager.unlock();
                                        break;
                                    }
                                case "LEADERDRAW":
                                    {
                                        data = data.Substring(command.Length + 1);
                                        int leadersToDraw = Convert.ToInt32(data.Substring(0, data.IndexOf(Constants.DELIM_FIELD)));
                                        data = data.Substring(data.IndexOf(Constants.DELIM_FIELD) + 1);

                                        for (int count = 0; count < leadersToDraw; count++)
                                        {
                                            String leaderLocation = "";

                                            try
                                            {
                                                leaderLocation = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                            }
#pragma warning disable 0168
                                            catch (ArgumentOutOfRangeException e)
#pragma warning restore 0168
                                            {
                                                leaderLocation = data;
                                            }

                                            Card leaderToDraw = new Card(leaderLocation, game.getPlayers()[0]);
                                            leaderToDraw.color = Card.CardColor.Roster;
                                            game.getPlayers()[0].playCard(leaderToDraw);

                                            try
                                            {
                                                data = data.Substring(data.IndexOf(Constants.DELIM_FIELD) + 1);
                                            }
#pragma warning disable 0168
                                            catch (ArgumentOutOfRangeException e)
                                            {
#pragma warning restore 0168

                                            }
                                        }

                                        playManager.unlock();

                                        break;
                                    }
                                case "CHAIN":
                                    {
                                        data = data.Substring(command.Length + 1);
                                        String username = data;

                                        Player player = form_.getPNLServer().getPNLGame().getPlayer(username);
                                        player.raiseChainBuildEvent();
                                        break;
                                    }
                                case "ENDOFGAMEINFO":
                                    {
                                        data = data.Substring(command.Length + 1);
                                        String user = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));

                                        data = data.Substring(user.Length + 1);
                                        String name = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));

                                        String info = data.Substring(name.Length + 1);

                                        playManager.receiveEndofGameInfo(user, name, info);
                                        break;
                                    }
                                case "SCIENCECHOICE":
                                    {
                                        data = data.Substring(command.Length + 1);

                                        String userName = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                        Player target = form_.getPNLServer().getPNLGame().getPlayer(userName);

                                        data = data.Substring(userName.Length + 1);
                                        int gear = Convert.ToInt32(data.Substring(0, data.IndexOf(Constants.DELIM_FIELD)));

                                        data = data.Substring(data.IndexOf(Constants.DELIM_FIELD) + 1);
                                        int tablet = Convert.ToInt32(data.Substring(0, data.IndexOf(Constants.DELIM_FIELD)));

                                        data = data.Substring(data.IndexOf(Constants.DELIM_FIELD) + 1);
                                        int compass = Convert.ToInt32(data);

                                        playManager.addScience(target,gear,tablet,compass);
                                        break;
                                    }
                                case "WARPROCESSED":
                                    {
                                        playManager.increaseMilitaryTokenProcessed();
                                        break;
                                    }
                                case "REMOVELEADERROSTER":
                                    {
                                        data = data.Substring(command.Length + 1);
                                        String userTarget = data;
                                        Player player = form_.getPNLServer().getPNLGame().getPlayer(userTarget);

                                        if (player != game.getPlayers()[0])
                                        {
                                            player.removeKnownLeader(Constants.PICTURES + "LEADERS\\Back");
                                        }

                                        break;
                                    }
                                default:{
                                    //Console.WriteLine("Unrecognized game command: " + command);
                                    //Console.WriteLine("Full command details: " + data.Replace(Constants.DELIM_FIELD,'#'));
                                    break;
                                }
                            }
                            break;
                        }
                    case "CONF":
                        {
                            data = data.Substring(command.Length + 1);
                            game.MSGOut.removeMessage(data);
                            break;
                        }
                    default:
                        {
                            //Console.WriteLine("Unrecognized command: " + command);
                            break;
                        }
                }

                if (command.Equals("EXIT"))
                {
                    //stage entered if contact with server is lost. return to main menu is likely ideal with error message
                    break;
                }
            }
        }
        public void start() {
            if (!started)
            {
                connected = true;
                //SocketKeepAlive cannot be launched from this object as the while(true) loop prevents the Timer.tick event from being executed.
                game.getForm().launchSocketKeepAliveD(socketKeepAlive);
                //socketKeepAlive.start();
                thread.Start();
                started = true;
            }
        }
        public void end() {
            //Console.WriteLine("In end for serverconnection.");
            if (started) {
                timer.Stop();
                socketKeepAlive.stop();
                //thread.Abort();
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
                started = false;
                try
                {
                    wonderthread.Abort();
                }
#pragma warning disable 0168
                catch (SystemException ex)
                {

                }
#pragma warning restore 0168
            }
        }
        private void lostConnection(object sender, EventArgs e)
        {
            //serv
                connected = false;
                game.getForm().launchTimerD(timer);
                //timer.Start();
                //game.getForm().getPNLMainContainer().showPopUp("Lost Connection", "The connection with the server was unexpectedly terminated. Please ensure both you and the host have a connecting network conneciton and try again.");
                //game.clearGame();
            //game.getForm().getPNLServer().showPopUp("Lost Connection", "Connection with the server was lost, please wait while we try to re-establish the connection.");
            //Console.WriteLine("Lost connection ServerConnection with user: " + username);
        }
        private void regainedConnection(object sender, EventArgs e)
        {
            connected = true;
            game.getForm().getPNLServer().hidePopUp();
            //Console.WriteLine("Regained connection ServerConnection with user: " + username);
        }
        private void disconnect(object sender, EventArgs e) {
            ////Console.WriteLine("Tick disconnected with status: " + connected);
            if (!connected)
            {
                timeout--;
                game.getForm().getPNLServer().showPopUp("Lost Connection", "Attempting to reconnect, timeout: " + timeout);
                ////Console.WriteLine("Timeout: " + timeout);
                
                if (timeout == 0)
                {
                    game.getForm().getPNLServer().hidePopUp();
                    timer.Stop();
                    timeout = Constants.NETWORK_RECONNECT_TIMEOUT;
                    game.getForm().getPNLMainContainer().showPopUp("Lost Connection", "The connection with the server was unexpectedly terminated. Please ensure both you and the host have a connecting network connection and try again.");
                    //game.getForm().getPNLMainContainer().
                    game.clearGame();
                }
            }
            else {
                game.getForm().getPNLServer().hidePopUp();
                timer.Stop();
                timeout = Constants.NETWORK_RECONNECT_TIMEOUT;
            }
        }
        private void chooseWonderSide(String wonder)
        {
            char wonderChoice = game.getForm().getGame().getWonder(wonder);
            game.sendMessage("GAME" + Constants.DELIM_FIELD + "WONDERCHOICE" + Constants.DELIM_FIELD + username + Constants.DELIM_FIELD + wonder + Constants.DELIM_FIELD + wonderChoice);
            game.getForm().getPNLServer().getPNLGame().bringToFrontD();
        }
        private void receiveCards(int nb, String data, Form1 form_)
        {
            for (int count = 0; count < form_.getGame().getnbPlayers(); count++)
            {
                form_.getPNLServer().getPNLGame().getPlayers()[count].clearHand();
            }

            for (int count = 0; count < nb; count++)
            {
                String card = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                data = data.Substring(card.Length + 1);
                //Console.WriteLine(card);
                form_.getPNLServer().getPNLGame().getPlayers()[0].addCard(card);
            }
        }
        private void unlockPlayManager(object source, EventArgs e)
        {
            playManager.unlock();
        }
    }
}
