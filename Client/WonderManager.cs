﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Drawing;

namespace Client
{
    public class WonderManager
    {
        protected List<WonderStage> wonderStages = new List<WonderStage>();
        List<Card> wonderBuildIndicators = new List<Card>();
        List<Panel> wonderPlaceholders = new List<Panel>();
        Form1 form;
        protected Player player;
        List<bool> wonderStagesPlayed = new List<bool>();
        List<String> neighborsLinkedEffectsToParse = new List<string>();

        Resources.Resource startingResource = new Resources.Resource();

        int currentWonderStageIndex = 0;

        public bool unordered {get; set;}
        int currentWonderIndex = -1;

        public bool resizeExternal { get; set; }
        bool ignoreStarting;

        public event DelayedBuildMessageSentHandler DelayedBuildMessageSent;
        public delegate void DelayedBuildMessageSentHandler(object source, DelayedBuildMessageSentEvent e);

        public class DelayedBuildMessageSentEvent : EventArgs
        {
            private string EventInfo;
            public DelayedBuildMessageSentEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }


        public WonderManager(String wonderLocation, Form1 form_, Player player_, bool ignoreStarting_ = false)
        {
            unordered = false;
            form = form_;
            player = player_;
            ignoreStarting = ignoreStarting_;
            loadFromXML(wonderLocation);
            form_.getPNLServer().getPNLGame().NeighborsLinked += linkedEffects;
        }
        public WonderManager(Player player_)
        {
            unordered = false;
            form = player_.form_;
            player = player_;
        }
        private void loadFromXML(String wonderLocation)
        {
            XmlTextReader xmlReader = new XmlTextReader(Constants.WONDERS_FOLDER + wonderLocation + ".XML");

            while (xmlReader.Read())
            {
                if (xmlReader.NodeType != XmlNodeType.EndElement && xmlReader.Name == "Wonder")
                {
                    //add PNL to be used for certain effects such as freebuild and resourceoption
                    Panel pnl = new Panel();

                    if (player.getUsername().Equals(player.form_.getConfig().getUsername()))
                    {
                        pnl.Location = getCardBackPoint(wonderPlaceholders.Count);
                        pnl.Location = new Point(pnl.Location.X, Convert.ToInt32(player.getWonder().Size.Height * 0.8));
                        pnl.Size = getCardBackSize();
                        pnl.Size = new Size(pnl.Size.Width, Convert.ToInt32(player.getWonder().Size.Height * 0.2));
                        player.getWonder().Controls.Add(pnl);
                        pnl.BackColor = Color.Transparent;

                        //Console.WriteLine("Point: (" + pnl.Location.X + ", " + pnl.Location.Y + ") and size (" + pnl.Size.Width + ", " + pnl.Size.Height + ")");
                    }

                    wonderPlaceholders.Add(pnl);
                    pnl.BringToFront();
                        WonderStage wonderStage = new WonderStage(xmlReader, player,pnl);
                        wonderStages.Add(wonderStage);
                        wonderStagesPlayed.Add(false);
                        wonderStage.DelayedBuildMessageSent += raiseDelayedMessageSent;
                }
                else if (xmlReader.Name == "Unordered")
                {
                    unordered = true;
                }
                else if (!ignoreStarting) { 
                    if (xmlReader.Name == "Resource")
                    {
                        Resources.Resource resourceToAdd = Resources.Resource.buildResourceFromXML(xmlReader);
                        startingResource += resourceToAdd;

                        CardEffects.ResourceGenerationFixed resourceGeneration = new CardEffects.ResourceGenerationFixed(player.getWonder(), player, resourceToAdd, form.getGame());
                        resourceGeneration.play();
                    }
                    else if (xmlReader.Name == "FreeLeaders")
                    {
                        CardEffects.LeadersFreeEffect leadersFree = new CardEffects.LeadersFreeEffect(player);
                        leadersFree.play();
                    }
                    else if (xmlReader.Name == "LeaderDiscount")
                    {
                        String postNeighborLinkMessage = "LeaderDiscount" + Constants.DELIM_FIELD + xmlReader.GetAttribute("Rate") + Constants.DELIM_FIELD + xmlReader.GetAttribute("Target");
                        neighborsLinkedEffectsToParse.Add(postNeighborLinkMessage);
                    }
                }
                
            }

            foreach (WonderStage item in wonderStages)
            {
                wonderBuildIndicators.Add(null);
            }

            player.getWonder().Resize += resizeWonderIndicators;
        }
        public virtual String getDesc(int position = -1)
        {
            if (position == -1){
                if (!unordered)
                {
                    if (currentWonderStageIndex < wonderStages.Count)
                    {
                        return wonderStages[currentWonderStageIndex].desc;
                    }
                }
                else
                {
                    return wonderStages[currentWonderIndex].desc;
                }
            }
            else
            {
                try
                {
                    return wonderStages[position].desc;
                }
                catch (ArgumentOutOfRangeException e)
                {
                    return "";
                }
            }
            return "";
            
        }
        public virtual WonderStage getNextWonderStage(int position = -1)
        {
            //int count = currentWonderStageIndex;

            //currentWonderStageIndex++;
            if (position == -1)
            {
                if (!unordered)
                {
                    currentWonderStageIndex = 0;

                    while (wonderStagesPlayed[currentWonderStageIndex])
                    {
                        currentWonderStageIndex++;
                    }

                    if (currentWonderStageIndex < wonderStages.Count)
                    {
                        return wonderStages[currentWonderStageIndex];
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    if (currentWonderIndex != -1)
                    {
                        wonderPlaceholders[currentWonderIndex].BackgroundImage = null;

                        if (currentWonderIndex + 1 == wonderStages.Count)
                        {
                            currentWonderIndex = -1;
                        }
                    }

                    for (int count = currentWonderIndex + 1; count < wonderStages.Count; count++)
                    {
                        if (!wonderStagesPlayed[count])
                        {
                            currentWonderIndex = count;
                            if (!ignoreStarting)
                            {
                                wonderPlaceholders[count].BackgroundImage = Image.FromFile(Constants.WHITE_TRANSPARENT);
                            }
                            return wonderStages[count];
                        }
                    }

                    for (int count = 0; count < wonderStages.Count; count++)
                    {
                        if (!wonderStagesPlayed[count])
                        {
                            currentWonderIndex = count;
                            if (!ignoreStarting)
                            {
                                wonderPlaceholders[count].BackgroundImage = Image.FromFile(Constants.WHITE_TRANSPARENT);
                            }
                            return wonderStages[count];
                        }
                    }

                    return null;
                }
            }
            else
            {
                return wonderStages[position];
            }
        }
        public virtual void sendBuildMessage(int position = -1)
        {
            if (position != -1)
            {
                currentWonderStageIndex = position;
            }

            if (!wonderStages[currentWonderStageIndex].moreInfoNeeded)
            {
                if (!unordered)
                {
                    form.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "WONDERSTAGE" + Constants.DELIM_FIELD + player.getUsername());
                }
                else
                {
                    form.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "WONDERSTAGE" + Constants.DELIM_FIELD + player.getUsername() + Constants.DELIM_FIELD + currentWonderIndex);
                }
            }
            else
            {
                foreach (CardEffects.CardEffect item in wonderStages[currentWonderStageIndex].immediateEffects)
                {
                    item.getMoreInfo();
                }
            }

            if (unordered)
            {
                wonderPlaceholders[currentWonderIndex].BackgroundImage = null;
                currentWonderIndex = -1;
            }
        }
        public void buildWonderStage(String data)
        {
            //this function is not used
            /*
            int wonderStageToBuild = 0;

            //Console.WriteLine("Build wonder stage: " + data.Replace(Constants.DELIM_FIELD, '#'));

            if (!data.Contains(Constants.DELIM_FIELD))
            {
                wonderStageToBuild = Convert.ToInt32(data);
            }
            else
            {
                wonderStageToBuild = Convert.ToInt32(data.Substring(0, data.IndexOf(Constants.DELIM_FIELD)));
                data = data.Substring(data.IndexOf(Constants.DELIM_FIELD) + 1);
            }

                if (wonderStages[wonderStageToBuild].moreInfoNeeded)
                {
                    foreach (CardEffects.CardEffect item in wonderStages[wonderStageToBuild].immediateEffects)
                    {
                        if (item.MoreInfoNeeded)
                        {
                            item.receiveMoreInfo(data);
                        }
                    }
                }

            wonderStages[wonderStageToBuild].play();*/
        }
        public virtual WonderStage getWonderStageToBuild(String buildData, int position = -1)
        {
            int count = 0;

            if (!unordered)
            {
                if (position == -1)
                {
                    while (wonderStagesPlayed[count])
                    {
                        count++;
                    }
                }
                else
                {
                    count = position;
                }
            }
            else
            {
                count = Convert.ToInt32(buildData);
            }

            if (wonderStages[count].moreInfoNeeded)
            {
                foreach (CardEffects.CardEffect item in wonderStages[count].immediateEffects)
                {
                    item.receiveMoreInfo(buildData);
                }
            }

            return wonderStages[count];
        }
        delegate void wonderStageBuiltD(WonderStage wonderBuilt);

        public virtual void wonderStageBuilt(WonderStage wonderBuilt)
        {
            if (player.InvokeRequired)
            {
                wonderStageBuiltD d = new wonderStageBuiltD(wonderStageBuilt);
                player.Invoke(d, new object[] { wonderBuilt });
            }
            else
            {

                for(int count = 0; count < wonderStages.Count; count ++)
                {
                    if (wonderStages[count] == wonderBuilt)
                    {
                        wonderStagesPlayed[count] = true;

                        Card cardBack = new Card(getCardBack(), player, player.getPlayerRotation());
                        //cardBack.play();
                        cardBack.play();
                        cardBack.Location = getCardBackPoint(count);
                        cardBack.Size = getCardBackSize();
                        player.Controls.Add(cardBack);
                        
                        wonderBuildIndicators[count] = cardBack;
                        //cardBack.Location = new Point(Convert.ToInt32((count * 1.0 / wonderStages.Count) * player.getWonder().Size.Width), player.getWonder().Location.Y + player.getWonder().Size.Height);
                        //cardBack.Size = new Size()

                        break;
                    }
                }
            }
        }
        protected String getCardBack()
        {
            Game game = player.form_.getGame();

            if (game.getAge() == Game.GameAge.AgeI)
            {
                return Constants.PICTURES + "\\Age I\\Back";
            }
            else if (game.getAge() == Game.GameAge.AgeII)
            {
                return Constants.PICTURES + "\\Age II\\Back";
            }
            else if (game.getAge() == Game.GameAge.AgeIII)
            {
                return Constants.PICTURES + "\\Age III\\Back";
            }
            else
            {
                return Constants.PICTURES + "\\Leaders\\Back";
            }
        }
        public Size getCardBackSize()
        {
            Size size = new Size(0, 0);
            if (resizeExternal)
            {
               size.Width = Convert.ToInt32(player.getWonder().Size.Width * 0.25);
               size.Height = Convert.ToInt32(size.Width * 4 / 2.5);
            }
            else
            {
                switch (player.getPlayerRotation())
                {
                    case Player.PlayerRotation.None:
                        {
                            if (wonderStages.Count == 1)
                            {
                                size.Width = Convert.ToInt32(player.getWonder().Size.Width / 3);
                            }
                            else
                            {
                                size.Width = Convert.ToInt32(player.getWonder().Size.Width * 0.25);
                            }

                            size.Height = Convert.ToInt32(size.Width * 4 / 2.5);

                            break;
                        }
                    case Player.PlayerRotation.Clockwise90:
                        {
                            if (wonderStages.Count == 1)
                            {
                                size.Height = Convert.ToInt32(player.getWonder().Size.Height / 3);
                            }
                            else
                            {
                                size.Height = Convert.ToInt32(player.getWonder().Size.Height * 0.25);
                            }

                            size.Width = Convert.ToInt32(size.Height * 4 / 2.5);

                            break;
                        }
                    case Player.PlayerRotation.ClockWise270:
                        {
                            if (wonderStages.Count == 1)
                            {
                                size.Height = Convert.ToInt32(player.getWonder().Size.Height / 3);
                            }
                            else
                            {
                                size.Height = Convert.ToInt32(player.getWonder().Size.Height * 0.25);
                            }

                            size.Width = Convert.ToInt32(size.Height * 4 / 2.5);

                            break;
                        }
                    case Player.PlayerRotation.Clockwise180:
                        {
                            if (wonderStages.Count == 1)
                            {
                                size.Width = Convert.ToInt32(player.getWonder().Size.Width / 3);
                            }
                            else
                            {
                                size.Width = Convert.ToInt32(player.getWonder().Size.Width * 0.25);
                            }

                            size.Height = Convert.ToInt32(size.Width * 4 / 2.5);

                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
            //Console.WriteLine("Calculated wonder playu size (" + size.Width + ", " + size.Height + ")");
            return size;
        }
        public Point getCardBackPoint(int count)
        {
            Point point = new Point(0, 0);
            if (resizeExternal)
            {
                point.Y = player.getWonder().Location.Y + player.getWonder().Size.Height;
                point.X = Convert.ToInt32(player.getWonder().Size.Width / 3 * count);
            }
            else {
                switch (player.getPlayerRotation())
                {
                    case Player.PlayerRotation.None:
                        {
                            point.Y = player.getWonder().Location.Y + player.getWonder().Size.Height;

                            if (wonderStages.Count == 1)
                            {
                                point.X = Convert.ToInt32(player.getWonder().Size.Width / 3);
                            }
                            else if (wonderStages.Count == 2)
                            {
                                point.X = Convert.ToInt32(player.getWonder().Size.Width / 4 * (count + 1));
                            }
                            else if (wonderStages.Count == 3)
                            {
                                point.X = Convert.ToInt32(player.getWonder().Size.Width / 3 * count);
                            }
                            else if (wonderStages.Count == 4)
                            {
                                point.X = Convert.ToInt32(player.getWonder().Size.Width / 4 * count);
                            }

                            break;
                        }
                    case Player.PlayerRotation.Clockwise180:
                        {
                            if (wonderStages.Count == 1)
                            {
                                point.X = Convert.ToInt32(player.getWonder().Size.Width / 3);
                            }
                            else if (wonderStages.Count == 2)
                            {
                                point.X = Convert.ToInt32(player.getWonder().Size.Width / 4 * (2 - count));
                            }
                            else if (wonderStages.Count == 3)
                            {
                                point.X = Convert.ToInt32(player.getWonder().Size.Width / 3 * (2 - count));
                            }
                            else if (wonderStages.Count == 4)
                            {
                                point.X = Convert.ToInt32(player.getWonder().Size.Width / 4 * (3 - count));
                            }

                            break;
                        }
                    case Player.PlayerRotation.Clockwise90:
                        {
                            if (wonderStages.Count == 1)
                            {
                                point.Y = Convert.ToInt32(player.getWonder().Size.Height / 3 * 2 + player.getWonder().Location.Y);
                            }
                            else if (wonderStages.Count == 2)
                            {
                                point.Y = Convert.ToInt32(player.getWonder().Size.Height / 4 * (count + 1) + player.getWonder().Location.Y);
                            }
                            else if (wonderStages.Count == 3)
                            {
                                point.Y = Convert.ToInt32(player.getWonder().Size.Height / 3 * count + player.getWonder().Location.Y);
                            }
                            else if (wonderStages.Count == 4)
                            {
                                point.Y = Convert.ToInt32(player.getWonder().Size.Height / 4 * count + player.getWonder().Location.Y);
                            }

                            break;
                        }
                    case Player.PlayerRotation.ClockWise270:
                        {
                            point.X = player.getWonder().Location.X + player.getWonder().Size.Width;

                            if (wonderStages.Count == 1)
                            {
                                point.Y = Convert.ToInt32(player.getWonder().Size.Height * 2 / 3 + player.getWonder().Location.Y);
                            }
                            else if (wonderStages.Count == 2)
                            {
                                point.Y = Convert.ToInt32(player.getWonder().Size.Height / 4 * (2 - count) + player.getWonder().Location.Y);
                            }
                            else if (wonderStages.Count == 3)
                            {
                                point.Y = Convert.ToInt32(player.getWonder().Size.Height / 3 * (2 - count) + player.getWonder().Location.Y);
                            }
                            else if (wonderStages.Count == 4)
                            {
                                point.Y = Convert.ToInt32(player.getWonder().Size.Height / 4 * (3 - count) + player.getWonder().Location.Y);
                            }

                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
        }
            //Console.WriteLine("Calculated wonder playu pint (" + point.X + ", " + point.Y + ")");
            return point;
        }
        private void resizeWonderIndicators(object source, EventArgs e)
        {
            if (!resizeExternal)
            {
                for (int count = 0; count < wonderStages.Count; count++)
                {
                    if (wonderStagesPlayed[count])
                    {
                        wonderBuildIndicators[count].Location = getCardBackPoint(count);
                        wonderBuildIndicators[count].Size = getCardBackSize();
                    }

                    wonderPlaceholders[count].Location = getCardBackPoint(count);
                    wonderPlaceholders[count].Location = new Point(wonderPlaceholders[count].Location.X, Convert.ToInt32(player.getWonder().Size.Height * 0.8));
                    wonderPlaceholders[count].Size = getCardBackSize();
                    wonderPlaceholders[count].Size = new Size(wonderPlaceholders[count].Size.Width, Convert.ToInt32(player.getWonder().Size.Height * 0.2));
                }
            }
        }
        public int getWonderSpot(WonderStage stage)
        {
            for (int count = 0; count < wonderStages.Count; count++)
            {
                if (wonderStages[count] == stage)
                {
                    return count;
                }
            }

            return 0;
        }
        protected void raiseDelayedMessageSent(object sender, EventArgs e)
        {
            if (DelayedBuildMessageSent != null)
            {
                DelayedBuildMessageSent(this, new DelayedBuildMessageSentEvent("Delayed Message Sent."));
            }
        }
        private void linkedEffects(object sender, EventArgs e)
        {
            foreach (String data in neighborsLinkedEffectsToParse)
            {
                String effectType = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                String dataParser = data.Substring(effectType.Length + 1);

                if (effectType.Equals("LeaderDiscount"))
                {
                    int rate = Convert.ToInt32(dataParser.Substring(0, dataParser.IndexOf(Constants.DELIM_FIELD)));
                    dataParser = dataParser.Substring(dataParser.IndexOf(Constants.DELIM_FIELD) + 1);
                    String target = dataParser;

                    Player targetPlayer = player;

                    if (target.Equals("Left"))
                    {
                        targetPlayer = player.leftNeighbor;
                    }
                    else if (target.Equals("Right"))
                    {
                        targetPlayer = player.rightNeighbor;
                    }

                    CardEffects.LeaderDiscountEffect leaderDiscount = new CardEffects.LeaderDiscountEffect(targetPlayer, rate);
                    leaderDiscount.play();
                }
            }
        }
        public void setPlaceHolderPositionSize(int place, int totalSpots = 3)
        {
            Size size = getCardBackSize();
            for (int count = 0; count < wonderStages.Count; count++)
            {
                /*
                if (!wonderStagesPlayed[count])
                {
                    wonderBuildIndicators[count].Location = getCardBackPoint(place);
                    wonderBuildIndicators[count].Size = getCardBackSize();
                }
                */
                if (!wonderStagesPlayed[count])
                {
                    wonderPlaceholders[count].Location = getCardBackPoint(place);
                    wonderPlaceholders[count].Location = new Point(wonderPlaceholders[place].Location.X, Convert.ToInt32(player.getWonder().Size.Height * 0.8));
                    wonderPlaceholders[count].Size = getCardBackSize();
                    wonderPlaceholders[count].Size = new Size(wonderPlaceholders[count].Size.Width, Convert.ToInt32(player.getWonder().Size.Height * 0.2));
                }
            }
        }
        public virtual int getNbWonderStages()
        {
            return wonderStages.Count;
        }
        public void markAsBuilt(WonderStage wonderStage)
        {
            for (int count = 0; count < wonderStages.Count; count++)
            {
                if (wonderStage == wonderStages[count])
                {
                    wonderStagesPlayed[count] = true;
                }
            }
        }
        public int getWonderStagePosition(WonderStage wonderStage)
        {
            for (int count = 0; count < wonderStages.Count; count++)
            {
                if (wonderStage == wonderStages[count])
                {
                    return count;
                }
            }

            return -1;
        }
        public List<Panel> getwonderPlaceholder()
        {
            return wonderPlaceholders;
        }
        public List<Card> getwonderBuildIndicators()
        {
            return wonderBuildIndicators;
        }
        public List<WonderStage> getWonderStages()
        {
            return wonderStages;
        }
        public virtual int getVictoryPoints()
        {
            int points = 0;

            for(int count = 0; count < wonderStages.Count; count ++)
            {
                if (wonderStagesPlayed[count])
                {
                    points += wonderStages[count].getVictoryPoints();
                }
                
            }

            return points;
        }
        public Resources.Resource getStartingResource()
        {
            return startingResource;
        }
        public virtual int getNbofWondersBuilt()
        {
            int total = 0;

            foreach (bool item in wonderStagesPlayed)
            {
                if (item)
                {
                    total++;
                }
            }

            return total;
        }
    }
}
