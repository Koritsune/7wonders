﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;

namespace Client
{
    public class PNLGame : Panel
    {
        Form1 form_;
        List<Player> players;

        Point[] PLAYER_LOCATIONS_3 = new Point[] { new Point(274, 308), new Point(936, 250), new Point(0, 250)};
        Size[] PLAYER_SIZES_3 = new Size[] { new Size(650, 430), new Size(230, 382), new Size(230, 382) };
        Player.PlayerRotation[] PLAYER_ROTATIONS_3 = { Player.PlayerRotation.None, Player.PlayerRotation.ClockWise270, Player.PlayerRotation.Clockwise90 };

        Point[] PLAYER_LOCATIONS_4 = new Point[] { new Point(274, 308), new Point(936, 250), new Point(316,0), new Point(0, 250) };
        Size[] PLAYER_SIZES_4 = new Size[] { new Size(650, 430), new Size(230, 382), new Size(350,275), new Size(230, 382) };
        Player.PlayerRotation[] PLAYER_ROTATIONS_4 = { Player.PlayerRotation.None, Player.PlayerRotation.ClockWise270, Player.PlayerRotation.Clockwise180, Player.PlayerRotation.Clockwise90 };

        Point[] PLAYER_LOCATIONS_5 = new Point[] { new Point(274, 308), new Point(936, 250), new Point(594,0), new Point(237, 0), new Point(0, 250) };
        Size[] PLAYER_SIZES_5 = new Size[] { new Size(650, 430), new Size(230, 382), new Size(350, 275), new Size(350,275), new Size(230, 382) };
        Player.PlayerRotation[] PLAYER_ROTATIONS_5 = { Player.PlayerRotation.None, Player.PlayerRotation.ClockWise270, Player.PlayerRotation.Clockwise180, Player.PlayerRotation.Clockwise180, Player.PlayerRotation.Clockwise90 };

        Point[] PLAYER_LOCATIONS_6 = new Point[] { new Point(274, 308), new Point(936, 383), new Point(936,0), new Point(316, 0), new Point(0, 0), new Point(0,382) };
        Size[] PLAYER_SIZES_6 = new Size[] { new Size(650, 430), new Size(230, 382), new Size(230, 382), new Size(350, 275), new Size(230, 382), new Size(230,382) };
        Player.PlayerRotation[] PLAYER_ROTATIONS_6 = { Player.PlayerRotation.None, Player.PlayerRotation.ClockWise270, Player.PlayerRotation.ClockWise270, Player.PlayerRotation.Clockwise180, Player.PlayerRotation.Clockwise90, Player.PlayerRotation.Clockwise90 };

        Point[] PLAYER_LOCATIONS_7 = new Point[] { new Point(274, 308), new Point(936, 383), new Point(936, 0), new Point(594, 0), new Point(237, 0), new Point(0, 0), new Point(0, 382) };
        Size[] PLAYER_SIZES_7 = new Size[] { new Size(650, 430), new Size(230, 382), new Size(230, 382), new Size(350, 275), new Size(350, 275), new Size(230, 382), new Size(230, 382) };
        Player.PlayerRotation[] PLAYER_ROTATIONS_7 = { Player.PlayerRotation.None, Player.PlayerRotation.ClockWise270, Player.PlayerRotation.ClockWise270, Player.PlayerRotation.Clockwise180, Player.PlayerRotation.Clockwise180, Player.PlayerRotation.Clockwise90, Player.PlayerRotation.Clockwise90 };

        Point[] PLAYER_LOCATIONS_8 = new Point[] { new Point(274, 308), new Point(936, 383), new Point(936, 0), new Point(713, 0), new Point(474,0), new Point(235, 0), new Point(0, 0), new Point(0, 382) };
        Size[] PLAYER_SIZES_8 = new Size[] { new Size(650, 430), new Size(230, 382), new Size(230, 382), new Size(234, 184), new Size(234, 184), new Size(234,184), new Size(230, 382), new Size(230, 382) };
        Player.PlayerRotation[] PLAYER_ROTATIONS_8 = { Player.PlayerRotation.None, Player.PlayerRotation.ClockWise270, Player.PlayerRotation.ClockWise270, Player.PlayerRotation.Clockwise180, Player.PlayerRotation.Clockwise180, Player.PlayerRotation.Clockwise180, Player.PlayerRotation.Clockwise90, Player.PlayerRotation.Clockwise90 };

        Size LBLInfo_Size = new Size(184, 300);
        Size TXTMSGBox_Size = new Size(184, 300);
        Size TXTMSG_Size = new Size(184, 148);

        Point[] locations;
        Size[] sizes;
        Player.PlayerRotation[] rotations;

        Point LBLInfo_Point = new Point(1166, 0);
        Point TXTMSGBox_Point = new Point(1166, 300);
        Point TXTMSG_Point = new Point(1166, 600);

        bool initialized = false;
        bool fullScreen = false;
        bool canToggleFullScreen = true;
        bool toggleScrollMSGs = false;

        int nbLoaded = 0;

        System.Windows.Forms.Timer timerSize = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        Point CursorLocation;

        ScreenManager screenManager;

        TranslucentTextBox LBLInfo = new TranslucentTextBox();
        TranslucentTextBox TXTMSGBox = new TranslucentTextBox();
        TranslucentTextBox TXTMSG = new TranslucentTextBox();

        double width_Multiplier = 1;
        double height_Multiplier = 1;

        public event NeighborsLinkedHandler NeighborsLinked;

        public delegate void NeighborsLinkedHandler(object source, NeighborsLinkedEvent e);

        public class NeighborsLinkedEvent : EventArgs
        {
            private string EventInfo;
            public NeighborsLinkedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public event AllWondersLoadedHandler AllWondersLoaded;

        public delegate void AllWondersLoadedHandler(object source, AllWondersLoadedEvent e);

        public class AllWondersLoadedEvent : EventArgs
        {
            private string EventInfo;
            public AllWondersLoadedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public PNLGame(Form1 form){
            
            form_ = form;
            screenManager = new ScreenManager(form);

            Rectangle resolution = Screen.PrimaryScreen.Bounds;
            
            if (resolution.Height <= 768)
            {
                canToggleFullScreen = false;
            }
            else
            {
                canToggleFullScreen = true;
            }

            timer.Interval = 100;

            this.DoubleClick += toggle_full_screen;

            BackgroundImage = Image.FromFile(Constants.ICONS + "WonderChoices.PNG");
            BackgroundImageLayout = ImageLayout.Stretch;

            LBLInfo.Location = new Point(1166, 0);
            LBLInfo.setSize(LBLInfo_Size.Width, LBLInfo_Size.Height);
            LBLInfo.getTextBox().ForeColor = Color.White;
            LBLInfo.getTextBox().ReadOnly = true;
            LBLInfo.BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
            LBLInfo.AppendText("This will be a description of the selected card.");
            LBLInfo.setFontSize(10);
            LBLInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;

            TXTMSGBox.Location = new Point(1166, 300);
            TXTMSGBox.setSize(TXTMSGBox_Size.Width, TXTMSGBox_Size.Height);
            TXTMSGBox.setFontSize(10);
            TXTMSGBox.getTextBox().ReadOnly = true;
            TXTMSGBox.BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
            TXTMSGBox.getTextBox().ForeColor = Color.White;
            TXTMSGBox.getTextBox().ScrollBars = RichTextBoxScrollBars.ForcedVertical;
            TXTMSGBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            //TXTMSGBox.AppendText("Messages will Appear here.");
            

            TXTMSG.Location = new Point(1166, 600);
            TXTMSG.setSize(TXTMSG_Size.Width, TXTMSG_Size.Height);
            TXTMSG.setFontSize(10);
            TXTMSG.BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
            TXTMSG.getTextBox().ForeColor = Color.White;
            TXTMSG.EnterKeyPress += sendMessage;
            TXTMSGBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            //TXTMSG.getTextBox().Text = "Message shall be typed here.";
            
            Controls.Add(LBLInfo);
            Controls.Add(TXTMSGBox);
            Controls.Add(TXTMSG);

            timerSize.Tick += tickSize;

            screenManager.FullScreen += resize;
            screenManager.NormalScreen += resize;

            AllWondersLoaded += linkNeighbors;
            AllWondersLoaded += resize;
        }
        private void tickSize(object sender, EventArgs e){
            try
            {
                if (players[0].Size.Width < PLAYER_SIZES_3[0].Width || players[0].Height < PLAYER_SIZES_3[0].Height) {
                    resize();
                }
                else
                {
                    timerSize.Stop();
                }
            }
            catch (ArgumentOutOfRangeException ex1) { }
            catch (IndexOutOfRangeException ex2) { }
        }
        private delegate void loadWonderD(String username, String wonder, char side);

        public void loadWonder(String username, String wonder, char side){
            if (this.InvokeRequired)
            {
                loadWonderD d = new loadWonderD(loadWonder);
                this.Invoke(d,new object[]{username, wonder,side});

            }
            else
            {
                while (!initialized) ;

                //Console.WriteLine("Showing Player: " + username + " with wonder: " + wonder + side);
                int count = 0;
                for (; count < players.Count; count++)
                {
                    if (players[count].getUsername().Equals(username))
                    {
                        players[count].setSize(sizes[count].Width, sizes[count].Height);
                        players[count].loadWonder(wonder, side);

                        break;
                    }
                }

                Controls.Add(players[count]);
                resize();
                timerSize.Start();
            }
        }
        private delegate void bringToFrontDD();

        public void bringToFrontD()
        {
            if (this.InvokeRequired)
            {
                bringToFrontDD d = new bringToFrontDD(bringToFrontD);
                this.Invoke(d, new object[] { });

            }
            else
            {
                this.BringToFront();
                screenManager.start();
                Rectangle resolution = Screen.PrimaryScreen.Bounds;
                //resolution.

                if (!canToggleFullScreen)
                {
                    screenManager.setFullScreen();
                }
                
                form_.Size = new System.Drawing.Size(1366, 788);

                while (!initialized) ;
                while (!form_.getGame().isPlayersSorted());

                players[0].Location = locations[0];
                players[0].setRotation(rotations[0]);
                players[0].setSize(sizes[0].Width, sizes[0].Height,true);

                for (int count = 1; count < players.Count; count++)
                {
                    players[count].setRotation(rotations[count]);
                }

                timer.Tick += scrollMSGs;
                timer.Tick += resize;
                timer.Start();
            }
        }

        public void determinePositions()
        {
            switch (players.Count)
            {
                case 3:
                    {
                        locations = PLAYER_LOCATIONS_3;
                        rotations = PLAYER_ROTATIONS_3;
                        sizes = PLAYER_SIZES_3;
                        break;
                    }
                case 4:
                    {
                        locations = PLAYER_LOCATIONS_4;
                        rotations = PLAYER_ROTATIONS_4;
                        sizes = PLAYER_SIZES_4;
                        break;
                    }
                case 5:
                    {
                        locations = PLAYER_LOCATIONS_5;
                        rotations = PLAYER_ROTATIONS_5;
                        sizes = PLAYER_SIZES_5;
                        break;
                    }
                case 6:
                    {
                        locations = PLAYER_LOCATIONS_6;
                        rotations = PLAYER_ROTATIONS_6;
                        sizes = PLAYER_SIZES_6;
                        break;
                    }
                case 7:
                    {
                        locations = PLAYER_LOCATIONS_7;
                        rotations = PLAYER_ROTATIONS_7;
                        sizes = PLAYER_SIZES_7;
                        break;
                    }
                case 8:
                    {
                        locations = PLAYER_LOCATIONS_8;
                        rotations = PLAYER_ROTATIONS_8;
                        sizes = PLAYER_SIZES_8;
                        break;
                    }
            }
            initialized = true;
        }
        
        delegate void clearD();
        
        public void clear()
        {

            if (this.InvokeRequired)
            {
                clearD d = new clearD(clear);
                this.Invoke(d, new object[] { });
            }
            else
            {
                screenManager.stop();

                form_.Size = new System.Drawing.Size(900, 720);

                try
                {
                    for (int count = 0; count < players.Count; count++)
                    {
                        //Console.WriteLine("Removing Player: " + players[count].getUsername());
                        Controls.Remove(players[count]);
                        players[count].setUsername("");
                        players[count].clearHand();
                    }
                }
#pragma warning disable 0168
                catch (SystemException ex)
                {

                }
#pragma warning restore 0168
                initialized = false;
            }
        }
        public void init()
        {
            nbLoaded = 0;
            players = form_.getGame().getPlayers();
            
            for (int count = 0; count < form_.getGame().getnbPlayers(); count++ )
            {
                players[count].WonderLoaded += checkAllWondersLoaded;
            }
            
            determinePositions();
        }
        private void toggle_full_screen(object sender, EventArgs e)
        {
            if (canToggleFullScreen) { 
                if (fullScreen)
                {
                    screenManager.setNormalScreen();
                    fullScreen = false;
                }
                else
                {
                    screenManager.setFullScreen();
                    fullScreen = true;
                }

                timer.Start();
            }
        }
        private void resize(object sender, EventArgs e)
        {
            //Console.WriteLine("Resize event fired.");

            timer.Tick -= resize;
            resize();
        }
        private void resize()
        {
            while (!initialized) ;

            
                if (form_.WindowState == FormWindowState.Maximized)
                {
                    height_Multiplier = Height / 768.0;
                    width_Multiplier = Width / 1368.0;
                }
                else
                {
                    height_Multiplier = 1;
                    width_Multiplier = 1;
                }

                LBLInfo.setSize(Convert.ToInt32(LBLInfo_Size.Width * width_Multiplier), Convert.ToInt32(LBLInfo_Size.Height * height_Multiplier));
                LBLInfo.Location = new Point(Convert.ToInt32(LBLInfo_Point.X * width_Multiplier), Convert.ToInt32(LBLInfo_Point.Y * height_Multiplier));

                TXTMSGBox.setSize(Convert.ToInt32(TXTMSGBox_Size.Width * width_Multiplier), Convert.ToInt32(TXTMSGBox_Size.Height * height_Multiplier));
                TXTMSGBox.Location = new Point(Convert.ToInt32(TXTMSGBox_Point.X * width_Multiplier), Convert.ToInt32(TXTMSGBox_Point.Y * height_Multiplier));

                TXTMSG.setSize(Convert.ToInt32(TXTMSG_Size.Width * width_Multiplier), Convert.ToInt32(TXTMSG_Size.Height * height_Multiplier));
                TXTMSG.Location = new Point(Convert.ToInt32(TXTMSG_Point.X * width_Multiplier), Convert.ToInt32(TXTMSG_Point.Y * height_Multiplier));

                try
                {
                    for (int count = 0; count < players.Count; count++)
                    {
                        players[count].setSize(Convert.ToInt32(sizes[count].Width * width_Multiplier), Convert.ToInt32(sizes[count].Height * height_Multiplier));
                        players[count].Location = new Point(Convert.ToInt32(locations[count].X * width_Multiplier), Convert.ToInt32(locations[count].Y * height_Multiplier));
                    }
                }
#pragma warning disable 0168
                catch (NullReferenceException e)
                {

                }
                catch (IndexOutOfRangeException e)
                {

                }
                catch (ArgumentOutOfRangeException e)
                {

                }
#pragma warning restore 0168

            if (form_.WindowState == FormWindowState.Maximized)
            {
                LBLInfo.setSize(LBLInfo.Width + 2 + Convert.ToInt32(16 * width_Multiplier), LBLInfo.Height);
                TXTMSGBox.setSize(TXTMSGBox.Width + 2 + Convert.ToInt32(16 * width_Multiplier), TXTMSGBox.Height);
                TXTMSG.setSize(TXTMSG.Width + 2 + Convert.ToInt32(16 * width_Multiplier), TXTMSG.Height + Convert.ToInt32(20 * height_Multiplier));
            }
        }
        private void scrollMSGs(object sender, EventArgs e)
        {
            //Console.WriteLine("In Scrollmsg tick");
            if (!toggleScrollMSGs)
            {
                Point temp = form_.DesktopLocation;
                temp.X += TXTMSGBox.Location.X + TXTMSGBox.Size.Width - 4;
                temp.Y += TXTMSGBox.Location.Y + TXTMSGBox.Size.Height - 4;
                CursorLocation = Cursor.Position;
                Cursor.Position = temp;
                toggleScrollMSGs = true;
            }
            else
            {
                Cursor.Position = CursorLocation;
                toggleScrollMSGs = false;
                timer.Stop();
            }
        }

        delegate void receiveMessageD(String user, String message);

        public void receiveMessage(String user, String message)
        {
            if (this.InvokeRequired)
            {
                receiveMessageD d = new receiveMessageD(receiveMessage);
                this.Invoke(d, new object[] {user,message });
            }
            else
            {
                TXTMSGBox.AppendText(user + ": " + message);
                TXTMSGBox.getTextBox().SelectionStart = TXTMSGBox.getTextBox().Text.Length;
                TXTMSGBox.getTextBox().ScrollToCaret();
            }
        }

        private void sendMessage(object sender, EventArgs e)
        {
            form_.getGame().sendMessage("MSG" + Constants.DELIM_FIELD + form_.getConfig().getUsername() + Constants.DELIM_FIELD + TXTMSG.getTextBox().Text + "\n");
            TXTMSG.getTextBox().Text = "";
        }

        public List<Player> getPlayers()
        {
            return players;
        }

        public void setDescription(String desc)
        {
            LBLInfo.getTextBox().Text = desc;
        }
        private delegate void playCardD(String card, String user);
        public void playCard(String card, String user)
        {
            if (this.InvokeRequired)
            {
                playCardD d = new playCardD(playCard);
                this.Invoke(d, new object[] { card, user });
            }
            else
            {
                Player player = getPlayer(user);

                player.playCard(card);
            }
        }
        public void playCard(Card card, Player player)
        {
            player.playCard(card);
        }
        public Player getPlayer(String user)
        {
            for (int count = 0; count < players.Count; count++)
            {
                if (players[count].getUsername().Equals(user))
                {
                    return players[count];
                }
            }

            return null;
        }
        public void cardPlayed(String username)
        {
            Player player = getPlayer(username);

            Game.GameAge age = form_.getGame().getAge();

            player.clearHand();

            if (age == Game.GameAge.AgeI)
            {
                player.addCard(Constants.PICTURES + "Age I\\Back");
            }
            else if (age == Game.GameAge.AgeII)
            {
                player.addCard(Constants.PICTURES + "Age II\\Back");
            }
            else if (age == Game.GameAge.AgeIII)
            {
                player.addCard(Constants.PICTURES + "Age III\\Back");
            }
            else if (age == Game.GameAge.LeaderRoster || age == Game.GameAge.LeaderHire1 || age == Game.GameAge.LeaderHire2 || age == Game.GameAge.LeaderHire3)
            {
                player.addCard(Constants.PICTURES + "Leaders\\Back");
            }
        }
        
        private void checkAllWondersLoaded(object sender, EventArgs e)
        {
            nbLoaded++;

            if (nbLoaded == form_.getGame().getnbPlayers())
            {
                AllWondersLoaded(this, new AllWondersLoadedEvent("All WOnders Loaded."));
            }
        }
        
        private void linkNeighbors(object sender, EventArgs e)
        {
            for (int count = 0; count < form_.getGame().getnbPlayers(); count++)
            {
                if (count == 0)
                {
                    players[count].leftNeighbor = players[players.Count - 1];
                }
                else
                {
                    players[count].leftNeighbor = players[count - 1];
                }

                if (count == players.Count - 1)
                {
                    players[count].rightNeighbor = players[0];
                }
                else
                {
                    players[count].rightNeighbor = players[count + 1];
                }

                players[count].TradeMade += displayTrade;
            }

            if (NeighborsLinked != null)
            {
                NeighborsLinked(this, new NeighborsLinkedEvent("Neighbors Linked."));
            }
        }
        public int getRightSectionXPoint()
        {
            return LBLInfo.Location.X;
        }
        private void displayTrade(object source_, Player.TradeMadeEvent e) 
        {
            if (this.InvokeRequired)
            {
                displayTradeD d = new displayTradeD(displayTrade);
                this.Invoke(d, new object[] { source_, e });
            }
            else
            {
                Player source = (Player)source_;

                if (involvesNeighbor(e, source))
                {
                    TXTMSGBox.AppendText(e.GetTradePartner().getUsername(), Color.Gold);
                    TXTMSGBox.AppendText(" received ");
                    TXTMSGBox.AppendText(e.getGold() + "",Color.Gold);
                    TXTMSGBox.AppendText(" gold from: ");
                    TXTMSGBox.AppendText(source.getUsername(), Color.Gold);
                    TXTMSGBox.AppendText(" for: ");
                    TXTMSGBox.AppendText(e.getDesc() + "\n", Color.Gold);
                }
            }
        }

        private bool involvesNeighbor(Player.TradeMadeEvent e, Player trader)
        {
            bool ans = false;

            if (e.GetTradePartner() == form_.getGame().getPlayers()[0] || e.GetTradePartner() == form_.getGame().getPlayers()[0].leftNeighbor || e.GetTradePartner() == form_.getGame().getPlayers()[0].rightNeighbor)
            {
                return true;
            }
            else if (trader == form_.getGame().getPlayers()[0] || trader == form_.getGame().getPlayers()[0].leftNeighbor || trader == form_.getGame().getPlayers()[0].rightNeighbor)
            {
                return true;
            }

            return false;
        }
        private delegate void displayTradeD(object source, Player.TradeMadeEvent e);

    }
}
