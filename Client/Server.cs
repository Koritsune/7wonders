﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Client
{
    public class Server
    {
        const string IP = "0.0.0.0";
        volatile bool started = false;
        Thread workerThread;
        List<ServerPlayerManager> players = new List<ServerPlayerManager>();
        Form1 form_;
        //static bool lockMessage = false;
        bool playerJoining = false;
        bool gamestartListenerAdded = false;
        public DeckBuilder deckBuilder { get; private set; }
        public static ManualResetEvent passCards = new ManualResetEvent(false);
        //public static ManualResetEvent messageQueing = new ManualResetEvent(true);
        Socket listener;
        int passCardRequests = 0;
        //public List<String> Graveyard = new List<string>();
        public int wondersChosen { get; set; }
        //static QueueWorker messageQueue = new QueueWorker();
        
        public Server(Form1 ans) {
            form_ = ans;
            deckBuilder = new DeckBuilder(form_.getConfig(),form_.getGame());
            wondersChosen = 0;   
        }

        public void start() {
            if (!started) {
                started = true;
                if (!gamestartListenerAdded)
                {
                    form_.getPNLServer().Gamestart += startGame;
                }
                workerThread = new Thread(()=>StartListening(IP, form_.getConfig().getPorth()));
                workerThread.Start();
            }
        }

        public void stop() {
            if (started)
            {
                ////Console.WriteLine("Stopping server.");
                //workerThread.Abort();
                started = false;
                ////Console.WriteLine("changed started.");
                try
                {
                    //allDone.Set();
                    listener.Dispose();
                    //listener.Shutdown(SocketShutdown.Both);
                    //listener.Close();
                }
                #pragma warning disable 0168
                catch (System.Net.Sockets.SocketException ex) {

                }
                #pragma warning disable 0168
                form_.getConfig().setHost(false);
                ////Console.WriteLine("halfway stop.");
                int nbPlayers = getForm().getGame().getnbPlayers();
                sendMessage("SERVEREND" + Constants.DELIM_FIELD);
                
                for (int count = 0; count < players.Count; count++)
                {
                    players[count].end();
                }

                players.Clear();
                ////Console.WriteLine("End stop.");
                
            }
        }
        public void StartListening(String IPAddress, int Port) {
            ////Console.WriteLine("Starting new server.");
            // Data buffer for incoming data.
            byte[] bytes = new Byte[1024];

            // Establish the local endpoint for the socket.
            // The DNS name of the computer
            // running the listener is "host.contoso.com".
            //IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
            IPEndPoint localEndPoint = new IPEndPoint(System.Net.IPAddress.Parse(IPAddress), Port);
            ////Console.WriteLine("Listening");
            // Create a TCP/IP socket.
            /*
            listener = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp );
            */
            // Bind the socket to the local endpoint and listen for incoming connections.
            try {

                listener = new Socket(AddressFamily.InterNetwork,
                                      SocketType.Stream, ProtocolType.Tcp);
                listener.Bind(localEndPoint);
                listener.Listen(form_.getConfig().getPorth());
                form_.getGame().connect();
                
            
                while (started) {
                  
                    AcceptCallback(listener.Accept());
                    ////Console.WriteLine("in end of loop.");

                    /*
                    // Set the event to nonsignaled state.
                    //Console.WriteLine("Started Loop.");
                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.
                    //Console.WriteLine("Waiting for a connection...");

                    listener.BeginAccept( 
                        new AsyncCallback(AcceptCallback),Tuple.Create(listener,this));
                    // Wait until a connection is made before continuing.
                    //Console.WriteLine("Waiting on alldone.");
                    allDone.WaitOne();
                     * */
                }
            
            } catch (Exception e) {
                ////Console.WriteLine("Thread aborted");
                return;
            }
            //listener.BeginAccept(new AsyncCallback(EndCallback), listener);
            //listener.Disconnect(true);
            //listener.Shutdown(SocketShutdown.Both);
            //listener.Close();
            ////Console.WriteLine("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        }
        public static void EndCallback(IAsyncResult ar) {
            Socket listener = (Socket)ar.AsyncState;
            listener.EndAccept(ar);
            ////Console.WriteLine("End Accept.");
        }
        public void AcceptCallback(Socket handler)
        {
            playerJoining = true;
            Server server_ = this;
            ////Console.WriteLine("Begin callback");
            // Data buffer for incoming data.
            byte[] bytes = new Byte[1024];
            
            String data = "";
            //game;
            // Get the socket that handles the client request.
            //Tuple<Socket, Server> passed = (Tuple<Socket, Server>)ar.AsyncState;
            //Socket listener = (Socket)passed.Item1;
            //Server server_ = (Server)passed.Item2;
            //Socket handler = listener.EndAccept(ar);
            ////Console.WriteLine("Listener end accept.");
            //players.Add(handler);

            data = receiveMessage(handler);

            String username = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
            data = data.Substring(username.Length + 1);

            String icon = data.Substring(0,data.IndexOf(Constants.DELIM_FIELD));// Remove(0, username.Length);
            data = data.Substring(icon.Length + 1);

            double version = Convert.ToDouble(data);

            //Console.WriteLine("Username: " + username + " Display: " + icon + " Version: " + version);

            /*
             TODO: CHeck for limitations (dup user, too many players, version)
             */
            int nbPlayers = server_.getForm().getGame().getnbPlayers();
            if ( version != Constants.VERSION) {
                playerJoining = false;
                sendSingleMessage(handler, "VERSION" + Constants.DELIM_FIELD + Constants.VERSION);
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
                
                //lockMessage = false;
                //allDone.Set();
                return;
            }
            else if (form_.getGame().isPlaying())
            {
                playerJoining = false;
                sendSingleMessage(handler, "GAME");
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
                
                //lockMessage = false;
                return;
            }
            else if(nbPlayers >= server_.getForm().getConfig().getMaxPlayers()){
                playerJoining = false;
                sendSingleMessage(handler, "MAXPLAYERS");
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
                
                //lockMessage = false;
                //allDone.Set();
                return;
            }
            else if (server_.getForm().getGame().hasUser(username))
            {
                playerJoining = false;
                sendSingleMessage(handler, "USERNAME");
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
                
                //lockMessage = false;
                return;
               
            }
            else if (form_.getGame().getNbActiveWonders() <= form_.getGame().getnbPlayers())
            {
                playerJoining = false;
                sendSingleMessage(handler, "WONDERS");
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
                form_.getPNLServer().receiveSystemMessage("A user failed to join because there were too few wonders to allow another player.");
                
                //lockMessage = false;
                return;
            }
            sendSingleMessage(handler, "CONNECTION");

            ServerPlayerManager serverPlayerManager = new ServerPlayerManager(server_, handler, username);
            serverPlayerManager.playCardReceived += canPassCards;
            serverPlayerManager.cardPassed += cardsPassed;
            server_.getPlayers().Add(serverPlayerManager);

            /*
             TODO: return number of configuration
             */
            char AB = form_.getConfig().getABPlayerChoice()?'1':'0';
            int turn = form_.getConfig().getTurnTime();
            char leaders = form_.getConfig().isLeaders() ? '1' : '0';
            char cities = form_.getConfig().isCities() ? '1' : '0';

            String msg = "" + AB + Constants.DELIM_FIELD + turn + Constants.DELIM_FIELD + leaders + Constants.DELIM_FIELD + cities + Constants.DELIM_FIELD;

            for (int count = 0; count < form_.getConfig().getWonders().Count; count++)
            {
                char status = form_.getConfig().getWonders()[count].isActive()?'1':'0';
                msg += "" + status + Constants.DELIM_FIELD;
            }

            ////Console.WriteLine("Sending Message: " + msg.Replace(Constants.DELIM_FIELD, '#'));
            sendSingleMessage(handler, msg);
            
            /*
             return nb Players + usernames & Display
             */

           
            sendSingleMessage( handler, Convert.ToString(nbPlayers));

            for (int count = 0; count < nbPlayers; count++) {
                Player temp = server_.getForm().getGame().getPlayers()[count];
                String message = server_.getForm().getGame().getPlayers()[count].getUsername() + Constants.DELIM_FIELD + server_.getForm().getGame().getPlayers()[count].getDisplay();
                //Console.WriteLine("Message sent in user info loop: " + message.Replace(Constants.DELIM_FIELD,'#'));
                sendSingleMessage(handler, message);
            }
            //send player status'
            msg = "";
            
            for (int count = 0; count < nbPlayers; count++)
            {
                if (form_.getPNLServer().getPlayerIcons()[count].isReady())
                {
                    msg += "" + '0' + Constants.DELIM_FIELD;
                }
                else if (form_.getPNLServer().getPlayerIcons()[count].isConnected())
                {
                    msg += "" + '1' + Constants.DELIM_FIELD;
                }
                else
                {
                    msg += "" + '2' + Constants.DELIM_FIELD;
                }
            }
            ////Console.WriteLine("Player status': " + msg.Replace(Constants.DELIM_FIELD, '#'));
            sendSingleMessage(handler, msg);
                /*
                 send new user to all
                 */

                playerJoining = false;

            serverPlayerManager.start();
            server_.sendMessage("USER" + Constants.DELIM_FIELD + username + Constants.DELIM_FIELD + icon);
            
            // Signal the main thread to continue.
            //allDone.Set();

        }
        public void sendMessage(String message) {
            while (playerJoining) ;
            byte[] msg = new byte[1024];
            msg = Encoding.ASCII.GetBytes(message.Replace("GAME", "GAME" + Constants.DELIM_FIELD + getForm().getGame().turn) + Constants.DELIM_STREAM);

            try
            {
                foreach (ServerPlayerManager item in players)
                {
                    ////Console.WriteLine("Sending");
                    item.MSGOut.addMessage(message);
                    int bytesSent = item.Send(msg);
                    ////Console.WriteLine("Sent");
                }
            }
            catch (SystemException e)
            {

            }
        }
        public static void sendSingleMessage(Socket handler,String message ){
            byte[] msg = new byte[1024];
            msg = Encoding.ASCII.GetBytes(message + Constants.DELIM_STREAM);
            try
            {
                int bytesSent = handler.Send(msg);
            }
            catch (System.Net.Sockets.SocketException ex)
            {
            }
            catch (ObjectDisposedException ex)
            {

            }
        }
        public static String receiveMessage(Socket handler) {
            
            try
            {
                byte[] bytes = new Byte[1024];
                String data = "";
                while (true)
                {
                    bytes = new byte[1024];
                    int bytesRec = handler.Receive(bytes);
                    data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                    if (data.IndexOf(Constants.DELIM_STREAM) > -1)
                    {
                        break;
                    }
                }

                return data.Remove(data.Length - 1, 1);
            }
            catch (SocketException ex)
            {
                ////Console.WriteLine("Socket Exception
               // //Console.WriteLine("Exception Caught");
            }
            catch (ObjectDisposedException ex) {
               // //Console.WriteLine("Exception Caught");
            }

            return "EXIT" + Constants.DELIM_FIELD;
        }
        public Form1 getForm() {
            return form_;
        }
        public List<ServerPlayerManager> getPlayers() {
            return players;
        }
        private void startGame(object sender, EventArgs e)
        {
            shufflePlayers();
            
            Deck<Wonder> wonders = buildWonderDeck();
            wonders.shuffle();

            Wonder card;
            for (int count = 0; count < players.Count; count++)
            {
                card = wonders.deal();
                sendSingleMessage(players[count].getHandler(), "GAME" + Constants.DELIM_FIELD + form_.getGame().turn + Constants.DELIM_FIELD + "WONDER" + Constants.DELIM_FIELD + card.getName());
                //players[count].Send("WONDER" + card.getName());
            }
                //sendMessage("STARTGAME" + Constants.DELIM_FIELD);

            Thread dealFirstHandThread = new Thread(() => dealFirstHand());
            dealFirstHandThread.Start();
        }
        private Deck<Wonder> buildWonderDeck()
        {
            List<Wonder> temp = new List<Wonder>(form_.getGame().getWonders());

            for (int count = 0; count < temp.Count; count++)
            {
                if (temp[count].isLeaders() && !form_.getGame().isLeaders() || temp[count].isCities() && !form_.getGame().isCities() || !temp[count].isActive())
                {
                    temp.RemoveAt(count);
                    count--;
                }
            }

            return new Deck<Wonder>(temp, deckBuilder.getRandomGenerator());
        }
        private void shufflePlayers()
        {
            List<String> playerNames = new List<string>();

            for (int count = 0; count < players.Count; count++)
            {
                playerNames.Add(players[count].getUsername());
            }

            Deck<String> playerDeck = new Deck<string>(playerNames,deckBuilder.getRandomGenerator());
            playerDeck.shuffle();

            String msg = "GAME" + Constants.DELIM_FIELD + "PLAYERORDER" + Constants.DELIM_FIELD;

            while(!playerDeck.isEmpty())
            {
                msg += playerDeck.deal() + Constants.DELIM_FIELD;
            }

            sendMessage(msg);
        }
        public void dealDeck(Deck<String> deck)
        {
            List<String> hands = new List<string>();

            int handSize = (form_.getGame().isCities() && players.Count < 8 ? 8:7);

            for (int count = 0; count < players.Count; count++)
            {
                hands.Add("GAME" + Constants.DELIM_FIELD + form_.getGame().turn + Constants.DELIM_FIELD + "CARDS" + Constants.DELIM_FIELD + handSize + Constants.DELIM_FIELD);
            }

            while (!deck.isEmpty())
            {
                for (int count = 0; count < players.Count; count++)
                {
                    hands[count] += deck.deal() + Constants.DELIM_FIELD;
                }
            }

            for (int count = 0; count < players.Count; count++)
            {
                sendSingleMessage(players[count].getHandler(), hands[count]);
            }
        }
        public ServerPlayerManager getPlayer(String username)
        {
            ServerPlayerManager ans = null;

            for (int count = 0; count < players.Count; count++)
            {
                if (players[count].getUsername().Equals(username))
                {
                    ans = players[count];
                    break;
                }
            }
            
            return ans;
        }
        private void canPassCards (object sender, EventArgs e)
        {
            
            passCardRequests++;

            if (!form_.getGame().serverConnection.playManager.isInDoublePlays())
            {
                if (passCardRequests == players.Count)
                {
                    passCards.Set();
                }
            }
            else
            {
                if (passCardRequests == form_.getGame().serverConnection.playManager.getDoublePlaysToMake())
                {
                    passCards.Set();
                }
            }
        }
        private void cardsPassed(object sender, EventArgs e)
        {
            passCardRequests--;

            if (passCardRequests == 0)
            {
                passCards.Reset();
            }
        }
        private void dealLeaders()
        {
            Deck<String> leaderDeck = deckBuilder.getLeaderDeck();
            List<String> hands = new List<string>();

            int handSize = (form_.getGame().isCities() && players.Count < 8 ? 8 : 7);

            for (int count = 0; count < players.Count; count++)
            {
                hands.Add("GAME" + Constants.DELIM_FIELD + form_.getGame().turn + Constants.DELIM_FIELD + "CARDS" + Constants.DELIM_FIELD + Constants.LEADERS_TO_DEAL + Constants.DELIM_FIELD);
            }


            for (int leadersDealt = 0; leadersDealt < Constants.LEADERS_TO_DEAL; leadersDealt++ )
            {
                for (int count = 0; count < players.Count; count++)
                {
                    hands[count] += leaderDeck.deal() + Constants.DELIM_FIELD;
                }
            }

            for (int count = 0; count < players.Count; count++)
            {
                sendSingleMessage(players[count].getHandler(), hands[count]);
            }
        }
        private void dealFirstHand()
        {
            deckBuilder.buildDecks();

            while (wondersChosen < form_.getGame().getnbPlayers()) ;

            if (form_.getGame().isLeaders())
            {
                dealLeaders();
            }
            else
            {
                dealDeck(deckBuilder.getAgeIDeck());
            }
        }
    }
}
