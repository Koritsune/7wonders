﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class VictoryPointsPerCardSetEffect : VictoryPointsEffect
    {
        Player player;
        int pointsPerSet = 0;
        public bool[] mask = new bool[Constants.MAX_COLORS];

        public VictoryPointsPerCardSetEffect(Player player_, int pointsPerSet_)
        {
            player = player_;
            pointsPerSet = pointsPerSet_;
        }
        public override int generateVicotryPoints()
        {
            bool firstValueObtained = false;
            int lastMin = 0;

            for (int count = 0; count < Constants.MAX_COLORS; count++)
            {
                if (mask[count])
                {
                    if (!firstValueObtained)
                    {
                        lastMin = player.playedCards[count].Count;
                        firstValueObtained = true;
                    }
                    else
                    {
                        lastMin = Math.Min(lastMin, player.playedCards[count].Count);
                    }
                }
            }

            return lastMin * pointsPerSet;
        }
    }
}
