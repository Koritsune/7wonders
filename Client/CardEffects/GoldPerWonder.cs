﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class GoldPerWonder : CardEffect
    {
        Player target;
        Resources.Resource gold = new Resources.Resource();

        public GoldPerWonder(Player player_, int gold_)
        {
            target = player_;
            gold.gold = gold_;
        }
        public override void play()
        {
            target.addResource(gold * target.getWonder().wonderManager.getNbofWondersBuilt());
            target.raiseGoldReceivedEvent();
        }
    }
}
