﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Client.CardEffects
{
    class LeaderCopyPNL : Panel
    {
        public event LeaderCancelHandler LeaderCancel;

        public delegate void LeaderCancelHandler(object source, LeaderCancelEvent e);
        public class LeaderCancelEvent : EventArgs
        {
            private string EventInfo;
            public LeaderCancelEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public event LeaderPickedHandler LeaderPicked;

        public delegate void LeaderPickedHandler(object source, LeaderPickedEvent e);
        public class LeaderPickedEvent : EventArgs
        {
            private string EventInfo;
            public LeaderPickedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        Player player;
        TranslucentTextBox LBLInfo = new TranslucentTextBox();
        TranslucentTextBox LBLCardInfo = new TranslucentTextBox();

        TextButton TBNext = new TextButton("Next");
        TextButton TBPrevious = new TextButton("Back");

        Timer timer = new Timer();
        Point CursorLocation;
        bool scrollCardInfo = false;

        List<Card> choices = new List<Card>();
        int currentViewingIndex = 0;

        public LeaderCopyPNL(Player player_)
        {
            player = player_;

            player = player_;
            BackColor = Color.Transparent;
            BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);

            LBLCardInfo.BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
            LBLCardInfo.getTextBox().ForeColor = Color.White;
            LBLCardInfo.getTextBox().ReadOnly = true;
            LBLCardInfo.setFontSize(12);
            LBLCardInfo.setSize(360, 80);
            LBLCardInfo.Location = new Point(0, 80);
            LBLCardInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            LBLCardInfo.getTextBox().ScrollBars = RichTextBoxScrollBars.ForcedVertical;
            //LBLCardInfo.getTextBox().Text = "Select the leader you wish to sacrifice. You will no longer receive the chosen leaders effect but you will receive victory points equal to double its cost.";

            LBLInfo.BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
            LBLInfo.getTextBox().ForeColor = Color.White;
            LBLInfo.getTextBox().ReadOnly = true;
            LBLInfo.setFontSize(12);
            LBLInfo.setSize(360, 80);
            LBLInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            LBLInfo.getTextBox().Text = "Select which of the following cards you wish to build.";

            Controls.Add(LBLCardInfo);
            Controls.Add(LBLInfo);

            TBNext.setFontSize(12);
            TBNext.startHover();
            TBNext.setSize(100, 30);
            TBNext.Location = new Point(260, 304); 
            TBNext.onTextButtonClickEvent += showNextCards;
            
            TBPrevious.setFontSize(12);
            TBPrevious.startHover();
            TBPrevious.setSize(100, 30);
            TBPrevious.Location = new Point(0, 304);
            TBPrevious.onTextButtonClickEvent += showPreviousCards;

            Controls.Add(TBNext);
            Controls.Add(TBPrevious);

            //player_.cardPlayedHandler[(int)Card.CardColor.Roster] += rosterUpdate;
            //getChoices();

            timer.Tick += scrollCard;
            timer.Start();
        }

        private void scrollCard(object sender, EventArgs e)
        {
            if (!scrollCardInfo)
            {
                Point temp = player.form_.DesktopLocation;
                temp.X += Location.X + LBLCardInfo.Location.X + LBLCardInfo.Size.Width - 4;
                temp.Y += Location.Y + LBLCardInfo.Location.Y + LBLCardInfo.Size.Height - 4;
                CursorLocation = Cursor.Position;
                Cursor.Position = temp;
                scrollCardInfo = true;
            }
            else
            {
                Cursor.Position = CursorLocation;
                scrollCardInfo = false;
                timer.Stop();
            }
        }

        private void cardClick(object source, EventArgs e)
        {
            Card cardClicked = (Card)source;

            LBLCardInfo.getTextBox().Text = cardClicked.getDesc();

            player.raiseCardSelected(cardClicked);
        }

        private void cardSelected(object source, EventArgs e)
        {
            Card card = (Card)source;
            player.raiseCardSelected(card);

            if (LeaderPicked != null)
            {
                LeaderPicked(source, new LeaderPickedEvent("Leader Hire Picked."));
            }
        }

        private void showNextCards(object source, EventArgs e)
        {
            if (currentViewingIndex == choices.Count / 4)
            {
                return;
            }
            else
            {
                for (int count = currentViewingIndex * 4; count < (currentViewingIndex + 1) * 4 && count < choices.Count; count++)
                {
                    choices[count].Visible = false;
                }

                currentViewingIndex++;

                for (int count = currentViewingIndex * 4; count < (currentViewingIndex + 1) * 4 && count < choices.Count; count++)
                {
                    choices[count].Visible = true;
                }

                LBLCardInfo.getTextBox().Text = choices[currentViewingIndex * 4].getDesc();
            }
        }

        private void showPreviousCards(object source, EventArgs e)
        {
            if (currentViewingIndex == 0)
            {
                return;
            }
            else
            {
                for (int count = currentViewingIndex * 4; count < (currentViewingIndex + 1) * 4 && count< choices.Count; count++)
                {
                    choices[count].Visible = false;
                }

                currentViewingIndex--;

                for (int count = currentViewingIndex * 4; count < (currentViewingIndex + 1) * 4 && count < choices.Count; count++)
                {
                    choices[count].Visible = true;
                }

                LBLCardInfo.getTextBox().Text = choices[currentViewingIndex * 4].getDesc();
            }
        }

        private void rosterUpdate(Card card)
        {
            updateCards();
        }

        private delegate void updateCardsD();

        private void updateCards()
        {
            if (this.InvokeRequired)
            {
                updateCardsD d = new updateCardsD(updateCards);
                this.Invoke(d, new object[] { });
            }
            else
            {
                clearChoices();
                getChoices();
            }
        }

        private void clearChoices()
        {
            foreach (Card item in choices)
            {
                Controls.Remove(item);
            }
            choices.Clear();
            currentViewingIndex = 0;

            LBLCardInfo.getTextBox().Text = "";
        }

        public void getChoices()
        {
            //bool canHireLeader = false;
            int totalLeaders = 0;

            for (int count = 0; count < player.leftNeighbor.playedCards[(int)Card.CardColor.White].Count; count++,totalLeaders++)
            {

                Card cardChoice = new Card(player.leftNeighbor.playedCards[(int)Card.CardColor.White][count].cardLocation, player);

                    cardChoice.hideButtons();
                    cardChoice.setSize(80, 144);

                    cardChoice.Location = new Point(totalLeaders % 4 * 80, 160);
                    choices.Add(cardChoice);

                    cardChoice.CardClick += cardClick;
                    cardChoice.showPictureOnly();
                    cardChoice.CardDoubleClick += cardSelected;

                    Controls.Add(cardChoice);
                
            }

            for (int count = 0; count < player.rightNeighbor.playedCards[(int)Card.CardColor.White].Count; count++,totalLeaders++)
            {

                Card cardChoice = new Card(player.rightNeighbor.playedCards[(int)Card.CardColor.White][count].cardLocation, player);

                cardChoice.hideButtons();
                cardChoice.setSize(80, 144);

                cardChoice.Location = new Point(totalLeaders % 4 * 80, 160);
                choices.Add(cardChoice);

                cardChoice.CardClick += cardClick;
                cardChoice.showPictureOnly();
                cardChoice.CardDoubleClick += cardSelected;

                Controls.Add(cardChoice);

            }

            

            if (totalLeaders <= 0)
            {
                if (LeaderCancel != null)
                {
                    LeaderCancel(this, new LeaderCancelEvent("Leader Hire Canceled."));
                }
            }
            else
            {
                LBLCardInfo.getTextBox().Text = choices[0].getDesc();
            }
        }
    }
}
