﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class ChainBuildGold : CardEffect
    {
        Player player;
        Resources.Resource gold = new Resources.Resource();

        bool used = false;

        public ChainBuildGold(Player player_, int gold_)
        {
            player = player_;
            gold.gold = gold_;
        }
        private void chainBuildDetected(object source, EventArgs e)
        {
            if (active && !used)
            {
                used = true;
                player.addResource(gold);
                player.raiseGoldReceivedEvent();
            }
        }
        public override void play()
        {
                player.ChainBuild += chainBuildDetected;
                player.form_.getGame().serverConnection.playManager.NewTurn += resetUse;
        }
        private void resetUse(object source, EventArgs e)
        {
            used = false;
        }
    }
}
