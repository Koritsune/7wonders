﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    public class TradeEffect : CardEffect
    {
        
        public enum TradeType{
            Brown,
            Gray,
            All
        }

        Player player;
        String direction;
        int rate;
        TradeType type;
        bool singleUse;
        bool used = false;
        Resources.Trade trade_;

        static Resources.Resource loom = new Resources.Resource();
        static Resources.Resource glass = new Resources.Resource();
        static Resources.Resource papyrus = new Resources.Resource();

        static Resources.Resource wood = new Resources.Resource();
        static Resources.Resource ore = new Resources.Resource();
        static Resources.Resource stone = new Resources.Resource();
        static Resources.Resource clay = new Resources.Resource();

        public TradeEffect(Player player_, String direction_, int rate_, TradeType type_, bool singleUse_ = false)
        {
            player = player_;
            direction = direction_;
            rate = rate_;
            type = type_;
            singleUse = singleUse_;

            loom.loom = 1;
            glass.glassworks = 1;
            papyrus.papyrus = 1;

            wood.wood = 1;
            clay.clay = 1;
            stone.silver = 1;
            ore.ore = 1;
        }
        public override void play()
        {
            if (rate < 0)
            {
                player.tradeManager.tradeEffects.Add(this);

                player.PNLCost.cardPurchase += resetUse;
                player.PNLCost.cardCancel += resetUse;
            }
            else
            {
                player.tradeManager.tradeEffects.Insert(0, this);
            }
        }
        public void applyRebate(Resources.Trade trade)
        {
            if (canApply(trade))
            {
                if (rate > 0)
                {
                    trade_ = trade;
                    trade.goldCost = rate;
                }
                else
                {
                    trade_ = trade;
                    trade.goldCost += rate;
                }

                if (singleUse)
                {
                    used = true;
                    trade.TradeComplete += resetUse;
                    trade.TradeCancel += checkOtherCompatible;
                }
            }            
        }

        private bool canApply(Resources.Trade trade)
        {
            if ((trade.owner == player.leftNeighbor && direction.Equals("West")) || (trade.owner == player.rightNeighbor && direction.Equals("East")))
            {
                if (!singleUse || (singleUse && !used))
                {
                    if (type == TradeType.Brown)
                    {
                        if (trade.resourceButton.generates == clay || trade.resourceButton.generates == ore || trade.resourceButton.generates == wood || trade.resourceButton.generates == stone)
                        {
                            return true;
                        }

                        return false;
                    }
                    else if (type == TradeType.Gray)
                    {
                        if (trade.resourceButton.generates == glass || trade.resourceButton.generates == loom || trade.resourceButton.generates == papyrus)
                        {
                            return true;
                        }
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void resetUse(object source, EventArgs e)
        {
            //Console.WriteLine("Used changed to false.");
            used = false;
        }

        private void checkOtherCompatible(object source, EventArgs e)
        {
            used = false;

            foreach (Resources.Trade item in player.tradeManager.trades)
            {
                if (canApply(item) && trade_ != item)
                {
                    Resources.Resource goldAdjustment = new Resources.Resource();

                    if (rate > 0)
                    {
                        int adjustment = item.goldCost - rate;
                        goldAdjustment.gold = adjustment;

                        player.addResource(goldAdjustment);
                        item.owner.addResource(goldAdjustment * -1);

                        item.goldCost = rate;
                    }
                    else
                    {
                        int adjustment = rate * -1;
                        goldAdjustment.gold = adjustment;

                        player.addResource(goldAdjustment);
                        item.owner.addResource(goldAdjustment * -1);

                        item.goldCost += rate;
                    }

                    item.TradeCancel += checkOtherCompatible;
                    item.TradeComplete += resetUse;
                    used = true;
                    break;
                }
            }
        }
    }
}
