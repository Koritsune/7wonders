﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class LeaderDrawEffect : CardEffect
    {
        int leadersToDraw;
        Player player;
        WonderStage wonderStage;

        public LeaderDrawEffect(Player player_, int leadersToDraw_, WonderStage wonderStage_)
        {
            player = player_;
            leadersToDraw = leadersToDraw_;
            wonderStage = wonderStage_;
        }

        public override void play()
        {
            if (player != player.form_.getGame().getPlayers()[0])
            {
                for (int count = 0; count < leadersToDraw; count++)
                {
                    Card leaderBack = new Card(Constants.PICTURES + "Leaders\\Back", player);
                    leaderBack.color = Card.CardColor.Roster;
                    player.playCard(leaderBack);
                }

                wonderStage.raiseWonderPlayedEvent();
            }
            else
            {
                player.form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "LEADERDRAW" + Constants.DELIM_FIELD + leadersToDraw);
            }
        }
    }
}
