﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class GoldGenerationOnIncome : CardEffect
    {
        Player player;
        int gain;
        bool used = false;

        public GoldGenerationOnIncome(Player player_, int gain_)
        {
            player = player_;
            gain = gain_;
        }
        public override void play()
        {
            player.form_.getGame().serverConnection.playManager.NewTurn += resetUsed;
            player.GoldReceived += income;
        }
        private void resetUsed(object source, EventArgs e)
        {
            used = false;
        }
        private void income(object source, EventArgs e)
        {
            if (active)
            {
                if (!used)
                {
                    used = true;

                    Resources.Resource goldToAdd = new Resources.Resource();
                    goldToAdd.gold = gain;

                    player.addResource(goldToAdd);
                }
            }
        }
    }
}
