﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class ResourceOptionAllAtCost : CardEffect
    {
        ResourceOption resourceOption;
        Card card;
        Player player;
        List<Resources.ResourceToggle> resourceToggleList;
        Resources.ResourceToggle lastToggle;
        
        Resources.Resource goldCost = new Resources.Resource();
        Resources.Resource givenResource = new Resources.Resource();
        bool used = false;

        public ResourceOptionAllAtCost(Player player_, Card card_)
        {
            player = player_;
            card = card_;
            goldCost.gold = -1;
        }
        public override void play()
        {
            Resources.Resource resourceMask = new Resources.Resource();

            for (int count = 1; count < 8; count++)
            {
                resourceMask[count] = 1;
            }

            resourceOption = new ResourceOption(player,resourceMask,card,player.form_.getGame(),true);
            resourceOption.play();

            resourceToggleList = resourceOption.getToggleButtons();

            foreach (Resources.ResourceToggle item in resourceToggleList)
            {
                item.ButtonClick += player.tradeManager.trade;
                item.ButtonClick += toggleClick;
                item.ValueChange += toggleValueChange;
                item.active = true;
            }

            player.PNLCost.cardPurchase += completeTransaction;
            player.PNLCost.cardCancel += cancelTransaction;
        }
        private void toggleClick(object source, EventArgs e)
        {
            Resources.ResourceToggle toggle = (Resources.ResourceToggle)source;
            bool status = toggle.active;

            if (lastToggle != null)
            {
                if (lastToggle != toggle)
                {
                    used = !status;
                }
            }
            else
            {
                used = true;
            }

            if (!status)
            {
                lastToggle = toggle;
            }
            else
            {
                lastToggle = null;
            }
        }
        private void toggleValueChange(object source, EventArgs e)
        {
            Resources.ResourceToggle toggle = (Resources.ResourceToggle)source;
            bool status = toggle.active;

            if (lastToggle == toggle)
            {
                foreach (Resources.ResourceToggle item in resourceToggleList)
                {
                    if (item != toggle)
                    {
                        if (item.active)
                        {
                            player.form_.getGame().getPlayers()[0].tradeManager.removeTrade(item);
                        }
                        item.setActive(status);
                    }


                }
            }
        }
        private void completeTransaction(object sender, EventArgs e)
        {
            if (used)
            {
                player.form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "COIN" + Constants.DELIM_FIELD + player.getUsername() + Constants.DELIM_FIELD + goldCost.gold);

                foreach (Resources.ResourceToggle item in resourceToggleList)
                {
                    item.setActive(true);
                }

                //player.addResource(givenResource * -1);
                //player.addResource(goldCost * -1);
                used = false;

                lastToggle = null;
            }
        }

        private void cancelTransaction(object sender, EventArgs e)
        {
            if (used)
            {
                used = false;
                //player.addResource(goldCost * -1);
                //player.addResource(givenResource * -1);
                foreach (Resources.ResourceToggle item in resourceToggleList)
                {
                    item.setActive(true);
                }

                lastToggle = null;
            }
        }

    }
}
