﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class VictoryPointsPerStoneEffect : VictoryPointsEffect
    {
        int pointsPerStone;
        Player player;

        public VictoryPointsPerStoneEffect(Player player_, int point)
        {
            pointsPerStone = point;
            player = player_;
        }
        public override int generateVicotryPoints()
        {
            Resources.Resource totalResources = new Resources.Resource();

            foreach (Card item in player.playedCards[(int)Card.CardColor.Brown])
            {
                try
                {
                    ResourceGenerationFixed resourceGeneration = (ResourceGenerationFixed)item.immediateEffects[0];
                    totalResources += resourceGeneration.resource;

                }
#pragma warning disable 0168
                catch (InvalidCastException e)
                {
#pragma warning restore 0168
                    ResourceOption resourceOption = (ResourceOption)item.immediateEffects[0];
                    totalResources += resourceOption.resource;
                }
            }

            return totalResources.silver * pointsPerStone;
        }
    }
}
