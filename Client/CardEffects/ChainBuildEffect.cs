﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client.CardEffects
{
    class ChainBuildEffect : CardEffect
    {
        String name;
        Player player;
        List<Card> madeFree = new List<Card>();
        List<Panel> madeFreeIcons = new List<Panel>();

        bool currentCard = false;

        public ChainBuildEffect(Player player_, String name_)
        {
            player = player_;
            name = name_;
        }
        public override void play()
        {
            player.CardSelected += makeFree;
            player.PNLCost.cardPurchase += checkIfBuilt;
            player.PNLCost.cardCancel += cancelBuild;
            player.form_.getGame().serverConnection.playManager.NewTurn += showChainIcon;
        }
        private void makeFree(object source, EventArgs e)
        {
            Card card = (Card)source;

            if (card.name.Equals(name))
            {
                card.cost = new Resources.Resource();
                player.PNLCost.update();
                currentCard = true;
            }
            else
            {
                currentCard = false;
            }
        }
        private void checkIfBuilt(object source, EventArgs e)
        {
            if (currentCard)
            {
                player.form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "CHAIN" + Constants.DELIM_FIELD);
            }
        }
        private void cancelBuild(object source, EventArgs e)
        {
            currentCard = false;
        }
        private delegate void showChainIconD();
        private void showChainIcon()
        {
            
            if (player == player.form_.getGame().getPlayers()[0])
            {
                if (player.InvokeRequired)
                {
                    showChainIconD d = new showChainIconD(showChainIcon);
                    player.Invoke(d, new object[] { });
                }
                else
                {
                    foreach (Card card in player.getHand())
                    {
                        if (name.Equals(card.name))
                        {
                            Panel pnlChainIcon = new Panel();
                            pnlChainIcon.Size = new Size(card.Size.Width / 10 * 6, card.Size.Height / 10 * 3);
                            pnlChainIcon.Location = new Point(card.Size.Width / 10 * 2, card.Size.Height / 20 * 7);
                            pnlChainIcon.BackgroundImage = Image.FromFile(Constants.ICONS + "ChainBuild.PNG");
                            pnlChainIcon.BackgroundImageLayout = ImageLayout.Stretch;
                            pnlChainIcon.BackColor = Color.Transparent;
                            pnlChainIcon.MouseClick += raiseCardClick;

                            card.getPicture().Controls.Add(pnlChainIcon);
                            pnlChainIcon.BringToFront();

                            madeFree.Add(card);
                            madeFreeIcons.Add(pnlChainIcon);

                            card.Resize += resize;
                        }
                    }
                }
            }
        }
        public void showChainIcon(object source, EventArgs e)
        {
            showChainIcon();
        }
        private void resize(object source, EventArgs e)
        {
            for (int count = 0; count < madeFree.Count; count++)
            {
                madeFreeIcons[count].Size = new Size(madeFree[count].Size.Width / 10 * 6, madeFree[count].Size.Height / 10 * 3);
                madeFreeIcons[count].Location = new Point(madeFree[count].Size.Width / 10 * 2, madeFree[count].Size.Height / 20 * 7);
            }
        }
        private void raiseCardClick(object source, EventArgs e)
        {
            Card card = (Card)source;

            card.raiseCardClick();
        }
    }
}
