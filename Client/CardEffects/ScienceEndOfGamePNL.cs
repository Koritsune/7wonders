﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client.CardEffects
{
    class ScienceEndOfGamePNL : Panel
    {
        Player player;

        int scienceChoices;
        int nbToCopy;

        int maxCompass;
        int maxTablet;
        int maxGear;

        int totalMax;

        bool validOptions = true;
        //ToggleGroupBox TGBGear;
        //ToggleGroupBox TGBTablet;
        //ToggleGroupBox TGBCompass;

        TransparentLabel LBLInfo = new TransparentLabel();

        TextButton TBOK = new TextButton("OK");
        TranslucentTextBox LBLError = new TranslucentTextBox();

        CardEffects.ScienceEndOfGameIndividualPNL gearPNL;
        CardEffects.ScienceEndOfGameIndividualPNL tabletPNL;
        CardEffects.ScienceEndOfGameIndividualPNL compassPNL;
        
        public ScienceEndOfGamePNL(Player player_)
        {
            player = player_;

            BackColor = Color.Transparent;

            scienceChoices = player.scienceChoice;
            nbToCopy = player.scienceCopy;

            totalMax = scienceChoices + nbToCopy;

            int maxGearCopy = Math.Min(nbToCopy, player.leftNeighbor.SCIENCE[0] + player.rightNeighbor.SCIENCE[0]);
            maxGear = maxGearCopy + scienceChoices;

            int maxTabletCopy = Math.Min(nbToCopy, player.leftNeighbor.SCIENCE[1] + player.rightNeighbor.SCIENCE[1]);
            maxTablet = maxTabletCopy + scienceChoices;

            int maxCompassCopy = Math.Min(nbToCopy, player.leftNeighbor.SCIENCE[2] + player.rightNeighbor.SCIENCE[2]);
            maxCompass = maxCompassCopy + scienceChoices;


            LBLInfo.setText("Select the number of each science you wish to have up to a total maximum of: " + totalMax);
            LBLInfo.setSize(370, 40);
            LBLInfo.setFontSize(10);
            LBLInfo.BackColor = Color.Transparent;
            Controls.Add(LBLInfo);

            gearPNL = new ScienceEndOfGameIndividualPNL(player, 0, maxGear);
            gearPNL.Size = new Size(370, 90);
            gearPNL.Location = new Point(0, 40);
            Controls.Add(gearPNL);

            tabletPNL = new ScienceEndOfGameIndividualPNL(player, 1, maxTablet);
            tabletPNL.Size = new Size(370, 90);
            tabletPNL.Location = new Point(0, 130);
            Controls.Add(tabletPNL);

            compassPNL = new ScienceEndOfGameIndividualPNL(player, 2, maxCompass);
            compassPNL.Size = new Size(370, 90);
            compassPNL.Location = new Point(0, 220);
            Controls.Add(compassPNL);

            LBLError.setFontSize(10);
            LBLError.AppendText("The total number of Sciences selected exceed the number allowed.", Color.Red);
            LBLError.setSize(270, 40);
            LBLError.Location = new Point(0, 310);
            LBLError.Visible = false;
            Controls.Add(LBLError);

            TBOK.onTextButtonClickEvent += sendOptions;
            TBOK.setSize(100, 30);
            TBOK.Location = new Point(270, 315);
            TBOK.startHover();
            Controls.Add(TBOK);
            //maxGear = scienceChoices + 

            gearPNL.ScienceChoiceChanged += updateOptionsValid;
            tabletPNL.ScienceChoiceChanged += updateOptionsValid;
            compassPNL.ScienceChoiceChanged += updateOptionsValid;
        }
        private void updateOptionsValid(object source, EventArgs e)
        {
            int totalPicked = 0;

            totalPicked += gearPNL.getNbScienceSelected();
            totalPicked += tabletPNL.getNbScienceSelected();
            totalPicked += compassPNL.getNbScienceSelected();

            if (totalPicked > totalMax)
            {
                LBLError.Visible = true;
                validOptions = false;
            }
            else
            {
                LBLError.Visible = false;
                validOptions = true;
            }
        }
        private void sendOptions(object source, EventArgs e)
        {
            sendOptions();
        }
        private void sendOptions()
        {
            if (validOptions)
            {
                player.form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "SCIENCECHOICE" + Constants.DELIM_FIELD + player.getUsername() + Constants.DELIM_FIELD + gearPNL.getNbScienceSelected() + Constants.DELIM_FIELD + tabletPNL.getNbScienceSelected() + Constants.DELIM_FIELD + compassPNL.getNbScienceSelected());
                player.form_.getPNLServer().getPNLGame().Controls.Remove(this);
            }
        }
    }
}
