﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class VictoryPointsFixedEffect : VictoryPointsEffect
    {
        int victoryPoints = 0;

        public VictoryPointsFixedEffect(int points)
        {
            victoryPoints = points;
        }

        public override int generateVicotryPoints()
        {
            return victoryPoints;
        }
    }
}
