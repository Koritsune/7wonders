﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class DebtEffectPerWonder : DebtEffect
    {
        //int goldPerWonder;
        //Player player;

        public DebtEffectPerWonder(Player player_, int gold_) : base(player_.form_,player_)
        {
            debtAmount = form.getGame().getPlayers()[0].getWonder().wonderManager.getNbofWondersBuilt() * gold_;
            buildDebtPanel();
        }
        private delegate void buildDebtPanelD();

        private void buildDebtPanel()
        {
            if (form.InvokeRequired)
            {
                buildDebtPanelD d = new buildDebtPanelD(buildDebtPanel);
                form.Invoke(d, new object[] { });
            }
            else
            {
                PNLDebt = new DebtEffectPNL(debtAmount);
            }
        }
    }
}
