﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class ReflectLossEffect : CardEffect
    {
        Player player;

        public ReflectLossEffect(Player player_)
        {
            player = player_;
        }
        public override void play()
        {
            player.reflectLoss = true;
        }
        public override void cancelAbilities()
        {
            player.reflectLoss = false;
        }
    }
}
