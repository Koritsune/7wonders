﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class IncomeOnTradeEffect: CardEffect
    {
        Player benefactor;
        Player target;
        Resources.Resource goldToGain = new Resources.Resource();

        bool used = false;

        public IncomeOnTradeEffect(Player benefactor_, Player target_, int gold_)
        {
            benefactor = benefactor_;
            target = target_;
            goldToGain.gold = gold_;
        }
        private void resetUse(object source, EventArgs e)
        {
            used = false;
        }
        private void tradeMade(object source, Player.TradeMadeEvent e)
        {
            if (active)
            {
                if (!used)
                {
                    if (e.GetTradePartner() == target)
                    {
                        benefactor.addResource(goldToGain);
                        benefactor.raiseGoldReceivedEvent();
                        used = true;
                    }
                }
            }
        }
        public override void play()
        {
            benefactor.TradeMade += tradeMade;
            benefactor.form_.getGame().serverConnection.playManager.NewTurn += resetUse;
        }
    }
}
