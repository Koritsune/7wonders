﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class ArmyIncrease : CardEffect
    {
        int armyIncrease = 0;
        Player player;

        public ArmyIncrease(Player player_, int increase)
        {
            player = player_;
            armyIncrease = increase;
        }
        public override void play()
        {
            player.increaseArmy(armyIncrease);
        }
        public override void cancelAbilities()
        {
            player.increaseArmy(-1 * armyIncrease);
        }
    }
}
