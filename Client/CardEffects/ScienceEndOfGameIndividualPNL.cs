﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client.CardEffects
{
    class ScienceEndOfGameIndividualPNL : Panel
    {
        int type;
        int nb;

        Panel PNLSciencePicture = new Panel();
        //TransparentLabel LBLInfo = new TransparentLabel();
        ToggleGroupBox TGBChoices = new ToggleGroupBox();

        public event ScienceChoiceChangedHandler ScienceChoiceChanged;
        public delegate void ScienceChoiceChangedHandler(object source, ScienceChoiceChangedEvent e);

        public class ScienceChoiceChangedEvent : EventArgs
        {
            private string EventInfo;
            public ScienceChoiceChangedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public ScienceEndOfGameIndividualPNL(Player player, int type_, int nb_)
        {
            BackColor = Color.Transparent;

            type = type_;
            nb = nb_;

            Controls.Add(PNLSciencePicture);
            PNLSciencePicture.Size = new Size(90, 90);
            PNLSciencePicture.BackgroundImageLayout = ImageLayout.Stretch;
            PNLSciencePicture.BackColor = Color.Transparent;

            if (type == 0)
            {
                PNLSciencePicture.BackgroundImage = Image.FromFile(Constants.ICONS + "Gear.PNG");
            }
            else if (type == 1)
            {
                PNLSciencePicture.BackgroundImage = Image.FromFile(Constants.ICONS + "Tablet.PNG");
            }
            else
            {
                PNLSciencePicture.BackgroundImage = Image.FromFile(Constants.ICONS + "Compass.PNG");
            }

            Controls.Add(TGBChoices);
            TGBChoices.Size = new Size(280, 90);
            TGBChoices.Location = new Point(90, 0);
            TGBChoices.Text = (type == 0 ? "Gear" : type == 1 ? "Tablet" : "Geometry") + " (Currently owned: " + player.SCIENCE[type] + ")";
            TGBChoices.setButtonSize(40, 30);

            TGBChoices.BackColor = Color.Transparent;

            for (int count = 0; count <= nb; count++)
            {
                TGBChoices.addButton("" + count);
            }

            TGBChoices.selectButton(0);
            TGBChoices.valueChange += scienceChoiceChanged;
        }
        private void scienceChoiceChanged(object source, EventArgs e)
        {
            if (ScienceChoiceChanged != null)
            {
                ScienceChoiceChanged(this, new ScienceChoiceChangedEvent("Science Choice Changed."));
            }
        }
        public int getNbScienceSelected()
        {
            return TGBChoices.getSlectedIndex();
        }
    }
}
