﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class CardDiscountEffect : CardEffect
    {
        Player player;
        Card.CardColor color;
        bool used = false;

        public CardDiscountEffect(Player player_, Card.CardColor color_)
        {
            player = player_;
            color = color_;
        }

        public override void play()
        {
            player.CardSelected += applyCardDiscount;
            player.PNLCost.cardPurchase += removeCardDiscount;
            player.PNLCost.cardCancel += removeCardDiscount;
        }

        private void applyCardDiscount(object source, EventArgs e)
        {
            //Console.WriteLine("In applycardDiscount");

            Card card = (Card)source;

            if (card.getCardColor() == color && !used)
            {
                //Console.WriteLine("Viarnace increased.");
                used = true;
                player.PNLCost.increaseVariance();
                player.PNLCost.update();
            }
        }

        private void removeCardDiscount(object source, EventArgs e)
        {
            if (used)
            {
                //Console.WriteLine("Variance decreased");
                used = false;
                player.PNLCost.decreaseVariance();
            }
        }
    }
}
