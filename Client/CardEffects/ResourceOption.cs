﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Client.CardEffects
{
    class ResourceOption : CardEffect
    {
        Player player;
        public Resources.Resource resource {get; private set;}
        Resources.Resource currentResource = new Resources.Resource();
        Card card;
        Game game;

        bool externalClickHandling = false;

        Panel PNLWonderPlaceHolder;

        double sizeFactorLarge = 0.32;
        double sizeFactor = 0.24;
        double sizeFactorSmall = 0.16;

        static double[] pointFactor_1 = { 0.34, 0.03 };
        static double[] pointFactor_2 = { 0.2, .03, 0.5, .03 };
        static double[] pointFactor_3 = { 0.08, .03, 0.38, .03, 0.68, .03 };
        static double[] pointFactor_4 = { 0.0, .03, 0.25, .03, 0.5, .03, .75, .03 };
        static double[] pointFactor_5 = { 0.08, .03, 0.38, .03, 0.68, .03, 0.2, .2, 0.5, .2 };
        static double[] pointFactor_6 = { 0.08, .03, 0.38, .03, 0.68, .03, 0.2, .2, 0.5, .2, 0.8, .2 };
        static double[] pointFactor_7 = { 0.0, .03, 0.25, .03, 0.5, .03, .75, .03, 0.08, .2, 0.38, .2, 0.68, .2 };

        List<Resources.ResourceToggle> resourceToggleList = new List<Resources.ResourceToggle>();

        public event ResourceGenerationChangedEventHandler ResourceGenerationChanged;
        public delegate void ResourceGenerationChangedEventHandler(object source, ResourceGenerationChangedEvent e);

        public class ResourceGenerationChangedEvent : EventArgs
        {
            private string EventInfo;
            public ResourceGenerationChangedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public ResourceOption(Player player_, Resources.Resource resource_, Card card_, Game game_, bool externalClickHandling_ = false)
        {
            player = player_;
            resource = resource_;
            card = card_;
            game = game_;
            externalClickHandling = externalClickHandling_;
        }

        public ResourceOption(Player player_, Resources.Resource resource_, Panel wonderStage_, Game game_)
        {
            player = player_;
            resource = resource_;
            PNLWonderPlaceHolder = wonderStage_;
            game = game_;
        }

        delegate void playD();
            
        public override void play()
        {
            bool invoke = false;

            if (card != null)
            {
                invoke = card.InvokeRequired;
            }
            else
            {
                invoke = PNLWonderPlaceHolder.InvokeRequired;
            }

            if (invoke)
            {
                playD d = new playD(play);

                if (card != null)
                {
                    card.Invoke(d, new object[] { });
                }
                else
                {
                    PNLWonderPlaceHolder.Invoke(d, new object[] { });
                }
            }
            else
            {
                if (card != null)
                {
                    playFromCard();
                }
                else
                {
                    playFromWonder();
                }
            
            }
        }
        private void resizeResourceButtons(object source, EventArgs e)
        {
            resizeResourceButtons();
        }
        private void resizeResourceButtons()
        {
            Size buttonSize;

            if (resourceToggleList.Count <= 2)
            {
                buttonSize = new Size(Convert.ToInt32(card.Width * sizeFactorLarge), Convert.ToInt32(card.Width * sizeFactorLarge));
            }
            if (resourceToggleList.Count <= 4){
                buttonSize = new Size(Convert.ToInt32(card.Width * sizeFactor), Convert.ToInt32(card.Width * sizeFactor));
            }
            else
            {
                buttonSize = new Size(Convert.ToInt32(card.Width * sizeFactorSmall), Convert.ToInt32(card.Width * sizeFactorSmall));
            }

            if (resourceToggleList.Count == 1)
            {
                resourceToggleList[0].Location = new Point(Convert.ToInt32(card.Width * pointFactor_1[0]), Convert.ToInt32(card.Height * pointFactor_1[1]));
                resourceToggleList[0].Size = buttonSize;
            }
            if (resourceToggleList.Count == 2)
            {
                resourceToggleList[0].Location = new Point(Convert.ToInt32(card.Width * pointFactor_2[0]), Convert.ToInt32(card.Height * pointFactor_2[1]));
                resourceToggleList[0].Size = buttonSize;

                resourceToggleList[1].Location = new Point(Convert.ToInt32(card.Width * pointFactor_2[2]), Convert.ToInt32(card.Height * pointFactor_2[3]));
                resourceToggleList[1].Size = buttonSize;
            }
            else if (resourceToggleList.Count == 3)
            {
                resourceToggleList[0].Location = new Point(Convert.ToInt32(card.Width * pointFactor_3[0]), Convert.ToInt32(card.Height * pointFactor_3[1]));
                resourceToggleList[0].Size = buttonSize;

                resourceToggleList[1].Location = new Point(Convert.ToInt32(card.Width * pointFactor_3[2]), Convert.ToInt32(card.Height * pointFactor_3[3]));
                resourceToggleList[1].Size = buttonSize;

                resourceToggleList[2].Location = new Point(Convert.ToInt32(card.Width * pointFactor_3[4]), Convert.ToInt32(card.Height * pointFactor_3[5]));
                resourceToggleList[2].Size = buttonSize;
            }
            else if (resourceToggleList.Count == 4)
            {
                resourceToggleList[0].Location = new Point(Convert.ToInt32(card.Width * pointFactor_4[0]), Convert.ToInt32(card.Height * pointFactor_4[1]));
                resourceToggleList[0].Size = buttonSize;

                resourceToggleList[1].Location = new Point(Convert.ToInt32(card.Width * pointFactor_4[2]), Convert.ToInt32(card.Height * pointFactor_4[3]));
                resourceToggleList[1].Size = buttonSize;

                resourceToggleList[2].Location = new Point(Convert.ToInt32(card.Width * pointFactor_4[4]), Convert.ToInt32(card.Height * pointFactor_4[5]));
                resourceToggleList[2].Size = buttonSize;

                resourceToggleList[3].Location = new Point(Convert.ToInt32(card.Width * pointFactor_4[6]), Convert.ToInt32(card.Height * pointFactor_4[7]));
                resourceToggleList[3].Size = buttonSize;
            }
            else if (resourceToggleList.Count == 5)
            {
                resourceToggleList[0].Location = new Point(Convert.ToInt32(card.Width * pointFactor_5[0]), Convert.ToInt32(card.Height * pointFactor_5[1]));
                resourceToggleList[0].Size = buttonSize;

                resourceToggleList[1].Location = new Point(Convert.ToInt32(card.Width * pointFactor_5[2]), Convert.ToInt32(card.Height * pointFactor_5[3]));
                resourceToggleList[1].Size = buttonSize;

                resourceToggleList[2].Location = new Point(Convert.ToInt32(card.Width * pointFactor_5[4]), Convert.ToInt32(card.Height * pointFactor_5[5]));
                resourceToggleList[2].Size = buttonSize;

                resourceToggleList[3].Location = new Point(Convert.ToInt32(card.Width * pointFactor_5[6]), Convert.ToInt32(card.Height * pointFactor_5[7]));
                resourceToggleList[3].Size = buttonSize;

                resourceToggleList[4].Location = new Point(Convert.ToInt32(card.Width * pointFactor_5[8]), Convert.ToInt32(card.Height * pointFactor_5[9]));
                resourceToggleList[4].Size = buttonSize;
            }
            else if (resourceToggleList.Count == 6)
            {
                resourceToggleList[0].Location = new Point(Convert.ToInt32(card.Width * pointFactor_6[0]), Convert.ToInt32(card.Height * pointFactor_6[1]));
                resourceToggleList[0].Size = buttonSize;

                resourceToggleList[1].Location = new Point(Convert.ToInt32(card.Width * pointFactor_6[2]), Convert.ToInt32(card.Height * pointFactor_6[3]));
                resourceToggleList[1].Size = buttonSize;

                resourceToggleList[2].Location = new Point(Convert.ToInt32(card.Width * pointFactor_6[4]), Convert.ToInt32(card.Height * pointFactor_6[5]));
                resourceToggleList[2].Size = buttonSize;

                resourceToggleList[3].Location = new Point(Convert.ToInt32(card.Width * pointFactor_6[6]), Convert.ToInt32(card.Height * pointFactor_6[7]));
                resourceToggleList[3].Size = buttonSize;

                resourceToggleList[4].Location = new Point(Convert.ToInt32(card.Width * pointFactor_6[8]), Convert.ToInt32(card.Height * pointFactor_6[9]));
                resourceToggleList[4].Size = buttonSize;

                resourceToggleList[5].Location = new Point(Convert.ToInt32(card.Width * pointFactor_6[10]), Convert.ToInt32(card.Height * pointFactor_6[11]));
                resourceToggleList[5].Size = buttonSize;
            }
            else if (resourceToggleList.Count == 7)
            {
                resourceToggleList[0].Location = new Point(Convert.ToInt32(card.Width * pointFactor_7[0]), Convert.ToInt32(card.Height * pointFactor_7[1]));
                resourceToggleList[0].Size = buttonSize;

                resourceToggleList[1].Location = new Point(Convert.ToInt32(card.Width * pointFactor_7[2]), Convert.ToInt32(card.Height * pointFactor_7[3]));
                resourceToggleList[1].Size = buttonSize;

                resourceToggleList[2].Location = new Point(Convert.ToInt32(card.Width * pointFactor_7[4]), Convert.ToInt32(card.Height * pointFactor_7[5]));
                resourceToggleList[2].Size = buttonSize;

                resourceToggleList[3].Location = new Point(Convert.ToInt32(card.Width * pointFactor_7[6]), Convert.ToInt32(card.Height * pointFactor_7[7]));
                resourceToggleList[3].Size = buttonSize;

                resourceToggleList[4].Location = new Point(Convert.ToInt32(card.Width * pointFactor_7[8]), Convert.ToInt32(card.Height * pointFactor_7[9]));
                resourceToggleList[4].Size = buttonSize;

                resourceToggleList[5].Location = new Point(Convert.ToInt32(card.Width * pointFactor_7[10]), Convert.ToInt32(card.Height * pointFactor_7[11]));
                resourceToggleList[5].Size = buttonSize;

                resourceToggleList[6].Location = new Point(Convert.ToInt32(card.Width * pointFactor_7[12]), Convert.ToInt32(card.Height * pointFactor_7[13]));
                resourceToggleList[6].Size = buttonSize;
            }
        }

        private void ensureSingleSelectedTrade(object source, EventArgs e)
        {
            Resources.ResourceToggle toggle = (Resources.ResourceToggle)source;
            bool status = toggle.active;

            foreach (Resources.ResourceToggle item in resourceToggleList)
            {
                if (item != toggle)
                {
                    if (item.active)
                    {
                        game.getPlayers()[0].tradeManager.removeTrade(item);
                    }
                    item.setActive(status);
                }
                
                
            }
        }

        private void ensureSingleSelectedSelf(object source, EventArgs e)
        {
            Resources.ResourceToggle toggle = (Resources.ResourceToggle)source;

            foreach (Resources.ResourceToggle item in resourceToggleList)
            {
                if (item != toggle)
                {
                    if (item.active)
                    {
                        game.getPlayers()[0].tradeManager.removeTrade(item);
                        item.active = false;
                    }

                }
            }
        }

        private void addResource(object source, EventArgs e)
        {
            Resources.ResourceToggle toggle = (Resources.ResourceToggle)source;

            player.addResource(currentResource * -1);

            foreach (Resources.ResourceToggle item in resourceToggleList)
            {
                if (item.active)
                {
                    item.setActive(false);
                }
            }

            toggle.setActive(true);
            player.addResource(toggle.generates);
            currentResource = toggle.generates;

            game.getPlayers()[0].PNLCost.update();

            if (ResourceGenerationChanged != null)
            {
                ResourceGenerationChanged(this, new ResourceGenerationChangedEvent("Resource Generation Changed."));
            }
        }
        public void clear()
        {
            player.addResource(currentResource * -1);

            if (card != null)
            {
                foreach (Resources.ResourceToggle item in resourceToggleList)
                {
                    card.Controls.Remove(item);
                }
            }
            else
            {
                foreach (Resources.ResourceToggle item in resourceToggleList)
                {
                    PNLWonderPlaceHolder.Controls.Remove(item);
                }
            }
        }
        public Resources.Resource getCurrentResource()
        {
            foreach (Resources.ResourceToggle item in resourceToggleList)
            {
                if (item.active)
                {
                    return item.generates;
                }
            }

            return new Resources.Resource();
        }
        public void selectResource(Resources.Resource ans)
        {
            player.addResource(currentResource * -1);

            foreach (Resources.ResourceToggle item in resourceToggleList)
            {
                if (item.generates == ans)
                {
                    currentResource = ans;
                    item.setActive(true);
                }
                else
                {
                    item.setActive(false);
                }
            }
            
            player.addResource(ans);
        }

        private void playFromCard()
        {
            for (int count = 0; count < resource.loom; count++)
            {
                if (card.getCardColor() == Card.CardColor.Gray)
                {
                    player.setProduces(1);
                }

                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Loom, player));
            }
            for (int count = 0; count < resource.glassworks; count++)
            {
                if (card.getCardColor() == Card.CardColor.Gray)
                {
                    player.setProduces(0);
                }
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Glass, player));
            }
            for (int count = 0; count < resource.papyrus; count++)
            {
                if (card.getCardColor() == Card.CardColor.Gray)
                {
                    player.setProduces(2);
                }
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Papyrus, player));
            }

            for (int count = 0; count < resource.clay; count++)
            {
                if (card.getCardColor() == Card.CardColor.Brown)
                {
                    player.setProduces(3);
                }
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Clay, player));
            }
            for (int count = 0; count < resource.ore; count++)
            {
                if (card.getCardColor() == Card.CardColor.Brown)
                {
                    player.setProduces(4);
                }
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Ore, player));
            }
            for (int count = 0; count < resource.silver; count++)
            {
                if (card.getCardColor() == Card.CardColor.Brown)
                {
                    player.setProduces(5);
                }
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Stone, player));
            }
            for (int count = 0; count < resource.wood; count++)
            {
                if (card.getCardColor() == Card.CardColor.Brown)
                {
                    player.setProduces(6);
                }
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Wood, player));
            }

            if (player.getUsername().Equals(game.getConfig().getUsername()) && resourceToggleList.Count > 0)
            {
                currentResource = resourceToggleList[0].generates;
                resourceToggleList[0].active = true;

                if (!externalClickHandling)
                {
                    player.addResource(resourceToggleList[0].generates);
                }

                for (int count = 1; count < resourceToggleList.Count; count++)
                {
                    resourceToggleList[count].active = false;
                }
            }
            else
            {
                for (int count = 0; count < resourceToggleList.Count; count++)
                {
                    resourceToggleList[count].active = true;
                }
            }

            for (int count = 0; count < resourceToggleList.Count; count++)
            {
                card.Controls.Add(resourceToggleList[count]);
                resourceToggleList[count].BringToFront();
                resourceToggleList[count].BackColor = Color.Transparent;

                if (!externalClickHandling)
                {
                    if ((card.getCardColor() == Card.CardColor.Brown || card.getCardColor() == Card.CardColor.Gray) && (game.getPlayers()[0].leftNeighbor.getUsername().Equals(player.getUsername()) || game.getPlayers()[0].rightNeighbor.getUsername().Equals(player.getUsername())))
                    {
                        resourceToggleList[count].ButtonClick += game.getPlayers()[0].tradeManager.trade;
                        resourceToggleList[count].ButtonClick += ensureSingleSelectedTrade;
                        resourceToggleList[count].ValueChange += ensureSingleSelectedTrade;
                    }
                    else if (player.getUsername().Equals(game.getConfig().getUsername()))
                    {
                        resourceToggleList[count].ButtonClick += addResource;
                        resourceToggleList[count].ButtonClick += ensureSingleSelectedSelf;
                    }
                }
            }

            resizeResourceButtons();

            card.Resize += resizeResourceButtons;
        }

        private void playFromWonder()
        {
            for (int count = 0; count < resource.loom; count++)
            {
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Loom, player));
            }
            for (int count = 0; count < resource.glassworks; count++)
            {
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Glass, player));
            }
            for (int count = 0; count < resource.papyrus; count++)
            {
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Papyrus, player));
            }

            for (int count = 0; count < resource.clay; count++)
            {
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Clay, player));
            }
            for (int count = 0; count < resource.ore; count++)
            {
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Ore, player));
            }
            for (int count = 0; count < resource.silver; count++)
            {
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Stone, player));
            }
            for (int count = 0; count < resource.wood; count++)
            {
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Wood, player));
            }

            if (player.getUsername().Equals(game.getConfig().getUsername()) && resourceToggleList.Count > 0)
            {
                currentResource = resourceToggleList[0].generates;
                resourceToggleList[0].active = true;
                player.addResource(resourceToggleList[0].generates);

                for (int count = 1; count < resourceToggleList.Count; count++)
                {
                    resourceToggleList[count].active = false;
                }
            }
            else
            {
                for (int count = 0; count < resourceToggleList.Count; count++)
                {
                    resourceToggleList[count].active = true;
                }
            }

            for (int count = 0; count < resourceToggleList.Count; count++)
            {
                if (player.getUsername().Equals(game.getConfig().getUsername()))
                {
                    PNLWonderPlaceHolder.Controls.Add(resourceToggleList[count]);
                    resourceToggleList[count].BringToFront();
                    resourceToggleList[count].BackColor = Color.Transparent;

                    resourceToggleList[count].ButtonClick += addResource;
                    resourceToggleList[count].ButtonClick += ensureSingleSelectedSelf;
                }
            }

            resizeResourceButtonsWonder();

            PNLWonderPlaceHolder.Resize += resizeResourceButtonsWonder;
        }

        private void resizeResourceButtonsWonder(object source, EventArgs e)
        {
            resizeResourceButtonsWonder();
        }
        private void resizeResourceButtonsWonder()
        {
            Size buttonSize;

            if (resourceToggleList.Count <= 2)
            {
                buttonSize = new Size(Convert.ToInt32(PNLWonderPlaceHolder.Width * sizeFactorLarge), Convert.ToInt32(PNLWonderPlaceHolder.Width * sizeFactorLarge));
            }
            if (resourceToggleList.Count <= 4)
            {
                buttonSize = new Size(Convert.ToInt32(PNLWonderPlaceHolder.Width * sizeFactor), Convert.ToInt32(PNLWonderPlaceHolder.Width * sizeFactor));
            }
            else
            {
                buttonSize = new Size(Convert.ToInt32(PNLWonderPlaceHolder.Width * sizeFactorSmall), Convert.ToInt32(PNLWonderPlaceHolder.Width * sizeFactorSmall));
            }

            if (resourceToggleList.Count == 1)
            {
                resourceToggleList[0].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_1[0]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.2));
                resourceToggleList[0].Size = buttonSize;
            }
            if (resourceToggleList.Count == 2)
            {
                resourceToggleList[0].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_2[0]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.2));
                resourceToggleList[0].Size = buttonSize;

                resourceToggleList[1].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_2[2]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.2));
                resourceToggleList[1].Size = buttonSize;
            }
            else if (resourceToggleList.Count == 3)
            {
                resourceToggleList[0].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_3[0]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.2));
                resourceToggleList[0].Size = buttonSize;

                resourceToggleList[1].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_3[2]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.2));
                resourceToggleList[1].Size = buttonSize;

                resourceToggleList[2].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_3[4]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.2));
                resourceToggleList[2].Size = buttonSize;
            }
            else if (resourceToggleList.Count == 4)
            {
                resourceToggleList[0].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_4[0]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.2));
                resourceToggleList[0].Size = buttonSize;

                resourceToggleList[1].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_4[2]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.2));
                resourceToggleList[1].Size = buttonSize;

                resourceToggleList[2].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_4[4]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.2));
                resourceToggleList[2].Size = buttonSize;

                resourceToggleList[3].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_4[6]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.2));
                resourceToggleList[3].Size = buttonSize;
            }
            else if (resourceToggleList.Count == 5)
            {
                resourceToggleList[0].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_5[0]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.1));
                resourceToggleList[0].Size = buttonSize;

                resourceToggleList[1].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_5[2]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.1));
                resourceToggleList[1].Size = buttonSize;

                resourceToggleList[2].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_5[4]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.1));
                resourceToggleList[2].Size = buttonSize;

                resourceToggleList[3].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_5[6]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.6));
                resourceToggleList[3].Size = buttonSize;

                resourceToggleList[4].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_5[8]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.6));
                resourceToggleList[4].Size = buttonSize;
            }
            else if (resourceToggleList.Count == 6)
            {
                resourceToggleList[0].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_6[0]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.1));
                resourceToggleList[0].Size = buttonSize;

                resourceToggleList[1].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_6[2]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.1));
                resourceToggleList[1].Size = buttonSize;

                resourceToggleList[2].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_6[4]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.1));
                resourceToggleList[2].Size = buttonSize;

                resourceToggleList[3].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_6[6]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.6));
                resourceToggleList[3].Size = buttonSize;

                resourceToggleList[4].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_6[8]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.6));
                resourceToggleList[4].Size = buttonSize;

                resourceToggleList[5].Location = new Point(Convert.ToInt32(PNLWonderPlaceHolder.Width * pointFactor_6[10]), Convert.ToInt32(PNLWonderPlaceHolder.Height * 0.6));
                resourceToggleList[5].Size = buttonSize;
            }
        }
        public List<Resources.ResourceToggle> getToggleButtons()
        {
            return resourceToggleList;
        }
    }
}
