﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Client.CardEffects
{
    class ResurrectionEffect : CardEffect
    {
        Player player;
        ResurrectionPNL resurrectionPNL;
        Card cardToResurrect;

        public ResurrectionEffect(Player player_)
        {
            player = player_;
        }

        public delegate void playD();

        public override void play()
        {
            if (player.InvokeRequired)
            {
                playD d = new playD(play);
                player.Invoke(d, new object[] { });
            }
            else
            {
                if (player == player.form_.getGame().getPlayers()[0])
                {
                    resurrectionPNL = new ResurrectionPNL(player);
                    resurrectionPNL.Size = new Size(360, 144 + 80 + 80 + 30);
                    resurrectionPNL.Location = new Point((player.form_.getPNLServer().getPNLGame().getRightSectionXPoint() - resurrectionPNL.Size.Width) / 2, (player.form_.Size.Height - resurrectionPNL.Size.Height) / 2);

                    player.form_.getPNLServer().getPNLGame().Controls.Add(resurrectionPNL);
                    resurrectionPNL.BringToFront();

                    resurrectionPNL.ResurrectionPicked += resurrectionPicked;
                    resurrectionPNL.ResurrectionCancel += resurrectionCancel;

                    resurrectionPNL.getChoices();
                }
            }
        }

        private void resurrectionPicked(object source, EventArgs e)
        {
            player.form_.getPNLServer().getPNLGame().Controls.Remove(resurrectionPNL);
            cardToResurrect = (Card)source;

            if (cardToResurrect.moreInfoNeeded)
            {
                cardToResurrect.MoreInfoReceived += moreInfoObtained;
                cardToResurrect.getMoreInfo();
            }
            else
            {
                player.form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "CARDRES" + Constants.DELIM_FIELD + player.getUsername() + Constants.DELIM_FIELD + cardToResurrect.cardLocation);
            }
        }

        private void moreInfoObtained(object source, AdditionalInfoAddedEvent e)
        {
            player.form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "CARDRES" + Constants.DELIM_FIELD + player.getUsername() + Constants.DELIM_FIELD + cardToResurrect.cardLocation + Constants.DELIM_FIELD + e.GetInfo());
        }

        private void resurrectionCancel(object sender, EventArgs e)
        {
            player.form_.getPNLServer().getPNLGame().Controls.Remove(resurrectionPNL);
            player.form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "UNLOCK" + Constants.DELIM_FIELD);
        }
    }
}
