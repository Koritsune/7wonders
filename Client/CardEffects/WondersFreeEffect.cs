﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class WondersFreeEffect : CardEffect
    {
        Player player;

        public WondersFreeEffect(Player player_)
        {
            player = player_;
        }
        public override void play()
        {
            WonderManager wonderManager = player.getWonder().wonderManager;

            foreach (WonderStage item in wonderManager.getWonderStages())
            {
                Resources.Resource newCost = new Resources.Resource();

                newCost.gold = item.cost.gold;

                item.cost = newCost;
                
            }
        }
    }
}
