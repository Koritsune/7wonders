﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client.CardEffects
{
    class ResourceNonGeneratedEffect : CardEffect
    {
        Player player;
        Card card;
        Panel PNL;

        ResourceOption resourceOption;
        Resources.Resource oldResource;

        public ResourceNonGeneratedEffect(Player player_, Card card_)
        {
            player = player_;
            card = card_;
        }
        public ResourceNonGeneratedEffect(Player player_, Panel PNL_)
        {
            player = player_;
            PNL = PNL_;
        }
        public delegate void playD();

        public override void play()
        {
            player.ResourceGenerationChanged += updateResourcesGenerated;

            if (card != null)
            {
                updateResourcesGeneratedCard();
            }
            else
            {
                updateResourcesGeneratedWonder();
            }
        }  
        
        private void updateResourcesGenerated(object source, EventArgs e)
        {
            if (card != null)
            {
                updateResourcesGeneratedCard();
            }
            else
            {
                updateResourcesGeneratedWonder();
            }
        }
        private void updateResourcesGeneratedCard()
        {
            if (resourceOption != null)
            {
                oldResource = resourceOption.getCurrentResource();
                resourceOption.clear();
            }

            Resources.Resource resourceMask = getResourcesToGenerate();

            resourceOption = new ResourceOption(player, resourceMask, card, player.form_.getGame());
            resourceOption.play();

            if (oldResource != null)
            {
                resourceOption.selectResource(oldResource);
            }
        }
        private void updateResourcesGeneratedWonder()
        {
            if (resourceOption != null)
            {
                oldResource = resourceOption.getCurrentResource();
                resourceOption.clear();
            }

            Resources.Resource resourceMask = getResourcesToGenerate();

            resourceOption = new ResourceOption(player, resourceMask, PNL, player.form_.getGame());
            resourceOption.play();

            if (oldResource != null)
            {
                resourceOption.selectResource(oldResource);
            }
        }
        private Resources.Resource getResourcesToGenerate()
        {
            Resources.Resource resourceMask = new Resources.Resource();

            for (int count = 0; count < 7; count++)
            {
                if (!player.produces[count])
                {
                    resourceMask[count + 1] = 1;
                }
            }

                return resourceMask;
        }
    }
}
