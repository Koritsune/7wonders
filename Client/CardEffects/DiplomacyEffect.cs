﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class DiplomacyEffect : CardEffect
    {
        Player player;
        Game.GameAge agePlayedin;

        public DiplomacyEffect(Player player_)
        {
            player = player_;
        }

        public override void play()
        {
            player.getWonder().diplomacy = true;
            agePlayedin = player.form_.getGame().getAge();
        }

        public override void cancelAbilities()
        {
            if (agePlayedin == player.form_.getGame().getAge())
            {
                player.getWonder().decreaseDiplomacy();
            }
            
        }
    }
}
