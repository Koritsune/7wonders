﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace Client.CardEffects
{
    class ResourceGenerationFixed : ResourceGeneration
    {
        Player player;
        public Resources.Resource resource {get; private set;}
        Card card;
        Game game;
        Wonder wonder;

        double sizeFactor = 0.32;

        static double[] pointFactor_1 = { 0.34, 0.03 };
        static double[] pointFactor_2 = { 0.2, 0.03, 0.5, .03 };

        //double[] pointFactorWonder;
        //double sizeFactorWonder;

        List<Resources.ResourceToggle> resourceToggleList = new List<Resources.ResourceToggle>();

        public ResourceGenerationFixed(Player player_, Resources.Resource resource_, Card card_, Game game_)
        {
            player = player_;
            resource = resource_;
            card = card_;
            game = game_;
        }

        public ResourceGenerationFixed(Wonder wonder_, Player player_, Resources.Resource resource_, Game game_)
        {
            player = player_;
            resource = resource_;
            wonder = wonder_;
            game = game_;

            //pointFactorWonder = new double[2];
            //pointFactorWonder[0] = pointFactorX;
            //pointFactorWonder[1] = pointFactorY;

            //sizeFactorWonder = sizeFactor;
        }
        public override void play()
        {

            if (resource.gold > 0)
            {
                player.raiseGoldReceivedEvent();
            }

            player.addResource(resource);

            for (int count = 0; count < resource.loom; count++)
            {
                //if (card.getCardColor() == Card.CardColor.Gray)
                //{
                    player.setProduces(1);
                //}
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Loom,player));
            }
            for (int count = 0; count < resource.glassworks; count++)
            {
                //if (card.getCardColor() == Card.CardColor.Gray)
                //{
                    player.setProduces(0);
                //}
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Glass,player));
            }
            for (int count = 0; count < resource.papyrus; count++)
            {
                //if (card.getCardColor() == Card.CardColor.Gray)
                //{
                    player.setProduces(2);
                //}
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Papyrus,player));
            }

            for (int count = 0; count < resource.clay; count++)
            {
                //if (card.getCardColor() == Card.CardColor.Brown)
                //{
                    player.setProduces(3);
                //}
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Clay,player));
            }
            for (int count = 0; count < resource.ore; count++)
            {
                //if (card.getCardColor() == Card.CardColor.Brown)
                //{
                    player.setProduces(4);
                //}
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Ore,player));
            }
            for (int count = 0; count < resource.silver; count++)
            {
                //if (card.getCardColor() == Card.CardColor.Brown)
                //{
                    player.setProduces(5);
                //}
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Stone,player));
            }
            for (int count = 0; count < resource.wood; count++)
            {
                //if (card.getCardColor() == Card.CardColor.Brown)
                //{
                    player.setProduces(6);
                //}
                resourceToggleList.Add(new Resources.ResourceToggle(Resources.ResourceToggle.Type.Wood,player));
            }

            for (int count = 0; count < resourceToggleList.Count; count++)
            {
                if (card != null)
                {
                    card.Controls.Add(resourceToggleList[count]);
                }
                else
                {
                    wonder.Controls.Add(resourceToggleList[count]);
                }
                resourceToggleList[count].BringToFront();
                resourceToggleList[count].BackColor = Color.Transparent;
                
            }

            if (game.getPlayers()[0].leftNeighbor == null || game.getPlayers()[0].rightNeighbor == null)
            {
                game.getForm().getPNLServer().getPNLGame().NeighborsLinked += linkTrading;
            }
            else
            {
                linkTrading();
            }

            resizeResourceButtons();

            if (card != null)
            {
                card.Resize += resizeResourceButtons;
            }
            else
            {
                game.getForm().getPNLServer().getPNLGame().AllWondersLoaded += resizeResourceButtons;
                wonder.Resize += resizeResourceButtons;
            }
        }
        private void resizeResourceButtons(object source, EventArgs e)
        {
            resizeResourceButtons();
        }
        private void resizeResourceButtons()
        {
            Size buttonSize = new Size(0,0);

            if (card != null)
            {
                buttonSize = new Size(Convert.ToInt32(card.Width * sizeFactor), Convert.ToInt32(card.Width * sizeFactor));
            }
                /*
            else
            {
                if (wonder.rotation == Player.PlayerRotation.None || wonder.rotation == Player.PlayerRotation.Clockwise180)
                {
                    buttonSize = new Size(Convert.ToInt32(wonder.Size.Height * sizeFactorWonder), Convert.ToInt32(wonder.Size.Height * sizeFactorWonder));
                }
                else
                {
                    buttonSize = new Size(Convert.ToInt32(wonder.Size.Width * sizeFactorWonder), Convert.ToInt32(wonder.Size.Width * sizeFactorWonder));
                }
            }*/

            try
            {
                if (resourceToggleList.Count == 1)
                {
                    if (card != null)
                    {
                        resourceToggleList[0].Location = new Point(Convert.ToInt32(card.Width * pointFactor_1[0]), Convert.ToInt32(card.Height * pointFactor_1[1]));
                        resourceToggleList[0].Size = buttonSize;
                    }
                    else
                    {
                        resourceToggleList[0].Location = wonder.getResourcePoint();
                        resourceToggleList[0].Size = wonder.PBArmy.Size;
                        /*
                        if (wonder.rotation == Player.PlayerRotation.Clockwise90)
                        {
                            Point resourcePoint = new Point(0, 0);
                            resourcePoint.X = Convert.ToInt32(wonder.Size.Width * pointFactorWonder[0]);
                            resourcePoint.Y = wonder.PBArmy.Location.Y - buttonSize.Height;
                            resourceToggleList[0].Location = resourcePoint;
                        }
                        else if (wonder.rotation == Player.PlayerRotation.ClockWise270)
                        {

                        }
                        else if (wonder.rotation == Player.PlayerRotation.None)
                        {
                            resourceToggleList[0].Location = new Point(Convert.ToInt32(wonder.Size.Height * pointFactorWonder[0]), Convert.ToInt32(wonder.Size.Height * pointFactorWonder[1]));
                        }
                        else
                        {
                            resourceToggleList[0].Location = new Point(wonder.Width - buttonSize.Width * 2, Convert.ToInt32(wonder.Size.Height * pointFactorWonder[1]));
                        }
                        
                        resourceToggleList[0].Size = buttonSize;
                         * */
                    }
                }
                else
                {
                    resourceToggleList[0].Location = new Point(Convert.ToInt32(card.Width * pointFactor_2[0]), Convert.ToInt32(card.Height * pointFactor_2[1]));
                    resourceToggleList[0].Size = buttonSize;

                    resourceToggleList[1].Location = new Point(Convert.ToInt32(card.Width * pointFactor_2[2]), Convert.ToInt32(card.Height * pointFactor_2[3]));
                    resourceToggleList[1].Size = buttonSize;
                }
                //Console.WriteLine("For user " + player.getUsername() + " Showing resourcetoggle at point: (" + resourceToggleList[0].Location.X + "," + resourceToggleList[0].Location.Y + ")");
                //Console.WriteLine("Registered rotation is: " + wonder.rotation);
            }
#pragma warning disable 0168
            catch (SystemException e)
#pragma warning restore 0168
            {

            }
            
        }
        private void linkTrading(object sender, EventArgs e)
        {
            linkTrading();
        }
        private void linkTrading()
        {
            for (int count = 0; count < resourceToggleList.Count; count++)
            {
                if (game.getPlayers()[0].leftNeighbor.getUsername().Equals(player.getUsername()) || game.getPlayers()[0].rightNeighbor.getUsername().Equals(player.getUsername()))
                {
                    resourceToggleList[count].ButtonClick += game.getPlayers()[0].tradeManager.trade;
                }
            }
        }
    }
}
