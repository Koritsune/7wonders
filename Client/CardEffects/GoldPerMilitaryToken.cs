﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class GoldPerMilitaryToken : CardEffect
    {
        Player benefactor;
        Player target;
        Resources.Resource gold = new Resources.Resource();

        public GoldPerMilitaryToken(Player benefactor_, Player target_, int gold_)
        {
            benefactor = benefactor_;
            target = target_;
            gold.gold = gold_;
        }
        public override void play()
        {
            benefactor.addResource(gold * target.PNLArmyVictoryToken.getNbVictoryTokens());
            benefactor.raiseGoldReceivedEvent();
        }
    }
}
