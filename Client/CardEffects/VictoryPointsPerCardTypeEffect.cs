﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class VictoryPointsPerCardTypeEffect : VictoryPointsEffect
    {
        Card.CardColor color;
        Player player;
        int pointsPerCard;

        public VictoryPointsPerCardTypeEffect(Player player_, Card.CardColor color_, int pointsPerCard_)
        {
            player = player_;
            color = color_;
            pointsPerCard = pointsPerCard_;
        }
        public override int generateVicotryPoints()
        {
            int nbOfCards = player.playedCards[(int)color].Count;

            return nbOfCards * pointsPerCard;
        }
    }
}
