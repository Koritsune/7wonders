﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class VictoryPointsPerScienceSetEffect : VictoryPointsEffect
    {
        Player player;
        int pointsPerSet;

        public VictoryPointsPerScienceSetEffect(Player player_, int pointsPerSet_)
        {
            player = player_;
            pointsPerSet = pointsPerSet_;
        }
        public override int generateVicotryPoints()
        {
            int min = Math.Min(player.SCIENCE[0], player.SCIENCE[1]);
            min = Math.Min(min, player.SCIENCE[2]);

            return min * pointsPerSet;
        }
    }
}
