﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class GoldGenerationPerCardEffect : CardEffect
    {
        Player benefactor;
        Player target;
        int goldPerCard;
        Card.CardColor cardColor;

        public GoldGenerationPerCardEffect(Player benefactor_, Player target_,Card.CardColor cardColor_, int goldPerCard_)
        {
            benefactor = benefactor_;
            target = target_;
            goldPerCard = goldPerCard_;
            cardColor = cardColor_;
        }
        public override void play()
        {
            Resources.Resource goldToAdd = new Resources.Resource();

            goldToAdd.gold = target.playedCards[(int)cardColor].Count * goldPerCard;

            benefactor.addResource(goldToAdd);
            benefactor.raiseGoldReceivedEvent();
        }
    }
}
