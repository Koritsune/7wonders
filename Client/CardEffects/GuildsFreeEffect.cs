﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class GuildsFreeEffect : CardEffect
    {
        Player player;
        Resources.Resource freeResource = new Resources.Resource();

        public GuildsFreeEffect(Player player_)
        {
            player = player_;
        }
        public override void play()
        {
            player.form_.getGame().serverConnection.playManager.NewTurn += checkCard;
        }
        private void checkCard(object source, EventArgs e)
        {
            if (active)
            {
                foreach (Card item in player.getHand())
                {
                    item.cost = freeResource;
                }
            }
        }
    }
}
