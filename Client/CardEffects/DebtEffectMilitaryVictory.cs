﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class DebtEffectMilitaryVictory : DebtEffect
    {
        public DebtEffectMilitaryVictory(Player player_, int goldamount_, Form1 form)
            : base(form, player_)
        {
            debtAmount = form.getGame().getPlayers()[0].PNLArmyVictoryToken.getNbVictoryTokens() * goldamount_;
            buildDebtPanel();
        }

        private delegate void buildDebtPanelD();

        private void buildDebtPanel()
        {
            if (form.InvokeRequired)
            {
                buildDebtPanelD d = new buildDebtPanelD(buildDebtPanel);
                form.Invoke(d, new object[] { });
            }
            else
            {
                PNLDebt = new DebtEffectPNL(debtAmount);
            }
        }
    }
}
