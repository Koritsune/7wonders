﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class VictoryPointsPerGold : VictoryPointsEffect
    {
        Player player;
        int gold;
        int points;

        public VictoryPointsPerGold(Player player_, int gold_, int points_)
        {
            player = player_;
            gold = gold_;
            points = points_;
        }
        public override int generateVicotryPoints()
        {
            return points * (player.resources.gold / gold);
        }
    }
}
