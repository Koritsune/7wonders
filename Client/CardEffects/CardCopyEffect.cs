﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Client.CardEffects
{
    class CardCopyEffect : VictoryPointsEffect
    {
        Player player;
        Card.CardColor color;
        Card cardCopied;
        CardCopyPNL pnl;
        Card card;
        WonderStage wonderStage;

        public CardCopyEffect(Card card_, Player player_, Card.CardColor color_)
        {
            player = player_;
            color = color_;
            card = card_;
        }
        public CardCopyEffect(WonderStage wonderStage_, Player player_, Card.CardColor color_)
        {
            player = player_;
            color = color_;
            wonderStage = wonderStage_;
        }
        public override void play()
        {
            base.play();
        }

        public override int generateVicotryPoints()
        {
            return 0;
        }
        public delegate void getEndOfGameInfo();
        public override void getEndofGameInfo()
        {
            if (player.InvokeRequired)
            {
                getEndOfGameInfo d = new getEndOfGameInfo(getEndofGameInfo);
                player.Invoke(d, new object[] { });
            }
            else
            {
                if (player == player.form_.getGame().getPlayers()[0])
                {
                    pnl = new CardCopyPNL(player);

                    pnl.Size = new Size(360, 144 + 80 + 80 + 30);
                    pnl.Location = new Point((player.form_.getPNLServer().getPNLGame().getRightSectionXPoint() - pnl.Size.Width) / 2, (player.form_.Size.Height - pnl.Size.Height) / 2);

                    pnl.CardCopyPicked += CardCopyPicked;
                    pnl.CardCopyCancel += CardCopyCancel;
                    player.form_.getPNLServer().getPNLGame().Controls.Add(pnl);
                }
            }
        }
        private void CardCopyPicked(object source, EventArgs e)
        {
            Card leaderPicked = (Card)source;

            player.form_.getPNLServer().getPNLGame().Controls.Remove(pnl);

            player.form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "ENDOFGAMEINFO" + Constants.DELIM_FIELD + player.getUsername() + Constants.DELIM_FIELD + card.name + Constants.DELIM_FIELD + cardCopied.cardLocation);    
        }
        private void CardCopyCancel(object source, EventArgs e)
        {
            player.form_.getPNLServer().getPNLGame().Controls.Remove(pnl);

            player.form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "ENDOFGAMEINFO" + Constants.DELIM_FIELD + player.getUsername() + Constants.DELIM_FIELD + card.name + Constants.DELIM_FIELD + "Age I\\Back");   
        }

        public override void receiveEndofGameInfo(string info)
        {
            cardCopied = new Card(info,player);

            if (wonderStage != null)
            {
                cardCopied.CardPlayed += raiseWonderPlayed;
            }
            else
            {
                cardCopied.CardPlayed += raiseWonderPlayed;
            }

            cardCopied.play();
        }

        private void raiseWonderPlayed(object source, EventArgs e)
        {
            wonderStage.raiseWonderPlayedEvent();
        }
        private void raiseCardPlayed(object source, EventArgs e)
        {
            card.raiseCardPlayed();
        }
    }
}
