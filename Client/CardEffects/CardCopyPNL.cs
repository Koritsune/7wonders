﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client.CardEffects
{
    class CardCopyPNL : Panel
    {
        public event CardCopyCancelHandler CardCopyCancel;

        public delegate void CardCopyCancelHandler(object source, CardCopyCancelEvent e);
        public class CardCopyCancelEvent : EventArgs
        {
            private string EventInfo;
            public CardCopyCancelEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public event CardCopyPickedHandler CardCopyPicked;

        public delegate void CardCopyPickedHandler(object source, CardCopyPickedEvent e);
        public class CardCopyPickedEvent : EventArgs
        {
            private string EventInfo;
            public CardCopyPickedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        Player player;
        TranslucentTextBox LBLInfo = new TranslucentTextBox();
        TranslucentTextBox LBLCardInfo = new TranslucentTextBox();

        TextButton TBNext = new TextButton("Next");
        TextButton TBPrevious = new TextButton("Back");

        Timer timer = new Timer();
        Point CursorLocation;
        bool scrollCardInfo = false;

        List<Card> choices = new List<Card>();
        int currentViewingIndex = 0;

        int goldCost = 0;

        public CardCopyPNL(Player player_)
        {
            player = player_;

            player = player_;
            BackColor = Color.Transparent;
            BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);

            LBLCardInfo.BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
            LBLCardInfo.getTextBox().ForeColor = Color.White;
            LBLCardInfo.getTextBox().ReadOnly = true;
            LBLCardInfo.setFontSize(12);
            LBLCardInfo.setSize(360, 80);
            LBLCardInfo.Location = new Point(0, 80);
            LBLCardInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            LBLCardInfo.getTextBox().ScrollBars = RichTextBoxScrollBars.ForcedVertical;
            //LBLCardInfo.getTextBox().Text = "Select the leader you wish to sacrifice. You will no longer receive the chosen leaders effect but you will receive victory points equal to double its cost.";

            LBLInfo.BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
            LBLInfo.getTextBox().ForeColor = Color.White;
            LBLInfo.getTextBox().ReadOnly = true;
            LBLInfo.setFontSize(12);
            LBLInfo.setSize(360, 80);
            LBLInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            LBLInfo.getTextBox().Text = "Select which of the following cards you wish to build.";

            Controls.Add(LBLCardInfo);
            Controls.Add(LBLInfo);

            TBNext.setFontSize(12);
            TBNext.startHover();
            TBNext.setSize(100, 30);
            TBNext.Location = new Point(260, 304); 
            TBNext.onTextButtonClickEvent += showNextCards;
            
            TBPrevious.setFontSize(12);
            TBPrevious.startHover();
            TBPrevious.setSize(100, 30);
            TBPrevious.Location = new Point(0, 304);
            TBPrevious.onTextButtonClickEvent += showPreviousCards;

            Controls.Add(TBNext);
            Controls.Add(TBPrevious);

            //player_.cardPlayedHandler[(int)Card.CardColor.Roster] += rosterUpdate;
            getChoices();

            timer.Tick += scrollCard;
            timer.Start();
        }

        private void scrollCard(object sender, EventArgs e)
        {
            if (!scrollCardInfo)
            {
                Point temp = player.form_.DesktopLocation;
                temp.X += Location.X + LBLCardInfo.Location.X + LBLCardInfo.Size.Width - 4;
                temp.Y += Location.Y + LBLCardInfo.Location.Y + LBLCardInfo.Size.Height - 4;
                CursorLocation = Cursor.Position;
                Cursor.Position = temp;
                scrollCardInfo = true;
            }
            else
            {
                Cursor.Position = CursorLocation;
                scrollCardInfo = false;
                timer.Stop();
            }
        }

        private void cardClick(object source, EventArgs e)
        {
            Resources.Resource goldToReimburse = new Resources.Resource();
            goldToReimburse.gold = goldCost;
            player.addResource(goldToReimburse);

            Card cardClicked = (Card)source;

            LBLCardInfo.getTextBox().Text = cardClicked.getDesc();

            player.raiseCardSelected(cardClicked);
            goldCost = cardClicked.cost.gold;

            Resources.Resource goldToDeduct = new Resources.Resource();
            goldToDeduct.gold = -1 * goldCost;

            player.addResource(goldToDeduct);
        }

        private void cardSelected(object source, EventArgs e)
        {
            Card card = (Card)source;
            player.raiseCardSelected(card);

            Resources.Resource goldToReimburse = new Resources.Resource();
            goldToReimburse.gold = goldCost;
            player.addResource(goldToReimburse);
            player.form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "COIN" + Constants.DELIM_FIELD + player.getUsername() + Constants.DELIM_FIELD + (-1 * card.cost.gold));

            if (CardCopyPicked != null)
            {
                CardCopyPicked(source, new CardCopyPickedEvent("CardCopy Picked."));
            }
        }

        private void showNextCards(object source, EventArgs e)
        {
            if (currentViewingIndex == choices.Count / 4)
            {
                return;
            }
            else
            {
                for (int count = currentViewingIndex * 4; count < (currentViewingIndex + 1) * 4 && count < choices.Count; count++)
                {
                    choices[count].Visible = false;
                }

                currentViewingIndex++;

                for (int count = currentViewingIndex * 4; count < (currentViewingIndex + 1) * 4 && count < choices.Count; count++)
                {
                    choices[count].Visible = true;
                }

                LBLCardInfo.getTextBox().Text = choices[currentViewingIndex * 4].getDesc();
            }
        }

        private void showPreviousCards(object source, EventArgs e)
        {
            if (currentViewingIndex == 0)
            {
                return;
            }
            else
            {
                for (int count = currentViewingIndex * 4; count < (currentViewingIndex + 1) * 4 && count< choices.Count; count++)
                {
                    choices[count].Visible = false;
                }

                currentViewingIndex--;

                for (int count = currentViewingIndex * 4; count < (currentViewingIndex + 1) * 4 && count < choices.Count; count++)
                {
                    choices[count].Visible = true;
                }

                LBLCardInfo.getTextBox().Text = choices[currentViewingIndex * 4].getDesc();
            }
        }

        private void rosterUpdate(Card card)
        {
            updateCards();
        }

        private delegate void updateCardsD();

        private void updateCards()
        {
            if (this.InvokeRequired)
            {
                updateCardsD d = new updateCardsD(updateCards);
                this.Invoke(d, new object[] { });
            }
            else
            {
                clearChoices();
                getChoices();
            }
        }

        private void clearChoices()
        {
            Resources.Resource goldToReimburse = new Resources.Resource();
            goldToReimburse.gold = goldCost;
            player.addResource(goldToReimburse);
            goldCost = 0;

            foreach (Card item in choices)
            {
                Controls.Remove(item);
            }
            choices.Clear();
            currentViewingIndex = 0;

            LBLCardInfo.getTextBox().Text = "";
        }

        private void getChoices()
        {
            bool canHireLeader = false;

            for (int count = 0; count < player.leftNeighbor.playedCards[(int)Card.CardColor.Purple].Count; count++)
            {

                Card cardChoice = new Card(player.leftNeighbor.playedCards[(int)Card.CardColor.Purple][count].cardLocation, player);

                if (!canHireLeader)
                {
                    player.raiseCardSelected(cardChoice);

                    if (cardChoice.cost.gold >= player.resources.gold)
                    {
                        canHireLeader = true;
                    }
                }
                    cardChoice.hideButtons();
                    cardChoice.setSize(80, 144);

                    cardChoice.Location = new Point(count % 4 * 80, 160);
                    choices.Add(cardChoice);

                    cardChoice.CardClick += cardClick;
                    cardChoice.showPictureOnly();
                    cardChoice.CardDoubleClick += cardSelected;

                    Controls.Add(cardChoice);
                
            }

            for (int count = 0; count < player.rightNeighbor.playedCards[(int)Card.CardColor.Purple].Count; count++)
            {

                Card cardChoice = new Card(player.rightNeighbor.playedCards[(int)Card.CardColor.Purple][count].cardLocation, player);

                if (!canHireLeader)
                {
                    player.raiseCardSelected(cardChoice);

                    if (cardChoice.cost.gold >= player.resources.gold)
                    {
                        canHireLeader = true;
                    }
                }
                cardChoice.hideButtons();
                cardChoice.setSize(80, 144);

                cardChoice.Location = new Point(count % 4 * 80, 160);
                choices.Add(cardChoice);

                cardChoice.CardClick += cardClick;
                cardChoice.showPictureOnly();
                cardChoice.CardDoubleClick += cardSelected;

                Controls.Add(cardChoice);

            }

            LBLCardInfo.getTextBox().Text = choices[0].getDesc();

            if (!canHireLeader)
            {
                if (CardCopyCancel != null)
                {
                    CardCopyCancel(this, new CardCopyCancelEvent("CardCopy Canceled."));
                }
            }
        }
    }
}
