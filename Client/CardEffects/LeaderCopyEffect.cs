﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Client.CardEffects
{
    class LeaderCopyEffect : VictoryPointsEffect
    {
        Player player;
        Card cardCopied;
        Card card;

        LeaderCopyPNL pnl;

        public event LeaderCopyPickedHandler LeaderCopyPicked;

        public delegate void LeaderCopyPickedHandler(object source, AdditionalInfoAddedEvent e);

        public LeaderCopyEffect(Player player_, Card card_)
        {
            MoreInfoNeeded = true;
            player = player_;
            card = card_;
        }
        public override int generateVicotryPoints()
        {
            return cardCopied.getVictoryPoint();
        }
        public override void getMoreInfo()
        {
            pnl = new LeaderCopyPNL(player);

            pnl.Size = new Size(360, 144 + 80 + 80 + 30);
            pnl.Location = new Point((player.form_.getPNLServer().getPNLGame().getRightSectionXPoint() - pnl.Size.Width) / 2, (player.form_.Size.Height - pnl.Size.Height) / 2);

            pnl.LeaderPicked += leaderPicked;
            pnl.LeaderCancel += leaderCancel;
            player.form_.getPNLServer().getPNLGame().Controls.Add(pnl);
            pnl.BringToFront();
            pnl.getChoices();
        }
        private void leaderPicked(object source, EventArgs e)
        {
            Card leaderPicked = (Card)source;

            player.form_.getPNLServer().getPNLGame().Controls.Remove(pnl);

            if (LeaderCopyPicked != null)
            {
                LeaderCopyPicked(this, new CardEffect.AdditionalInfoAddedEvent(leaderPicked.cardLocation));
            }

            player.form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "PLAYCARD" + Constants.DELIM_FIELD + card.getLocation() + Constants.DELIM_FIELD + leaderPicked.cardLocation);

            card.raiseMoreInfoReceived();

           
        }
        private void leaderCancel(object source, EventArgs e)
        {
            player.form_.getPNLServer().getPNLGame().Controls.Remove(pnl);

            if (LeaderCopyPicked != null)
            {
                LeaderCopyPicked(this, new CardEffect.AdditionalInfoAddedEvent("Canceled"));
            }

            player.form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "PLAYCARD" + Constants.DELIM_FIELD + card.getLocation() + Constants.DELIM_FIELD + "Canceled");

            card.raiseMoreInfoReceived();

            
        }
        public override void receiveMoreInfo(string info)
        {
            if (!info.Equals("Canceled"))
            {
                //Console.WriteLine(info.Replace(Constants.DELIM_FIELD, '#'));
                String leaderToHire = "";

                if (leaderToHire.Contains(Constants.DELIM_FIELD))
                {
                    leaderToHire = info.Substring(0, info.IndexOf(Constants.DELIM_FIELD));
                }
                else
                {
                    leaderToHire = info;
                }

                if (info.Length > leaderToHire.Length)
                {
                    info = info.Substring(leaderToHire.Length + 1);
                }

                cardCopied = new Card(leaderToHire, player);
            }
            else
            {

            }
        }
        public override void play()
        {
            if (cardCopied != null)
            {
                cardCopied.Location = new Point(0, Convert.ToInt32(card.Size.Height * 0.1));
                cardCopied.Size = new Size(player.Size.Width / 5, player.Size.Width / 3);
                cardCopied.CardPlayed += raiseCardPlayed;
                card.Controls.Add(cardCopied);
                cardCopied.BringToFront();
                cardCopied.play();
            }
            else
            {
                card.raiseCardPlayedForUnlocking();
            }
        }
        private void raiseCardPlayed(object source, EventArgs e)
        {
            card.raiseCardPlayedForUnlocking();
        }
    }
}
