﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class VictoryPointsPerMilitaryTokenEffect : VictoryPointsEffect
    {
        Player target;
        int pointsPerToken;

        public VictoryPointsPerMilitaryTokenEffect(Player target_, int pointsPerToken_)
        {
            target = target_;
            pointsPerToken = pointsPerToken_;
        }
        public override int generateVicotryPoints()
        {
            return target.PNLArmyVictoryToken.getNbVictoryTokens() * pointsPerToken;
        }
    }
}
