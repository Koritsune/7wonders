﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client.CardEffects
{
    class FreeBuild : CardEffect
    {
        Player player;
        bool singleUse;
        bool applyAll;
        bool currentlyUsing = false;
        bool ignoreCancel = false;
        bool used = false;

        Resources.Resource oldcardCost = new Resources.Resource();

        TogglePicutreButton TPBFreeBuild;
        Card.CardColor cardColor;
        public Panel pnl;

        Card cardToMakeFee;

        Card card;

        public FreeBuild(Player player_, Card.CardColor cardColor_, Panel pnl_, bool singleUse_ = false, bool applyAll_ = false)
        {
            player = player_;
            singleUse = singleUse_;
            applyAll = applyAll_;
            cardColor = cardColor_;
            pnl = pnl_;            
        }

        public FreeBuild(Player player_, Card.CardColor cardColor_, Card card_, bool singleUse_ = false, bool applyAll_ = false)
        {
            player = player_;
            singleUse = singleUse_;
            applyAll = applyAll_;
            cardColor = cardColor_;
            card = card_;
        }

        delegate void playD();

        public override void play()
        {
            if (player.InvokeRequired)
            {
                playD d = new playD(play);
                player.Invoke(d, new object[] {  });
            }
            else
            {
                if (player.getUsername().Equals(player.form_.getConfig().getUsername()))
                {
                    int colorIndex = (applyAll ? -1 : (int)cardColor);
                    TPBFreeBuild = new TogglePicutreButton(Constants.ICONS + "FreeBuild" + colorIndex + "Active.PNG", Constants.ICONS + "FreeBuild" + colorIndex + "InActive.PNG", false);

                    if (pnl != null)
                    {
                        pnl.Controls.Add(TPBFreeBuild);
                        TPBFreeBuild.setSize(pnl.Size.Width, pnl.Size.Height);
                        pnl.Resize += resize;
                    }
                    else
                    {
                        card.getPicture().Controls.Add(TPBFreeBuild);
                        TPBFreeBuild.setSize(card.Size.Width / 10 * 4, card.Size.Height / 10 * 2);
                        TPBFreeBuild.Location = new Point(Convert.ToInt32(card.Size.Width * 0.3), card.Size.Height / 20);
                        card.Resize += resize;
                    }

                    TPBFreeBuild.toggleActive();
                    
                    

                    player.CardSelected += updateCurrentCard;

                    if (singleUse)
                    {
                        TPBFreeBuild.ToggleClick += makeCardFreeSingleUse;

                        player.PNLCost.cardCancel += cardCancel;
                        player.PNLCost.cardPurchase += cardplayed;

                        player.form_.getGame().GameAgeChange += resetUse;
                    }
                    else
                    {

                    }
                }

            }
        }
        protected void resize(object source, EventArgs e)
        {
            if (pnl != null)
            {
                TPBFreeBuild.setSize(pnl.Size.Width, pnl.Size.Height);
            }
            else
            {
                TPBFreeBuild.setSize(card.Size.Width / 10 * 4, card.Size.Height / 10 * 2);
                TPBFreeBuild.Location = new Point(Convert.ToInt32(card.Size.Width * 0.3), card.Size.Height/20);
            }
        }
        private void updateCurrentCard(object sender, EventArgs e)
        {
            if (cardToMakeFee != null)
            {
                cardToMakeFee.cost = oldcardCost;
            }

            Card card = (Card)sender;

            cardToMakeFee = card;
            oldcardCost = card.cost;
        }
        private void makeCardFreeSingleUse(object sender, EventArgs e)
        {
            Game.GameAge age = player.form_.getGame().getAge();

            if (TPBFreeBuild.isActive())
            {
                if (!player.buildingWonder && age != Game.GameAge.LeaderHire1 && age != Game.GameAge.LeaderHire2 && age != Game.GameAge.AgeIII)
                {
                    if (cardToMakeFee.getCardColor() == cardColor || applyAll)
                    {
                        TPBFreeBuild.toggleActive();
                        currentlyUsing = true;
                        cardToMakeFee.cost = new Resources.Resource();
                        ignoreCancel = true;
                        player.PNLCost.update();
                    }
                }
            }
            else
            {
                if (!used)
                {
                    TPBFreeBuild.toggleActive();
                    currentlyUsing = false;
                    cardToMakeFee.cost = oldcardCost;
                    ignoreCancel = false;
                    player.PNLCost.update();
                }
                
            }
        }
        private void cardCancel(object source, EventArgs e)
        {
            if (!ignoreCancel)
            {
                if (currentlyUsing)
                {
                    currentlyUsing = false;
                    TPBFreeBuild.toggleActive();
                    //Console.WriteLine("In cancel");
                }
            }
            else
            {
                ignoreCancel = false;
            }
        }
        private void cardplayed(object source, EventArgs e)
        {
            if (currentlyUsing)
            {
                used = true;
                currentlyUsing = false;
            }            
        }
        private void resetUse(object source, EventArgs e)
        {
            if (!TPBFreeBuild.isActive())
            {
                used = false;
                TPBFreeBuild.setActive(true);
            }
        }
    }
}
