﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class ResourceDouble : CardEffect
    {
        Card card;
        Player player;

        ResourceOption resourceOption;

        Resources.Resource oldResource = new Resources.Resource();
        Resources.Resource resourceProductionOptions = new Resources.Resource();
        Resources.Resource emptyResource = new Resources.Resource();

        public ResourceDouble(Player player_, Card card_)
        {
            card = card_;
            player = player_;
        }
        public override void play()
        {
            //player.ResourceAdded += update;
            addInitialListeners();
            update();
        }
        private void update(object sender, EventArgs e)
        {
            update();
        }
        private void update()
        {
            

            Resources.Resource newAllowedProduction = buildCurrentResourceOptions();

            if (needsUpdate(newAllowedProduction))
            {
                

                if (resourceOption != null)
                {
                    oldResource = resourceOption.getCurrentResource();
                    //Resources.Resource canProduceMask = player.resources - resourceOldProduction;
                    resourceProductionOptions = buildCurrentResourceOptions();
                    //resourceProductionOptions -= resourceOption.getCurrentResource();
                    //player.addResource(resourceOption.getCurrentResource() * -1);
                    resourceOption.clear();
                }

                //resourceProductionOptions = buildCurrentResourceOptions();
                //Resources.Resource temp = buildCurrentResourceOptions();

                resourceOption = new ResourceOption(player, newAllowedProduction, card, player.form_.getGame());
                resourceOption.play();

                if (oldResource != emptyResource)
                {
                    //lockLoop = true;
                    if (resourceOption.resource.contains(oldResource))
                    {
                        resourceOption.selectResource(oldResource);
                    }
                    //lockLoop = false;
                }
                //resourceProductionOptions = buildCurrentResourceOptions();
            }
        }
        
        private bool needsUpdate(Resources.Resource newAllowedProduction)
        {
            if (resourceOption == null)
            {
                return true;
            }

            return newAllowedProduction != resourceProductionOptions;
        }
        private Resources.Resource buildCurrentResourceOptions()
        {
            Resources.Resource options = new Resources.Resource();

            options += player.getWonder().wonderManager.getStartingResource();

            foreach (Card item in player.playedCards[(int)Card.CardColor.Brown])
            {
                try
                {
                    ResourceGenerationFixed resourceGenerationFixed  = (ResourceGenerationFixed)item.immediateEffects[0];

                    options += resourceGenerationFixed.resource;
                }
#pragma warning disable 0168
                catch (InvalidCastException e)
                {
#pragma warning restore 0168
                    ResourceOption resourceOption = (ResourceOption)item.immediateEffects[0];

                    options += resourceOption.getCurrentResource();
                }
            }

            for (int count = 1; count <= 7 + 1; count++)
            {
                if (options[count] > 0)
                {
                    options[count] = 1;
                }
            }

            return options;
        }
        private void resourceCardPlayed(Card card)
        {
            try
            {
                ResourceOption resourceOption = (ResourceOption)card.immediateEffects[0];

                resourceOption.ResourceGenerationChanged += update;
            }
#pragma warning disable 0168
            catch (InvalidCastException ex)
            {
#pragma warning restore 0168
            }

            update();
        }
        private void addInitialListeners()
        {
            foreach (Card item in player.playedCards[(int)Card.CardColor.Brown])
            {
                try
                {
                    ResourceOption resourceOption = (ResourceOption)item.immediateEffects[0];

                    resourceOption.ResourceGenerationChanged += update;
                }
#pragma warning disable 0168
                catch (InvalidCastException ex)
                {
#pragma warning restore 0168
                }
            }

            player.cardPlayedHandler[(int)Card.CardColor.Brown] += resourceCardPlayed;
            player.cardPlayedHandler[(int)Card.CardColor.Gray] += resourceCardPlayed;
        }
    }
}
