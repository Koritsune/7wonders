﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class GoldPerStoneEffect : CardEffect
    {
        Player player;
        int goldPerStone;

        public GoldPerStoneEffect(Player player_, int goldPerStone_)
        {
            player = player_;
            goldPerStone = goldPerStone_;
        }
        public override void play()
        {
            Resources.Resource totalResources = new Resources.Resource();

            foreach (Card item in player.playedCards[(int)Card.CardColor.Brown])
            {
                try
                {
                    ResourceGenerationFixed resourceGeneration = (ResourceGenerationFixed)item.immediateEffects[0];
                    totalResources += resourceGeneration.resource;

                }
#pragma warning disable 0168
                catch (InvalidCastException e)
                {
#pragma warning restore 0168
                    ResourceOption resourceOption = (ResourceOption)item.immediateEffects[0];
                    totalResources += resourceOption.resource;
                }
            }

            Resources.Resource goldToAdd = new Resources.Resource();

            goldToAdd.gold = totalResources.silver * goldPerStone;
            player.addResource(goldToAdd);
            player.raiseGoldReceivedEvent();
        }
    }
}
