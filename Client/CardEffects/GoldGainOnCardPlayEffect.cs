﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class GoldGainOnCardPlayEffect: CardEffect
    {
        Player player;
        Card.CardColor color;
        Resources.Resource goldGain = new Resources.Resource();

        public GoldGainOnCardPlayEffect(Player player_, Card.CardColor color_, int goldGain_)
        {
            player = player_;
            color = color_;
            goldGain.gold = goldGain_;
        }
        public override void play()
        {
            player.cardPlayedHandler[(int)color] += goldGainTrigger;
        }
        private void goldGainTrigger(Card card)
        {
            if (active)
            {
                player.addResource(goldGain);
                player.raiseGoldReceivedEvent();
            }
        }
    }
}
