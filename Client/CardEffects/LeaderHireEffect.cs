﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Client.CardEffects
{
    class LeaderHireEffect : CardEffect
    {
        Player player;
        LeaderHirePNL leaderHirePNL;
        Card leaderCard;

        public event LeaderHirePickedHandler LeaderHirePicked;

        public delegate void LeaderHirePickedHandler(object source, AdditionalInfoAddedEvent e);

        public LeaderHireEffect(Player player_)
        {
            player = player_;
            MoreInfoNeeded = true;
        }
        public override void getMoreInfo()
        {
            leaderHirePNL = new LeaderHirePNL(player);

            leaderHirePNL.Size = new Size(360, 144 + 80 + 80 + 30);
            leaderHirePNL.Location = new Point((player.form_.getPNLServer().getPNLGame().getRightSectionXPoint() - leaderHirePNL.Size.Width) / 2, (player.form_.Size.Height - leaderHirePNL.Size.Height) / 2);

            player.form_.getPNLServer().getPNLGame().Controls.Add(leaderHirePNL);
            leaderHirePNL.BringToFront();
            leaderHirePNL.LeaderHirePicked += raiseLeaderHiredEvent;
            leaderHirePNL.LeaderHireCancel += cancelLeaderHiredEvent;
            leaderHirePNL.getChoices();
        }
        public override void receiveMoreInfo(string info)
        {
            if (!info.Equals("Canceled"))
            {
                //Console.WriteLine(info.Replace(Constants.DELIM_FIELD, '#'));
                String leaderToHire = info.Substring(0, info.IndexOf(Constants.DELIM_FIELD));

                if (info.Length > leaderToHire.Length)
                {
                    info = info.Substring(leaderToHire.Length + 1);
                }

                leaderCard = new Card(leaderToHire, player);
            }
            else
            {

            }
        }
        public override void play()
        {
            if (leaderCard != null)
            {
                player.playCard(leaderCard);

                if (player != player.form_.getGame().getPlayers()[0])
                {
                    player.removeUnknownLeader();
                }
                else
                {
                    player.removeKnownLeader(leaderCard.cardLocation);
                }
            }
        }
        private void raiseLeaderHiredEvent(object sender, EventArgs e)
        {
            Card leaderPicked = (Card)sender;

            player.form_.getPNLServer().getPNLGame().Controls.Remove(leaderHirePNL);

            if (LeaderHirePicked != null)
            {
                LeaderHirePicked(this, new CardEffect.AdditionalInfoAddedEvent(leaderPicked.cardLocation));
            }
        }
        private void cancelLeaderHiredEvent(object sender, EventArgs e)
        {
            player.form_.getPNLServer().getPNLGame().Controls.Remove(leaderHirePNL);

            if (LeaderHirePicked != null)
            {
                LeaderHirePicked(this, new CardEffect.AdditionalInfoAddedEvent("Canceled"));
            }
        }
    }
}
