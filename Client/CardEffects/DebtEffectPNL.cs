﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client.CardEffects
{
    class DebtEffectPNL : Panel
    {
        int debtAmount;
        TransparentLabel LBLText = new TransparentLabel();
        public ToggleGroupBox PNLDebtOptions {get; private set;}
        List<ToggleTextButton> DebtOptions = new List<ToggleTextButton>();

        public event DebtPayedHandler DebtPayed;

        public delegate void DebtPayedHandler(object source, DebtPayedEvent e);
        public class DebtPayedEvent : EventArgs
        {
            private string EventInfo;
            public DebtPayedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public DebtEffectPNL(int debtAmount_)
        {
            PNLDebtOptions = new ToggleGroupBox();
            BackColor = Color.Transparent;
            Controls.Add(LBLText);
            Controls.Add(PNLDebtOptions);

            PNLDebtOptions.BackColor = Color.Transparent;
            PNLDebtOptions.Size = new Size(300, 100);
            PNLDebtOptions.Location = new Point(0, 100);
            PNLDebtOptions.setButtonSize(90, 30);
            PNLDebtOptions.valueChange += debtPaid;
            LBLText.setSize(300, 100);
            LBLText.Location = new Point(0, 0);
            Size buttonSize = new Size(100,30);
            debtAmount = debtAmount_;

            LBLText.setText("Please select the number of coins you want to pay (Suffer -1 point for each unpaid coin)");
            LBLText.setFontSize(12);

            for (int count = 0; count <= debtAmount; count++)
            {
                PNLDebtOptions.addButton("" + count);
            }
        }
        private void debtPaid(object source, EventArgs e)
        {
            if (DebtPayed != null)
            {
                DebtPayed(this, new DebtPayedEvent("Debt Payed."));
            }
        }
    }
}
