﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class ScienceChoiceEffect : CardEffect
    {
        Player player;

        public ScienceChoiceEffect(Player player_)
        {
            player = player_;
        }

        public override void play()
        {
            player.scienceChoice++;
        }
    }
}
