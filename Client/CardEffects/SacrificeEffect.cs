﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Client.CardEffects
{
    class SacrificeEffect : VictoryPointsEffect
    {
        Player player;
        Card sacrifice;

        SacrificePNL sacrificePNL;

        public event SacrificePickedHandler SacrificePicked;

        public delegate void SacrificePickedHandler(object source, AdditionalInfoAddedEvent e);

        public SacrificeEffect(Player player_)
        {
            MoreInfoNeeded = true;
            player = player_;
        }

        public override void getMoreInfo()
        {
            //Console.WriteLine("In Get more info for sacrifice effect.");
            sacrificePNL = new SacrificePNL(player);

            sacrificePNL.Size = new Size(360, 144 + 80 + 80);
            sacrificePNL.Location = new Point((player.form_.getPNLServer().getPNLGame().getRightSectionXPoint() - sacrificePNL.Size.Width) / 2, (player.form_.Size.Height - sacrificePNL.Size.Height) / 2);

            player.form_.getPNLServer().getPNLGame().Controls.Add(sacrificePNL);
            sacrificePNL.BringToFront();
            sacrificePNL.SacrificePicked += sacrificePicked;
        }

        private void sacrificePicked(object sender, EventArgs e)
        {
            sacrifice = (Card)sender;

            if (!sacrifice.cardLocation.Equals(Constants.PICTURES + "Leaders\\Back"))
            {
                player.form_.getPNLServer().getPNLGame().Controls.Remove(sacrificePNL);
            }
            
            SacrificePicked(this, new AdditionalInfoAddedEvent(sacrifice.cardLocation));
        }

        public override int generateVicotryPoints()
        {
            return sacrifice.cost.gold * 2;
        }

        public override void receiveMoreInfo(string info)
        {
            if (player != player.form_.getGame().getPlayers()[0])
            {
                //Console.WriteLine(info.Replace(Constants.DELIM_FIELD, '#'));
                String leaderToSacrifice = info.Substring(0, info.IndexOf(Constants.DELIM_FIELD));

                if (info.Length > leaderToSacrifice.Length)
                {
                    info = info.Substring(leaderToSacrifice.Length + 1);
                }

                sacrifice = new Card(leaderToSacrifice, player);
            }
        }
        public override void play()
        {
            if (player != player.form_.getGame().getPlayers()[0] && !sacrifice.cardLocation.Equals(Constants.PICTURES + "Leaders\\Back"))
            {
                player.removeKnownHiredLeader(sacrifice.cardLocation);
            }
        }
    }
}
