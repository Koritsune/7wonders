﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class LeadersFreeEffect : CardEffect
    {
        Player player;

        public LeadersFreeEffect(Player player_)
        {
            player = player_;
        }

        public override void play()
        {
            player.CardSelected += makeFree;
        }

        private void makeFree(object sender, EventArgs e)
        {
            if (active)
            {
                Card card = (Card)sender;

                if (card.getCardColor() == Card.CardColor.White)
                {
                    card.cost = new Resources.Resource();
                }

                player.PNLCost.update();
            }
        }
    }
}
