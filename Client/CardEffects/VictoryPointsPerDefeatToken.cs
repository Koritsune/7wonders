﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class VictoryPointsPerDefeatToken : VictoryPointsEffect
    {
        int points;
        Player target;

        public VictoryPointsPerDefeatToken(Player target_, int points_)
        {
            points = points_;
            target = target_;
        }
        public override int generateVicotryPoints()
        {
            return target.PNLArmyVictoryToken.getNbDefeatTokens() * points;
        }
    }
}
