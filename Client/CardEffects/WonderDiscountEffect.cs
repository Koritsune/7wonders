﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class WonderDiscountEffect : CardEffect
    {
        Player player;
        bool inUse = false;

        public WonderDiscountEffect(Player player_)
        {
            player = player_;
        }
        public override void play()
        {
            player.BuildingWonderChanged += wonderStatusChanged;
        }
        private void wonderStatusChanged(object source, EventArgs e)
        {
            if (active)
            {
                if (player.buildingWonder)
                {
                    if (!inUse)
                    {
                        player.PNLCost.increaseVariance();
                        player.PNLCost.update();
                        inUse = true;
                    }
                }
                else
                {
                    //Console.WriteLine("in else of wonderstatus changed.");
                    if (inUse)
                    {
                        //Console.WriteLine("in inuse of wonderstatus changed.");
                        player.PNLCost.decreaseVariance();
                        player.PNLCost.update();
                        inUse = false;
                    }
                }
            }
        }
        
    }
}
