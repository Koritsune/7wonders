﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    public class CardEffect
    {
        protected bool active = true;

        public bool MoreInfoNeeded { get; protected set; }
        public virtual void play() { }
        public virtual void getMoreInfo() { }
        public virtual void receiveMoreInfo(String info) { }
        public virtual void cancelAbilities() 
        {
            active = false;
        }
        public virtual void receiveEndofGameInfo(String info) { }
        public virtual void getEndofGameInfo() { }
        public class AdditionalInfoAddedEvent : EventArgs
        {
            private string EventInfo;
            public AdditionalInfoAddedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }
    }
}
