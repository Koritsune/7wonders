﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Drawing;

namespace Client.CardEffects
{
    class DebtEffect : CardEffect
    {
        protected Player player;
        protected DebtEffectPNL PNLDebt;
        protected int debtAmount;
        protected Form1 form;
        bool paymentMade = false;
        int unlockCount = 0;

        protected DebtEffect(Form1 form_, Player player_)
        {
            form = form_;
            player = player_;
        }

        private delegate void playD();

        public override void play()
        {
            if (form.InvokeRequired)
            {
                playD d = new playD(play);
                form.Invoke(d,new object[]{});

            }
            else
            {
                PNLDebt.Size = new System.Drawing.Size(300, 200);
                PNLDebt.Location = new Point((player.form_.getPNLServer().getPNLGame().getRightSectionXPoint() - PNLDebt.Size.Width) / 2, (player.form_.Size.Height - PNLDebt.Size.Height) / 2);
                
                foreach (Player item in form.getGame().getPlayers())
                {
                    if (item != player)
                    {
                        //item.ResourceAdded += incUnlock;
                        item.PNLDebtToken.DebtTokenReceived += incUnlock;
                    }
                }
                
                if (player != form.getGame().getPlayers()[0])
                {
                    PNLDebt.DebtPayed += paymentMadeTrigger;
                    form.getPNLServer().getPNLGame().Controls.Add(PNLDebt);
                    PNLDebt.PNLDebtOptions.Text = "Debt to pay: " + debtAmount;
                    PNLDebt.BringToFront();
                    Thread paymentThread = new Thread(() => getPayment());
                    paymentThread.Start();
                }
            }
        }
        private void paymentMadeTrigger(object source, EventArgs e)
        {
            paymentMade = true;
        }
        private void incUnlock(object source, EventArgs e)
        {
            unlockCount++;

            if (unlockCount == form.getGame().getnbPlayers() - 1)
            {
                form.getGame().serverConnection.playManager.unlock();
            }
        }
        private void getPayment()
        {
            while (!paymentMade) ;
            //form.getGame().serverConnection.playManager.unlock();
            int amountPaid = Convert.ToInt32(PNLDebt.PNLDebtOptions.getSelectedValue());
            //Console.WriteLine("User has chosen to pay: " + amountPaid);

            if (amountPaid > player.resources.gold)
            {
                amountPaid = player.resources.gold;
            }

            form.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "COINNOW" + Constants.DELIM_FIELD + form.getConfig().getUsername() + Constants.DELIM_FIELD + (amountPaid * -1));
            form.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "DEBT" + Constants.DELIM_FIELD + form.getConfig().getUsername() + Constants.DELIM_FIELD + (debtAmount - amountPaid));
            finishDebt();
            //form.getGame().sendMessage
        }

        private delegate void finishDebtD();

        private void finishDebt()
        {
            if (form.InvokeRequired)
            {
                finishDebtD d = new finishDebtD(finishDebt);
                form.Invoke(d,new object[]{});

            }
            else
            {
                form.getPNLServer().getPNLGame().Controls.Remove(PNLDebt);
            }
        }
    }
}
