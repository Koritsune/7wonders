﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class GoldIncomeOnWarVictory : CardEffect
    {
        Player player;
        Resources.Resource gold = new Resources.Resource();

        public GoldIncomeOnWarVictory(Player player_, int gold_)
        {
            player = player_;
            gold.gold = gold_;
        }
        private void receiveGold(object source, EventArgs e)
        {
            player.addResource(gold);
            player.raiseGoldReceivedEvent();
        }
        public override void play()
        {
            player.PNLArmyVictoryToken.VictoryTokenReceived += receiveGold;
        }
    }
}
