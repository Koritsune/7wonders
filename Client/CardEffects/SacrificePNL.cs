﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client.CardEffects
{
    class SacrificePNL : Panel
    {
        Player player;
        TranslucentTextBox LBLInfo = new TranslucentTextBox();
        TranslucentTextBox LBLCardInfo = new TranslucentTextBox();
        List<Card> leaders = new List<Card>();
        int currentSelectedIndex = 0;
        bool scrollCardInfo = false;

        Timer timer = new Timer();
        Point CursorLocation;

        public event SacrificePickedHandler SacrificePicked;

        public delegate void SacrificePickedHandler(object source, SacrificePickedEvent e);
        public class SacrificePickedEvent : EventArgs
        {
            private string EventInfo;
            public SacrificePickedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public SacrificePNL(Player player_)
        {
            player = player_;
            BackColor = Color.Transparent;

            LBLCardInfo.BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
            LBLCardInfo.getTextBox().ForeColor = Color.White;
            LBLCardInfo.getTextBox().ReadOnly = true;
            LBLCardInfo.setFontSize(12);
            LBLCardInfo.setSize(360, 80);
            LBLCardInfo.Location = new Point(0, 80);
            LBLCardInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            LBLCardInfo.getTextBox().ScrollBars = RichTextBoxScrollBars.ForcedVertical;
            //LBLCardInfo.getTextBox().Text = "Select the leader you wish to sacrifice. You will no longer receive the chosen leaders effect but you will receive victory points equal to double its cost.";

            LBLInfo.BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
            LBLInfo.getTextBox().ForeColor = Color.White;
            LBLInfo.getTextBox().ReadOnly = true;
            LBLInfo.setFontSize(12);
            LBLInfo.setSize(360, 80);
            LBLInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            LBLInfo.getTextBox().Text = "Select the leader you wish to sacrifice. You will no longer receive the chosen leaders effect but you will receive victory points equal to double its cost.";

            Controls.Add(LBLCardInfo);
            Controls.Add(LBLInfo);

            for (int count = 0; count < player.playedCards[(int)Card.CardColor.White].Count; count ++)
            {
                Card leaderToAdd = new Card(player.playedCards[(int)Card.CardColor.White][count].cardLocation,player);

                leaderToAdd.hideButtons();
                leaderToAdd.setSize(80, 144);
                
                leaderToAdd.Location = new Point(count * 80, 160);
                leaders.Add(leaderToAdd);
                leaderToAdd.CardClick += sacrificePicked;
                leaderToAdd.showPictureOnly();
                leaderToAdd.CardDoubleClick += completeSacrifice;
                Controls.Add(leaderToAdd);
            }

            if (leaders.Count == 0)
            {
                Card nullLeader = new Card(Constants.PICTURES + "Leaders\\Back", player);
                SacrificePicked(nullLeader, new SacrificePickedEvent("Sacrifice Picked."));
            }
            else
            {
                LBLCardInfo.getTextBox().Text = leaders[0].getDesc();
            }

            timer.Tick += scrollCard;
            timer.Interval = 100;
            timer.Start();
        }

        private void sacrificePicked(object sender, EventArgs e)
        {
            Card leaderToSacrifice = (Card)sender;
            int count = 0;

            for (; count < leaders.Count; count++)
            {
                if (leaderToSacrifice == leaders[count])
                {
                    break;
                }
            }

            currentSelectedIndex = count;
            LBLCardInfo.getTextBox().Text = leaderToSacrifice.getDesc();
                        
        }

        private void completeSacrifice(object sender, EventArgs e)
        {
            Card leaderToSacrifice = (Card)sender;
            int count = 0;

            for (; count < leaders.Count; count++)
            {
                if (leaderToSacrifice == leaders[count])
                {
                    break;
                }
            }

            completeSacrifice();
        }

        private void completeSacrifice()
        {
            Card leaderToSacrifice = leaders[currentSelectedIndex];

            player.removeKnownHiredLeader(leaderToSacrifice.cardLocation);
            SacrificePicked(leaderToSacrifice, new SacrificePickedEvent(leaderToSacrifice.cardLocation));
        }
        private void scrollCard(object sender, EventArgs e)
        {
            if (!scrollCardInfo)
            {
                Point temp = player.form_.DesktopLocation;
                temp.X += Location.X + LBLCardInfo.Location.X + LBLCardInfo.Size.Width - 4;
                temp.Y += Location.Y + LBLCardInfo.Location.Y + LBLCardInfo.Size.Height - 4;
                CursorLocation = Cursor.Position;
                Cursor.Position = temp;
                scrollCardInfo = true;
            }
            else
            {
                Cursor.Position = CursorLocation;
                scrollCardInfo = false;
                timer.Stop();
            }
        }
    }
}
