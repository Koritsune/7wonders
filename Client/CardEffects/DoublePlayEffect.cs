﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class DoublePlayEffect : CardEffect
    {
        Player player;

        public DoublePlayEffect(Player player_)
        {
            player = player_;
        }
        public override void play()
        {
            player.DoublePlay = true;
        }
    }
}
