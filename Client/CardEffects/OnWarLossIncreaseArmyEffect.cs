﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class OnWarLossIncreaseArmyEffect : CardEffect
    {
        Player player;
        int totalArmyAdded = 0;

        public OnWarLossIncreaseArmyEffect(Player player_)
        {
            player = player_;
        }
        private void increaseArmy(object source, EventArgs e)
        {
            if (active)
            {
                player.increaseArmy(player.PNLArmyVictoryToken.getNbDefeatTokens() - totalArmyAdded);
                totalArmyAdded = player.PNLArmyVictoryToken.getNbDefeatTokens();
            }
        }
        public override void play()
        {
            totalArmyAdded = player.PNLArmyVictoryToken.getNbDefeatTokens();
            player.increaseArmy(totalArmyAdded);
            player.form_.getGame().GameAgeChange += increaseArmy;
        }
        public override void cancelAbilities()
        {
            base.cancelAbilities();

            player.increaseArmy(-1 * totalArmyAdded);
        }
    }
}
