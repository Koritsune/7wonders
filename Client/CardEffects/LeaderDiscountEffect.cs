﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class LeaderDiscountEffect : CardEffect
    {
        Player player;
        int discount;

        int oldGoldCost;
        Card currentCard;

        public LeaderDiscountEffect(Player player_, int discount_)
        {
            player = player_;
            discount = discount_;
        }

        public override void play()
        {
            player.CardSelected += applyDiscount;
            //player.PNLCost.cardCancel += revertDiscount;
        }

        private void applyDiscount(object sender, EventArgs e)
        {
            if (currentCard != null)
            {
                if (currentCard.getCardColor() == Card.CardColor.White)
                {
                    currentCard.cost.gold = oldGoldCost;
                }
            }

            Card card = (Card)sender;
            
            if (card.getCardColor() == Card.CardColor.White)
            {
                currentCard = card;

                int goldCost = card.cost.gold;
                oldGoldCost = goldCost;

                goldCost -= discount;

                if (goldCost < 0)
                {
                    card.cost.gold = 0;
                }
                else
                {
                    card.cost.gold = goldCost;
                }

                if (player.form_.getGame().getAge() != Game.GameAge.LeaderRoster)
                {
                    player.PNLCost.update();
                }
            }
        }

        private void revertDiscount(object sender, EventArgs e)
        {
            if (currentCard != null)
            {
                if (currentCard.getCardColor() == Card.CardColor.White)
                {
                    currentCard.cost.gold = oldGoldCost;
                }
            }
        }
    }
}
