﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class ScienceEffect : CardEffect
    {
        Player player;
        int scienceType;

        public ScienceEffect(Player player_, int scienceType_)
        {
            player = player_;
            scienceType = scienceType_;
        }

        public override void play()
        {
            player.SCIENCE[scienceType]++;
        }

        public override void cancelAbilities()
        {
            player.SCIENCE[scienceType]--;
        }
    }
}
