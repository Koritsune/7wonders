﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardEffects
{
    class VictoryPointsPerWonderEffect : VictoryPointsEffect
    {
        Player target;
        int pointsPerWonder;

        public VictoryPointsPerWonderEffect(Player target_, int pointsPerWonder_)
        {
            target = target_;
            pointsPerWonder = pointsPerWonder_;
        }
        public override int generateVicotryPoints()
        {
            return target.getWonder().wonderManager.getNbofWondersBuilt() * pointsPerWonder;
        }
    }
}
