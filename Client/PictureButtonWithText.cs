﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client
{
    public class PictureButtonWithText : Panel
    {
        TransparentLabel LBLText = new TransparentLabel();

        public event ButtonClickHandler ButtonClick;

        public delegate void ButtonClickHandler(object source, ButtonClickEvent e);

        public class ButtonClickEvent : EventArgs
        {
            private string EventInfo;
            public ButtonClickEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public PictureButtonWithText(String picture)
        {
            Controls.Add(LBLText);
            BackgroundImage = Image.FromFile(picture);
            BackgroundImageLayout = ImageLayout.Stretch;

            this.MouseClick += raiseButtonClickEvent;
            LBLText.MouseClick += raiseButtonClickEvent;
            LBLText.getLabel().MouseClick += raiseButtonClickEvent;

            LBLText.setFontSize(12);
            LBLText.getLabel().TextAlign = ContentAlignment.MiddleCenter;
            LBLText.BackgroundImageLayout = ImageLayout.Stretch;

            this.Resize += objectResized;
        }
        delegate void setTextD(String ans);

        public void setText(String ans)
        {
            if (this.InvokeRequired)
            {
                setTextD d = new setTextD(setText);
                this.Invoke(d, new object[] { ans});
            }
            else
            {
                LBLText.setText(ans);
            }
        }
        public void setSize(int width, int height)
        {
            this.Size = new Size(width, height);
            LBLText.Size = new Size(width, 20);
            LBLText.Location = new Point(0, (height - 20) / 2);
        }
        private void raiseButtonClickEvent(object sender, EventArgs e)
        {
            if (ButtonClick != null)
            {
                ButtonClick(this, new ButtonClickEvent("Button was Clicked."));
            }
        }
        private void objectResized(object sender, EventArgs e)
        {
            //Console.WriteLine("In resizing.");
            LBLText.setSize(Width, 20);
            LBLText.Location = new Point(0, (Height - 20)/2);
        }
        public void setTextColor(Color color)
        {
            LBLText.ForeColor = color;
        }
        public void addSemiTransparentBack()
        {
            LBLText.BackgroundImage = Image.FromFile(Constants.ICONS + "WhiteTransparent.PNG");
        }
        public void removeSemiTransparentBack()
        {
            LBLText.BackgroundImage = null;
        }
        public void hideText()
        {
            LBLText.Visible = false;
        }
        public void showText()
        {
            LBLText.Visible = true;
        }
    }
}
