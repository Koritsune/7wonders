﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client
{
    public class WonderChoices : Panel
    {
        Form1 form_;

        const int ICONS_PER_ROW = 4;
        const int ICON_WIDTH = 160;
        const int ICON_HEIGHT = 160;

        List<Wonder> wonders;
        List<TogglePicutreButton> wonderButtons = new List<TogglePicutreButton>();

        TogglePictureGroupBox GBWonders = new TogglePictureGroupBox();
        TogglePictureGroupBox GBWondersLeaders = new TogglePictureGroupBox();
        TogglePictureGroupBox GBWondersCities = new TogglePictureGroupBox();

        TextButton TBBase = new TextButton("Base");
        TextButton TBLeaders = new TextButton("Leaders");
        TextButton TBCities = new TextButton("Cities");
        TextButton TBSave = new TextButton("Save");
        TextButton TBCancel = new TextButton("Cancel");

        TransparentLabel LBLMessageLeaders = new TransparentLabel();
        TransparentLabel LBLMessageCities = new TransparentLabel();

        bool enable = true;

        int wondersBase = 0;
        int wondersLeaders = 0;
        int wondersCities = 0;

        public WonderChoices(Form1 form)
            : base()
        {
            form_ = form;
            wonders = form_.getConfig().getWonders();

            form_.getGame().leadersChange += leadersChanged;
            form_.getGame().citiesChange += citiesChanged;

            LBLMessageLeaders.BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
            LBLMessageLeaders.BackgroundImageLayout = ImageLayout.Stretch;
            LBLMessageLeaders.setSize(ICON_WIDTH * ICONS_PER_ROW, 40);
            LBLMessageLeaders.getLabel().ForeColor = Color.White;
            LBLMessageLeaders.Location = new Point(50, 50);
            LBLMessageLeaders.setFontSize(10);
            LBLMessageLeaders.setText("These wonders will not be dealt because the Leaders expansion is disabled.");

            LBLMessageCities.BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
            LBLMessageCities.BackgroundImageLayout = ImageLayout.Stretch;
            LBLMessageCities.setSize(ICON_WIDTH * ICONS_PER_ROW, 40);
            LBLMessageCities.getLabel().ForeColor = Color.White;
            LBLMessageCities.Location = new Point(50, 50);
            LBLMessageCities.setFontSize(10);
            LBLMessageCities.setText("These wonders will not be dealt because the Cities expansion is disabled.");

            //LBLMessage.


            BackgroundImage = Image.FromFile(Constants.ICONS + "WonderChoices.PNG");
            BackgroundImageLayout = ImageLayout.Stretch;

            for (int count = 0; count < wonders.Count; count++){
                String activePicture = Constants.WONDERS_FOLDER + wonders[count].getName() + " Active.PNG";
                String inActivePicture = Constants.WONDERS_FOLDER + wonders[count].getName() + " InActive.PNG";
                TogglePicutreButton currentWonder = new TogglePicutreButton(activePicture,inActivePicture);
                
                currentWonder.Size = new Size(ICON_WIDTH, ICON_HEIGHT);
                
                if (form_.getConfig().getWonders()[count].isActive())
                {
                    currentWonder.setActive(true);
                }

                if (!wonders[count].isCities() && !wonders[count].isLeaders())
                {
                    currentWonder.Location = nextPoint();
                    wondersBase++;
                    GBWonders.add(currentWonder);
                }
                else if (wonders[count].isLeaders())
                {
                    currentWonder.Location = nextPointLeaders();
                    wondersLeaders++;
                    GBWondersLeaders.add(currentWonder);
                }
                else if (wonders[count].isCities())
                {
                    currentWonder.Location = nextPointCities();
                    wondersCities++;
                    GBWondersCities.add(currentWonder);
                }
                //GBWonders.Controls.Add(currentWonder);
                
                wonderButtons.Add(currentWonder);

            }

            int heightIndex = maxWonderCount() / ICONS_PER_ROW + (maxWonderCount() % ICONS_PER_ROW == 0 ? 0 : 1);

            GBWonders.Text = "Wonders (Base)";
            GBWonders.Location = new Point(40, 104);
            GBWonders.BackColor = Color.Transparent;
            GBWonders.Size = new Size(ICONS_PER_ROW * ICON_WIDTH + 20, heightIndex * ICON_HEIGHT + 30);
            GBWonders.Font = new Font(GBWonders.Font.FontFamily.Name, 10);

            GBWondersLeaders.Text = "Wonders (Leaders)";
            GBWondersLeaders.Location = new Point(40, 104);
            GBWondersLeaders.BackColor = Color.Transparent;
            GBWondersLeaders.Size = new Size(ICONS_PER_ROW * ICON_WIDTH + 20, heightIndex * ICON_HEIGHT + 30);
            GBWondersLeaders.Font = new Font(GBWondersLeaders.Font.FontFamily.Name, 10);

            GBWondersCities.Text = "Wonders (Cities)";
            GBWondersCities.Location = new Point(40, 104);
            GBWondersCities.BackColor = Color.Transparent;
            GBWondersCities.Size = new Size(ICONS_PER_ROW * ICON_WIDTH + 20, heightIndex * ICON_HEIGHT + 30);
            GBWondersCities.Font = new Font(GBWondersCities.Font.FontFamily.Name, 10);

            TBBase.startHover();
            TBBase.Location = new Point(730, 150);
            TBBase.setSize(100, 30);
            TBBase.setFontSize(Constants.FONT_SIZE);

            TBLeaders.startHover();
            TBLeaders.Location = new Point(730, 200);
            TBLeaders.setSize(100, 30);
            TBLeaders.setFontSize(Constants.FONT_SIZE);
            
            TBCities.startHover();
            TBCities.Location = new Point(730, 250);
            TBCities.setSize(100, 30);
            TBCities.setFontSize(Constants.FONT_SIZE);
            
            TBSave.startHover();
            TBSave.Location = new Point(730, 300);
            TBSave.setSize(100, 30);
            TBSave.setFontSize(Constants.FONT_SIZE);

            TBCancel.startHover();
            TBCancel.Location = new Point(730, 350);
            TBCancel.setSize(100, 30);
            TBCancel.setFontSize(Constants.FONT_SIZE);

            Controls.Add(GBWonders);
            Controls.Add(GBWondersLeaders);
            Controls.Add(GBWondersCities);

            Controls.Add(TBBase);
            Controls.Add(TBLeaders);
            Controls.Add(TBCities);
            Controls.Add(TBCancel);
            Controls.Add(TBSave);

            Controls.Add(LBLMessageLeaders);
            Controls.Add(LBLMessageCities);

            GBWondersLeaders.Visible = false;
            GBWondersCities.Visible = false;

            TBCancel.MouseClick += cancel;
            TBCancel.getLabel().MouseClick += cancel;

            TBSave.MouseClick += save;
            TBSave.getLabel().MouseClick += save;

            TBBase.MouseClick += showBase;
            TBBase.getLabel().MouseClick += showBase;

            TBLeaders.MouseClick += showLeaders;
            TBLeaders.getLabel().MouseClick += showLeaders;

            TBCities.MouseClick += showCities;
            TBCities.getLabel().MouseClick += showCities;

            showBase();
        }
        private Point nextPoint()
        {
            int xIndex = wondersBase % ICONS_PER_ROW;
            int yIndex = wondersBase / ICONS_PER_ROW;
            return new Point(xIndex * ICON_WIDTH + 10 , yIndex * ICON_HEIGHT + 19);
        }
        private Point nextPointLeaders()
        {
            int xIndex = wondersLeaders % ICONS_PER_ROW;
            int yIndex = wondersLeaders / ICONS_PER_ROW;
            return new Point(xIndex * ICON_WIDTH + 10, yIndex * ICON_HEIGHT + 19);
        }
        private Point nextPointCities()
        {
            int xIndex = wondersCities % ICONS_PER_ROW;
            int yIndex = wondersCities / ICONS_PER_ROW;
            return new Point(xIndex * ICON_WIDTH + 10, yIndex * ICON_HEIGHT + 19);
        }
        private void save(object sender, EventArgs e)
        {
            form_.getPNLServer().showScrollBar();
            this.SendToBack();

            if (!minWonders())
            {
                form_.getPNLServer().receiveSystemMessage("The given wonder settings could not be set because with the current expansion settings the number of wonders to be dealt would be less than 3.");
                for (int count = 0; count < wonderButtons.Count; count++)
                {
                    wonderButtons[count].setActive(wonders[count].isActive());
                }
                return;
            }

            if (enable)
            {
                for (int count = 0; count < wonderButtons.Count; count++)
                {
                    if (wonderButtons[count].isActive() != wonders[count].isActive())
                    {
                        wonders[count].setActive(wonderButtons[count].isActive());
                        /*
                         send message to clients of update
                         */
                        String msg = "OPTIONS" + Constants.DELIM_FIELD + "WONDER" + Constants.DELIM_FIELD + count + Constants.DELIM_FIELD;
                        msg += wonderButtons[count].isActive() ? "1" : "0";

                        form_.getPNLMainContainer().getServer().sendMessage(msg);
                    }
                }
            }

            
        }
        private void cancel(object sender, EventArgs e)
        {
            for (int count = 0; count < wonderButtons.Count; count++)
            {
                wonderButtons[count].setActive(wonders[count].isActive());
            }

            form_.getPNLServer().showScrollBar();
            this.SendToBack();
        }
        public void setEnable(bool ans)
        {
            enable = ans;

            for (int count = 0; count < wonderButtons.Count; count++)
            {
                wonderButtons[count].setEnable(ans);
            }
        }
        delegate void setWonderStatusD(int index, bool ans);

        public void setWonderStatus(int index, bool ans)
        {
            if (this.InvokeRequired)
            {
                setWonderStatusD d = new setWonderStatusD(setWonderStatus);
                this.Invoke(d, new object[] { index, ans });
            }
            else {
                wonderButtons[index].setActive(ans);
            }
        }
        private void showBase(object sender, EventArgs e)
        {
            showBase();
        }
        private void showLeaders(object sender, EventArgs e)
        {
            GBWonders.Visible = false;
            GBWondersLeaders.Visible = true;
            GBWondersCities.Visible = false;

            LBLMessageLeaders.Visible = !form_.getGame().isLeaders();
            LBLMessageCities.Visible = false;
        }
        private void showCities(object sender, EventArgs e)
        {
            GBWonders.Visible = false;
            GBWondersLeaders.Visible = false;
            GBWondersCities.Visible = true;

            LBLMessageLeaders.Visible = false;
            LBLMessageCities.Visible = !form_.getGame().isCities();
        }
        private int maxWonderCount()
        {
            return Math.Max(Math.Max(wondersBase, wondersLeaders), wondersLeaders);
        }

        delegate void changeVisibilityD(Control control, bool ans);

        public void changeVisibility(Control control, bool ans)
        {
            if (this.InvokeRequired)
            {
                changeVisibilityD d = new changeVisibilityD(changeVisibility);
                this.Invoke(d, new object[] { control, ans });
            }
            else
            {
                control.Visible = ans;
            }
        }

        private void leadersChanged(object sender, EventArgs e)
        {
            if (form_.getConfig().isHost() && form_.getGame().getnbPlayers() > 0 )
            {
                if (!minWonders())
                {
                    form_.getConfig().setCities(true);
                    form_.getPNLMainContainer().getServer().sendMessage("OPTIONS" + Constants.DELIM_FIELD + "EXP" + Constants.DELIM_FIELD + "0" + Constants.DELIM_FIELD + "1");
                    form_.getPNLServer().receiveSystemMessage("Leaders could not be disabled because it would make the number of wonders to be dealt less than 3.");
                    return;
                }
            }
            if (GBWondersLeaders.Visible)
            {
                Game game = (Game)sender;

                changeVisibility(LBLMessageLeaders, !game.isLeaders());
            }
        }

        private void citiesChanged(object sender, EventArgs e)
        {
            if (form_.getConfig().isHost() && form_.getGame().getnbPlayers() > 0)
            {
                if (!minWonders())
                {
                    form_.getConfig().setCities(true);
                    form_.getPNLMainContainer().getServer().sendMessage("OPTIONS" + Constants.DELIM_FIELD + "EXP" + Constants.DELIM_FIELD + "1" + Constants.DELIM_FIELD + "1");
                    form_.getPNLServer().receiveSystemMessage("Cities could not be disabled because it would make the number of wonders to be dealt less than 3.");
                    return;
                }
            }

            if (GBWondersCities.Visible)
            {
                Game game = (Game)sender;

                changeVisibility(LBLMessageCities, !game.isCities());
            }
        }
        private void showBase()
        {
            GBWonders.Visible = true;
            GBWondersLeaders.Visible = false;
            GBWondersCities.Visible = false;

            LBLMessageLeaders.Visible = false;
            LBLMessageCities.Visible = false;
        }
        private bool minWonders()
        {
            int active = 0;
            for (int count = 0; count < wonderButtons.Count; count++)
            {
                if (wonderButtons[count].isActive() && !wonders[count].isCities() && !wonders[count].isLeaders())
                {
                    active++;
                }
                else if (wonderButtons[count].isActive() && form_.getGame().isLeaders() && wonders[count].isLeaders())
                {
                    active++;
                }
                else if (wonderButtons[count].isActive() && form_.getGame().isCities() && wonders[count].isCities())
                {
                    active++;
                }
            }
            return active >= 3;
        }
    }
}
