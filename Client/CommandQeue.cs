﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace Client
{
    public class CommandQeue
    {
        Queue<String> commands = new Queue<string>();
        String data = "";
        Socket handler;

        public CommandQeue(Socket handler_) {
            handler = handler_;
        }
        public String Dequeue() {
            return commands.Dequeue();
        }
        public void read() {
            try
            {
                
                byte[] bytes = new Byte[1024];
                //String data = "";
                while (true)
                {
                    bytes = new byte[1024];
                    int bytesRec = handler.Receive(bytes);
                    data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                    if (data.IndexOf(Constants.DELIM_STREAM) > -1)
                    {
                        break;
                    }
                }
                ////Console.WriteLine(data);
                while (data.IndexOf(Constants.DELIM_STREAM) != -1)
                {
                    ////Console.WriteLine("Raw data is: " + data.Replace(Constants.DELIM_FIELD, '#').Replace(Constants.DELIM_STREAM, '!'));
                    String command = data.Substring(0, data.IndexOf(Constants.DELIM_STREAM));
                    if (data.Length == data.IndexOf(Constants.DELIM_STREAM) + 1) //if stream delimtere is the last character in data
                    {
                        data = "";
                    }
                    else
                    {
                        data = data.Substring(command.Length + 1);
                    }
                    ////Console.WriteLine("In Reading Loop with : " + command.Replace(Constants.DELIM_FIELD,'#'));
                    commands.Enqueue(command);
                }
                //commands.Enqueue(
                //return data.Remove(data.Length - 1, 1);
            }
            #pragma warning disable 0168
            catch (SocketException ex)
            {
                //Console.WriteLine("Socket Exception in commandQeue");
            }
            catch (ObjectDisposedException ex)
            {
                //Console.WriteLine("ObjectDisposed Exception in commandqueue");
            }
            #pragma warning restore 0168
        }
        public bool isEmpty() {
            return commands.Count == 0;
        }
        public String next() {
            if (commands.Count == 0)
            {
                read();
            }
            if (commands.Count == 0)
            {
                return "EXIT" + Constants.DELIM_FIELD;
            }
            else
            {
                String msgTemp = commands.Dequeue();

                if (msgTemp.Equals(""))
                {
                    return "ACKREQUEST" + Constants.DELIM_FIELD;
                }

                return msgTemp;
            }
        }
    }
}
