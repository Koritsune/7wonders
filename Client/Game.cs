﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Client
{
    public class Game
    {
        Config config;
        Form1 form_;

        byte[] bytes = new byte[1024];
        Socket handler;

        String IP;
        int Port;

        public MessageOutWithConfirm MSGOut;
        public int turn = 0;

        //int nbPlayers = 0;
        bool connected = false;
        bool started = false;
        bool playing = false;
        bool sortedPlayers = false;

        public event ServerConnectionUpdatedHandler ServerConnectionUpdated;
        public delegate void ServerConnectionUpdatedHandler(object source, ServerConnectionUpdatedEvent e);

        public class ServerConnectionUpdatedEvent : EventArgs
        {
            private string EventInfo;
            public ServerConnectionUpdatedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        ServerConnection serverConnectionBack;

        public ServerConnection serverConnection {
            get {
                return serverConnectionBack;
            }
            private set
            {
                serverConnectionBack = value;

                if (ServerConnectionUpdated != null)
                {
                    ServerConnectionUpdated(this, new ServerConnectionUpdatedEvent("Server connection update."));
                }
            }
        }
        List<Player> players = new List<Player>();
        List<Wonder> wonders = new List<Wonder>();
        //Player[] players = new Player[Constants.MAX_PLAYERS_CITIES];
        int turnTime = 60;
        bool ABChoicePlayer = false;
        bool leaders = false;
        bool cities = false;
        bool babel = false;

        GameAge gameAge = GameAge.AgeI;

        private List<String> Graveyard_Back = new List<string>();
        public List<String> Graveyard { get; set; }

        PNLWonderChoice PNLWonderChoice_ = new PNLWonderChoice();
        ManualResetEvent wonderChoiceLock = new ManualResetEvent(false);

        public event GraveyardUpdatedHandler GraveyardUpdated;
        public delegate void GraveyardUpdatedHandler(object source, GraveyardUpdatedEvent e);

        public class GraveyardUpdatedEvent : EventArgs
        {
            private string EventInfo;
            public GraveyardUpdatedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public event GameAgeChangeHandler GameAgeChange;
        public delegate void GameAgeChangeHandler(object source, GameAgeChangeEvent e);

        public class GameAgeChangeEvent : EventArgs
        {
            private string EventInfo;
            public GameAgeChangeEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public event LeadersChangeHandler leadersChange;
        public event CitiesChangeHandler citiesChange;

        //First we have to define a delegate that acts as a signature for the
        //function that is ultimately called when the event is triggered.
        //You will notice that the second parameter is of MyEventArgs type.
        //This object will contain information about the triggered event.
        public delegate void LeadersChangeHandler(object source, LeadersChangeEvent e);

        //This is a class which describes the event to the class that recieves it.
        //An EventArgs class must always derive from System.EventArgs.
        public class LeadersChangeEvent : EventArgs
        {
            private string EventInfo;
            public LeadersChangeEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }
        

        //First we have to define a delegate that acts as a signature for the
        //function that is ultimately called when the event is triggered.
        //You will notice that the second parameter is of MyEventArgs type.
        //This object will contain information about the triggered event.
        public delegate void CitiesChangeHandler(object source, CitiesChangeEvent e);

        //This is a class which describes the event to the class that recieves it.
        //An EventArgs class must always derive from System.EventArgs.
        public class CitiesChangeEvent : EventArgs
        {
            private string EventInfo;
            public CitiesChangeEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public enum GameAge
        {
            LeaderRoster,
            LeaderHire1,
            AgeI,
            LeaderHire2,
            AgeII,
            LeaderHire3,
            AgeIII,
            End
        };

        public Game(Form1 ans){
            form_ = ans;
            config = ans.getConfig();

            for (int count = 0; count < config.getWonders().Count; count++)
            {
                wonders.Add(new Wonder(form_.getConfig().getWonders()[count]));
            }

            PNLWonderChoice_.ChoiceMade += unlockWonder;
            form_.Controls.Add(PNLWonderChoice_);

            Graveyard = new List<string>();
        }
        public Form1 getForm() {
            return form_;
        }
        public String getIP()
        {
            return IP;
        }

        public void setIP(String ans) {
            IP = ans;
        }

        public int getPort() {
            return Port;
        }

        public void setPort(int ans) {
            Port = ans;
        }

        public void connect(/*String IP, int Port*/) {
            ////Console.WriteLine("Connecting attempt from client");
            IPAddress IP = System.Net.IPAddress.Parse("127.0.0.1");
            int Port;

            if (config.isHost())
            {
                IP = System.Net.IPAddress.Parse("127.0.0.1");
                Port = config.getPorth();
            }
            else {
                try
                {
                    IPHostEntry ipHostInfo = Dns.Resolve(config.getIP());
                    IP = ipHostInfo.AddressList[0];
                }
                #pragma warning disable 0168
                catch (System.Net.Sockets.SocketException ex)
                {
                    #pragma warning restore 0168
                    form_.getPNLMainContainer().showPopUp("No Host.", "A Host with that IP Address (or Hostname) cold not be found please ensure you have entered the correct information and try again.");
                    form_.getPNLMainContainer().setConnectVisible();
                    form_.getPNLMainContainer().BringToFront();
                    return;
                }

                
                //IP = config.getIP();
                Port = config.getPort();
            }
            IPEndPoint remoteEP = new IPEndPoint(IP,Port);

            // Create a TCP/IP  socket.
            handler = new Socket(AddressFamily.InterNetwork, 
                SocketType.Stream, ProtocolType.Tcp );

            // Connect the socket to the remote endpoint. Catch any errors.
            try {
                ////Console.WriteLine("Connection Attempt on " + config.getIP());
                handler.BeginConnect(remoteEP,new AsyncCallback(AcceptCallback), Tuple.Create(handler, form_));
                //connectDone.WaitOne();
                //connected = true;
                
            } catch (ArgumentNullException ane) {
                //Console.WriteLine("ArgumentNullException : {0}",ane.ToString());
            } catch (SocketException se) {
                //Console.WriteLine("SocketException : {0}",se.ToString());
            } catch (Exception ex) {
                //Console.WriteLine("Unexpected exception : {0}", ex.ToString());
            }

        }
        public static void AcceptCallback(IAsyncResult ar/*, Game game*/)
        {
            ////Console.WriteLine("Accept Callback.");
            // Data buffer for incoming data.
            byte[] bytes = new Byte[1024];

            
            
            String data = "";

            // Get the socket that handles the client request.
            Tuple<Socket, Form1> passed = (Tuple<Socket, Form1>)ar.AsyncState;
            Socket handler = (Socket)passed.Item1;
            Form1 form_ = (Form1)passed.Item2;
            CommandQeue commandsReader = new CommandQeue(handler);
            // Encode the data string into a byte array.
            byte[] msg = Encoding.ASCII.GetBytes(form_.getConfig().getUsername() + Constants.DELIM_FIELD + form_.getConfig().getDisplay() +  Constants.DELIM_FIELD + Constants.VERSION + Constants.DELIM_STREAM);

            // Send the data through the socket.
            // not using static method from Server so error or false connection can be caught and dealt with
            try
            {
                int bytesSent = handler.Send(msg);
            }
            #pragma warning disable 0168
            catch (SocketException ex) {

                String title = "Network Error";
                String message = "A connection could not be established please ensure you have entered the correct IP Address (or hostname) and Port, and that the host has the appropriate port forwarded.";
                form_.getPNLMainContainer().setConnectVisible();
                form_.getPNLMainContainer().showPopUp(title,message);
                form_.getGame().endGame();

                return;
            }
            #pragma warning restore 0168

            /*
             TODO: Receive limitation check (dup user, too many players, version)
             */
            data = commandsReader.next();

            if (!data.Equals("CONNECTION"))
            {
                String failureReason = data;
                if (data.Contains(Constants.DELIM_FIELD))
                {
                    failureReason = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                }
                switch (failureReason) { 
                    case "VERSION":{
                        String version = data.Substring(failureReason.Length + 1);
                        form_.getPNLMainContainer().showPopUp("Version Mismatch", "Connection failed because you are using version: " + Constants.VERSION + " And the server has version: " + version);
                        break;
                    }
                    case "MAXPLAYERS":{
                        form_.getPNLMainContainer().showPopUp("Max Players", "Connection failed because the maximum number of players are already present");
                        break;
                    }
                    case "USERNAME":{
                        form_.getPNLMainContainer().showPopUp("Username already in use", "Connection failed because the username: " + form_.getConfig().getUsername() + " is already in use by another player.");
                        break;
                    }
                    case "WONDERS":
                        {
                            form_.getPNLMainContainer().showPopUp("Insufficient Wonders", "Connection failed because there were too few wonders active to allow another player to join.");
                            break;
                        }
                    case "GAME":
                        {
                            form_.getPNLMainContainer().showPopUp("Game started", "Connection failed because a game is already underway.");
                            break;
                        }
                    default:{
                        //Console.WriteLine("Connection failed due to unknown reason: " + failureReason);
                        break;
                    }
                }

                form_.getGame().endGame();
                return;
              

            }

            /*
             TODO: receive configuration
             */

            data = commandsReader.next();

            //determine playerchoice AB
            bool AB = data[0] == '1';
            form_.getGame().setABChoicePlayer(AB);
            form_.getPNLServer().changeChoiceAB(AB?1:0);
            data = data.Substring(2);

            //determine time
            String timeBuffer = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
            int time = Convert.ToInt32(timeBuffer);

            form_.getGame().setTurnTime(time);
            form_.getPNLServer().changeTime(time);

            data = data.Substring(timeBuffer.Length + 1);

            //get leader expansion status
            bool leaders = data[0] == '1';
            form_.getGame().setLeaders(leaders);
            form_.getPNLServer().changeExpansionStatus(0, leaders);
            data = data.Substring(2);

            //get cities expansion status
            bool cities = data[0] == '1';
            form_.getGame().setCities(cities);
            form_.getPNLServer().changeExpansionStatus(1, cities);

            //get wonder status'
            for (int count = 0; count < form_.getConfig().getWonders().Count; count++)
            {
                data = data.Substring(2);
                bool wonderstatus = data[0] == '1';
                form_.getGame().getWonders()[count].setActive(wonderstatus);
                form_.getPNLServer().getPNLWonder().setWonderStatus(count, wonderstatus);
            }

            /*
              receive nb Players + usernames & Display
            */

            data = commandsReader.next();

            int nbPlayers = Convert.ToInt32(data);
            for (int count = 0; count < nbPlayers; count++) {
                
                data = commandsReader.next();
                //Console.WriteLine(data);
                String username = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                String icon = data.Substring(username.Length + 1);// Remove(0, username.Length);
                form_.getGame().addPlayer(username, icon);
            }

            //receive player status'
            data = commandsReader.next();
            for (int count = 0; count < nbPlayers; count++)
            {
                char status = data[0];
                data = data.Substring(2);

                if (status == '0')
                {
                    form_.getGame().setPlayerReady(count, true);
                    form_.getPNLServer().getPlayerIcons()[count].ready();
                }
                else if (status == '1')
                {
                    form_.getPNLServer().getPlayerIcons()[count].connected();
                }
                else
                {
                    form_.getPNLServer().getPlayerIcons()[count].disconnected();
                }
            }
                /*
                 create and launch general game listener
                 */

                //connection valid show game room
                form_.getPNLServer().BringToFrontD();
            form_.getPNLServer().updateNetworkInfo();

            ServerConnection serverConnection = new ServerConnection(form_.getGame(), handler, form_.getConfig().getUsername(), commandsReader);
            form_.getGame().setServerConnection(serverConnection);
            serverConnection.start();
            form_.getGame().setConnected(true);

        }
        public Config getConfig()
        {
            return config;
        }
        public void endGame() {
            if (connected)
            {
                connected = false;
                try
                {
                    Server.sendSingleMessage(handler, "EXIT" + Constants.DELIM_FIELD );
                    serverConnection.end();
                }
#pragma warning disable 0168
                catch (SocketException ex) {
                    //Console.WriteLine("Exception caught");
                }
#pragma warning restore 0168

            }
        }

        public void addPlayer(String username, String display) {

            bool nameinUse = false;

            foreach (Player item in players)
            {
                if (item.getUsername().Equals(username))
                {
                    nameinUse = true;
                    break;
                }
            }

            if (!nameinUse)
            {
                players.Add(new Player(form_));

                players[players.Count - 1].setDisplay(display);
                players[players.Count - 1].setUsername(username);
                //nbPlayers++;
                form_.getPNLServer().addUserName(username, display);
            }
        }
        public void removePlayer(String username) {
            try
            {
                for (int count = 0; count < players.Count; count++)
                {
                    if (String.Equals(players[count].getUsername(), username, StringComparison.OrdinalIgnoreCase))
                    {
                        players.RemoveAt(count);
                        break;
                    }

                }

                form_.getPNLServer().removeUsername(username);
            }
#pragma warning disable 0168
            catch (ArgumentOutOfRangeException e)
            {
#pragma warning restore 0168

            }
            //players[config.getMaxPlayers() - 1] = new Player();
            //nbPlayers--;
        }
        public int getnbPlayers()
        {
            return players.Count;
        }

        public List<Player> getPlayers() {
            return players;
        }
        public bool hasUser(String username)
        {
            for (int count = 0; count < this.getnbPlayers(); count++) {
                if (String.Equals(players[count].getUsername(),username,StringComparison.OrdinalIgnoreCase)) {
                    return true;
                }
            }
            return false;
        }
        public void clearGame() {
            if (connected) {

                form_.getPNLServer().getPNLGame().clear();

                while (getnbPlayers() > 0)
                {
                    removePlayer(players[0].getUsername());
                    
                    //form_.getGame().
                }
                form_.getPNLMainContainer().bringToFrontDelegate();
                form_.getPNLServer().stopGame();
                form_.getPNLServer().hidePopUp();
                form_.getPNLServer().getPNLGame().clear();
                sortedPlayers = false;
                Graveyard.Clear();
                endGame();
            }
        }
        public void setServerConnection(ServerConnection serverConnection_) {
            serverConnection = serverConnection_;
        }
        public void lostConnection(String username) {
            if (!started) {
                form_.getPNLServer().setPlayerDisconnected(username);
            }
        }
        public void regainedConnection(String username) {
            if (!started) {
                form_.getPNLServer().setPlayerConnected(username);
            }
        }
        public void setConnected(bool ans) {
            connected = ans;
            
            if (ans)
            {
                MSGOut = new MessageOutWithConfirm(handler);
                serverConnection.playManager.NewTurn += incTurn;
            }
        }
        private void incTurn(object source, EventArgs e)
        {
            turn++;
        }
        public void sendMessage(String ans) {
            ans = ans.Replace("GAME", "GAME" + Constants.DELIM_FIELD + turn);
            MSGOut.addMessage(ans);
            Server.sendSingleMessage(handler, ans);
        }
        public void receiveMessage(String user, String msg)
        {
            if (playing)
            {
                form_.getPNLServer().getPNLGame().receiveMessage(user, msg);
            }
            else
            {
                form_.getPNLServer().receiveMessage(user, msg);
            }
        }
        public void receiveSystemMessage(String msg)
        {
            if (playing)
            {

            }
            else
            {
                form_.getPNLServer().receiveSystemMessage(msg);
            }
        }
        public void setTurnTime(int ans)
        {
            turnTime = ans;
        }
        public void setLeaders(bool ans) {
            
            leaders = ans;

            if (leaders)
            {
                gameAge = GameAge.LeaderRoster;
            }
            else
            {
                gameAge = GameAge.AgeI;
            }

           if (leadersChange != null)
           {
               leadersChange(this, new LeadersChangeEvent("Leader status changed"));
           }
           
        }
        public void setCities(bool ans)
        {
            cities = ans;

                if (citiesChange != null)
                {
                    citiesChange(this, new CitiesChangeEvent("Cities status changed"));
                }
            
        }
        public void setABChoicePlayer(bool ans)
        {
            ABChoicePlayer = ans;
        }
        public void setBabel(bool ans)
        {
            babel = ans;
        }
        public List<Wonder> getWonders()
        {
            return wonders;
        }
        public bool isLeaders()
        {
            return leaders;
        }
        public bool isCities()
        {
            return cities;
        }
        public void ready(String user)
        {
            setPlayerReady(user, true);
            form_.getPNLServer().setPlayerReady(user);
        }
        public void notready(String user)
        {
            setPlayerReady(user, false);
            form_.getPNLServer().setPlayerConnected(user);
        }
        public int getNbActiveWonders()
        {
            int ans = 0;

            for (int count = 0; count < wonders.Count; count++)
            {
                if (wonders[count].isActive())
                {
                    if (wonders[count].isLeaders() && leaders)
                    {
                        ans++;
                    }
                    else if (wonders[count].isCities() && cities)
                    {
                        ans++;
                    }
                    else if (!wonders[count].isCities() && !wonders[count].isLeaders())
                    {
                        ans++;
                    }
                }
            }

            return ans;
        }
        
        public bool allReady()
        {
            ////Console.WriteLine("NB Players: " + form_.getGame().getnbPlayers());
            for (int count = 0; count < players.Count; count++)
            {
                if (!players[count].isReady())
                {
                    return false;
                }
            }
            ////Console.WriteLine("Players ready: " + countReady + " players inGame: " + form_.getGame().getnbPlayers());
            return true;
        }
        public void setPlayerReady(String user, bool ready)
        {
            for (int count = 0; count < players.Count; count++)
            {
                if (players[count].getUsername().Equals(user))
                {
                    //Console.WriteLine("Player status changed.");
                    players[count].setReady(ready);
                    break;
                }
            }
        }

        public void setPlayerReady(int index, bool ready)
        {
            players[index].setReady(ready);
        }

        public bool isPlaying()
        {
            return playing;
        }
        public void setPlaying(bool ans)
        {
            playing = ans;
        }
        public bool isABPlayerChoice()
        {
            return ABChoicePlayer;
        }
        public char getWonder(String wonder)
        {
            if (form_.getGame().isABPlayerChoice())
            {
                PNLWonderChoice_.showPanel();
                PNLWonderChoice_.load(wonder);
                wonderChoiceLock.WaitOne();
                wonderChoiceLock.Reset();
                return PNLWonderChoice_.getSise();
            }
            else
            {
                Random rnd = new Random();
                int sideIndex = rnd.Next(0, 2);

                if (sideIndex == 0)
                {
                    return 'A';
                }
                else
                {
                    return 'B';
                }
            }

        }
        private void unlockWonder(object sender, EventArgs e)
        {
            wonderChoiceLock.Set();
        }
        public void setPlayerOrder(List<String> usernameOrder)
        {
            List<Player> newOrder = new List<Player>();

            //find current player in list and add them as index 0
            int startIndex = 0;

            for (; startIndex < usernameOrder.Count; startIndex++)
            {
                //own username found in list
                if (usernameOrder[startIndex].Equals(config.getUsername()))
                {
                    //add own username as index 0
                    for (int count = 0; count < players.Count; count++)
                    {
                        if ( players[count].getUsername().Equals(config.getUsername()))
                        {
                            newOrder.Add(players[count]);
                            break;
                        }
                    }

                    break;
                }
            }

            //add players after current plyer starting at startIndex + 1 to the end

            for (int count = startIndex + 1; count < usernameOrder.Count; count++)
            {
                //find next player in udername order in current player object
                for (int count2 = 0; count2 < players.Count; count2++)
                {
                    if (usernameOrder[count].Equals(players[count2].getUsername()))
                    {
                        newOrder.Add(players[count2]);
                        break;
                    }
                }
            }

            //add players befor currentplayer starting at 0 to startindex - 1

            for (int count = 0; count < startIndex; count++)
            {
                for (int count2 = 0; count2 < players.Count; count2++)
                {
                    if (usernameOrder[count].Equals(players[count2].getUsername()))
                    {
                        newOrder.Add(players[count2]);
                        break;
                    }
                }
            }

            //set the new playerorder

            players = newOrder;
            sortedPlayers = true;
        }
        public void setAge(int num)
        {
            if (num == 0)
            {

            }
        }
        public bool isPlayersSorted()
        {
            return sortedPlayers;
        }
        public void nextAge()
        {
            if (gameAge == GameAge.AgeIII)
            {
                gameAge = GameAge.End;
                
            }
            else
            {
                if (leaders)
                {
                    gameAge++;

                    if (gameAge == GameAge.LeaderHire1 || gameAge == GameAge.LeaderHire2 || gameAge == GameAge.LeaderHire3)
                    {
                        serverConnection.playManager.setHandBufferLeaders();
                    }
                }
                else
                {
                    gameAge+= 2;
                }
            }

            if (config.isHost())
            {
                if (gameAge == GameAge.AgeI)
                {
                    form_.getPNLMainContainer().getServer().dealDeck(form_.getPNLMainContainer().getServer().deckBuilder.getAgeIDeck());
                }
                else if (gameAge == GameAge.AgeII)
                {
                    form_.getPNLMainContainer().getServer().dealDeck(form_.getPNLMainContainer().getServer().deckBuilder.getAgeIIDeck());
                }
                else if (gameAge == GameAge.AgeIII)
                {
                    form_.getPNLMainContainer().getServer().dealDeck(form_.getPNLMainContainer().getServer().deckBuilder.getAgeIIIDeck());
                }
            }

            if (!isLeaders())
            {
                if (gameAge == GameAge.AgeII || gameAge == GameAge.AgeIII)
                {
                    foreach (Player item in players)
                    {
                        item.getWonder().diplomacy = false;
                    }
                }
            }
            else
            {
                if (gameAge == GameAge.LeaderHire2 || gameAge == GameAge.LeaderHire3)
                {
                    foreach (Player item in players)
                    {
                        item.getWonder().diplomacy = false;
                    }
                }
            }

            if (GameAgeChange != null)
            {
                GameAgeChange(this, new GameAgeChangeEvent("Game Age Changed."));
            }
        }
        public GameAge getAge()
        {
            return gameAge;
        }
        public String getCardBack()
        {
            if (gameAge == GameAge.AgeI)
            {
                return Constants.PICTURES + "Age I\\Back";
            }
            else if (gameAge == GameAge.AgeII)
            {
                return Constants.PICTURES + "Age II\\Back";
            }
            else if (gameAge == GameAge.AgeIII)
            {
                return Constants.PICTURES + "Age III\\Back";
            }
            else
            {
                return Constants.PICTURES + "Leaders\\Back";
            }
        }
        public int getAgeMaxHandSize()
        {
            if (gameAge == GameAge.LeaderRoster)
            {
                return 4;
            }
            else if (gameAge == GameAge.AgeI || gameAge == GameAge.AgeII || gameAge == GameAge.AgeIII)
            {
                if (isCities())
                {
                    return 8;
                }
                else
                {
                    return 7;
                }
            }

            return 7;
        }
        public void raiseUpdatedGraveyard()
        {
            if (GraveyardUpdated != null)
            {
                GraveyardUpdated(this, new GraveyardUpdatedEvent("Graveyard update."));
            }
        }
        }
    }