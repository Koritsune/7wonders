﻿namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.PNLMAIN = new System.Windows.Forms.Panel();
            this.PBLogo = new System.Windows.Forms.PictureBox();
            this.PNLMAIN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // PNLMAIN
            // 
            this.PNLMAIN.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PNLMAIN.BackgroundImage")));
            this.PNLMAIN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PNLMAIN.Controls.Add(this.PBLogo);
            this.PNLMAIN.Location = new System.Drawing.Point(202, 447);
            this.PNLMAIN.Name = "PNLMAIN";
            this.PNLMAIN.Size = new System.Drawing.Size(884, 682);
            this.PNLMAIN.TabIndex = 5;
            // 
            // PBLogo
            // 
            this.PBLogo.InitialImage = null;
            this.PBLogo.Location = new System.Drawing.Point(465, 70);
            this.PBLogo.Name = "PBLogo";
            this.PBLogo.Size = new System.Drawing.Size(258, 77);
            this.PBLogo.TabIndex = 0;
            this.PBLogo.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 682);
            this.Controls.Add(this.PNLMAIN);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "7Wonders Client";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.PNLMAIN.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PBLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PNLMAIN;
        private System.Windows.Forms.PictureBox PBLogo;
    }
}

