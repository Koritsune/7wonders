﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    class TimerPopUpMessage : PopupMessage
    {
        int time = 5;
        int timeout = 5;
        int tickInterval = 1000;

        String msg = "";

        Timer timer = new Timer();

        public event TimeExpiredHandler TimeExpired;

        //First we have to define a delegate that acts as a signature for the
        //function that is ultimately called when the event is triggered.
        //You will notice that the second parameter is of MyEventArgs type.
        //This object will contain information about the triggered event.
        public delegate void TimeExpiredHandler(object source, TimeExpiredEvent e);

        //This is a class which describes the event to the class that recieves it.
        //An EventArgs class must always derive from System.EventArgs.
        public class TimeExpiredEvent : EventArgs
        {
            private string EventInfo;
            public TimeExpiredEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public TimerPopUpMessage(String message)
        {
            timer.Interval = tickInterval;
            timer.Tick += tick;

            msg = message;
        }
        public void setTimeout(int ans)
        {
            timeout = ans;
            time = ans;
        }
        public void setTickInterval(int ans)
        {
            tickInterval = ans;
            timer.Interval = ans;
        }
        private void tick (object sender, EventArgs e){
           
            setText(msg.Replace("<TIME>", "" + time));
            //Console.WriteLine(time);
            time--;
            //String temp = msg.Replace("<TIME>", "" + time);
            ////Console.WriteLine(msg);
            ////Console.WriteLine(temp);
            this.BringToFront();
            ////Console.WriteLine("In Tick");
            if (time == -1)
            {
                
                time = timeout;
                setText(msg.Replace("<TIME>", "" + time));
                ////Console.WriteLine("New Time: " + time);
                if (TimeExpired != null)
                {
                    TimeExpired(this, new TimeExpiredEvent("Time expires on message"));
                }

                timer.Stop();
            }
        }
        public void start()
        {
            timer.Start();
        }
        public void stop()
        {
            timer.Stop();
            time = timeout;
        }
    }
}
