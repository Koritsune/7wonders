﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client
{
    class PNLWonderChoice : Panel
    {
        PictureBox wonderA = new PictureBox();
        PictureBox wonderB = new PictureBox();

        char side;

        public event SideChoiceHandler ChoiceMade;

        //First we have to define a delegate that acts as a signature for the
        //function that is ultimately called when the event is triggered.
        //You will notice that the second parameter is of MyEventArgs type.
        //This object will contain information about the triggered event.
        public delegate void SideChoiceHandler(object source, SideChoiceEvent e);

        //This is a class which describes the event to the class that recieves it.
        //An EventArgs class must always derive from System.EventArgs.
        public class SideChoiceEvent : EventArgs
        {
            private string EventInfo;
            public SideChoiceEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public PNLWonderChoice()
        {
            this.Dock = DockStyle.Fill;

            wonderA.Size = new Size(400, 200);
            wonderA.SizeMode = PictureBoxSizeMode.StretchImage;
            wonderA.MouseClick += setSideA;
            
            wonderB.Size = new Size(400, 200);
            wonderB.Location = new Point(0, 400);
            wonderB.SizeMode = PictureBoxSizeMode.StretchImage;
            wonderB.MouseClick += setSideB;

            Controls.Add(wonderA);
            Controls.Add(wonderB);

        }
        private void setSideA(object sender, EventArgs e)
        {
            side = 'A';
            this.SendToBack();

            if (ChoiceMade != null)
            {
                ChoiceMade(this, new SideChoiceEvent("Side A chosen"));
            }

        }
        private void setSideB(object sender, EventArgs e)
        {
            side = 'B';
            this.SendToBack();

            if (ChoiceMade != null)
            {
                ChoiceMade(this, new SideChoiceEvent("Side B chosen"));
            }
        }
        public char getSise()
        {
            return side;
        }
        public void load(String wonder)
        {
            wonderA.ImageLocation = Constants.WONDERS_FOLDER + wonder + "A.PNG";
            wonderB.ImageLocation = Constants.WONDERS_FOLDER + wonder + "B.PNG";
        }
        delegate void showPanelD();

        public void showPanel()
        {
            if (this.InvokeRequired)
            {
                showPanelD d = new showPanelD(showPanel);
                this.Invoke(d, new object[] { });
            }
            else
            {
                this.BringToFront();
            }
        }
    }
}
