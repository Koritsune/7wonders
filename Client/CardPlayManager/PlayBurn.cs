﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardPlayManager
{
    class PlayBurn : PlayEffect
    {
        String card;
        Player player;
        Game game;

        public PlayBurn(Game game_, String user_, String card_)
        {
            prio = 0;
            game = game_;
            player = game.getForm().getPNLServer().getPNLGame().getPlayer(user_);
            card = card_;
        }
        public override void play()
        {
            Resources.Resource goldFromBurn = new Resources.Resource();
            goldFromBurn.gold = 3;
            player.addResource(goldFromBurn);
            player.raiseGoldReceivedEvent();

            game.Graveyard.Add(card);
            //Console.WriteLine("Added the following to the graveyard: " + card);
            base.play();
        }
    }
}
