﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardPlayManager
{
    class PlayLeaderRoster : PlayCard
    {
        public PlayLeaderRoster(Form1 form_, String user_, String cardLocation)
            : base(form_,user_,cardLocation)
        {

        }
        public override void play()
        {
            if (!user.Equals(form.getConfig().getUsername()))
            {
                base.play();
            }
            else
            {
                raiseCardPlayer();
            }
        }
    }
}
