﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client.CardPlayManager
{
    class pointPlayerPNL : Panel
    {
        public const int LBLWidth = 120;
        public const int LBLHeight = 40;
        const int fontSize = 12;

        int counter = 0;
        int pointsTotal = 0;

        TransparentLabel LBLUsername = new TransparentLabel();
        TransparentLabel LBLArmyPoints = new TransparentLabel();
        TransparentLabel LBLWonderPoints = new TransparentLabel();
        TransparentLabel LBLCoinPoints = new TransparentLabel();
        TransparentLabel LBLBluePoints = new TransparentLabel();
        TransparentLabel LBLYellowPoints = new TransparentLabel();
        TransparentLabel LBLGreenPoints = new TransparentLabel();
        TransparentLabel LBLPurplePoints = new TransparentLabel();
        TransparentLabel LBLLeaderPoints = new TransparentLabel();
        TransparentLabel LBLBlackPoints = new TransparentLabel();
        TransparentLabel LBLTotalPoints = new TransparentLabel();

        public pointPlayerPNL(Player player)
        {
            setUpLabel(LBLUsername);
            LBLUsername.setText(player.getUsername());

            setUpLabel(LBLArmyPoints);
            int armyPoints = player.PNLArmyVictoryToken.getVictoryPoints();
            pointsTotal += armyPoints;
            LBLArmyPoints.setText("" + armyPoints);

            setUpLabel(LBLCoinPoints);
            int coinPoints = player.resources.gold / 3 - player.PNLDebtToken.getVictoryPoints();
            pointsTotal += coinPoints;
            LBLCoinPoints.setText("" + coinPoints);

            setUpLabel(LBLWonderPoints);
            int wonderPoints = player.getWonder().wonderManager.getVictoryPoints();
            pointsTotal += wonderPoints;
            LBLWonderPoints.setText("" + wonderPoints);

            setUpLabel(LBLBluePoints);
            int bluePoints = player.getVictoryPointsForColor(Card.CardColor.Blue);
            pointsTotal += bluePoints;
            LBLBluePoints.setText("" + bluePoints);

            setUpLabel(LBLYellowPoints);
            int yellowPoints = player.getVictoryPointsForColor(Card.CardColor.Yellow);
            pointsTotal += yellowPoints;
            LBLYellowPoints.setText("" + yellowPoints);

            setUpLabel(LBLGreenPoints);
            CardEffects.VictoryPointsPerScienceSetEffect sciencePoints = new CardEffects.VictoryPointsPerScienceSetEffect(player, 7);
            int greenPoints = sciencePoints.generateVicotryPoints() + player.SCIENCE[0] * player.SCIENCE[0] + player.SCIENCE[1] * player.SCIENCE[1] + player.SCIENCE[2] * player.SCIENCE[2];
            pointsTotal += greenPoints;
            LBLGreenPoints.setText("" + greenPoints);

            setUpLabel(LBLPurplePoints);
            int purplePoints = player.getVictoryPointsForColor(Card.CardColor.Purple);
            pointsTotal += purplePoints;
            LBLPurplePoints.setText("" + purplePoints);

            setUpLabel(LBLLeaderPoints);
            int leaderPoints = player.getVictoryPointsForColor(Card.CardColor.White);
            pointsTotal += leaderPoints;
            LBLLeaderPoints.setText("" + leaderPoints);

            setUpLabel(LBLBlackPoints);
            int blackPoints = player.getVictoryPointsForColor(Card.CardColor.Black);
            pointsTotal += blackPoints;
            LBLBlackPoints.setText("" + blackPoints);

            setUpLabel(LBLTotalPoints);
            LBLTotalPoints.setText("" + pointsTotal);
        }
        public void setUpLabel(TransparentLabel lbl)
        {
            lbl.setSize(LBLWidth, LBLHeight);
            lbl.setFontSize(fontSize);
            lbl.Location = new Point(0, LBLHeight * counter);
            lbl.getLabel().TextAlign = ContentAlignment.MiddleCenter;
            lbl.BackColor = Color.Transparent;
            Controls.Add(lbl);

            counter++;
        }
    }
}
