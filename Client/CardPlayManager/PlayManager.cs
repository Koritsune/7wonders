﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Client.CardPlayManager
{
    public class PlayManager
    {
        Game game;
        List<PlayEffect> playsToMake = new List<PlayEffect>();
        List<PlayEffect> playsToMakeFuture = new List<PlayEffect>();
        List<PlayEffect> playsInfoAtEndOfGame = new List<PlayEffect>();

        bool inDoublePlays = false;
        List<PlayEffect> doublePlayEffectsToMake = new List<PlayEffect>();

        List<String> handBuffer = new List<string>();
        bool lockPlaying = false;
        bool lockAdding = false;
        bool war = false;
        bool leaveRoster = false;
        bool addingToBuffer = false;

        Thread playThread;
        public String lastCardLocation {get; set;}

        int doublePlaysToMake = 0;
        int doublePlayedMade = 0;

        int sciencesAdded = 0;
        int warTokensProcessed = 0;
        public event NewTurnHandler NewTurn;

        //public delegate void MoreInfoReceivedHandler(object source, CardEffects.CardEffect.AdditionalInfoAddedEvent e);

        public delegate void NewTurnHandler(object source, NewTurnEvent e);
        public class NewTurnEvent : EventArgs
        {
            private string EventInfo;
            public NewTurnEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public PlayManager(Game game_)
        {
            game = game_;

            playThread = new Thread(() => playAll());
        }
        public void addCardToPlay(PlayEffect playToMake)
        {
            if (!inDoublePlays)
            {
                playsToMakeFuture.Add(playToMake);
                playToMake.CardPlayed += unlock;

                if (playsToMakeFuture.Count == game.getnbPlayers())
                {
                    while (lockAdding) ;
                    //Console.WriteLine("Beginning new play thread.");
                    playsToMake = playsToMakeFuture;
                    playsToMakeFuture = new List<PlayEffect>();
                    orderPlays();
                    playThread.Start();
                }
            }
            else
            {
                playToMake.CardPlayed += unlock;
                doublePlayEffectsToMake.Add(playToMake);
                //Console.WriteLine("Added play to Make.");
            }

            if (playToMake.infoAtEndOfGame)
            {
                playsInfoAtEndOfGame.Add(playToMake);
            }

        }
        private void playAll()
        {
            lockAdding = true;

            if (war)
            {
                if (!game.getPlayers()[0].DoublePlay)
                {
                    game.sendMessage("GAME" + Constants.DELIM_FIELD + "GRAVEADD" + Constants.DELIM_FIELD + lastCardLocation);
                }
                else
                {
                    setHandBuffer(1, lastCardLocation);
                }
            }

            int countPlays = 0;

            while(playsToMake.Count != 0)
            {
                lockPlaying = true;
                playsToMake[0].play();
                playsToMake.RemoveAt(0);
                //Console.WriteLine("Making Play: " + countPlays);
                while (lockPlaying) ;
                countPlays++;
            }

            if (war)
            {
                if (doublePlaysToMake != 0)
                {
                    if (NewTurn != null)
                    {
                        NewTurn(this, new NewTurnEvent("New Turn."));
                    }
                }

                if (game.getPlayers()[0].DoublePlay)
                {
                    drawHand();
                }

                inDoublePlays = true;

                while (doublePlayEffectsToMake.Count != doublePlaysToMake) ;

                orderDoublePlays();

                while (doublePlayEffectsToMake.Count != 0)
                {
                    lockPlaying = true;
                    doublePlayEffectsToMake[0].play();
                    doublePlayEffectsToMake.RemoveAt(0);
                    while (lockPlaying) ;
                }

                inDoublePlays = false;

                //if(doub)
                //Console.WriteLine("War is happening.");

                if (!game.getPlayers()[0].getWonder().diplomacy)
                {
                    //do war
                    int armyPoints = (game.getAge() == Game.GameAge.AgeI ? 1 : game.getAge() == Game.GameAge.AgeII ? 3 : 5);

                    Player rightTarget = game.getPlayers()[0].rightNeighbor;

                    while (rightTarget.getWonder().diplomacy)
                    {
                        rightTarget = rightTarget.rightNeighbor;
                    }

                    if (rightTarget != game.getPlayers()[0])
                    {
                        if (game.getPlayers()[0].armySize > rightTarget.armySize)
                        {
                            if (!rightTarget.reflectLoss)
                            {
                                game.sendMessage("GAME" + Constants.DELIM_FIELD + "WAR" + Constants.DELIM_FIELD + rightTarget.getUsername() + Constants.DELIM_FIELD + -1);
                            }
                            else
                            {
                                game.sendMessage("GAME" + Constants.DELIM_FIELD + "WAR" + Constants.DELIM_FIELD + game.getConfig().getUsername() + Constants.DELIM_FIELD + -1);
                            }

                            game.sendMessage("GAME" + Constants.DELIM_FIELD + "WAR" + Constants.DELIM_FIELD + game.getConfig().getUsername() + Constants.DELIM_FIELD + armyPoints);
                        }
                    }

                    Player leftTarget = game.getPlayers()[0].leftNeighbor;

                    while (leftTarget.getWonder().diplomacy)
                    {
                        leftTarget = leftTarget.leftNeighbor;
                    }

                    if (leftTarget != game.getPlayers()[0] && leftTarget != rightTarget)
                    {
                        if (game.getPlayers()[0].armySize > leftTarget.armySize)
                        {
                            if (!leftTarget.reflectLoss)
                            {
                                game.sendMessage("GAME" + Constants.DELIM_FIELD + "WAR" + Constants.DELIM_FIELD + leftTarget.getUsername() + Constants.DELIM_FIELD + -1);
                            }
                            else
                            {
                                game.sendMessage("GAME" + Constants.DELIM_FIELD + "WAR" + Constants.DELIM_FIELD + game.getConfig().getUsername() + Constants.DELIM_FIELD + -1);
                            }
                            game.sendMessage("GAME" + Constants.DELIM_FIELD + "WAR" + Constants.DELIM_FIELD + game.getConfig().getUsername() + Constants.DELIM_FIELD + armyPoints);
                        }
                    }
                }
                game.sendMessage("GAME" + Constants.DELIM_FIELD + "WARPROCESSED" + Constants.DELIM_FIELD);
                while (warTokensProcessed != game.getnbPlayers()) ;
                warTokensProcessed = 0;
                game.nextAge();

                war = false;
            }
            else if (leaveRoster)
            {
                game.nextAge();
                leaveRoster = false;
            }
            else if (game.getAge() == Game.GameAge.LeaderHire1 || game.getAge() == Game.GameAge.LeaderHire2 || game.getAge() == Game.GameAge.LeaderHire3)
            {
                game.nextAge();
                
            }

            playThread = new Thread(() => playAll());
            lockAdding = false;

            while (handBuffer.Count == 0) ;
            while (addingToBuffer) ;
            
            if (game.getAge() == Game.GameAge.End)
            {
                foreach (PlayEffect item in playsInfoAtEndOfGame)
                {
                    lockPlaying = true;
                    item.CardPlayed += unlock;
                    item.getEndofGameInfo();
                    while (lockPlaying) ;
                }

                //Console.WriteLine("Waiting on science choices");
                game.getPlayers()[0].getScienceChoices();

                while (sciencesAdded != game.getnbPlayers()) ;

                showPoints();
            }
            else
            {
                drawHand();

                if (NewTurn != null)
                {
                    NewTurn(this, new NewTurnEvent("A new turn has begun."));
                }
            }

        }
        public void unlock()
        {
            lockPlaying = false;
        }
        private void unlock(object source, EventArgs e)
        {
            unlock();
        }
        private void orderPlays()
        {
            for (int count = 0; count < playsToMake.Count - 1; count++)
            {
                for (int count2 = count + 1; count2 < playsToMake.Count; count2++)
                {
                    if (playsToMake[count].prio > playsToMake[count2].prio)
                    {
                        PlayEffect temp = playsToMake[count];
                        playsToMake[count] = playsToMake[count2];
                        playsToMake[count2] = temp;
                        count = 0;
                        break;
                    }
                }
            }
        }
        public void orderDoublePlays()
        {
            for (int count = 0; count < doublePlayEffectsToMake.Count - 1; count++)
            {
                for (int count2 = count + 1; count2 < doublePlayEffectsToMake.Count; count2++)
                {
                    if (doublePlayEffectsToMake[count].prio > doublePlayEffectsToMake[count2].prio)
                    {
                        PlayEffect temp = doublePlayEffectsToMake[count];
                        doublePlayEffectsToMake[count] = doublePlayEffectsToMake[count2];
                        doublePlayEffectsToMake[count2] = temp;
                        count = 0;
                        break;
                    }
                }
            }
        }
        public void setHandBuffer(int nb, String data)
        {
            addingToBuffer = true;

            handBuffer.Clear();

            if (nb > 1)
            {
                if (data.Length > 0)
                {
                    for (int count = 0; count < nb; count++)
                    {
                        String card = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                        data = data.Substring(card.Length + 1);
                        handBuffer.Add(card);
                    }
                }
            }
            else if (nb == 1 && game.getAge() == Game.GameAge.LeaderRoster)
            {
                for (int count = 0; count < nb; count++)
                {
                    String card = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                    data = data.Substring(card.Length + 1);
                    handBuffer.Add(card);
                }
            }
            else if (nb == 0 && game.isLeaders() && game.getAge() == Game.GameAge.LeaderRoster)
            {
                leaveRoster = true;
            }
            else if (nb == 1 && game.getAge() != Game.GameAge.LeaderRoster)
            {
                handBuffer.Add(data);
            }

            addingToBuffer = false;

        }
        private void drawHand()
        {
            
                //Console.WriteLine("In draw hand");
                while (addingToBuffer);
                Form1 form_ = game.getForm();


                    for (int count = 0; count < form_.getGame().getnbPlayers(); count++)
                    {
                        form_.getPNLServer().getPNLGame().getPlayers()[count].clearHand();
                    }

                    for (int count = 0; count < handBuffer.Count; count++)
                    {
                        if (handBuffer[count].Contains(Constants.DELIM_FIELD))
                        {
                            handBuffer[count] = handBuffer[count].Remove(handBuffer[count].IndexOf(Constants.DELIM_FIELD), 1);
                        }
                        //Console.WriteLine("Drawing card: " + handBuffer[count].Replace(Constants.DELIM_FIELD,'#').Replace(Constants.DELIM_STREAM,'!'));
                        form_.getPNLServer().getPNLGame().getPlayers()[0].addCard(handBuffer[count]);
                    }

                    if (handBuffer.Count == 2 && game.getAge() != Game.GameAge.LeaderRoster && game.getAge() != Game.GameAge.LeaderHire3)
                    {
                        war = true;
                    }

                    handBuffer.Clear();
            
        }
        public void setHandBufferLeaders()
        {
            addingToBuffer = true;

            List<Card> leaderCards = game.getPlayers()[0].playedCards[(int)Card.CardColor.Roster];

            handBuffer.Clear();

            for (int count = 0; count < leaderCards.Count; count++)
            {
                handBuffer.Add(leaderCards[count].cardLocation);
            }

            addingToBuffer = false;
        }
        public void increaseDoublePlays()
        {
            doublePlaysToMake++;
        }
        public void registerDoublePlayMade()
        {
            doublePlayedMade++;
        }
        public bool isInDoublePlays()
        {
            return inDoublePlays;
        }
        public int getDoublePlaysToMake()
        {
            return doublePlaysToMake;
        }
        public void receiveEndofGameInfo(String user, String name, String info)
        {
            foreach (PlayEffect item in playsInfoAtEndOfGame)
            {
                if (item.match(user, name))
                {
                    item.CardPlayed += unlock;
                    item.receiveEndOfGameInfo(info);
                }
            }
        }
        public void addScience(Player user, int gear, int tablet, int geometry)
        {
            //Player player = game.getForm().getPNLServer().getPNLGame().getPlayer(user);

            user.SCIENCE[0] += gear;
            user.SCIENCE[1] += tablet;
            user.SCIENCE[2] += geometry;

            sciencesAdded++;

            //Console.WriteLine("Science choices increased to: " + sciencesAdded);
        }
        public void increaseMilitaryTokenProcessed()
        {
            warTokensProcessed++;
        }
        private delegate void showPointsd();
        private void showPoints()
        {
            if (game.getForm().InvokeRequired)
            {
                showPointsd d = new showPointsd(showPoints);
                game.getForm().Invoke(d, new object[] { });
            }
            else
            {
                pointPNL pointsPNL = new pointPNL(game);
                pointsPNL.setup();
                pointsPNL.BringToFront();
            }
        }
    }
}
