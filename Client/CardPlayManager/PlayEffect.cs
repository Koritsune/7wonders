﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardPlayManager
{
    public class PlayEffect
    {
        public int prio { get; set; }
        public bool infoAtEndOfGame{get; protected set;}
        public event CardPlayedHandler CardPlayed;

        public delegate void CardPlayedHandler(object source, CardPlayedEvent e);
        public class CardPlayedEvent : EventArgs
        {
            private string EventInfo;
            public CardPlayedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public PlayEffect()
        {
            infoAtEndOfGame = false;
        }
        public virtual void play()
        {
            raiseCardPlayer();
        }
        public void raiseCardPlayer() 
        {
            if (CardPlayed != null) 
            {
                CardPlayed(this, new CardPlayedEvent("Card was Played."));
            }
        }
        public virtual void receiveEndOfGameInfo(String info)
        {

        }
        public virtual void getEndofGameInfo()
        {
            
        }

        public virtual bool match(String user, String name)
        {
            return false;
        }
    }
}
