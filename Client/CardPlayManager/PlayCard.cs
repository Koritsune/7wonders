﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardPlayManager
{
    class PlayCard : PlayEffect
    {
        protected Form1 form;
        protected Card card;
        protected String user;
        protected Player player;
        protected String moreInfo;

        public PlayCard(Form1 form_, String user_, String cardLocation, String moreInfo_ = "")
        {
            form = form_;
            user = user_;
            player = form.getPNLServer().getPNLGame().getPlayer(user);
            card = new Card(cardLocation, player);
            this.prio = card.priority;
            card.CardPlayed += raiseCardPlayed;

            moreInfo = moreInfo_;
            infoAtEndOfGame = card.infoAtEndOfGame;

            if (card.moreInfoNeeded)
            {
                foreach (CardEffects.CardEffect item in card.immediateEffects)
                {
                    item.receiveMoreInfo(moreInfo);
                }
            }
        }
        public override void play()
        {
            form.getPNLServer().getPNLGame().playCard(card, player);
        }

        private void raiseCardPlayed(object source, EventArgs e)
        {
            this.raiseCardPlayer();
        }

        public override void receiveEndOfGameInfo(string info)
        {
            card.receiveEndofGameInfo(info);
        }

        public override void getEndofGameInfo()
        {
            card.getEndofGameInfo();
        }

        public override bool match(string user, string name)
        {
            return user.Equals(player.getUsername()) && name.Equals(card.name);
        }
    }
}
