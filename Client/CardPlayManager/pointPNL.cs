﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client.CardPlayManager
{
    class pointPNL : Panel
    {
        List<pointPlayerPNL> playerPoints = new List<pointPlayerPNL>();
        List<Panel> pointIndicatorpnl = new List<Panel>();

        Game game;

        public pointPNL(Game game_)
        {
            game = game_;
            //setup();
        }
        private delegate void setupD();
        public void setup()
        {
            if (this.InvokeRequired)
            {
                setupD d = new setupD(setup);
                game.getForm().getPNLServer().getPNLGame().Invoke(d, new object[] { });
            }
            else
            {
                for (int count = 0; count <= 10; count++)
                {
                    Panel pnl = new Panel();
                    pnl.Size = new Size(pointPlayerPNL.LBLWidth, pointPlayerPNL.LBLHeight);
                    pnl.Location = new Point(0, pointPlayerPNL.LBLHeight * pointIndicatorpnl.Count);
                    pnl.BackColor = Color.Transparent;
                    pnl.BackgroundImageLayout = ImageLayout.Stretch;
                    pnl.BackgroundImage = Image.FromFile(Constants.ICONS + "PointIcon" + count + ".PNG");
                    Controls.Add(pnl);
                    pointIndicatorpnl.Add(pnl);
                }
                for (int count = 0; count < game.getnbPlayers(); count++)
                {
                    pointPlayerPNL pnl = new pointPlayerPNL(game.getPlayers()[count]);
                    pnl.Size = new Size(pointPlayerPNL.LBLWidth, pointPlayerPNL.LBLHeight * 11);
                    pnl.Location = new Point((playerPoints.Count + 1) * pointPlayerPNL.LBLWidth, 0);
                    Controls.Add(pnl);
                    playerPoints.Add(pnl);
                }

                BackColor = Color.Transparent;
                this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;

                this.Size = new Size(pointPlayerPNL.LBLWidth * (playerPoints.Count + 1), pointPlayerPNL.LBLHeight * 11);
                this.Location = new Point((game.getForm().getPNLServer().getPNLGame().getRightSectionXPoint() - this.Size.Width) / 2, (game.getForm().Size.Height - this.Size.Height) / 2);
                game.getForm().getPNLServer().getPNLGame().Controls.Add(this);
            }
        }
    }
}
