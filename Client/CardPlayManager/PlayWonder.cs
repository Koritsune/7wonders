﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.CardPlayManager
{
    class PlayWonder : PlayEffect
    {
        Form1 form;
        String user;
        String buildData;
        Player player;
        WonderStage wonderStageToBuild;

        public PlayWonder(Form1 form_, String user_, String buildData_) : base()
        {
            form = form_;
            user = user_;
            player = form_.getPNLServer().getPNLGame().getPlayer(user);
            buildData = buildData_;
            wonderStageToBuild = player.getWonder().wonderManager.getWonderStageToBuild(buildData);
            prio = wonderStageToBuild.priority;
            infoAtEndOfGame = wonderStageToBuild.infoAtEndOfGame;
        }
        public override void play()
        {
            if (prio > 1)
            {
                wonderStageToBuild.WonderStagePlayed += unlockWonderPlay;
            }

            wonderStageToBuild.play();
            player.getWonder().wonderManager.wonderStageBuilt(wonderStageToBuild);

            if (prio <= 1)
            {
                this.raiseCardPlayer();
            }
        }
        private void unlockWonderPlay(object source, EventArgs e)
        {
            this.raiseCardPlayer();
        }

        public override void receiveEndOfGameInfo(string info)
        {
            wonderStageToBuild.receiveEndofGameInfo(info);
        }

        public override void getEndofGameInfo()
        {
            wonderStageToBuild.getEndofGameInfo();
        }

        public override bool match(string user, string name)
        {
            return user.Equals(player.getUsername());
        }
    }
}
