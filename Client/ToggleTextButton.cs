﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Client
{
    class ToggleTextButton: TextButton
    {
        bool active = false;
        bool enable = true;

        public event activeToggleHandler activeToggle;

        //First we have to define a delegate that acts as a signature for the
        //function that is ultimately called when the event is triggered.
        //You will notice that the second parameter is of MyEventArgs type.
        //This object will contain information about the triggered event.
        public delegate void activeToggleHandler(object source, activeToggleEvent e);

        //This is a class which describes the event to the class that recieves it.
        //An EventArgs class must always derive from System.EventArgs.
        public class activeToggleEvent : EventArgs
        {
            private string EventInfo;
            public activeToggleEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public ToggleTextButton(String text):base(text){
            startHover();
            getLabel().ForeColor = Color.Gray;

            this.MouseClick += click;
            getLabel().MouseClick += click;
        }
        public void setActive(bool ans){
            active = ans;

            if (active)
            {
                getLabel().ForeColor = Color.Black;
            }
            else
            {
                getLabel().ForeColor = Color.Gray;
            }
        }
        public bool getActive(){
            return active;
        }
        private void click(object sender, EventArgs e)
        {
            if (enable)
            {
                active = !active;

                if (active)
                {
                    getLabel().ForeColor = Color.Black;
                }
                else
                {
                    getLabel().ForeColor = Color.Gray;
                }

                if (activeToggle != null)
                {
                    activeToggle(this, new activeToggleEvent("active status was toggled."));
                }
            }
        }
        public void setEnable(bool ans)
        {
            enable = ans;

            if (!enable)
            {
                stopHover();
            }
            else
            {
                startHover();
            }
        }
    }
    
}
