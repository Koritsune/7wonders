﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client
{
    class ToggleGroupBox : GroupBox
    {
        List<ToggleTextButton> buttons = new List<ToggleTextButton>();
        int buttonWidth = 0;
        int buttonHeight = 0;

        public event ValueChangeHandler valueChange;

        //First we have to define a delegate that acts as a signature for the
        //function that is ultimately called when the event is triggered.
        //You will notice that the second parameter is of MyEventArgs type.
        //This object will contain information about the triggered event.
        public delegate void ValueChangeHandler(object source, ValueChangeEvent e);

        //This is a class which describes the event to the class that recieves it.
        //An EventArgs class must always derive from System.EventArgs.
        public class ValueChangeEvent : EventArgs
        {
            private string EventInfo;
            public ValueChangeEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public ToggleGroupBox() : base()
        {

        }
        public void setButtonSize(int width, int height)
        {
            buttonWidth = width;
            buttonHeight = height;
        }
        public void addButton(String text)
        {
            ToggleTextButton ans = new ToggleTextButton(text);
            ans.setSize(buttonWidth, buttonHeight);
            ans.setFontSize(10);
            ans.Location = nextPoint();
            ans.activeToggle += singleSelect;
            //ans.Location = new Point()
            buttons.Add(ans);
            this.Controls.Add(ans);
        }
        private Point nextPoint()
        {
            int widthIndex = 0;
            int heightIndex = 0;

            widthIndex = (buttons.Count % (this.Size.Width/buttonWidth));
            heightIndex = (buttons.Count / (this.Size.Width/buttonWidth));

            ////Console.WriteLine("Width Index: " + widthIndex);
            ////Console.WriteLine("Height Index: " + heightIndex);

            return new Point(widthIndex * buttonWidth + 10, heightIndex * buttonHeight + 20);
        }
        public void selectButton(int index)
        {
            clearSelection();
            buttons[index].setActive(true);
        }
        private void clearSelection()
        {
            for (int count = 0; count < buttons.Count; count++)
            {
                if (buttons[count].getActive())
                {
                    buttons[count].setActive(false);
                }
            }
        }
        public void selectButton(String value)
        {
            clearSelection();

            for (int count = 0; count < buttons.Count; count++)
            {
                if (buttons[count].getLabel().Text.Equals(value))
                {
                    buttons[count].setActive(true);
                    break;
                }
            }
        }
        private void singleSelect(object sender, EventArgs e)
        {
            ////Console.WriteLine("In singleSelect.");
            int count = 0;
            int nbSelected = 0;
            ToggleTextButton sender_ = (ToggleTextButton)sender;

            for (; count < buttons.Count; count++)
            {
                if (buttons[count].getActive())
                {
                    nbSelected++;
                }
            }

            if (nbSelected == 0)
            {
                sender_.setActive(true);
            }
            else if (nbSelected > 1)
            {
                for (int count2 = 0; count2 < buttons.Count; count2++)
                {
                    //if button is active and the button in iteration is not the button that was just turned active
                    if (buttons[count2].getActive() && !sender_.getLabel().Text.Equals(buttons[count2].getLabel().Text))
                    {
                        buttons[count2].setActive(false);
                    }
                }
            }

            if (valueChange != null)
            {
                valueChange(this, new ValueChangeEvent("Value of ToggleGroupBox changed."));
            }

        }
        public void setEnable(bool ans)
        {
            for (int count = 0; count < buttons.Count; count++)
            {
                buttons[count].setEnable(ans);
            }
        }
        public String getSelectedValue()
        {
            for (int count = 0; count < buttons.Count; count++)
            {
                if (buttons[count].getActive())
                {
                    return buttons[count].getLabel().Text;
                }
            }

            return "";
        }
        public int getSlectedIndex()
        {
            for (int count = 0; count < buttons.Count; count++)
            {
                if (buttons[count].getActive())
                {
                    return count;
                }
            }

            return -1;
        }
    }
}
