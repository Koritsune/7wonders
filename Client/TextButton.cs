﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client
{
    public class TextButton : Panel
    {
        Label text;
        volatile bool isEnabled = false;
        const String HOVER = "Pictures\\Icons\\ButtonHover.PNG";

        public event TextBUttonClickHandler onTextButtonClickEvent;

        //First we have to define a delegate that acts as a signature for the
        //function that is ultimately called when the event is triggered.
        //You will notice that the second parameter is of MyEventArgs type.
        //This object will contain information about the triggered event.
        public delegate void TextBUttonClickHandler(object source, TextButtonClickEvent e);

        //This is a class which describes the event to the class that recieves it.
        //An EventArgs class must always derive from System.EventArgs.
        public class TextButtonClickEvent : EventArgs
        {
            private string EventInfo;
            public TextButtonClickEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public TextButton(String ans) {

            this.DoubleBuffered = true;
            
            text = new Label();
            Text = "text";
            text.Text = ans;
            text.Location = new Point(0, 5);

            this.MouseLeave += hideHover;
            this.MouseEnter += displayHover;
            text.MouseLeave += hideHover;
            text.MouseEnter += displayHover;

            this.MouseClick += onClick;
            text.MouseClick += onClick;

            this.BackgroundImageLayout = ImageLayout.Stretch;  
            this.BackColor = Color.Transparent;
            this.Visible = true;
            this.Controls.Add(text);
            text.BringToFront();
        }
        private void displayHover(object sender, EventArgs e)
        {
            if (isEnabled)
            {
                this.BackgroundImage = Image.FromFile(HOVER);
            }
                            
        }
        public void setFontSize(int size) {
            text.Font = new Font(text.Font.FontFamily.Name, size);
        }
        private void hideHover(object sender, EventArgs e)
        {
            if (isEnabled)
            {
                this.BackgroundImage = null;
            }
        }
        public void setSize(int width, int height) {
            Size = new Size(width, height);
            text.Size = new Size(width, height);
        }
        public void setPoint(int x, int y) {
            text.Location = new Point(0,5);
            Location = new Point(x, y);
        }
        public Label getLabel() {
            return text;
        }
        public void stopHover() {
            isEnabled = false;
        }
        public void startHover()
        {
            isEnabled = true;
        }
        private void onClick(object sender, EventArgs e) {
            if (onTextButtonClickEvent != null)
            {
                onTextButtonClickEvent(this, new TextButtonClickEvent("TextButton was clicked."));
            }
        }
    }
}
