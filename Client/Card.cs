﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Xml;
using System.IO;

namespace Client
{
    public class Card : Panel
    {
        Player player;

        Panel picture = new Panel();

        PictureBox PBPlay = new PictureBox();
        PictureBox PBBurn = new PictureBox();
        PictureBox PBWonder = new PictureBox();

        public CardColor color {get;set;}
        public bool infoAtEndOfGame {get; private set;}
        public int priority { get; private set; }
        public Resources.Resource cost { get; set; }
        Resources.Resource generates;

        List<CardEffects.VictoryPointsEffect> victoryPoints = new List<CardEffects.VictoryPointsEffect>();
        public List<CardEffects.CardEffect> immediateEffects {get; private set;}

        public String cardLocation {get; private set;}
        public String name {get; private set;}
        String desc = "";

        bool played = false;
        public bool moreInfoNeeded { get; private set; }
        public Player.PlayerRotation rotation { get; set; }
        public event CardPlayHandler CardPlay;
        public event CardClickHandler CardClick;
        public event CardBurnHandler CardBurn;
        public event CardWonderPlayHandler CardWonder;
        public event CardPlayedHandler CardPlayed;
        public event MoreInfoReceivedHandler MoreInfoReceived;
        public event CardDoubleClickHandler CardDoubleClick;

        //public delegate void MoreInfoReceivedHandler(object source, CardEffects.CardEffect.AdditionalInfoAddedEvent e);

        public delegate void CardDoubleClickHandler(object source, CardDoubleClickEvent e);
        public class CardDoubleClickEvent : EventArgs
        {
            private string EventInfo;
            public CardDoubleClickEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public delegate void MoreInfoReceivedHandler(object source, CardEffects.CardEffect.AdditionalInfoAddedEvent e);

        public delegate void CardPlayedHandler(object source, CardPlayedEvent e);
        public class CardPlayedEvent : EventArgs
        {
            private string EventInfo;
            public CardPlayedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }
        public delegate void CardWonderPlayHandler(object source, CardWonderPlayEvent e);
        public class CardWonderPlayEvent : EventArgs
        {
            private string EventInfo;
            public CardWonderPlayEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public delegate void CardBurnHandler(object source, CardBurnEvent e);
        public class CardBurnEvent : EventArgs
        {
            private string EventInfo;
            public CardBurnEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public delegate void CardPlayHandler(object source, CardPlayEvent e);
        public class CardPlayEvent : EventArgs
        {
            private string EventInfo;
            public CardPlayEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public delegate void CardClickHandler(object source, CardClickEvent e);
        public class CardClickEvent : EventArgs
        {
            private string EventInfo;
            public CardClickEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public enum CardColor
        {
            Brown = 0,
            Gray = 1,
            Yellow = 2,
            Blue = 3,
            Red = 4,
            Green = 5,
            Purple = 6,
            White = 7,
            Black = 8,
            Roster = 9
        }

        public Card(String CardLocation, Player player_, Player.PlayerRotation rotation = Player.PlayerRotation.None)
        {
            infoAtEndOfGame = false;
            name = "";
            moreInfoNeeded = false;
            immediateEffects = new List<CardEffects.CardEffect>();
            Resize += resize;
            color = 0;
            cardLocation = CardLocation;
            priority = 0;
            rotation = Player.PlayerRotation.None;

           this.DoubleBuffered = true;
           BackColor = Color.Transparent;

           player = player_;

           //this.Parent = player_.getWonder();

           if (!CardLocation.Contains(Constants.PICTURES))
           {
               CardLocation = Constants.PICTURES + CardLocation;
           }

           while (cardLocation.Contains(Constants.DELIM_FIELD))
           {
               cardLocation = cardLocation.Remove(cardLocation.IndexOf(Constants.DELIM_FIELD),1);
               //Console.WriteLine("Loading card: " + cardLocation.Replace(Constants.DELIM_FIELD, '#').Replace(Constants.DELIM_STREAM, '!'));
           }
           
            Image img = Image.FromFile(CardLocation + ".PNG");

            if (rotation == Player.PlayerRotation.Clockwise90)
            {
                img.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            else if (rotation == Player.PlayerRotation.ClockWise270)
            {
                img.RotateFlip(RotateFlipType.Rotate270FlipNone);
            }
            else if (rotation == Player.PlayerRotation.Clockwise180)
            {
                img.RotateFlip(RotateFlipType.Rotate180FlipNone);
            }

           picture.BackgroundImage = img;
           picture.BackgroundImageLayout = ImageLayout.Stretch;
           picture.Click += cardClick;
           picture.MouseDoubleClick += raiseCardDoubleClick;
           Controls.Add(picture);

           PBBurn.ImageLocation = Constants.ICONS + "Burn.PNG";
           PBBurn.SizeMode = PictureBoxSizeMode.StretchImage;
           PBBurn.Visible = false;
           PBBurn.MouseClick += raiseBurn;
           Controls.Add(PBBurn);

           PBPlay.ImageLocation = Constants.ICONS + "Play.PNG";
           PBPlay.SizeMode = PictureBoxSizeMode.StretchImage;
           PBPlay.MouseClick += raiseCardPlayEvent;
           PBPlay.Visible = false;

           Controls.Add(PBPlay);

           PBWonder.ImageLocation = Constants.ICONS + "Wonder.PNG";
           PBWonder.SizeMode = PictureBoxSizeMode.StretchImage;
           PBWonder.Visible = false;
           PBWonder.MouseClick += raiseWonder;
           Controls.Add(PBWonder);

           cardLocation = CardLocation;
            name = CardLocation;

            cost = new Resources.Resource();
            while (name.Contains('\\'))
            {
                name = name.Substring(name.IndexOf('\\') + 1);
            }

            if (player.getPlayerRotation() == Player.PlayerRotation.Clockwise180)
            {
                picture.BackgroundImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
            }
            
            if (File.Exists(cardLocation + ".XML"))
            {
                loadCard(cardLocation + ".XML");
            }
            else
            {
                loadCard(Constants.PICTURES + "Age I\\Back.XML");
            }

            if (name.Equals("Back"))
            {
                Controls.Remove(PBBurn);
                Controls.Remove(PBPlay);
                Controls.Remove(PBWonder);
            }
        }
        public void setSize(int width, int height)
        {
            this.Width = width;
            this.Height = height;

            if (!played)
            {
                picture.Location = new Point(0, Convert.ToInt32(height * 0.15));
                picture.Size = new Size(width, Convert.ToInt32(height * 0.85));

                Size buttonSize = new Size(Convert.ToInt32(width / 3), Convert.ToInt32(height * 0.15));

                PBPlay.Size = buttonSize;
                PBPlay.Location = new Point(0, 0);

                PBWonder.Size = buttonSize;
                PBWonder.Location = new Point(Convert.ToInt32(width / 3), 0);

                PBBurn.Size = buttonSize;
                PBBurn.Location = new Point(Convert.ToInt32(width / 3 * 2), 0);
            }
            else
            {
                picture.Location = new Point(0, 0);
                picture.Width = width;
                picture.Height = height;
            }
        }
        public CardColor getCardColor()
        {
            return color;
        }
        private void cardClick(object sender, EventArgs e)
        {
            raiseCardClick();
        }
        public void raiseCardClick()
        {
            PBPlay.Visible = true;
            PBWonder.Visible = true;
            PBBurn.Visible = true;

            if (CardClick != null)
            {
                CardClick(this, new CardClickEvent("Card was clicked"));
            }
        }
        public void hideButtons()
        {
            PBPlay.Visible = false;
            PBWonder.Visible = false;
            PBBurn.Visible = false;
        }
        public void showPictureOnly()
        {
            played = true;

            picture.Location = new Point(0, 0);
            picture.Size = this.Size;
        }
        public Panel getPicture()
        {
            return picture;
        }
        private void loadCard(String xml){

            desc = name + "\n\n";

            XmlTextReader xmlReader = new XmlTextReader(xml);

            while (xmlReader.Read())
            {
                if (xmlReader.NodeType != XmlNodeType.EndElement)
                {
                    if (xmlReader.Name == "Color")
                    {
                        color = (CardColor)Convert.ToInt32(xmlReader.GetAttribute("Value"));
                    }
                    else if (xmlReader.Name == "VictoryPointsFixed")
                    {
                        int value = Convert.ToInt32(xmlReader.GetAttribute("Points"));
                        victoryPoints.Add(new CardEffects.VictoryPointsFixedEffect(value));
                        desc += "This card generates " + value + " Victory Point(s)\n\n";
                    }
                    else if (xmlReader.Name == "BuildsTo")
                    {
                        String buildsTo = xmlReader.GetAttribute("Name");
                        String buildsToDesc = xmlReader.GetAttribute("BuildsToDesc");

                        desc += "Builds To: This card allows you to build the following card at no cost: " + buildsTo + " which " + buildsToDesc + "\n\n";

                        CardEffects.ChainBuildEffect chainBuild = new CardEffects.ChainBuildEffect(player, buildsTo);
                        immediateEffects.Add(chainBuild);
                    }
                    else if (xmlReader.Name == "Resource")
                    {
                        generates = Resources.Resource.buildResourceFromXML(xmlReader);

                        desc += "This card grants: " + generates.generateDescription();

                        CardEffects.ResourceGenerationFixed resourGeneration = new CardEffects.ResourceGenerationFixed(player, generates, this, player.form_.getGame());
                        immediateEffects.Add(resourGeneration);
                    }
                    else if (xmlReader.Name == "ResourceOption")
                    {
                        Resources.Resource resourceOption = Resources.Resource.buildResourceFromXML(xmlReader);

                        desc += "This card grants one of the following: " + resourceOption.generateDescription() + "\n\n";

                        CardEffects.ResourceOption resourceOptionEffect = new CardEffects.ResourceOption(player, resourceOption, this, player.form_.getGame());
                        immediateEffects.Add(resourceOptionEffect);
                    }
                    else if (xmlReader.Name == "War")
                    {
                        int armyIncrease = Convert.ToInt32(xmlReader.GetAttribute("Value"));

                        CardEffects.ArmyIncrease armyIncreaseEffect = new CardEffects.ArmyIncrease(player, armyIncrease);
                        immediateEffects.Add(armyIncreaseEffect);

                        desc += "This card increase your military force by: " + armyIncrease + "\n\n";
                    }
                    else if (xmlReader.Name == "Cost")
                    {
                        cost = Resources.Resource.buildResourceFromXML(xmlReader);
                        desc += "Cost: " + cost.generateDescription() + "\n\n";
                    }
                    else if (xmlReader.Name == "Science")
                    {
                        int scienceType = Convert.ToInt32(xmlReader.GetAttribute("Type"));

                        CardEffects.ScienceEffect science = new CardEffects.ScienceEffect(player, scienceType);
                        immediateEffects.Add(science);

                        desc += "This card generates one: " + (scienceType == 0 ? "Gear" : scienceType == 1 ? "Tablet" : "Geometry") + " science." + "\n\n";
                    }
                    else if (xmlReader.Name == "Trade")
                    {
                        int type = Convert.ToInt32(xmlReader.GetAttribute("Type"));
                        String direction = xmlReader.GetAttribute("Direction");
                        int rate = Convert.ToInt32(xmlReader.GetAttribute("Rate"));
                        bool singleUse = false;

                        desc += "This card grants trades with (" + (type == 0 ? "Brown" : type == 1 ? "Gray" : "Brown and Gray") + ") Cards at a " + (rate < 0 ? "Discounted rate of: " : " rate of ") + rate + " with your " + (direction.Equals("West")?"Left":"Right") + " neighbor." + (singleUse?"Once per turn":"") + "\n\n";
                        
                        try{
                            singleUse = Convert.ToInt32(xmlReader.GetAttribute("SingleUse")) == 1;
                        }
#pragma warning disable 0168
                        catch ( SystemException e)
                        {
#pragma warning restore 0168

                        }
                        CardEffects.TradeEffect tradeEffect = new CardEffects.TradeEffect(player, direction, rate, (CardEffects.TradeEffect.TradeType)type, singleUse);
                        immediateEffects.Add(tradeEffect);
                    }
                    else if (xmlReader.Name == "Donation")
                    {
                        int gold = Convert.ToInt32(xmlReader.GetAttribute("Gold"));

                        desc += "This card gives " + gold + " Gold to your neighbors.\n\n";

                        Resources.Resource goldToGive = new Resources.Resource();
                        goldToGive.gold = gold;

                        CardEffects.ResourceGenerationFixed leftDonation = new CardEffects.ResourceGenerationFixed(player.leftNeighbor, goldToGive, this, player.form_.getGame());
                        immediateEffects.Add(leftDonation);
                        CardEffects.ResourceGenerationFixed rightDonation = new CardEffects.ResourceGenerationFixed(player.rightNeighbor, goldToGive, this, player.form_.getGame());
                        immediateEffects.Add(rightDonation);
                    }
                    else if (xmlReader.Name == "ResourceDouble")
                    {
                        desc += "This card generates one resource that the player already produces.\n\n";

                        CardEffects.ResourceDouble resourceDouble = new CardEffects.ResourceDouble(player, this);
                        immediateEffects.Add(resourceDouble);
                    }
                    else if (xmlReader.Name == "DebtFixed")
                    {
                        int debtAmount = Convert.ToInt32(xmlReader.GetAttribute("Gold"));
                        priority = 7;

                        CardEffects.DebtEffectFixed debtEffect = new CardEffects.DebtEffectFixed(player, debtAmount, player.form_);
                        immediateEffects.Add(debtEffect);

                        desc += "This card causes all other players to incur a debt of " + debtAmount + " or lose points as a penalty.\n\n";
                    }
                    else if (xmlReader.Name == "Spy")
                    {
                        CardEffects.SpyEffect spyEffect = new CardEffects.SpyEffect(player);
                        immediateEffects.Add(spyEffect);

                        desc += "This card Allows you to copy once science a neighbouring city has played (you may not copy more sciences of any type then they actually posess.\n\n";
                    }
                    else if (xmlReader.Name == "Diplomacy")
                    {
                        CardEffects.DiplomacyEffect diplomacyEffect = new CardEffects.DiplomacyEffect(player);
                        immediateEffects.Add(diplomacyEffect);

                        desc += "This card grants you diplomacy; thus excluding you from war (you suffer neither the negative nor the positive effects).\n\n";
                    }
                    else if (xmlReader.Name == "FreeLeaders")
                    {
                        CardEffects.LeadersFreeEffect leadersFree = new CardEffects.LeadersFreeEffect(player);
                        immediateEffects.Add(leadersFree);

                        desc += "All of your leaders card are now built at no cost.\n\n";
                    }
                    else if (xmlReader.Name == "CardCopy")
                    {
                        int cardType = Convert.ToInt32(xmlReader.GetAttribute("cardType"));

                        desc += "This card allows you to copy one of your enighbors cards of the corresponding color. The copied card is chosen at the end of the game.\n\n";
                        CardEffects.CardCopyEffect cardCopyEffect = new CardEffects.CardCopyEffect(this, player, (Card.CardColor)cardType);
                        immediateEffects.Add(cardCopyEffect);
                    }
                    else if (xmlReader.Name == "FreeBuild")
                    {
                        //change sending of panel to sending of card and change size and loation accordingly

                        int cardType = Convert.ToInt32(xmlReader.GetAttribute("cardType"));
                        bool applyAll = cardType == -1;
                        Card.CardColor color = (Card.CardColor)(applyAll ? 0 : cardType);
                        bool singleUse = Convert.ToInt32(xmlReader.GetAttribute("SingleUse")) == 1;

                        CardEffects.FreeBuild freeBuild = new CardEffects.FreeBuild(player, color, this, singleUse, applyAll);
                        immediateEffects.Add(freeBuild);

                        desc += "This card allows you to build one ";

                        if (applyAll)
                        {
                            desc += "of any card ";
                        }
                        else
                        {
                            desc += Card.getStringFromCardColor(color) + " card ";
                        }

                        desc += "for free once per age.\n\n";
                    }
                    else if (xmlReader.Name == "CardCopy")
                    {
                        int cardType = Convert.ToInt32(xmlReader.GetAttribute("cardType"));

                        desc += "This card allows you to copy one of your enighbors cards of the corresponding color. The copied card is chosen at the end of the game.\n\n";
                        CardEffects.CardCopyEffect cardCopyEffect = new CardEffects.CardCopyEffect(this, player, (Card.CardColor)cardType);
                        immediateEffects.Add(cardCopyEffect);
                    }
                    else if (xmlReader.Name == "VictoryPointsPerCardType")
                    {
                        String direction = xmlReader.GetAttribute("Target");
                        Player targetPlayer = player;
                        String endofDesc = " card you have played.\n\n";
                        int cardColorNum = Convert.ToInt32(xmlReader.GetAttribute("cardType"));
                        int pointsPerCard = Convert.ToInt32(xmlReader.GetAttribute("Points"));

                        if (direction.Equals("Left"))
                        {
                            targetPlayer = player.leftNeighbor;
                            endofDesc = " card your left neighbor has played.\n\n";
                        }
                        else if (direction.Equals("Right"))
                        {
                            targetPlayer = player.rightNeighbor;
                            endofDesc = " card your right neighbor has played.\n\n";
                        }

                        Card.CardColor color = (Card.CardColor)cardColorNum;

                        CardEffects.VictoryPointsPerCardTypeEffect pointsPerCardType = new CardEffects.VictoryPointsPerCardTypeEffect(targetPlayer, color, pointsPerCard);
                        victoryPoints.Add(pointsPerCardType);

                        desc += "This card generates " + pointsPerCard + " Victory Point(s) per " + Card.getStringFromCardColor(color) + endofDesc;

                    }
                    else if (xmlReader.Name == "Resurrection")
                    {
                        priority = Convert.ToInt32(xmlReader.GetAttribute("Priority"));

                        desc += "This card allows the player to build one discarded card for free.\n\n";

                        CardEffects.ResurrectionEffect ressurectionEffect = new CardEffects.ResurrectionEffect(player);
                        immediateEffects.Add(ressurectionEffect);
                    }
                    else if (xmlReader.Name == "ResourceNonGenerated")
                    {
                        CardEffects.ResourceNonGeneratedEffect resourceNonGeneratedEffect = new CardEffects.ResourceNonGeneratedEffect(player,this);
                        immediateEffects.Add(resourceNonGeneratedEffect);

                        desc += "This card generates one resource the user can't produce.\n\n";
                    }
                    else if (xmlReader.Name == "GoldGenerationCard")
                    {
                        String targetName = xmlReader.GetAttribute("Target");
                        Player target = player;
                        int goldPerCard = Convert.ToInt32(xmlReader.GetAttribute("Gold"));
                        Card.CardColor color = (CardColor)Convert.ToInt32(xmlReader.GetAttribute("CardColor"));
                        desc += "This card generates " + goldPerCard + " per " + Card.getStringFromCardColor(color) + " card your ";

                        if (targetName.Equals("Left"))
                        {
                            target = player.leftNeighbor;
                            desc += " left neighbor has.\n\n";
                        }
                        else if (targetName.Equals("Right"))
                        {
                            target = player.rightNeighbor;
                            desc += " right neighbor has.\n\n";
                        }
                        else{
                            desc += " you have.\n\n";
                        }

                        CardEffects.GoldGenerationPerCardEffect goldEffect = new CardEffects.GoldGenerationPerCardEffect(player,target,color,goldPerCard);
                        immediateEffects.Add(goldEffect);
                    }
                    else if (xmlReader.Name == "WondersFree")
                    {
                        CardEffects.WondersFreeEffect wondersFree = new CardEffects.WondersFreeEffect(player);
                        immediateEffects.Add(wondersFree);

                        desc += "This card makes all of your wonders cost 0 resources (with the exception of gold)\n\n";
                    }
                    else if (xmlReader.Name == "CardDiscount")
                    {
                        Card.CardColor color = (Card.CardColor)Convert.ToInt32(xmlReader.GetAttribute("cardType"));

                        CardEffects.CardDiscountEffect cardDiscount = new CardEffects.CardDiscountEffect(player, color);
                        immediateEffects.Add(cardDiscount);

                        desc += "This card allows " + Card.getStringFromCardColor(color) + " cards to be built at one resource less than their usual cost.\n\n";
                    }
                    else if (xmlReader.Name == "VictoryPointsPerMilitaryToken")
                    {
                        String targetUsername = xmlReader.GetAttribute("Target");
                        int pointsPerToken = Convert.ToInt32(xmlReader.GetAttribute("Points"));

                        String beginningDesc = "This card generates " + pointsPerToken + " Vcitory Point(s) for each military victory token ";

                        Player target = player;

                        if (targetUsername.Equals("Left"))
                        {
                            target = player.leftNeighbor;
                            beginningDesc += "your left neighbor has.\n\n";
                        }
                        else if (targetUsername.Equals("Right"))
                        {
                            target = player.rightNeighbor;
                            beginningDesc += "your right neighbor has.\n\n";
                        }
                        else
                        {
                            beginningDesc += "you have.\n\n";
                        }

                        desc += beginningDesc;

                        CardEffects.VictoryPointsPerMilitaryTokenEffect victoryPointsPerMilitaryToken = new CardEffects.VictoryPointsPerMilitaryTokenEffect(target, pointsPerToken);
                        victoryPoints.Add(victoryPointsPerMilitaryToken);                        
                    }
                    else if (xmlReader.Name == "VictoryPointsPerWonder")
                    {
                        String targetUsername = xmlReader.GetAttribute("Target");
                        int pointsPerWonder = Convert.ToInt32(xmlReader.GetAttribute("Points"));

                        String beginningDesc = "This card generates " + pointsPerWonder + " Victory Point(s) for each wonder ";

                        Player target = player;

                        if (targetUsername.Equals("Left"))
                        {
                            target = player.leftNeighbor;
                            beginningDesc += "your left neighbor has built.\n\n";
                        }
                        else if (targetUsername.Equals("Right"))
                        {
                            target = player.rightNeighbor;
                            beginningDesc += "your right neighbor has built.\n\n";
                        }
                        else
                        {
                            beginningDesc += "you have built.\n\n";
                        }

                        desc += beginningDesc;

                        CardEffects.VictoryPointsPerWonderEffect victoryPointsPerWonderEffect = new CardEffects.VictoryPointsPerWonderEffect(target, pointsPerWonder);
                        immediateEffects.Add(victoryPointsPerWonderEffect);
                        victoryPoints.Add(victoryPointsPerWonderEffect);
                    }
                    else if (xmlReader.Name == "VictoryPointsPerScienceSet")
                    {
                        int pointsPerSet = Convert.ToInt32(xmlReader.GetAttribute("Points"));
                        desc += "This card generates " + pointsPerSet + " Victory Point(s) per full science set you have.\n\n";

                        CardEffects.VictoryPointsPerScienceSetEffect victoryPointsPerScienceSet = new CardEffects.VictoryPointsPerScienceSetEffect(player, pointsPerSet);
                        victoryPoints.Add(victoryPointsPerScienceSet);
                    }
                    else if (xmlReader.Name == "GoldGainOnIncome")
                    {
                        int gain = Convert.ToInt32(xmlReader.GetAttribute("Gold"));

                        desc += "This card generates " + gain + " gold everytime you get gold from the bank. (limited to once per turn).\n\n";

                        CardEffects.GoldGenerationOnIncome goldGenerationOnIncome = new CardEffects.GoldGenerationOnIncome(player, gain);
                        immediateEffects.Add(goldGenerationOnIncome);
                    }
                    else if (xmlReader.Name == "ResourceOptionAllAtCost")
                    {
                        desc += "This card allows the user to get one of any reason for the turn at the cost of 1 gold.\n\n";

                        CardEffects.ResourceOptionAllAtCost resourceOptionAllAtCost = new CardEffects.ResourceOptionAllAtCost(player, this);
                        immediateEffects.Add(resourceOptionAllAtCost);
                    }
                    else if (xmlReader.Name == "FreeBuild")
                    {
                        int cardType = Convert.ToInt32(xmlReader.GetAttribute("cardType"));
                        bool applyAll = cardType == -1;
                        Card.CardColor color = (Card.CardColor)(applyAll ? 0 : cardType);
                        bool singleUse = Convert.ToInt32(xmlReader.GetAttribute("SingleUse")) == 1;

                        CardEffects.FreeBuild freeBuild = new CardEffects.FreeBuild(player, color, this, singleUse, applyAll);
                        immediateEffects.Add(freeBuild);

                        desc += "This card allows you to build one ";

                        if (applyAll)
                        {
                            desc += "of any card ";
                        }
                        else
                        {
                            desc += Card.getStringFromCardColor(color) + " card ";
                        }

                        desc += "once per age.\n\n";
                    }
                    else if (xmlReader.Name == "GoldGainOnCardPlay")
                    {
                        Card.CardColor color = (Card.CardColor)Convert.ToInt32(xmlReader.GetAttribute("cardType"));
                        int gold = Convert.ToInt32(xmlReader.GetAttribute("Gold"));

                        CardEffects.GoldGainOnCardPlayEffect goldGainEffect = new CardEffects.GoldGainOnCardPlayEffect(player, color, gold);
                        immediateEffects.Add(goldGainEffect);

                        desc += "This card grants " + gold + " gold every time you play a " + Card.getStringFromCardColor(color) + " card.\n\n";
                    }
                    else if (xmlReader.Name == "IncomeOnTrade")
                    {
                        String direction = xmlReader.GetAttribute("Target");
                        int gold = Convert.ToInt32(xmlReader.GetAttribute("Gold"));

                        Player target = player.leftNeighbor;

                        if (direction.Equals("Right"))
                        {
                            desc += "This card grants " + gold + " gold once per turn when you trade with your right neighbor.\n\n";
                            target = player.rightNeighbor;
                        }
                        else
                        {
                            desc += "This card grants " + gold + " gold once per turn when you trade with your left neighbor.\n\n";
                        }

                        CardEffects.IncomeOnTradeEffect incomeOnTrade = new CardEffects.IncomeOnTradeEffect(player, target, gold);
                        immediateEffects.Add(incomeOnTrade);
                    }
                    else if (xmlReader.Name == "WonderDiscount")
                    {
                        desc += "This card allows wonders to be built at one resource less than their usual cost.\n\n";

                        CardEffects.WonderDiscountEffect wonderDiscount = new CardEffects.WonderDiscountEffect(player);
                        immediateEffects.Add(wonderDiscount);
                    }
                    else if (xmlReader.Name == "VictoryPointsPerCardSet")
                    {
                        String maskDescription = "";

                        int points = Convert.ToInt32(xmlReader.GetAttribute("Points"));
                        int nb = Convert.ToInt32(xmlReader.GetAttribute("nb"));

                        CardEffects.VictoryPointsPerCardSetEffect pointsEffect = new CardEffects.VictoryPointsPerCardSetEffect(player, points);

                        for (int count = 0; count < nb; count++)
                        {
                            int cardType = Convert.ToInt32(xmlReader.GetAttribute("cardType" + count));
                            pointsEffect.mask[cardType] = true;

                            Card.CardColor color = (Card.CardColor)cardType;
                            maskDescription += Card.getStringFromCardColor(color) + (count == nb - 1 ? " " : ", ");
                        }

                        victoryPoints.Add(pointsEffect);
                        desc += "This card grants " + points + " Victory Point(s) per set of: " + maskDescription + "you have played.\n\n";
                    }
                    else if (xmlReader.Name == "VictoryPointsPerGold")
                    {
                        int gold = Convert.ToInt32(xmlReader.GetAttribute("Gold"));
                        int points = Convert.ToInt32(xmlReader.GetAttribute("Points"));

                        CardEffects.VictoryPointsPerGold pointsPerGold = new CardEffects.VictoryPointsPerGold(player, gold, points);

                        desc += "This card generates " + points + " Victory Point(s) per " + gold + " gold you have in your posession.\n\n";
                        victoryPoints.Add(pointsPerGold);
                    }
                    else if (xmlReader.Name == "GoldIncomeOnWarVictory")
                    {
                        int gold = Convert.ToInt32(xmlReader.GetAttribute("Gold"));

                        desc += "This card generates " + gold + " gold when you receive a military victory token.\n\n";

                        CardEffects.GoldIncomeOnWarVictory goldGain = new CardEffects.GoldIncomeOnWarVictory(player, gold);
                        immediateEffects.Add(goldGain);
                    }
                    else if (xmlReader.Name == "OnWarLossIncreaseArmy")
                    {
                        CardEffects.OnWarLossIncreaseArmyEffect warLossIncrease = new CardEffects.OnWarLossIncreaseArmyEffect(player);
                        immediateEffects.Add(warLossIncrease);

                        desc += "This card gives one army for each defeat token you have (both current and futur).\n\n";
                    }
                    else if (xmlReader.Name == "GuildsFree")
                    {
                        CardEffects.GuildsFreeEffect guildsFree = new CardEffects.GuildsFreeEffect(player);
                        immediateEffects.Add(guildsFree);

                        desc += "This card makes all purple cards cost no resources.\n\n";
                    }
                    else if (xmlReader.Name == "OnWarLossReflect")
                    {
                        CardEffects.ReflectLossEffect reflectEffect = new CardEffects.ReflectLossEffect(player);
                        immediateEffects.Add(reflectEffect);

                        desc += "This card sends defeat tokens back to its sender.\n\n";
                    }
                    else if (xmlReader.Name == "OnChainBuildReceiveGold")
                    {
                        int gold = Convert.ToInt32(xmlReader.GetAttribute("Gold"));
                        desc += "This card generates " + gold + " gold everytime you chain build.\n\n";

                        CardEffects.ChainBuildGold chainBuildGold = new CardEffects.ChainBuildGold(player, gold);
                        immediateEffects.Add(chainBuildGold);                        
                    }
                    else if (xmlReader.Name == "GoldPerWonder")
                    {
                        int gold = Convert.ToInt32(xmlReader.GetAttribute("Gold"));
                        desc += "This card generates " + gold + " gold per wonder you have built.\n\n";

                        CardEffects.GoldPerWonder goldPerWonder = new CardEffects.GoldPerWonder(player, gold);
                        immediateEffects.Add(goldPerWonder);
                    }
                    else if (xmlReader.Name == "DebtPerWonder")
                    {
                        int debt = Convert.ToInt32(xmlReader.GetAttribute("Gold"));
                        desc += "This card makes all players (except you) pay a debt of up to " + debt + " gold per wonder they have built.\n\n";

                        CardEffects.DebtEffectPerWonder debtPerWonder = new CardEffects.DebtEffectPerWonder(player, debt);
                        immediateEffects.Add(debtPerWonder);
                    }
                    else if (xmlReader.Name == "GoldPerMilitaryToken")
                    {
                        String direction = xmlReader.GetAttribute("Target");
                        int gold = Convert.ToInt32(xmlReader.GetAttribute("Gold"));
                        Player target = player;

                        if (direction.Equals("Self"))
                        {
                            desc += "This card generates " + gold + " gold for each victory token you have.\n\n";
                        }
                        else if (direction.Equals("Right"))
                        {
                            desc += "This card generates " + gold + " gold for each victory token your right neighbor has.\n\n";
                            target = player.rightNeighbor;
                        }
                        else
                        {
                            desc += "This card generates " + gold + " gold for each victory token your left neighbor has.\n\n";
                            target = player.leftNeighbor;
                        }

                        CardEffects.GoldPerMilitaryToken goldMilitaryToken = new CardEffects.GoldPerMilitaryToken(player, target, gold);
                        immediateEffects.Add(goldMilitaryToken);
                    }
                    else if (xmlReader.Name == "VictoryPointsPerMilitaryDefeat")
                    {
                        String direction = xmlReader.GetAttribute("Target");
                        int points = Convert.ToInt32(xmlReader.GetAttribute("Points"));
                        Player target = player;

                        if (direction.Equals("Self"))
                        {
                            desc += "This card generates " + points + " Victory Point(s) for each defeat token you have.\n\n";
                        }
                        else if (direction.Equals("Right"))
                        {
                            desc += "This card generates " + points + " Victory Point(s) for each defeat token your right neighbor has.\n\n";
                            target = player.rightNeighbor;
                        }
                        else
                        {
                            desc += "This card generates " + points + " Victory Point(s) for each defeat token your left neighbor has.\n\n";
                            target = player.leftNeighbor;
                        }

                        CardEffects.VictoryPointsPerDefeatToken defeatTokenPoints = new CardEffects.VictoryPointsPerDefeatToken(target, points);
                        victoryPoints.Add(defeatTokenPoints);
                    }
                    else if (xmlReader.Name == "LeaderCopy")
                    {
                        moreInfoNeeded = true;
                        desc += "This card allows you to a copy the effect(s) one of your neighbors leaders. \n\n";

                        CardEffects.LeaderCopyEffect leaderCopy = new CardEffects.LeaderCopyEffect(player,this);
                        immediateEffects.Add(leaderCopy);
                        victoryPoints.Add(leaderCopy);

                        priority = 7;
                    }
                    else if (xmlReader.Name == "ScienceChoice")
                    {
                        desc += "This card generates one of the following: Gear Science, Tablet Science or Geometry Science.\n\n";

                        CardEffects.ScienceChoiceEffect scienceChoice = new CardEffects.ScienceChoiceEffect(player);
                        immediateEffects.Add(scienceChoice);
                    }
                    else if (xmlReader.Name == "DebtPerMilitaryVictory")
                    {
                        int debt = Convert.ToInt32(xmlReader.GetAttribute("Gold"));
                        CardEffects.DebtEffectMilitaryVictory debtMilitaryEffect = new CardEffects.DebtEffectMilitaryVictory(player, debt, player.form_);
                        immediateEffects.Add(debtMilitaryEffect);

                        desc += "This card makes all players (except you) pay a debt of up to " + debt + " gold per victory token they have.\n\n";
                    }
                }
            }

            xmlReader.Close();
        }
        public String getDesc()
        {
            return desc;
        }
        public void play()
        {
            played = true;

            if (rotation != Player.PlayerRotation.Clockwise180)
            {
                picture.Location = new Point(0, 0);
                picture.Size = new Size(Width, Height);
            }

            if (color != CardColor.Roster)
            {
                for (int count = 0; count < immediateEffects.Count; count++)
                {
                    immediateEffects[count].play();
                }
            }

            BackgroundImage = picture.BackgroundImage;
            BackgroundImageLayout = ImageLayout.Stretch;

            Controls.Remove(PBBurn);
            Controls.Remove(PBPlay);
            Controls.Remove(PBWonder);

            if (priority <= 1)
            {
                if (CardPlayed != null)
                {
                    CardPlayed(this, new CardPlayedEvent("Card was Played"));
                }
            }
        }
        private void raiseCardPlayEvent(object sender, EventArgs e)
        {
            raiseCardPlayed();
        }
        public String getName()
        {
            return name;
        }
        public String getLocation()
        {
            return cardLocation;
        }
        public int getVictoryPoint()
        {
            int vicotryPointsTotal = 0;

            for (int count = 0; count < victoryPoints.Count; count++)
            {
                vicotryPointsTotal += victoryPoints[count].generateVicotryPoints();
            }

            return vicotryPointsTotal;
        }
        public void rotateCard(Player.PlayerRotation rotation_)
        {
            rotation = rotation_;

            if (rotation == Player.PlayerRotation.Clockwise180)
            {
                //Console.WriteLine("In rotate card.");
                Image img = Image.FromFile(cardLocation + ".PNG");
                img.RotateFlip( RotateFlipType.Rotate180FlipNone);
                picture.BackgroundImage = img;
                picture.Location = new Point(0, Convert.ToInt32(Height * 0.15));
            }
        }
        private void raiseBurn(object sender, EventArgs e)
        {
            if (CardBurn != null)
            {
                CardBurn(this, new CardBurnEvent("Card Burned."));
            }
        }
        private void raiseWonder(object sender, EventArgs e)
        {
            if (CardWonder != null)
            {
                CardWonder(this, new CardWonderPlayEvent("Wonder play attempt."));
            }
        }
        private void resize(object sender, EventArgs e)
        {
            if (played)
            {
                picture.Size = this.Size;
            }
        }
        public void raiseCardPlayedForUnlocking()
        {
            if (CardPlayed != null)
            {
                CardPlayed(this, new CardPlayedEvent("Card played."));
            }
        }
        public void raiseCardPlayed()
        {
            if (CardPlay != null)
            {
                CardPlay(this, new CardPlayEvent("Card was Played"));
            }
        }
        private void raiseMoreInfoReceived(object sender, CardEffects.CardEffect.AdditionalInfoAddedEvent e)
        {
            if (MoreInfoReceived != null)
            {
                MoreInfoReceived(this, e);
            }
        }
        public void getMoreInfo()
        {
            foreach (CardEffects.CardEffect item in immediateEffects)
            {
                if (item.MoreInfoNeeded)
                {
                    item.getMoreInfo();
                }
            }
        }
        public void raiseMoreInfoReceived()
        {
            if (MoreInfoReceived != null)
            {
                MoreInfoReceived(this, new CardEffects.CardEffect.AdditionalInfoAddedEvent(""));
            }
        }
        public static String getStringFromCardColor( CardColor color)
        {
            switch (color)
            {
                case CardColor.Brown:
                    {
                        return "Brown";
                    }
                case CardColor.Gray:
                    {
                        return "Gray";
                    }
                case CardColor.Yellow:
                    {
                        return "Yellow";
                    }
                case CardColor.Blue:
                    {
                        return "Blue";
                    }
                case CardColor.Red:
                    {
                        return "Red";
                    }
                case CardColor.Green:
                    {
                        return "Green";
                    }
                case CardColor.Purple:
                    {
                        return "Purple";
                    }
                case CardColor.White:
                    {
                        return "Leader";
                    }
                case CardColor.Black:
                    {
                        return "Black";
                    }
                default:
                    {
                        return "";
                    }
            }
        }
        private void raiseCardDoubleClick(object sender, EventArgs e)
        {
            if (CardDoubleClick != null)
            {
                CardDoubleClick(this, new CardDoubleClickEvent("Card was double clicked."));
            }
        }
        public void removeCardAbililities()
        {
            foreach (CardEffects.CardEffect item in immediateEffects)
            {
                item.cancelAbilities();
            }
        }
        private void delayedplayCardMessage(object source, CardEffects.CardEffect.AdditionalInfoAddedEvent e)
        {
            player.form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "PLAYCARD" + Constants.DELIM_FIELD + cardLocation + Constants.DELIM_FIELD + e.GetInfo());
        }
        public void receiveEndofGameInfo(String info)
        {
            foreach (CardEffects.CardEffect item in immediateEffects)
            {
                item.receiveEndofGameInfo(info);
            }
        }
        public void getEndofGameInfo()
        {
            foreach (CardEffects.CardEffect item in immediateEffects)
            {
                item.getEndofGameInfo();
            }
        }
    }
}
