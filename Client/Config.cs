﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace Client
{
    public class Config
    {
        Form1 form;

        String username = Environment.UserName;
        String IP = "192.168.1.1";

        String Display = Constants.WONDERS_FOLDER + "AlexandriaIcon.PNG";
        List<String> Displays = new List<String>();//[Constants.WONDERS];
        List<Wonder> Wonders = new List<Wonder>();

        int port = 6114;
        int porth = 6114;

        Boolean Host = false;
        String filePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\7Wonders\\" + Constants.CONFIGURATION;

        bool Leaders = false;
        bool Cities = false;
        bool Babel = false;

        int time = 60;
        bool ABPlayerChoice = false;

        public Config(Form1 form_) {
            form = form_;
            loadDisplays();

            ////Console.WriteLine(file);
            if (File.Exists(filePath))
            {
                loadFromFile();
            }
            else {
                FileInfo dir = new FileInfo(filePath);
                dir.Directory.Create();
            }
        }

        public String getUsername(){
            return username;
        }
        public void setUsername(String ans) {
            username = ans;
        }
        public String getIP() {
            return IP;
        }
        public void setIP(String ans) {
            IP = ans;
        }
        public int getPort() {
            return port;
        }
        public void setPort(int ans) {
            port = ans;
        }
        public void setPorth(int ans) {
            porth = ans;
        }
        public int getPorth() {
            return porth;
        }
        public bool isHost() {
            return Host;
        }
        public void setHost(bool ans) {
            Host = ans;
        }
        public string getDisplay() {
            return Display;
        }
        public void setDisplay(String ans) {
            Display = ans;
        }
        public void loadDisplays() {
            int count = 0;
           
            XmlTextReader xmlReader = new XmlTextReader(Constants.WONDERS_XML);

            while (xmlReader.Read())
            {

                if (xmlReader.Name == "Cards" && xmlReader.NodeType != XmlNodeType.EndElement)
                {
                    bool Leaders = xmlReader.GetAttribute("Leaders").Equals("1");
                    bool Cities = xmlReader.GetAttribute("Cities").Equals("1");
                    int nb = Convert.ToInt32(xmlReader.GetAttribute("nb"));
                    ////Console.WriteLine(nb);
                    for (int inc = 0; inc < nb; inc++)
                    {

                        do
                        {
                            xmlReader.Read();
                        } while (xmlReader.NodeType != XmlNodeType.Element);

                        //wonders[count] = new Wonder(xmlReader.GetAttribute("name"), Leaders, Cities);
                        count++;
                        String name = xmlReader.GetAttribute("Name");
                        Wonders.Add(new Wonder(name,Leaders,Cities,form));
                        bool display = xmlReader.GetAttribute("Icon").Equals("1");
                        Displays.Add(Constants.WONDERS_FOLDER + name + "Icon.PNG");
                        ////Console.WriteLine(xmlReader.GetAttribute("name"));
                        ////Console.WriteLine(inc);

                    }
                }
            }

            xmlReader.Close();
        }

        public List<String> getDisplays() {
            return Displays;
        }
        public List<Wonder> getWonders()
        {
            return Wonders;
        }
        public void loadFromFile() {
            XmlTextReader xmlReader = new XmlTextReader(filePath);

            while (xmlReader.Read()) {
                if (xmlReader.Name == "Config") {

                    double version = Convert.ToDouble(xmlReader.GetAttribute("Version"));

                    setUsername(xmlReader.GetAttribute("Username"));
                    setIP(xmlReader.GetAttribute("IP"));
                    setPort(Convert.ToInt32(xmlReader.GetAttribute("Port")));
                    setPorth(Convert.ToInt32(xmlReader.GetAttribute("Porth")));
                    setDisplay(xmlReader.GetAttribute("Display"));

                    int wonderCount = Convert.ToInt32(xmlReader.GetAttribute("Wonders"));
                    

                    setTime(Convert.ToInt32(xmlReader.GetAttribute("Time")));
                    setABPlayerChoice(xmlReader.GetAttribute("PlayerAB").Equals("1"));
                    setLeaders(xmlReader.GetAttribute("Exp0").Equals("1"));
                    setCities(xmlReader.GetAttribute("Exp1").Equals("1"));

                    for (int count = 0; count < wonderCount; count++)
                    {
                        //Console.WriteLine(Wonders[count].getName() + ": " + xmlReader.GetAttribute("Wonder" + count).Equals("1"));
                        Wonders[count].setActive(xmlReader.GetAttribute("Wonder" + count).Equals("1"));
                    }
                }
            }

            xmlReader.Close();
        }
        public void saveToFile() {
            
            StreamWriter file = new StreamWriter(filePath);
            XmlDocument doc = new XmlDocument();
            XmlElement el = (XmlElement)doc.AppendChild(doc.CreateElement("Config"));

            el.SetAttribute("Username", getUsername());
            el.SetAttribute("IP", getIP());
            el.SetAttribute("Port", Convert.ToString(getPort()));
            el.SetAttribute("Porth", Convert.ToString(getPorth()));
            el.SetAttribute("Display", getDisplay());
            el.SetAttribute("Version", "" + Constants.VERSION);
            el.SetAttribute("Wonders", "" + Wonders.Count);

            el.SetAttribute("Time", "" + getTurnTime());
            el.SetAttribute("PlayerAB", getABPlayerChoice() ? "1" : "0");
            el.SetAttribute("Exp0", isLeaders()?"1":"0");
            el.SetAttribute("Exp1", isCities()?"1":"0");

            for (int count = 0; count < Wonders.Count; count++)
            {
                el.SetAttribute("Wonder" + count,Wonders[count].isActive()?"1":"0");
            }

            file.Write(doc.OuterXml);
            file.Close();
            //el.AppendChild(doc.CreateElement("Nested")).InnerText = "data";
           // //Console.WriteLine(doc.OuterXml);
        }
        public int getMaxPlayers() {
            if (Cities)
            {
                return Constants.MAX_PLAYERS_CITIES;
            }
            else {
                return Constants.MAX_PLAYERS;
            }
        }
        public bool isLeaders()
        {
            return Leaders;
        }
        public void setLeaders(bool ans)
        {
            Leaders = ans;
        }
        public bool isCities()
        {
            return Cities;
        }
        public void setCities(bool ans)
        {
            Cities = ans;
        }
        public int getTurnTime()
        {
            return time;
        }
        public void setTime(int ans)
        {
            time = ans;
        }
        public bool getABPlayerChoice()
        {
            return ABPlayerChoice;
        }
        public void setABPlayerChoice(bool ans)
        {
            ABPlayerChoice = ans;
        }
        public void setBabel(bool ans)
        {
            Babel = ans;
        }
        public bool isBabel()
        {
            return Babel;
        }
    }
}
