﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;

namespace Client
{
    public class ServerPlayerManager
    {
        Thread thread;
        Thread passCardsThread;
        String username;
        Socket handler;
        Server server;
        SocketKeepAlive socketKeepAlive;
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        bool started = false;
        bool connected = false;
        int timeout = Constants.NETWORK_RECONNECT_TIMEOUT_SERVER;

        public MessageOutWithConfirm MSGOut { get; private set; }

        String cardToPlayMessage = "";
        List<String> postPassMessages = new List<string>();
        List<String> processedCommands = new List<string>();
        //public postPlayHandler[] postPlay = new postPlayHandler[Constants.MAX_PRIO + 1];

        public delegate void postPlayHandler();
        
        public event PlayCardReceivedHandler playCardReceived;
        
        public event CardPassedHandler cardPassed;

        //First we have to define a delegate that acts as a signature for the
        //function that is ultimately called when the event is triggered.
        //You will notice that the second parameter is of MyEventArgs type.
        //This object will contain information about the triggered event.
        public delegate void CardPassedHandler(object source, CardPassedEvent e);

        //This is a class which describes the event to the class that recieves it.
        //An EventArgs class must always derive from System.EventArgs.
        public class CardPassedEvent : EventArgs
        {
            private string EventInfo;
            public CardPassedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        

        //First we have to define a delegate that acts as a signature for the
        //function that is ultimately called when the event is triggered.
        //You will notice that the second parameter is of MyEventArgs type.
        //This object will contain information about the triggered event.
        public delegate void PlayCardReceivedHandler(object source, PlayCardReceivedEvent e);

        //This is a class which describes the event to the class that recieves it.
        //An EventArgs class must always derive from System.EventArgs.
        public class PlayCardReceivedEvent : EventArgs
        {
            private string EventInfo;
            public PlayCardReceivedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }
        
        //SocketKeepAlive ACK;
        //CommandQeue commandQeue;

        public ServerPlayerManager(Server server_, Socket handler_, String username_) {
            server = server_;
            handler = handler_;
            thread = new Thread(() => host(this));
            username = username_;
            socketKeepAlive = new SocketKeepAlive(handler,server_);

            socketKeepAlive.onSocketAlive += regainedConnection;
            socketKeepAlive.onSocketDied += lostConnection;

            timer.Interval = Constants.NETWORK_INTERVAL;
            timer.Tick += disconnect;

            MSGOut = new MessageOutWithConfirm(handler);
        }
        public Server getServer(){
            return server;
        }
        public Socket getHandler(){
            return handler;
        }
        public String getUsername() {
            return username;
        }
        public void host(ServerPlayerManager serverPlayerManager) {
            ServerPlayerManager serverPlayerManager_ = serverPlayerManager;

            Server server_ = serverPlayerManager.getServer();
            String data = "";
            String username = serverPlayerManager_.getUsername();
            CommandQeue commandsReader = new CommandQeue(serverPlayerManager_.getHandler());
            //SocketKeepAlive socketKeepAlive = new SocketKeepAlive(handler);
            serverPlayerManager_.getServer().getForm().launchSocketKeepAliveD(socketKeepAlive);
            
            //String data = "";
            String command = "";
            while (started)
            {
                data = commandsReader.next();
                command = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                switch (command)
                {
                        
                    case "EXIT":
                    {
                        serverPlayerManager_.getServer().getForm().getGame().removePlayer(username);

                        for (int index = 0; index < serverPlayerManager_.getServer().getPlayers().Count; index++) {
                            if (serverPlayerManager_.getServer().getPlayers()[index].getUsername().Equals(username)) {
                                serverPlayerManager_.getServer().getPlayers().RemoveAt(index);
                                break;
                            }
                        }

                        server_.sendMessage("LEAVE" + Constants.DELIM_FIELD + username);

                        bool canStart = canStartGame(server_);

                        if (canStart)
                        {
                            server_.sendMessage("START" + Constants.DELIM_FIELD); 
                        }
                        else
                        {
                            server_.sendMessage("CANCEL" + Constants.DELIM_FIELD);
                        }

                        server_.getForm().getGame().setPlaying(canStart);

                        this.end();

                        break;
                    }
                    case "MSG":
                    {
                        server_.sendMessage(data);
                            break;
                    }
                    case "READY":
                    {
                        //Console.WriteLine("Received ready command.");
                        String user = data.Substring(command.Length + 1);

                        server_.getForm().getGame().setPlayerReady(user, true);
                        server_.sendMessage(data);

                        bool canStart = canStartGame(server_);
                        server_.getForm().getGame().setPlaying(canStart);

                        if (canStart)
                        {
                            server_.sendMessage("START" + Constants.DELIM_FIELD);
                        }
                        else
                        {
                            server_.sendMessage("CANCEL" + Constants.DELIM_FIELD);
                        }

                        break;
                    }
                    case "NOTREADY":
                    {
                        String user = data.Substring(command.Length + 1);
                        server_.getForm().getGame().setPlayerReady(user, false);
                        server_.sendMessage(data);

                        bool canStart = canStartGame(server_);
                        server_.getForm().getGame().setPlaying(canStart);

                        if (canStart)
                        {
                            server_.sendMessage("START" + Constants.DELIM_FIELD);
                        }
                        else
                        {
                            server_.sendMessage("CANCEL" + Constants.DELIM_FIELD);
                        }
                        break;
                    }
                    case "ACKREQUEST": 
                    {
                        Server.sendSingleMessage(handler, "ACKRESPONSE" + Constants.DELIM_FIELD);
                        break;
                    }
                    case "ACKRESPONSE": 
                    {
                        ////Console.WriteLine("Received Ack from: " + username);
                        socketKeepAlive.keepAlive();
                        break;
                    }
                    case "GAME":
                    {
                        data = data.Substring(command.Length + 1);

                        String turnSentString = data.Substring(0,data.IndexOf(Constants.DELIM_FIELD));
                        int turnSent = Convert.ToInt32(turnSentString);

                        if (turnSent != server.getForm().getGame().turn || processedCommands.Contains(data))
                        {
                            Server.sendSingleMessage(handler, "CONF" + Constants.DELIM_FIELD + "GAME" + Constants.DELIM_FIELD + data);
                            break;
                        }
                        else
                        {
                            Server.sendSingleMessage(handler, "CONF" + Constants.DELIM_FIELD + "GAME" + Constants.DELIM_FIELD + data);
                            processedCommands.Add(data);
                        }
                        
                        data = data.Substring(turnSentString.Length + 1);
                        String gameCommand = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));

                        switch (gameCommand)
                        {
                            case "WONDERCHOICE":
                                {
                                    server_.wondersChosen++;
                                    server_.sendMessage("GAME" + Constants.DELIM_FIELD + data);
                                    break;
                                }
                            case "PLAYCARD":
                                {
                                    data = data.Substring(gameCommand.Length + 1);
                                    try
                                    {
                                        String cardLocation = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                        String addInfo = data.Substring(cardLocation.Length + 1);
                                        cardToPlayMessage = "GAME" + Constants.DELIM_FIELD + "PLAYCARD" + Constants.DELIM_FIELD + cardLocation + Constants.DELIM_FIELD + username + Constants.DELIM_FIELD + addInfo;
                                    }
                                    catch (ArgumentOutOfRangeException e)
                                    {
                                        cardToPlayMessage = "GAME" + Constants.DELIM_FIELD + "PLAYCARD" + Constants.DELIM_FIELD + data + Constants.DELIM_FIELD + username;
                                    }
                                    
                                    server_.sendMessage("GAME" + Constants.DELIM_FIELD + "CARDPLAYED" + Constants.DELIM_FIELD + username);
                                    //server_.sendMessage("GAME" + Constants.DELIM_FIELD + data + Constants.DELIM_FIELD + username);
                                    break;
                                }
                            case "PASSHAND":
                                {
                                    data = data.Substring(gameCommand.Length + 1);
                                    Game.GameAge age = (Game.GameAge)Convert.ToInt32(data.Substring(0, data.IndexOf(Constants.DELIM_FIELD)));

                                    if (age == server_.getForm().getGame().getAge())
                                    {
                                        data = data.Substring(data.IndexOf(Constants.DELIM_FIELD) + 1);
                                        
                                        String user = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));
                                        data = data.Substring(user.Length + 1);

                                        Socket receiver = server_.getPlayer(user).getHandler();
                                        String cardsToPass = data.Substring(0);
                                        passCardsThread = new Thread(() => passCards(receiver, cardsToPass));
                                        passCardsThread.Start();

                                        if (playCardReceived != null)
                                        {
                                            playCardReceived(this, new PlayCardReceivedEvent("Play Card received."));
                                        }
                                    }
                                    break;
                                }
                            case "TRADE":
                                {
                                    postPassMessages.Add("GAME" + Constants.DELIM_FIELD + data);
                                    break;
                                }
                            case "COIN":
                                {
                                    postPassMessages.Add("GAME" + Constants.DELIM_FIELD + data);
                                    break;
                                }
                            case "COINNOW":
                                {
                                    data = data.Substring(gameCommand.Length + 1);
                                    server.sendMessage("GAME" + Constants.DELIM_FIELD + "COIN" + Constants.DELIM_FIELD + data);
                                    break;
                                }
                            case "DEBT":
                                {
                                    server.sendMessage("GAME" + Constants.DELIM_FIELD + data);
                                    break;
                                }
                            case "BURN":
                                {
                                    data = data.Substring(gameCommand.Length + 1);
                                    //postPassMessages.Add("GAME" + Constants.DELIM_FIELD + "COIN" + Constants.DELIM_FIELD + username + Constants.DELIM_FIELD + 3);
                                    //server.Graveyard.Add(data.Substring(0));
                                    cardToPlayMessage = "GAME" + Constants.DELIM_FIELD + "BURN" + Constants.DELIM_FIELD + username + Constants.DELIM_FIELD + data;
                                    server_.sendMessage("GAME" + Constants.DELIM_FIELD + "CARDPLAYED" + Constants.DELIM_FIELD + username);
                                    break;
                                }
                            case "WAR":
                                {
                                    server.sendMessage("GAME" + Constants.DELIM_FIELD + data);
                                    break;
                                }
                            case "ROSTER":
                                {
                                    cardToPlayMessage = "GAME" + Constants.DELIM_FIELD + "PLAYCARD" + Constants.DELIM_FIELD + Constants.PICTURES + "LEADERS\\Back" + Constants.DELIM_FIELD + username;
                                    server_.sendMessage("GAME" + Constants.DELIM_FIELD + "CARDPLAYED" + Constants.DELIM_FIELD + username);
                                    break;
                                }
                            case "LEADERHIRE":
                                {
                                    cardToPlayMessage = "GAME" + Constants.DELIM_FIELD + data + Constants.DELIM_FIELD + username;
                                    server_.sendMessage("GAME" + Constants.DELIM_FIELD + "CARDPLAYED" + Constants.DELIM_FIELD + username);
                                    break;
                                }
                            case "WONDERSTAGE":
                                {
                                    cardToPlayMessage = "GAME" + Constants.DELIM_FIELD + data;
                                    server_.sendMessage("GAME" + Constants.DELIM_FIELD + "CARDPLAYED" + Constants.DELIM_FIELD + username);
                                    break;
                                }
                            case "GRAVEADD":
                                {
                                    server_.sendMessage("GAME" + Constants.DELIM_FIELD + data);
                                    break;
                                }
                            case "GRAVEDEL":
                                {
                                    server_.sendMessage("GAME" + Constants.DELIM_FIELD + data);
                                    break;
                                }
                            case "CARDRES":
                                {
                                    server_.sendMessage("GAME" + Constants.DELIM_FIELD + data);
                                    break;
                                }
                            case "UNLOCK":
                                {
                                    server_.sendMessage("GAME" + Constants.DELIM_FIELD + data);
                                    break;
                                }
                            case "LEADERDRAW":
                                {
                                    data = data.Substring(gameCommand.Length + 1);
                                    int leadersToDraw = Convert.ToInt32(data);

                                    String leaderMsg = "GAME" + Constants.DELIM_FIELD + "LEADERDRAW" + Constants.DELIM_FIELD + leadersToDraw;

                                    for (int count = 0; count < leadersToDraw; count++)
                                    {
                                        String newLeader = server.getForm().getPNLMainContainer().getServer().deckBuilder.getLeaderDeck().deal();
                                        leaderMsg += Constants.DELIM_FIELD + newLeader;
                                    }

                                    MSGOut.addMessage(leaderMsg.Replace("GAME", "GAME" + Constants.DELIM_FIELD + server.getForm().getGame().turn));
                                    Server.sendSingleMessage(handler, leaderMsg);
                                    break;
                                }
                            case "CHAIN":
                                {
                                    postPassMessages.Add("GAME" + Constants.DELIM_FIELD + "CHAIN" + Constants.DELIM_FIELD + username);
                                    break;
                                }
                            case "ENDOFGAMEINFO":
                                {
                                    server.sendMessage("GAME" + Constants.DELIM_FIELD + data);
                                    break;
                                }
                            case "SCIENCECHOICE":
                                {
                                    server.sendMessage("GAME" + Constants.DELIM_FIELD + data);
                                    break;
                                }
                            case "WARPROCESSED":
                                {
                                    server.sendMessage("GAME" + Constants.DELIM_FIELD + data);
                                    break;
                                }
                            case "REMOVELEADERROSTER":
                                {
                                    postPassMessages.Add("GAME" + Constants.DELIM_FIELD + data);
                                    break;
                                }
                            default:
                                {
                                    //Console.WriteLine("Unrecognized game command: " + gameCommand);
                                    break;
                                }
                        }
                        
                        break;
                    }
                    case "CONF":
                    {
                        data = data.Substring(command.Length + 1);
                        MSGOut.removeMessage(data);
                        break;
                    }
                    default:
                    {
                        //Console.WriteLine("Unrecognized command: " + command);
                            break;
                    }
                }
            }
        }
        public int Send(byte[] msg) {
            try
            {
                return handler.Send(msg);
            }
            #pragma warning disable 0168
            catch (SocketException e)
            {
                return 0;
            }
            catch (ObjectDisposedException e)
            {
                return 0;
            }
            #pragma warning restore 0168
        }
        private void lostConnection(object sender, EventArgs e)
        {
            connected = false;
            //Console.WriteLine("Lost connection ServerPlayerManage with user: " + username);
            server.sendMessage("LOST" + Constants.DELIM_FIELD + username);
            timer.Start();
        }
        private void regainedConnection(object sender, EventArgs e)
        {
            connected = true;
            //Console.WriteLine("Regained connection ServerPlayerManager with user: " + username);
            server.sendMessage("FOUND" + Constants.DELIM_FIELD + username);
        }
        public void start() {
            if (!started)
            {
                connected = true;
                //SocketKeepAlive cannot be launched from this object as the while(true) loop prevents the Timer.tick event from being executed.
                server.getForm().launchSocketKeepAliveD(socketKeepAlive);
                thread.Start();
                started = true;
            }
        }
        public void end() {
            //Console.WriteLine("In end for serverplayermanager.");
            if (started)
            {
                timer.Stop();
                socketKeepAlive.stop();
                try
                {
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }
#pragma warning disable 0168
                catch (System.ObjectDisposedException ex) {

                }
#pragma warning restore 0168
                //thread.Abort();
                started = false;
            }
        }
        private void disconnect(object sender, EventArgs e) {
            if (!connected)
            {
                //Console.WriteLine("Timeout: " + timeout);
                timeout--;
                if (timeout == 0)
                {
                    timer.Stop();
                    timeout = Constants.NETWORK_RECONNECT_TIMEOUT_SERVER;
                    server.sendMessage("LEAVE" + Constants.DELIM_FIELD + username);
                    //game.getForm().getPNLMainContainer().showPopUp("Lost Connection", "The connection with the server was unexpectedly terminated. Please ensure both you and the host have a connecting network conneciton and try again.");
                    //game.clearGame();
                }
            }
            else
            {
                timer.Stop();
                timeout = Constants.NETWORK_RECONNECT_TIMEOUT_SERVER;
            }
        }
        private bool canStartGame(Server server_)
        {
            //Console.WriteLine("In can start Game.");
            
            if (server_.getForm().getGame().allReady())
            {
                //Console.WriteLine("Passed initial check.");
                if (server_.getForm().getGame().getnbPlayers() < 3)
                {
                    server_.sendMessage("SYS" + Constants.DELIM_FIELD + "A Game cannot be started because there are fewer than 3 players.");
                    return false;
                }
                else if (!server_.getForm().getGame().isCities() && server_.getForm().getGame().getnbPlayers() == Constants.MAX_PLAYERS_CITIES)
                {
                    server_.sendMessage("SYS" + Constants.DELIM_FIELD + "A Game cannot be started because there are 8 players and the cities expansion is not enabled.");
                    return false;
                }
                else if (server_.getForm().getGame().getNbActiveWonders() < server_.getForm().getGame().getnbPlayers())
                {
                    server_.sendMessage("SYS" + Constants.DELIM_FIELD + "A Game cannot be started because there are insufficient wonders active for the number of players in the game.\n + Players: " + server_.getPlayers().Count + " Wonders: " + server_.getForm().getGame().getNbActiveWonders());
                    return false;
                }

                return true;
            }
            
            return false;
        }

        private void passCards(Socket handler, String cardsWithNumberToPass)
        {
            //Console.WriteLine("In before passing of cards.");
            Server.passCards.WaitOne();
            //Console.WriteLine("Passing cards: " + cardsWithNumberToPass);
            String msg = "GAME" + Constants.DELIM_FIELD + server.getForm().getGame().turn + Constants.DELIM_FIELD + "CARDS" + Constants.DELIM_FIELD + cardsWithNumberToPass;
            MSGOut.addMessage(msg);
            Server.sendSingleMessage(handler, msg);

            while (postPassMessages.Count != 0)
            {
                MSGOut.addMessage(postPassMessages[0]);
                server.sendMessage(postPassMessages[0]);
                postPassMessages.RemoveAt(0);
            }

            server.sendMessage(cardToPlayMessage);

            //Console.WriteLine("Cards passed.");

            if (cardPassed != null)
            {
                cardPassed(this, new CardPassedEvent("Cards Passed."));
            }
        }
    }
}
