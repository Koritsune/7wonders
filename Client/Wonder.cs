﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client
{
    public class Wonder : Panel
    {
        String name;
        char side;
        bool active = true;

        bool cities = false;
        bool leaders = false;

        public Player.PlayerRotation rotation { get; set; }
        Image img;
        String imgLocation;

        public WonderManager wonderManager { get; private set; }
        Form1 form;

        Player player;
        private bool diplomacy_back = false;
        private int diplomacy_counter = 0;

        public bool diplomacy 
        {
            get
            {
                return diplomacy_back;
            }
            set
            {
                diplomacy_back = value;

                if (value)
                {
                    diplomacy_counter++;
                }
                else
                {
                    diplomacy_counter = 0;
                }

                updateDiplomacyGraphic();
            }
        }
        public PictureButtonWithText PBCoin {get; private set;}
        //public PictureButtonWithText PBResource {get; private set;}
        public PictureButtonWithText PBArmy {get; private set;}

        public Wonder(String name_, bool leaders_, bool cities_, Form1 form_)
        {
            PBCoin = new PictureButtonWithText(Constants.ICONS + "Gold.PNG");
            //PBResource = new PictureButtonWithText(Constants.ICONS + "Papyrus.PNG");
            PBArmy = new PictureButtonWithText(Constants.ICONS + "Gladius.PNG");

            this.DoubleBuffered = true;

            name = name_;
            leaders = leaders_;
            cities = cities_;

            BackgroundImage = img;
            BackgroundImageLayout = ImageLayout.Stretch;

            form = form_;
        }
        public Wonder(Wonder ans)
        {
            PBCoin = new PictureButtonWithText(Constants.ICONS + "Gold.PNG");
            //PBResource = new PictureButtonWithText(Constants.ICONS + "Papyrus.PNG");
            PBArmy = new PictureButtonWithText(Constants.ICONS + "Gladius.PNG");

            this.DoubleBuffered = true;

            name = ans.getName();
            leaders = ans.isLeaders();
            cities = ans.isCities();

            BackgroundImage = img;
            BackgroundImageLayout = ImageLayout.Stretch;

            form = ans.getForm();
        }
        public String getName()
        {
            return name;
        }

        public bool isLeaders()
        {
            return leaders;
        }

        public bool isCities()
        {
            return cities;
        }

        public bool isActive()
        {
            return active;
        }

        public void setActive(bool ans)
        {
            active = ans;
        }

        public void load(char side, Player player_)
        {
            this.side = side;
            player = player_;

            player.ResourceAdded += updateGold;
            player.ArmySizeIncrease += updateArmy;

            PBArmy.addSemiTransparentBack();
            //PBArmy.setTextColor(Color.Blue);

            imgLocation = Constants.WONDERS_FOLDER + name + side + ".PNG";
            img = Image.FromFile(imgLocation);
            
            if (rotation == Player.PlayerRotation.Clockwise90)
            {
                PBCoin.Location = new Point(Width - 40, 20);
                PBCoin.Size = new Size(Width / 5, Width / 5);

                //PBResource.Location = new Point(Width - 40, Width / 5 + 20);
                //PBResource.Size = new Size(Width / 5, Width / 5);

                PBArmy.Location = new Point(Width - 40, Width / 5 * 2 + 20);
                PBArmy.Size = new Size(Width / 5, Width / 5);

                img.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            else if (rotation == Player.PlayerRotation.Clockwise180)
            {
                img.RotateFlip(RotateFlipType.Rotate180FlipNone);
            }
            else if (rotation == Player.PlayerRotation.ClockWise270)
            {
                PBCoin.Location = new Point(0, 20);
                PBCoin.Size = new Size(Width / 5, Width / 5);

                //PBResource.Location = new Point(0, Width / 5 + 20);
                //PBResource.Size = new Size(Width / 5, Width / 5);

                PBArmy.Location = new Point(0, Width / 5 * 2 + 20);
                PBArmy.Size = new Size(Width / 5, Width / 5);

                img.RotateFlip(RotateFlipType.Rotate270FlipNone);
            }
            else
            {

            }
            Resources.Resource goldForBeginning = new Resources.Resource();
            goldForBeginning.gold = (form.getGame().isLeaders() ? 6 : 3);
            player.addResource(goldForBeginning);

            player.increaseArmy(0);

            Controls.Add(PBCoin);
            Controls.Add(PBArmy);
            //Controls.Add(PBResource);

            BackgroundImage = img;

            if ((name + side).Equals("Manneken PisA"))
            {
                wonderManager = new WonderManagerMannekenPisA(player);
            }
            else
            {
                wonderManager = new WonderManager(name + side, form, player);
            }

        }
        public void setRotation(Player.PlayerRotation ans)
        {
            rotation = ans;

            img = Image.FromFile(imgLocation);

            if (rotation == Player.PlayerRotation.Clockwise90)
            {
                img.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            else if (rotation == Player.PlayerRotation.Clockwise180)
            {
                img.RotateFlip(RotateFlipType.Rotate180FlipNone);
            }
            else if (rotation == Player.PlayerRotation.ClockWise270)
            {
                img.RotateFlip(RotateFlipType.Rotate270FlipNone);
            }

            BackgroundImage = img;

        }
        public void setSize(int width, int height)
        {
            if (rotation == Player.PlayerRotation.Clockwise90)
            {
                Size ButtonSize = new Size(width / 10 * 3,width / 10 * 3);

                PBCoin.Location = new Point(width - ButtonSize.Width,20);
                PBCoin.Size = ButtonSize;

                //PBResource.Location = new Point(width - ButtonSize.Width, ButtonSize.Height + 20);
                //PBResource.Size = ButtonSize;

                PBArmy.Location = new Point(width - ButtonSize.Width, ButtonSize.Height * 2 + 20);
                PBArmy.Size = ButtonSize;
            }
            else if (rotation == Player.PlayerRotation.Clockwise180)
            {
                Size ButtonSize = new Size(height / 6, height / 6);

                PBCoin.Location = new Point(width - ButtonSize.Width * 3,height - ButtonSize.Height);
                PBCoin.Size = ButtonSize;

                //PBResource.Location = new Point(Width - ButtonSize.Width * 2, Height - ButtonSize.Height);
                //PBResource.Size = ButtonSize;

                PBArmy.Location = new Point(width - ButtonSize.Width, height - ButtonSize.Height);
                PBArmy.Size = ButtonSize;
            }
            else if (rotation == Player.PlayerRotation.ClockWise270)
            {
                Size ButtonSize = new Size(width / 10 * 3, width / 10 * 3);

                PBCoin.Location = new Point(0, 20);
                PBCoin.Size = ButtonSize;

                //PBResource.Location = new Point(0, ButtonSize.Height + 20);
                //PBResource.Size = ButtonSize;

                PBArmy.Location = new Point(0, ButtonSize.Height * 2 + 20);
                PBArmy.Size = ButtonSize;
            }
            else
            {
                PBCoin.Location = new Point(0, 0);
                PBCoin.Size = new Size(height / 6, height / 6 );

                //PBResource.Location = new Point(height / 6, 0);
                //PBResource.Size = new Size(height / 6, height / 6);

                PBArmy.Location = new Point(height / 6 * 2, 0);
                PBArmy.Size = new Size(height / 6, height / 6);
            }

            this.Size = new Size(width, height);
            //Resize(this, new EventArgs());
        }
        delegate void updateGoldD(object sender, EventArgs e);
        private void updateGold(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                updateGoldD d = new updateGoldD(updateGold);
                this.Invoke(d, new object[] { sender, e });
            }
            else
            {
                Resources.Resource resource = (Resources.Resource)sender;

                if (resource.gold != 0)
                {
                    PBCoin.setText("" + player.resources.gold);
                }
            }
        }
        private void updateArmy(object sender, EventArgs e)
        {
            PBArmy.setText("" + player.armySize);
        }
        private Form1 getForm()
        {
            return form;
        }
        private delegate void updateDiplomacyGraphicD();
        private void updateDiplomacyGraphic()
        {
            if (this.InvokeRequired)
            {
                updateDiplomacyGraphicD d = new updateDiplomacyGraphicD(updateDiplomacyGraphic);
                this.Invoke(d, new object[]{});
            }
            else
            {
                if (diplomacy)
                {
                    PBArmy.hideText();
                    PBArmy.BackgroundImage = Image.FromFile(Constants.ICONS + "Diplomacy.PNG");
                }
                else
                {
                    PBArmy.showText();
                    PBArmy.BackgroundImage = Image.FromFile(Constants.ICONS + "Gladius.PNG");
                }
                
            }
        }
        public Point getResourcePoint()
        {
            Point resourcePoint;

            if (rotation == Player.PlayerRotation.Clockwise90)
            {
                Size ButtonSize = new Size(Width / 10 * 3, Width / 10 * 3);
                resourcePoint = new Point(Width - ButtonSize.Width, ButtonSize.Height + 20);
            }
            else if (rotation == Player.PlayerRotation.Clockwise180)
            {
                Size ButtonSize = new Size(Height / 6, Height / 6);
                resourcePoint = new Point(Width - ButtonSize.Width * 2, Height - ButtonSize.Height);
            }
            else if (rotation == Player.PlayerRotation.ClockWise270)
            {
                Size ButtonSize = new Size(Width / 10 * 3, Width / 10 * 3);
                resourcePoint = new Point(0, ButtonSize.Height + 20);
            }
            else
            {
                resourcePoint = new Point(Height / 6, 0);
            }

            return resourcePoint;
        }
        public char getSide()
        {
            return side;
        }
        public void decreaseDiplomacy()
        {
            diplomacy_counter--;

            if (diplomacy_counter == 0)
            {
                diplomacy = false;
            }
        }
    }
}
