﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Windows.Forms;

namespace Client
{
    public class MessageOutWithConfirm
    {
        Socket handler;
        Timer timer = new Timer();
        List<String> messages = new List<string>();

        public MessageOutWithConfirm(Socket handler_)
        {
            handler = handler_;
            timer.Interval = Constants.NETWORK_INTERVAL;
            timer.Tick += checkMessages;
            timer.Start();
        }
        private void checkMessages(object source, EventArgs e){
            if (messages.Count > 0)
            {
                //Console.WriteLine("Sending message again: " + messages[0]);
                Server.sendSingleMessage(handler, messages[0]);
            }
        }
        public void addMessage(String msg)
        {
            //Console.WriteLine("Adding Message: " + msg);
            messages.Add(msg);
        }
        public void removeMessage(String msg)
        {
            //Console.WriteLine("Attempting Removal of: " + msg);
            for (int count = 0; count < messages.Count; count++)
            {
                if (msg.Equals(messages[count]))
                {
                    //Console.WriteLine("Message removed.");
                    messages.RemoveAt(count);
                    break;
                }
            }
        }
    }
}
