﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Drawing;

namespace Client
{
    public class WonderStage
    {
        public List<CardEffects.CardEffect> immediateEffects {get; private set;}
        public List<CardEffects.VictoryPointsEffect> victoryPoints {get; private set;}

        public String desc {get; private set;}
        Panel pnl;

        Resources.Resource generates = new Resources.Resource();
        public Resources.Resource cost {get; set;}
        public bool infoAtEndOfGame { get; private set; }

        List<String> neighborsLinkedEffectsToParse = new List<string>();

        Player player;

        public int priority { get; set; }
        public bool moreInfoNeeded { get; private set; }

        public event WonderStagePlayedHandler WonderStagePlayed;
        public delegate void WonderStagePlayedHandler(object source, WonderStagePlayedEvent e);

        public class WonderStagePlayedEvent : EventArgs
        {
            private string EventInfo;
            public WonderStagePlayedEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public event DelayedBuildMessageSentHandler DelayedBuildMessageSent;
        public delegate void DelayedBuildMessageSentHandler(object source, DelayedBuildMessageSentEvent e);

        public class DelayedBuildMessageSentEvent : EventArgs
        {
            private string EventInfo;
            public DelayedBuildMessageSentEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public WonderStage(XmlTextReader xmlReader, Player player_, Panel pnl_)
        {
            infoAtEndOfGame = false;
            immediateEffects = new List<CardEffects.CardEffect>();
            victoryPoints = new List<CardEffects.VictoryPointsEffect>();

            moreInfoNeeded = false;
            cost = new Resources.Resource();
            player = player_;
            desc = "";
            pnl = pnl_;

            player.form_.getPNLServer().getPNLGame().NeighborsLinked += neighborsLinkedEffects;

            while (xmlReader.Read())
            {
                if (xmlReader.NodeType == XmlNodeType.EndElement && xmlReader.Name == "Wonder")
                {
                    break;
                }
                else if (xmlReader.Name == "VictoryPointsFixed")
                {
                    int value = Convert.ToInt32(xmlReader.GetAttribute("Points"));
                    victoryPoints.Add(new CardEffects.VictoryPointsFixedEffect(value));
                    desc += "This card generates " + value + " Victory Point(s)\n\n";
                }
                else if (xmlReader.Name == "Resource")
                {
                    generates = Resources.Resource.buildResourceFromXML(xmlReader);

                    desc += "This card grants: " + generates.generateDescription();

                    CardEffects.ResourceGenerationFixed resourGeneration = new CardEffects.ResourceGenerationFixed(player.getWonder(),player, generates, player.form_.getGame());
                    immediateEffects.Add(resourGeneration);
                }
                else if (xmlReader.Name == "ResourceOption")
                {
                    Resources.Resource resourceOption = Resources.Resource.buildResourceFromXML(xmlReader);

                    desc += "This card grants one of the following: " + resourceOption.generateDescription() + "\n\n";

                    CardEffects.ResourceOption resourceOptionEffect = new CardEffects.ResourceOption(player, resourceOption, pnl, player.form_.getGame());
                    immediateEffects.Add(resourceOptionEffect);
                }
                else if (xmlReader.Name == "War")
                {
                    int armyIncrease = Convert.ToInt32(xmlReader.GetAttribute("Value"));

                    CardEffects.ArmyIncrease armyIncreaseEffect = new CardEffects.ArmyIncrease(player, armyIncrease);
                    immediateEffects.Add(armyIncreaseEffect);

                    desc += "This card increase your military force by: " + armyIncrease + "\n\n";
                }
                else if (xmlReader.Name == "Cost")
                {
                    cost = Resources.Resource.buildResourceFromXML(xmlReader);
                    desc += "Cost: " + cost.generateDescription() + "\n\n";
                }
                else if (xmlReader.Name == "Trade")
                {
                    int type = Convert.ToInt32(xmlReader.GetAttribute("Type"));
                    String direction = xmlReader.GetAttribute("Direction");
                    int rate = Convert.ToInt32(xmlReader.GetAttribute("Rate"));
                    bool singleUse = false;

                    desc += "This card grants trades with (" + (type == 0 ? "Brown" : type == 1 ? "Gray" : "Brown and Gray") + ") Cards at a " + (rate < 0 ? "Discounted rate of: " : " rate of ") + rate + " with your " + (direction.Equals("West") ? "Left" : "Right") + " neighbor." + (singleUse ? "Once per turn" : "") + "\n\n";

                    try
                    {
                        singleUse = Convert.ToInt32(xmlReader.GetAttribute("SingleUse")) == 1;
                    }
#pragma warning disable 0168
                    catch (SystemException e)
                    {
#pragma warning restore 0168

                    }
                    CardEffects.TradeEffect tradeEffect = new CardEffects.TradeEffect(player, direction, rate, (CardEffects.TradeEffect.TradeType)type, singleUse);
                    immediateEffects.Add(tradeEffect);
                }
                else if (xmlReader.Name == "Donation")
                {
                    int gold = Convert.ToInt32(xmlReader.GetAttribute("Gold"));

                    neighborsLinkedEffectsToParse.Add("Donation" + Constants.DELIM_FIELD + gold);
                }
                else if (xmlReader.Name == "DebtFixed")
                {
                    int debtAmount = Convert.ToInt32(xmlReader.GetAttribute("Gold"));
                    priority = 7;

                    CardEffects.DebtEffectFixed debtEffect = new CardEffects.DebtEffectFixed(player, debtAmount, player.form_);
                    immediateEffects.Add(debtEffect);

                    desc += "This card causes all other players to incur a debt of " + debtAmount + " or lose points as a penalty.\n\n";
                }
                else if (xmlReader.Name == "Spy")
                {
                    CardEffects.SpyEffect spyEffect = new CardEffects.SpyEffect(player);
                    immediateEffects.Add(spyEffect);

                    desc += "This card Allows you to copy once science a neighbouring city has played (you may not copy more sciences of any type then they actually posess.\n\n";
                }
                else if (xmlReader.Name == "Diplomacy")
                {
                    CardEffects.DiplomacyEffect diplomacyEffect = new CardEffects.DiplomacyEffect(player);
                    immediateEffects.Add(diplomacyEffect);

                    desc += "This card grants you diplomacy; thus excluding you from war (you suffer neither the negative nor the positive effects).\n\n";
                }
                else if (xmlReader.Name == "FreeBuild")
                {
                    int cardType = Convert.ToInt32(xmlReader.GetAttribute("cardType"));
                    bool applyAll = cardType == -1;
                    Card.CardColor color = (Card.CardColor)(applyAll? 0 : cardType);
                    bool singleUse = Convert.ToInt32(xmlReader.GetAttribute("SingleUse")) == 1;

                    CardEffects.FreeBuild freeBuild = new CardEffects.FreeBuild(player, color, pnl, singleUse, applyAll);
                    immediateEffects.Add(freeBuild);

                    desc += "This card allows you to build one ";

                    if (applyAll)
                    {
                        desc += "of any card ";
                    }
                    else
                    {
                        desc += Card.getStringFromCardColor(color) + " card ";
                    }

                    desc += "for free once per age.\n\n";
                }
                else if (xmlReader.Name == "CardCopy")
                {
                    int cardType = Convert.ToInt32(xmlReader.GetAttribute("cardType"));
                    Card.CardColor color = (Card.CardColor)cardType;

                    infoAtEndOfGame = true;

                    desc += "This card allows you to copy one of your neighbors " + Card.getStringFromCardColor(color) + " cards. The copied card is chosen at the end of the game.\n\n";
                    CardEffects.CardCopyEffect cardCopyEffect = new CardEffects.CardCopyEffect(this, player, (Card.CardColor)cardType);
                    immediateEffects.Add(cardCopyEffect);
                    victoryPoints.Add(cardCopyEffect);
                }
                else if (xmlReader.Name == "VictoryPointsPerStone")
                {
                    int pointsperStone = Convert.ToInt32(xmlReader.GetAttribute("Points"));
                    CardEffects.VictoryPointsPerStoneEffect stonePoints = new CardEffects.VictoryPointsPerStoneEffect(player, pointsperStone);
                    immediateEffects.Add(stonePoints);

                    desc += "This card generates " + pointsperStone + " points per stone card generated by the owners brown cardds.\n\n";
                }
                else if (xmlReader.Name == "GoldPerStone")
                {
                    int goldPerStone = Convert.ToInt32(xmlReader.GetAttribute("Gold"));
                    CardEffects.GoldPerStoneEffect stoneGold = new CardEffects.GoldPerStoneEffect(player, goldPerStone);
                    immediateEffects.Add(stoneGold);

                    desc += "This card generates " + goldPerStone + " gold per stone card generated by the owners brown cardds.\n\n";
                }
                else if (xmlReader.Name == "VictoryPointsPerCardType")
                {
                    String direction = xmlReader.GetAttribute("Target");
                    int cardColorNum = Convert.ToInt32(xmlReader.GetAttribute("cardType"));
                    int pointsPerCard = Convert.ToInt32(xmlReader.GetAttribute("Points"));

                    neighborsLinkedEffectsToParse.Add("VictoryPointsPerCardType" + Constants.DELIM_FIELD + direction + Constants.DELIM_FIELD + cardColorNum + Constants.DELIM_FIELD + pointsPerCard);
                    
                }
                else if (xmlReader.Name == "Sacrifice")
                {
                    moreInfoNeeded = true;
                    priority = -1;

                    CardEffects.SacrificeEffect sacrificeEffect = new CardEffects.SacrificeEffect(player);

                    immediateEffects.Add(sacrificeEffect);
                    victoryPoints.Add(sacrificeEffect);

                    desc += "Select a previously hired leader to sacrifice, negating their effect but giving you victory points equal to double their cost.\n\n";

                    sacrificeEffect.SacrificePicked += sendDelayedWonderMessage;
                }
                else if (xmlReader.Name == "Resurrection")
                {
                    priority = Convert.ToInt32(xmlReader.GetAttribute("Priority"));

                    desc += "This card allows the player to build one discarded card for free.\n\n";

                    CardEffects.ResurrectionEffect ressurectionEffect = new CardEffects.ResurrectionEffect(player);
                    immediateEffects.Add(ressurectionEffect);
                }
                else if (xmlReader.Name == "LeaderDraw")
                {
                    int leadersToDraw = Convert.ToInt32(xmlReader.GetAttribute("nb"));
                    CardEffects.LeaderDrawEffect leaderDrawEffect = new CardEffects.LeaderDrawEffect(player, leadersToDraw,this);

                    desc += "This Card gives the player an additional " + leadersToDraw + " leaders to add to their roster.\n\n";

                    immediateEffects.Add(leaderDrawEffect);
                    priority = 2;
                }
                else if (xmlReader.Name == "LeaderHire")
                {
                    CardEffects.LeaderHireEffect leaderHireEffect = new CardEffects.LeaderHireEffect(player);
                    immediateEffects.Add(leaderHireEffect);

                    desc += "This Card allows the player to immediately hire a leader.\n\n";
                    moreInfoNeeded = true;

                    leaderHireEffect.LeaderHirePicked += sendDelayedWonderMessage;
                }
                else if (xmlReader.Name == "DoublePlay")
                {
                    CardEffects.DoublePlayEffect doublePlayEffect = new CardEffects.DoublePlayEffect(player);
                    immediateEffects.Add(doublePlayEffect);

                    desc += "This card allows the player to play both of the last two cards after all of the previous cards have had their plays completed.\n\n";
                }
                else if (xmlReader.Name == "ResourceNonGenerated")
                {
                    CardEffects.ResourceNonGeneratedEffect resourceNonGeneratedEffect = new CardEffects.ResourceNonGeneratedEffect(player,pnl);
                    immediateEffects.Add(resourceNonGeneratedEffect);

                    desc += "This card generates one resource the user does not already produce.\n\n";
                }
                else if (xmlReader.Name == "ScienceChoice")
                {
                    desc += "This card generates one of the following: Gear Science, Tablet Science or Geometry Science.\n\n";

                    CardEffects.ScienceChoiceEffect scienceChoice = new CardEffects.ScienceChoiceEffect(player);
                    immediateEffects.Add(scienceChoice);
                }
            }
        }
        public void play()
        {
            foreach (CardEffects.CardEffect item in immediateEffects)
            {
                item.play();
            }
        }
        private void sendDelayedWonderMessage(object sender, CardEffects.CardEffect.AdditionalInfoAddedEvent e)
        {
            int wonderSpot = player.getWonder().wonderManager.getWonderSpot(this);

            player.form_.getGame().sendMessage("GAME" + Constants.DELIM_FIELD + "WONDERSTAGE" + Constants.DELIM_FIELD + player.getUsername() + Constants.DELIM_FIELD + e.GetInfo() + Constants.DELIM_FIELD);

            if (DelayedBuildMessageSent != null)
            {
                DelayedBuildMessageSent(this, new DelayedBuildMessageSentEvent("Delayed Message Sent."));
            }
        }
        public void raiseWonderPlayedEvent()
        {
            if (WonderStagePlayed != null)
            {
                WonderStagePlayed(this, new WonderStagePlayedEvent("WonderStage Played."));
            }
        }
        public void neighborsLinkedEffects()
        {
            foreach (String data in neighborsLinkedEffectsToParse)
            {
                String effect = data.Substring(0, data.IndexOf(Constants.DELIM_FIELD));

                if (effect.Equals("Donation"))
                {
                    String goldString = data.Substring(effect.Length + 1);
                    int gold = Convert.ToInt32(goldString);

                    desc += "This card gives " + gold + " Gold to your neighbors.\n\n";

                    Resources.Resource goldToGive = new Resources.Resource();
                    goldToGive.gold = gold;

                    CardEffects.ResourceGenerationFixed leftDonation = new CardEffects.ResourceGenerationFixed(player.getWonder(), player.leftNeighbor, goldToGive, player.form_.getGame());
                    immediateEffects.Add(leftDonation);
                    CardEffects.ResourceGenerationFixed rightDonation = new CardEffects.ResourceGenerationFixed(player.getWonder(), player.rightNeighbor, goldToGive, player.form_.getGame());
                    immediateEffects.Add(rightDonation);
                }
                else if (effect.Equals("VictoryPointsPerCardType"))
                {
                    String effectData = data.Substring(effect.Length + 1);
                    String direction = effectData.Substring(0, effectData.IndexOf(Constants.DELIM_FIELD));
                    effectData = effectData.Substring(direction.Length + 1);
                    String endofDesc = "You have played.\n\n";
                    Player targetPlayer = player;

                    if (direction.Equals("Left"))
                    {
                        targetPlayer = player.leftNeighbor;
                        endofDesc = " your left neighbor has played.\n\n";
                    }
                    else if (direction.Equals("Right"))
                    {
                        targetPlayer = player.rightNeighbor;
                        endofDesc = " your right neighbor has played.\n\n";
                    }

                    int cardColorNum = Convert.ToInt32(effectData.Substring(0, effectData.IndexOf(Constants.DELIM_FIELD)));
                    Card.CardColor color = (Card.CardColor)cardColorNum;
                    effectData = effectData.Substring(effectData.IndexOf(Constants.DELIM_FIELD) + 1);
                    int pointsPerCard = Convert.ToInt32(effectData);

                    CardEffects.VictoryPointsPerCardTypeEffect pointsPerCardType = new CardEffects.VictoryPointsPerCardTypeEffect(targetPlayer, color, pointsPerCard);
                    victoryPoints.Add(pointsPerCardType);

                    desc += "This card generates" + pointsPerCard + " Victory Point(s) per " + Card.getStringFromCardColor(color) + endofDesc;
                }
            }
        }
        private void neighborsLinkedEffects(object sender, EventArgs e)
        {
            neighborsLinkedEffects();
        }
        public int getVictoryPoints()
        {
            int points = 0;

            foreach(CardEffects.VictoryPointsEffect item in victoryPoints)
            {
                points += item.generateVicotryPoints();
            }

            return points;
        }
        public void receiveEndofGameInfo(String info)
        {
            foreach (CardEffects.CardEffect item in immediateEffects)
            {
                item.receiveEndofGameInfo(info);
            }
        }
        public void getEndofGameInfo()
        {
            foreach (CardEffects.CardEffect item in immediateEffects)
            {
                item.getEndofGameInfo();
            }
        }
        
    }
}
