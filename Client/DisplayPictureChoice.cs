﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client
{
    class DisplayPictureChoice : Panel
    {
        const int SCROLL_PICTURE = 4;
        int index = 0;
        

        Config config_;
        TextButton TBCancel = new TextButton("Cancel");
        TextButton TBNext = new TextButton("Next");
        TextButton TBPrevious = new TextButton("Previous");
        PictureBox[] display = new PictureBox[SCROLL_PICTURE];
        Point[] points = { new Point(0, 0), new Point(100, 0), new Point(0, 100), new Point(100, 100) };
        bool selectable = false;
        bool nullChoice = false;

        public DisplayPictureChoice(Config config) {
            config_ = config;
            BackColor = Color.Transparent;
            BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
            BackgroundImageLayout = ImageLayout.Stretch;

            TBCancel.Location = new Point(300, 100);
            TBCancel.setSize(100, 30);
            TBCancel.setFontSize(Constants.FONT_SIZE);
            //TBCancel.startHover();
            //TBCancel.MouseClick += cancel;
            //TBCancel.getLabel().MouseClick += cancel;

            TBNext.Location = new Point(200, 100);
            TBNext.setFontSize(Constants.FONT_SIZE);
            TBNext.setSize(100, 30);
            //TBNext.startHover();
            TBNext.MouseClick += next;
            TBNext.getLabel().MouseClick += next;

            TBPrevious.Location = new Point(0, 100);
            TBPrevious.setFontSize(Constants.FONT_SIZE);
            TBPrevious.setSize(100, 30);
            //TBPrevious.startHover();
            TBPrevious.MouseClick += previous;
            TBPrevious.getLabel().MouseClick += previous;

            for (int count = 0; count < SCROLL_PICTURE; count++) {
                display[count] = new PictureBox();
                display[count].ImageLocation = config.getDisplays()[count];
                display[count].SizeMode = PictureBoxSizeMode.StretchImage;
                display[count].Size = new Size(Constants.ICON_WIDTH, Constants.ICON_HEIGHT);
                //display[count].Location = points[count];
                display[count].Location = new Point(Constants.ICON_WIDTH * count, 0);
                display[count].MouseClick += select;

                this.Controls.Add(display[count]);
            }

            this.Controls.Add(TBPrevious);
            this.Controls.Add(TBNext);
            this.Controls.Add(TBCancel);
        }
        private void next(object sender, EventArgs e)
        { 
            if ( (index + 1) * SCROLL_PICTURE < config_.getDisplays().Count){
                index++;
                updateDisplay();                
            }
        }
        private void previous(object sender, EventArgs e)
        {
            if (index > 0) {
                index--;
                updateDisplay();
            }
        }
        private void updateDisplay() {
            for (int count = 0; count < SCROLL_PICTURE; count++)
            {
                if (index * SCROLL_PICTURE + count < config_.getDisplays().Count)
                {
                    display[count].ImageLocation = config_.getDisplays()[index * SCROLL_PICTURE + count];
                }
                else
                {
                    display[count].ImageLocation = null;
                }
            }
        }
        private void cancel(object sender, EventArgs e)
        {
            close();
            TBCancel.BackgroundImage = null;
            
        }
        private void close() {
            this.Visible = false;
        }
        private void select(object sender, EventArgs e)
        {
            if (selectable)
            {
                PictureBox senderObject = (PictureBox)sender;

                if (senderObject.ImageLocation != null)
                {
                    nullChoice = false;
                    config_.setDisplay(senderObject.ImageLocation);
                    //close();
                }
                else {
                    nullChoice = true;
                }
            }
        }
        public void startHover() {
            selectable = true;
            TBNext.startHover();
            TBPrevious.startHover();
            TBCancel.startHover();
        }
        public void stopHover() {
            selectable = false;
            TBNext.startHover();
            TBPrevious.stopHover();
            TBCancel.stopHover();
        }

        public TextButton getTBCancel() {
            return TBCancel;
        }

        public PictureBox[] getdisplay() {
            return display;
        }

        public int getSCROLL_PICTURE() {
            return SCROLL_PICTURE;
        }
        public bool isnullChoice() {
            return nullChoice;
        }
    }
}
