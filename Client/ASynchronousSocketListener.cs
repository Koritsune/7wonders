﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text.RegularExpressions;

namespace Client
{
   
    class ASynchronousSocketListener
    {
        Boolean started = false;
        Thread workerThread;
        //String IPAddress;
            // Thread signal.
        public static ManualResetEvent allDone = new ManualResetEvent(false);

        public ASynchronousSocketListener() {

            
        }

        public void start(String host, int Port) {
            if (!started) {
                started = true;
                //Worker workerObject = new StartListening();
                workerThread = new Thread(()=>StartListening(host, Port));
                workerThread.Start();
            }
        }

        public void stop() {
            if (started)
            {
                workerThread.Abort();
                started = false;
            }
        }
        public void StartListening(String host, int Port) {
           
            //Match match = Regex.Match(TXTIP.Text, @"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}");
            // Data buffer for incoming data.
            byte[] bytes = new Byte[1024];

            // Establish the local endpoint for the socket.
            // The DNS name of the computer
            // running the listener is "host.contoso.com".
            IPHostEntry ipHostInfo = Dns.Resolve(host);
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, Port);

            // Create a TCP/IP socket.
            Socket listener = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp );

            // Bind the socket to the local endpoint and listen for incoming connections.
            try {
                listener.Bind(localEndPoint);
                listener.Listen(6114);

            
                while (true) {
                    // Set the event to nonsignaled state.
                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.
                    //Console.WriteLine("Waiting for a connection...");
                    //listener.
                
                    //Game game = new Game();
                    listener.BeginAccept( 
                        new AsyncCallback(AcceptCallback),Tuple.Create(listener,10));

                    // Wait until a connection is made before continuing.
                    allDone.WaitOne();
                }

            } catch (Exception e) {
                //Console.WriteLine(e.ToString());
            }
        
        }

        public static void AcceptCallback(IAsyncResult ar/*, Game game*/)
        {
            // Data buffer for incoming data.
            byte[] bytes = new Byte[1024];
            // Signal the main thread to continue.
            allDone.Set();
            String data = "";
            //game;
            // Get the socket that handles the client request.
            Tuple<Socket, int> passed = (Tuple<Socket, int>)ar.AsyncState;
            Socket listener = (Socket)passed.Item1;
            //Game game = (Game)passed.Item2;
            Socket handler = listener.EndAccept(ar);

            while (true)
            {
                bytes = new byte[1024];
                int bytesRec = handler.Receive(bytes);
                data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                if (data.IndexOf("<EOF>") > -1)
                {
                    break;
                }
            }
            data = data.Remove(data.IndexOf("<EOF>"), 5);
            //game.addPlayer(data);
            //Console.WriteLine(data);

        }
    }
}
