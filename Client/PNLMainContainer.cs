﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client
{
    public class PNLMainContainer : Panel
    {
        Form1 form_;
        Timer timer = new Timer();
        const int MAIN_MENU_ITEMS = 5;
        const int MOVE_SPEED = 12;
        const int TICK_INTERVAL = 30;
        const int FONT_SIZE = 12;
        const int BUTTON_WIDTH = 100;
        const int BUTTON_HEIGHT = 30;
        const int OFF_SCREEN_X = 412;
        //const int start_X = 584;
        /// <summary>
        /// Variables for Main menu
        /// </summary>
        TextButton[] mainMenu = new TextButton[MAIN_MENU_ITEMS];
        String[] mainMenuText = { "Join Game", "Host Game", "Options", "How to Play", "Credits" };
        bool main = false;
        Server server_;// = new Server();

        /// <summary>
        /// Variables for Connection Menu
        /// </summary>
        Label JOINLBLIP = new Label();
        Label JOINLBLPort = new Label();
        TextButton JOINTBConnect = new TextButton("Connect");
        TextButton JOINTBBack = new TextButton("Back");
        TranslucentTextBox JOINTTBIP = new TranslucentTextBox();
        TranslucentTextBox JOINTTBPort = new TranslucentTextBox();
        //Timer timer = new Timer();
        bool join = false;

        /// <summary>
        /// Variables for Options Menu
        /// </summary>
        Label OPTIONSLBLPort = new Label();
        TranslucentTextBox OPTIONSTTBPort = new TranslucentTextBox();
        Label OPTIONSLBLUSER = new Label();
        TranslucentTextBox OPTIONSTTBUsername = new TranslucentTextBox();
        Label OPTIONSLBLDisplay = new Label();
        PictureBox OPTIONSPBDisplay = new PictureBox();
        TextButton OPTIONSTBBack = new TextButton("Cancel");
        TextButton OPTIONSTBSave = new TextButton("Save");
        PopupMessage connectingPopUp = new PopupMessage();
        bool options = false;
        
        /// <summary>
        /// Variable for DisplayPictureChoice
        /// </summary>
        DisplayPictureChoice displayPictureChoice_;
        String oldDisplay;
        bool display = false;

        /// <summary>
        /// 
        /// </summary>
        
        PopupMessage popUp = new PopupMessage();

        String state = "";
        public PNLMainContainer(Form1 ans) {
            form_ = ans;
            server_ = new Server(ans);
            timer.Interval = TICK_INTERVAL;
            displayPictureChoice_ = new DisplayPictureChoice(form_.getConfig());
            //add/remove containers for join, fix graphics using objects directly isntead of label
            this.DoubleBuffered = true;
            /*
             * Declaring variables for main window
             */

            popUp.setSize(400, 200);
            popUp.setFontSize(12);
            popUp.Visible = false;
            this.Controls.Add(popUp);

            for (int count = 0; count < MAIN_MENU_ITEMS; count++) {
                mainMenu[count] = new TextButton(mainMenuText[count]);
                mainMenu[count].setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
                mainMenu[count].setFontSize(FONT_SIZE);
                mainMenu[count].setPoint(5, count * 32);
                mainMenu[count].getLabel().Location = new Point(0, 5);
                this.Controls.Add(mainMenu[count]);
            }

            mainMenu[0].MouseClick += setModeJoin;
            mainMenu[0].getLabel().MouseClick += setModeJoin;
            mainMenu[1].MouseClick += setModeHost;
            mainMenu[1].getLabel().MouseClick += setModeHost;
            mainMenu[2].MouseClick += setModeOptions;
            mainMenu[2].getLabel().MouseClick += setModeOptions;
            setModeMain();

            /*
             * declaring variables for connection window
             * */

             
            JOINLBLIP.Text = "Ip Address (or Hostname): ";
            JOINLBLIP.Font = new Font(JOINLBLIP.Font.FontFamily.Name, FONT_SIZE);
            JOINLBLPort.Text = "Port : ";
            JOINLBLPort.Font = new Font(JOINLBLPort.Font.FontFamily.Name, FONT_SIZE);

            JOINTBConnect.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
            JOINTBConnect.setFontSize(FONT_SIZE);

            JOINTBBack.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
            JOINTBBack.setFontSize(FONT_SIZE);

            JOINTTBIP.setSize(BUTTON_WIDTH + 20, BUTTON_HEIGHT - 10);
            //JOINTTBIP.setFontSize(FONT_SIZE);

            JOINTTBPort.setSize(BUTTON_WIDTH + 20, BUTTON_HEIGHT - 10);
            JOINTTBPort.getTextBox().Text = "" + form_.getConfig().getPort();
            JOINTTBIP.EnterKeyPress += connect;
            
            JOINLBLIP.Size = new Size(200, 20);
            JOINTTBIP.getTextBox().Text = form_.getConfig().getIP();
            JOINTTBIP.EnterKeyPress += connect;
            
            JOINTTBIP.Location = new Point(OFF_SCREEN_X, 37);
            JOINLBLIP.Location = new Point(OFF_SCREEN_X, 5);
            JOINLBLPort.Location = new Point(OFF_SCREEN_X, 59);
            JOINTBConnect.Location = new Point(OFF_SCREEN_X, 96);
            JOINTBBack.Location = new Point(OFF_SCREEN_X, 128);
            JOINTTBPort.Location = new Point(OFF_SCREEN_X, 37);
            
            JOINTBBack.MouseClick += setModeMain;
            JOINTBBack.getLabel().MouseClick += setModeMain;

            JOINTBConnect.MouseClick += connect;
            JOINTBConnect.getLabel().MouseClick += connect;

            
            connectingPopUp.getX().Visible = false;
            connectingPopUp.setTitle("Connecting");
            connectingPopUp.setText("Attempting connection please wait...");
            connectingPopUp.Visible = false;
            connectingPopUp.setSize(150, 80);
            connectingPopUp.setFontSize(10);
            connectingPopUp.Location = new Point(200, 60);

            this.Controls.Add(JOINLBLIP);
            this.Controls.Add(JOINLBLPort);
            this.Controls.Add(JOINTBConnect);
            this.Controls.Add(JOINTBBack);
            this.Controls.Add(JOINTTBIP);
            this.Controls.Add(JOINTTBPort);
            this.Controls.Add(connectingPopUp);

            /*
              Declaring variables for Options Menu
             */

            oldDisplay = form_.getConfig().getDisplay();

            OPTIONSLBLPort.Text = "Port (on Host) : ";
            OPTIONSLBLPort.Size = new Size(118, OPTIONSLBLPort.Size.Height);
            OPTIONSLBLPort.Font = new Font(OPTIONSLBLPort.Font.FontFamily.Name, FONT_SIZE);
            OPTIONSLBLUSER.Text = "Username: ";
            OPTIONSLBLUSER.Font = new Font(OPTIONSLBLUSER.Font.FontFamily.Name, FONT_SIZE);
            OPTIONSLBLDisplay.Text = "Display Picture: ";
            OPTIONSLBLDisplay.Size = new Size(118, OPTIONSLBLDisplay.Size.Height);
            OPTIONSLBLDisplay.Font = new Font(OPTIONSLBLDisplay.Font.FontFamily.Name, FONT_SIZE);
            OPTIONSPBDisplay.ImageLocation = form_.getConfig().getDisplay();
            OPTIONSPBDisplay.SizeMode = PictureBoxSizeMode.StretchImage;
            OPTIONSPBDisplay.Size = new Size(Constants.ICON_WIDTH, Constants.ICON_HEIGHT);
            OPTIONSPBDisplay.MouseClick += setModeDisplay;

            OPTIONSTTBPort.Size = new Size(BUTTON_WIDTH, BUTTON_HEIGHT - 10);
            OPTIONSTTBUsername.Size = new Size(BUTTON_WIDTH, BUTTON_HEIGHT - 10);
            OPTIONSTTBUsername.getTextBox().Text = form_.getConfig().getUsername();
            OPTIONSTTBUsername.getTextBox().MaxLength = Constants.MAX_USERNAME_LENGTH;

            OPTIONSTTBPort.getTextBox().Text = Convert.ToString(form_.getConfig().getPorth());

            OPTIONSTBBack.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
            OPTIONSTBBack.setFontSize(FONT_SIZE);

            OPTIONSTBBack.MouseClick += cancelOptions;
            OPTIONSTBBack.getLabel().MouseClick += cancelOptions;

            OPTIONSTBSave.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
            OPTIONSTBSave.setFontSize(FONT_SIZE);

            OPTIONSTBSave.MouseClick += saveOptions;
            OPTIONSTBSave.getLabel().MouseClick += saveOptions;

            OPTIONSLBLUSER.Location = new Point(OFF_SCREEN_X, 0);
            OPTIONSTTBUsername.Location = new Point(OFF_SCREEN_X + OPTIONSLBLPort.Size.Width + 10, 0);
            OPTIONSLBLPort.Location = new Point(OFF_SCREEN_X, 32);
            OPTIONSTTBPort.Location = new Point(OFF_SCREEN_X + OPTIONSLBLPort.Size.Width + 10, 32);
            OPTIONSLBLDisplay.Location = new Point(OFF_SCREEN_X, 64);
            OPTIONSPBDisplay.Location = new Point(OFF_SCREEN_X + OPTIONSLBLPort.Size.Width + 10, 64);
            OPTIONSTBSave.Location = new Point(OFF_SCREEN_X, 166);
            OPTIONSTBBack.Location = new Point(OFF_SCREEN_X, 198);

            this.Controls.Add(OPTIONSLBLUSER);
            this.Controls.Add(OPTIONSTTBUsername);
            this.Controls.Add(OPTIONSLBLPort);
            this.Controls.Add(OPTIONSTTBPort);
            this.Controls.Add(OPTIONSTBSave);
            this.Controls.Add(OPTIONSTBBack);
            this.Controls.Add(OPTIONSLBLDisplay);
            this.Controls.Add(OPTIONSPBDisplay);

            /*
             *  Declare variables for DisplayPictureChoice 
             * 
             */

            for (int count = 0; count < displayPictureChoice_.getSCROLL_PICTURE(); count++) {
                displayPictureChoice_.getdisplay()[count].MouseClick += updateDisplay;
            }

            displayPictureChoice_.getTBCancel().MouseClick += setModeOptionsFromDisplay;
            displayPictureChoice_.getTBCancel().getLabel().MouseClick += setModeOptionsFromDisplay;

            displayPictureChoice_.Size = new Size(400, 130);
            displayPictureChoice_.Location = new Point(OFF_SCREEN_X, 0);
            this.Controls.Add(displayPictureChoice_);
           
        }

        private void cleanState() { 
        
            if (state.Equals("Main")){
                timer.Tick += mainTickOut;

                for (int count2 = 0; count2 < MAIN_MENU_ITEMS; count2++)
                {
                    mainMenu[count2].stopHover();
                }

            }
            else if (state.Equals("Join")) {
                timer.Tick += connTickOut;
                JOINTBBack.stopHover();
                JOINTBConnect.stopHover();
            }
            else if (state.Equals("Options")) {
                timer.Tick += optionsTickOut;

                OPTIONSTBSave.stopHover();
                OPTIONSTBBack.stopHover();
            }
            else if (state.Equals("Display")) {
                timer.Tick += displayPictureTickOut;

                displayPictureChoice_.stopHover();
            }
        }

        public void setModeJoin() {
            cleanState();

            JOINTTBIP.Location = new Point(OFF_SCREEN_X + 200, 6);
            JOINLBLIP.Location = new Point(OFF_SCREEN_X, 5);
            JOINLBLPort.Location = new Point(OFF_SCREEN_X, 37);
            JOINTBConnect.Location = new Point(OFF_SCREEN_X + 195, 64);
            JOINTBBack.Location = new Point(OFF_SCREEN_X + 195, 96);
            JOINTTBPort.Location = new Point(OFF_SCREEN_X + 200, 33);
            timer.Tick += connTickIn;
            timer.Start();
 
            if (state.Equals("Main")) {
                timer.Tick += transition_main_join;
            }

            state = "Join";
        }
        private void connectGame(object sender, EventArgs e) {
            timer.Stop();
            timer.Tick -= connectGame;
            connect();

        }
        public void setModeMain() {
            cleanState();
            
            timer.Tick += mainTickIn;

            if (state.Equals("Join")) { 
                timer.Tick += transition_join_main;
            }
            else if (state.Equals("")) {
                timer.Tick += transition_main;
            }
            else if (state.Equals("Options")) {
                timer.Tick += transition_options_main;
            }

            timer.Start();

            state = "Main";
        }
        private void setModeMain(object sender, EventArgs e)
        {
            if (join && !main || options && !main)
            {
                OPTIONSTBBack.BackgroundImage = null;
                JOINTBBack.BackgroundImage = null;
                setModeMain();
            }
        }
        private void setModeJoin(object sender, EventArgs e)
        {
            if (main && !join && !options)
            {
                mainMenu[0].BackgroundImage = null;
                JOINTBBack.BackgroundImage = null;
                setModeJoin();
            }
        }
        private void mainTickOut(object sender, EventArgs e)
        {
            if (main)
            {
                main = !(mainMenu[0].Location.X - MOVE_SPEED + mainMenu[0].Size.Width <= 0);

                for (int count = 0; count < MAIN_MENU_ITEMS; count++)
                {
                    moveControl(mainMenu[count], -1 * MOVE_SPEED);
                                        
                    if (!main)
                    {
                        //this.Controls.Remove(mainMenu[count]);
                        mainMenu[count].stopHover();
                        timer.Tick -= mainTickOut;
                    }
                }
            }
        }
        private void mainTickIn(object sender, EventArgs e)
        {
            if (!main)
            {
                for (int count = 0; count < MAIN_MENU_ITEMS; count++)
                {
                    moveControl(mainMenu[count], MOVE_SPEED);

                    if (mainMenu[count].Location.X >= 2*MOVE_SPEED)
                    {
                        timer.Tick -= mainTickIn;
                        main = true;
                        for (int count2 = 0; count2 < MAIN_MENU_ITEMS; count2++)
                        {
                            mainMenu[count2].startHover();
                            this.Invalidate();
                            timer.Tick -= mainTickIn;
                        }
                    }
                }
            }
        
        }
        private void connTickOut(object sender, EventArgs e)
        {
            ////Console.WriteLine("Tick out Join");
            if (join)
            {
                ////Console.WriteLine("Tick out Join");
                moveControl(JOINLBLIP, MOVE_SPEED);
                moveControl(JOINLBLPort, MOVE_SPEED);
                moveControl(JOINTBBack, MOVE_SPEED);
                moveControl(JOINTBConnect, MOVE_SPEED);
                moveControl(JOINTTBIP, MOVE_SPEED);
                moveControl(JOINTTBPort, MOVE_SPEED);

                if (JOINLBLIP.Location.X >= this.Size.Width)
                {
                    join = false;
                    timer.Tick -= connTickOut;
                }
            }
        }
        private void connTickIn(object sender, EventArgs e)
        {
            ////Console.WriteLine("Tick In conn");
            if (!join)
            {
                moveControl(JOINLBLIP, -1 * MOVE_SPEED);
                moveControl(JOINLBLPort, -1 * MOVE_SPEED);
                moveControl(JOINTBBack, -1 * MOVE_SPEED);
                moveControl(JOINTBConnect, -1 * MOVE_SPEED);
                moveControl(JOINTTBIP, -1 * MOVE_SPEED);
                moveControl(JOINTTBPort, -1 * MOVE_SPEED);

                if (JOINLBLIP.Location.X <= MOVE_SPEED)
                {
                    join = true;
                    timer.Tick -= connTickIn;
                    JOINTBBack.startHover();
                    JOINTBConnect.startHover();
                    JOINTTBPort.getTextBox().Invalidate();
                    JOINTTBIP.getTextBox().Invalidate();
                }
            }
        
        }
        private void moveControl(Control ans, int speed) {
            ans.Location = new Point(ans.Location.X + speed, ans.Location.Y);
        }
        private void transition_main_join(object sender, EventArgs e) {

            if (!main && join) {
                timer.Tick -= transition_main_join;
                timer.Stop();
            }
        }
        private void transition_join_main(object sender, EventArgs e) {
            if (!join && main) {
                timer.Tick -= transition_join_main;
                timer.Stop();
            }
        }
        private void transition_main(object sender, EventArgs e) {
            if (main) {
                timer.Tick -= transition_main;
                timer.Stop();
            }
        }
        public void setModeOptions() {
            cleanState();

            timer.Tick += optionsTickIn;

            if (state.Equals("Main")) {
                timer.Tick += transition_main_options;
            }

            OPTIONSLBLUSER.Location = new Point(OFF_SCREEN_X, 0);
            OPTIONSTTBUsername.Location = new Point(OFF_SCREEN_X + OPTIONSLBLPort.Size.Width + 10, 0);
            OPTIONSLBLPort.Location = new Point(OFF_SCREEN_X, 32);
            OPTIONSTTBPort.Location = new Point(OFF_SCREEN_X + OPTIONSLBLPort.Size.Width + 10, 32);
            OPTIONSLBLDisplay.Location = new Point(OFF_SCREEN_X, 64);
            OPTIONSPBDisplay.Location = new Point(OFF_SCREEN_X + OPTIONSLBLPort.Size.Width + 10, 64);
            OPTIONSTBSave.Location = new Point(OFF_SCREEN_X + OPTIONSLBLPort.Size.Width + 10, 166);
            OPTIONSTBBack.Location = new Point(OFF_SCREEN_X + OPTIONSLBLPort.Size.Width + 10, 198);

            state = "Options";

            timer.Start();
        }
        private void setModeOptions(object sender, EventArgs e)
        {
            if (!options && main && !join)
            {
                mainMenu[2].BackgroundImage = null;
                setModeOptions();
            }
        }
        private void transition_main_options(object sender, EventArgs e) {
            if (!main && options) {
                timer.Tick -= transition_main_options;
                timer.Stop();
            }
        
        }
        private void transition_options_main(object sender, EventArgs e)
        {
            if (!options && main) {
                timer.Tick -= transition_options_main;
                timer.Stop();
            }
        }
        private void optionsTickIn(object sender, EventArgs e) {
            if (!options) {
                moveControl(OPTIONSLBLPort,-1 *  MOVE_SPEED);
                moveControl(OPTIONSTTBPort, -1 * MOVE_SPEED);
                moveControl(OPTIONSLBLUSER, -1 * MOVE_SPEED);
                moveControl(OPTIONSTTBUsername, -1 * MOVE_SPEED);
                moveControl(OPTIONSTBBack, -1 * MOVE_SPEED);
                moveControl(OPTIONSTBSave, -1 * MOVE_SPEED);
                moveControl(OPTIONSLBLDisplay, -1 * MOVE_SPEED);
                moveControl(OPTIONSPBDisplay, -1 * MOVE_SPEED);

                if (OPTIONSLBLUSER.Location.X <= MOVE_SPEED) {
                    options = true;
                    timer.Tick -= optionsTickIn;
                    OPTIONSTTBPort.getTextBox().Invalidate();
                    OPTIONSTTBUsername.getTextBox().Invalidate();
                    OPTIONSTBSave.startHover();
                    OPTIONSTBBack.startHover();
                }
            }
        }
        private void optionsTickOut(object sender, EventArgs e) {
            if (options)
            {
                moveControl(OPTIONSLBLPort, MOVE_SPEED);
                moveControl(OPTIONSTTBPort, MOVE_SPEED);
                moveControl(OPTIONSLBLUSER, MOVE_SPEED);
                moveControl(OPTIONSTTBUsername, MOVE_SPEED);
                moveControl(OPTIONSTBBack, MOVE_SPEED);
                moveControl(OPTIONSTBSave, MOVE_SPEED);
                moveControl(OPTIONSLBLDisplay,MOVE_SPEED);
                moveControl(OPTIONSPBDisplay, MOVE_SPEED);

                if (OPTIONSLBLUSER.Location.X >= this.Size.Width)
                {
                    options = false;
                    timer.Tick -= optionsTickOut;
                    OPTIONSTBSave.stopHover();
                    OPTIONSTBBack.stopHover();
                }
            }
        }
        private void setModeHost(object sender, EventArgs e){
            setModeHost();
        }
        public void setModeHost() {
            //server_ = new Server(form_);
            server_.start();
            form_.getConfig().setHost(true);
            form_.getPNLServer().BringToFront();
        }
        private void connect() {
            form_.getConfig().setPort(Convert.ToInt32(JOINTTBPort.getTextBox().Text));
            form_.getConfig().setIP(JOINTTBIP.getTextBox().Text);
            form_.getGame().connect();
        }

        delegate void setConnectVisibleD();

        public void setConnectVisible() {
            if (this.InvokeRequired)
            {
                setConnectVisibleD d = new setConnectVisibleD(setConnectVisible);
                this.Invoke(d, new object[] {  });
            }
            else
            {
                connectingPopUp.Visible = false;
                JOINTBBack.Visible = true;
                JOINTBConnect.Visible = true;
            }
            
        }
        private void connect (object sender, EventArgs e){
            //JOINTBConnect.BackgroundImage = null;
            if (!connectingPopUp.Visible){
                JOINTBConnect.Visible = false;
                JOINTBBack.Visible = false;
                connectingPopUp.Visible = true;
                //connect is launched on a timer because the listen on socket locks the drawing and therefor the popup message will not be drawn properly otherwise.
                timer.Tick += connectGame;
                timer.Start();
            }
            //connect();
        }
        public Server getServer() {
            return server_;
        }
        private void optionsTickOutLeft(object sender, EventArgs e) {

            if (options)
            {
                moveControl(OPTIONSLBLPort, -1 * MOVE_SPEED);
                moveControl(OPTIONSTTBPort, -1 * MOVE_SPEED);
                moveControl(OPTIONSLBLUSER, -1 * MOVE_SPEED);
                moveControl(OPTIONSTTBUsername, -1 * MOVE_SPEED);
                moveControl(OPTIONSTBBack, -1 * MOVE_SPEED);
                moveControl(OPTIONSTBSave, -1 * MOVE_SPEED);
                moveControl(OPTIONSLBLDisplay, -1 * MOVE_SPEED);
                moveControl(OPTIONSPBDisplay, -1 * MOVE_SPEED);

                if (OPTIONSTTBUsername.Location.X + OPTIONSTTBUsername.Size.Width <= 0 /* OPTIONSLBLUSER.Location.X >= this.Size.Width*/)
                {
                    options = false;
                    timer.Tick -= optionsTickOutLeft;
                    OPTIONSTBSave.stopHover();
                    OPTIONSTBBack.stopHover();
                }
            }
        }

        private void optionsTickInRight(object sender, EventArgs e) {
            if (!options)
            {
                moveControl(OPTIONSLBLPort, MOVE_SPEED);
                moveControl(OPTIONSTTBPort, MOVE_SPEED);
                moveControl(OPTIONSLBLUSER, MOVE_SPEED);
                moveControl(OPTIONSTTBUsername, MOVE_SPEED);
                moveControl(OPTIONSTBBack, MOVE_SPEED);
                moveControl(OPTIONSTBSave, MOVE_SPEED);
                moveControl(OPTIONSLBLDisplay, MOVE_SPEED);
                moveControl(OPTIONSPBDisplay, MOVE_SPEED);

                if (OPTIONSLBLUSER.Location.X >= 2 * MOVE_SPEED)
                {
                    options = true;
                    timer.Tick -= optionsTickInRight;
                    OPTIONSTTBUsername.getTextBox().Invalidate();
                    OPTIONSTTBPort.getTextBox().Invalidate();
                    OPTIONSTBSave.startHover();
                    OPTIONSTBBack.startHover();
                }
            }
        }
        private void displayPictureTickIn(object sender, EventArgs e) {
            if (!display) {
                moveControl(displayPictureChoice_, -1 * MOVE_SPEED);

                if (displayPictureChoice_.Location.X <= MOVE_SPEED) {
                    display = true;
                    timer.Tick -= displayPictureTickIn;
                    displayPictureChoice_.startHover();
                }
            }
        }
        private void displayPictureTickOut(object sender, EventArgs e) {
            if (display) {
                moveControl(displayPictureChoice_, MOVE_SPEED);

                if (displayPictureChoice_.Location.X >= this.Size.Width) {
                    display = false;
                    timer.Tick -= displayPictureTickOut;
                    displayPictureChoice_.stopHover();
                }
            }
        }
        private void transition_options_display(object sender, EventArgs e) {

            if (!options && display) {
                timer.Tick -= transition_options_display;
                timer.Stop();
            }
        }
        private void transition_display_options(object sender, EventArgs e) {
            if (!display && options) {
                timer.Tick -= transition_display_options;
                timer.Stop();
            }
        }
        private void setModeDisplay(object sender, EventArgs e) {
            if (!display && options) {
                setModeDisplay();
            }
        }
        private void setModeOptionsFromDisplay(object sender, EventArgs e) {
            if (!options && display) {
                setModeOptionsFromDisplay();
            }
        }
        public void setModeDisplay() {

            timer.Tick += optionsTickOutLeft;
            timer.Tick += displayPictureTickIn;
            timer.Tick += transition_options_display;

            displayPictureChoice_.Location = new Point(OFF_SCREEN_X, 0);

            timer.Start();

            state = "Display";
        }
        public void setModeOptionsFromDisplay() {

            cleanState();
            timer.Tick += optionsTickInRight;
            timer.Tick += transition_display_options;
            displayPictureChoice_.getTBCancel().BackgroundImage = null;
            timer.Start();

            state = "Options";
        }
        private void updateDisplay(object sender, EventArgs e) {
            if (display && ! displayPictureChoice_.isnullChoice())
            {
                updateDisplay();
                setModeOptionsFromDisplay();
            }
        }
        public void updateDisplay() {
            OPTIONSPBDisplay.ImageLocation = form_.getConfig().getDisplay();
        }
        private void saveOptions(object sender, EventArgs e) {
            if (options && !main || options && !display)
            {
                OPTIONSTBSave.BackgroundImage = null;

                oldDisplay = form_.getConfig().getDisplay();

                String username = OPTIONSTTBUsername.getTextBox().Text;
                username = username.Trim();
                if (username.Length >= 1)
                {
                    form_.getConfig().setUsername(OPTIONSTTBUsername.getTextBox().Text);
                }
                else {
                    showPopUp("Invalid username", "A username must be a minimum of 1 character, trailing whitespace will also be removed");
                }

                int portH = 0;
                bool isPort = int.TryParse(OPTIONSTTBPort.getTextBox().Text, out portH);
                isPort = isPort && portH >= 1024 && portH <= 49151;

                if (isPort)
                {
                    form_.getConfig().setPorth(portH);
                }
                else {
                    showPopUp("Invalid port", "A port number must be inclusively between 1024 and 49151");
                }

                if (!isPort || username.Length < 1) {
                    return;
                }
                
                setModeMain();
            }
        }
        private void cancelOptions(object sender, EventArgs e) {
            if (options && !main || options && !display)
            {
                OPTIONSPBDisplay.ImageLocation = oldDisplay;
                form_.getConfig().setDisplay(oldDisplay);
                OPTIONSTTBPort.getTextBox().Text = Convert.ToString(form_.getConfig().getPorth());
                OPTIONSTTBUsername.getTextBox().Text = form_.getConfig().getUsername();
                OPTIONSTBBack.BackgroundImage = null;
                setModeMain();
            }
        }
        delegate void bringtoFrontD();

        public void bringToFrontDelegate()
        {
            if (this.InvokeRequired)
            {
                bringtoFrontD d = new bringtoFrontD(bringToFrontDelegate);
                this.Invoke(d, new object[] {  });
            }
            else
            {
                form_.getPNLServer().SendToBack();
                form_.getPNLServer().getPNLWonder().SendToBack();
                form_.getPNLServer().getPNLGame().SendToBack();
                this.BringToFront();
                form_.BringToFront();
            }
        }

        delegate void popUpD(String title, String text);

        public void showPopUp(String title, String text) {
            if (this.InvokeRequired)
            {
                popUpD d = new popUpD(showPopUp);
                this.Invoke(d, new object[] {title,text });
            }
            else
            {
                setConnectVisible();
                popUp.setTitle(title);
                popUp.setText(text);
                popUp.Visible = true;
                popUp.BringToFront();
            }
        }

        public void hidePopUp()
        {
            popUp.Visible = false;
        }
    }
}
