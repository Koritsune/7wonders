﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    class TogglePictureGroupBox: GroupBox
    {
        List<TogglePicutreButton> buttons = new List<TogglePicutreButton>();
        public bool singleSelectable { get; set; }

        public TogglePictureGroupBox(): base()
        {
            singleSelectable = false;
        }
        public void add(TogglePicutreButton ans)
        {
            ans.setID(buttons.Count);
            buttons.Add(ans);
            Controls.Add(ans);
            ans.valueChange += ensureSingleSelection;
        }
        public void setEnable(bool ans)
        {
            for (int count = 0; count < buttons.Count; count++)
            {
                buttons[count].setEnable(ans);
            }
        }
        public List<TogglePicutreButton> getButtons()
        {
            return buttons;
        }
        public void activateIndex(int index)
        {
            buttons[index].setActive(true);
        }
        public void deactivateIndex(int index)
        {
            buttons[index].setActive(false);
        }
        private void ensureSingleSelection(object sender, EventArgs e)
        {
            if (singleSelectable)
            {
                TogglePicutreButton TPBAns = (TogglePicutreButton)sender;

                if (TPBAns.isActive())
                {
                    foreach (TogglePicutreButton item in buttons)
                    {
                        if (item != sender && item.isActive())
                        {
                            item.toggleActive();
                        }
                    }
                }
            }
        }
        public int getCurrentActiveIndex()
        {
            foreach (TogglePicutreButton item in buttons)
            {
                if (item.isActive())
                {
                    return item.getID();
                }
            }

            return -1;
        }
    }
}
