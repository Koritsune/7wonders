﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Client
{
    class TranslucentTextBox : TranslucentPanel
    {
        TransparentTextBox text;
        const String BACK_IMAGE = @"Pictures\\Icons\\WhiteTransparent.PNG";
        public BorderStyle borderStyle
        {
            get
            {
                return text.BorderStyle;
            }
            set
            {
                text.BorderStyle = value;
            }
        }
        
        public event EnterKeyEventHandler EnterKeyPress;

        //First we have to define a delegate that acts as a signature for the
        //function that is ultimately called when the event is triggered.
        //You will notice that the second parameter is of MyEventArgs type.
        //This object will contain information about the triggered event.
        public delegate void EnterKeyEventHandler(object source, EnterKeyEvent e);

        //This is a class which describes the event to the class that recieves it.
        //An EventArgs class must always derive from System.EventArgs.
        public class EnterKeyEvent : EventArgs
        {
            private string EventInfo;
            public EnterKeyEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public TranslucentTextBox() : base() {

            text = new TransparentTextBox();
            //text.li
            this.BackgroundImage = Image.FromFile(BACK_IMAGE);
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(text);
            text.KeyDown += keyPress;
        }

        public TransparentTextBox getTextBox() {
            return text;
        }
        public void setFontSize(int size)
        {
            text.Font = new Font(text.Font.FontFamily.Name, size);
        }
        public void setSize(int width, int height)
        {
            text.Size = new Size(width, height);
            Size = new Size(width, height);
        }
        public void setPoint(int x, int y)
        {
            text.Location = new Point(0, 5);
            Location = new Point(x, y);
        }
        private void keyPress(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Return)
            {
                if (EnterKeyPress != null)
                {
                    EnterKeyPress(this, new EnterKeyEvent("Enter key was pressed."));
                }
            }
        }
        public void AppendText(string textToAdd, Color color)
        {
            text.SelectionStart = text.TextLength;
            text.SelectionLength = 0;

            text.SelectionColor = color;
            text.AppendText(textToAdd);
            text.SelectionColor = ForeColor;
        }
        public void AppendText(String textToAdd)
        {
            AppendText(textToAdd, getTextBox().ForeColor);
        }
    }
}
