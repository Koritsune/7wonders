﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Client
{
    public class DeckBuilder
    {
        Config config;
        Game game;

        Random rnd = new Random();

        Deck<String> BlackAgeI;
        Deck<String> BlackAgeII;
        Deck<String> BlackAgeIII;

        Deck<String> Leaders;

        Deck<String> Guilds;

        Deck<String> AgeI;
        Deck<String> AgeII;
        Deck<String> AgeIII;

        private enum DeckAge
        {
            None = 0,
            AgeI = 1,
            AgeII = 2,
            AgeIII = 3,
            Leaders = 4
        }

        public DeckBuilder(Config config_, Game game_) {
            config = config_;
            game = game_;
        }
        public void buildDecks()
        {
            if (config.isCities())
            {
                BlackAgeI = buildDeck("BlackI.XML", DeckAge.None);
                BlackAgeII = buildDeck("BlackII.XML", DeckAge.None);
                BlackAgeIII = buildDeck("BlackIII.XML", DeckAge.None);
            }

            if (config.isLeaders())
            {
                Leaders = buildDeck("Leaders.XML", DeckAge.Leaders);
            }

            Guilds = buildDeck("Guilds.XML", DeckAge.None);

            AgeI = buildDeck("AgeI.XML", DeckAge.AgeI);
            AgeII = buildDeck("AgeII.XML", DeckAge.AgeII);
            AgeIII = buildDeck("AgeIII.XML", DeckAge.AgeIII);
        }
        private Deck<String> buildDeck(String xmlLocation, DeckAge deckAge)
        {
            List<String> cards = new List<String>();
            Deck<String> blackDeck = null;

            XmlTextReader xmlReader = new XmlTextReader(xmlLocation);

            String prefix = "";

            switch (deckAge)
            {
                case DeckAge.AgeI:
                    {
                        prefix = "Age I\\";
                        blackDeck = BlackAgeI;
                        break;
                    }
                case DeckAge.AgeII:
                    {
                        prefix = "Age II\\";
                        blackDeck = BlackAgeII;
                        break;
                    }
                case DeckAge.AgeIII:
                    {
                        prefix = "Age III\\";
                        blackDeck = BlackAgeIII;
                        break;
                    }
                case DeckAge.Leaders:
                    {
                        prefix = "Leaders\\";
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            while (xmlReader.Read())
            {

                if (xmlReader.Name == "Cards" && xmlReader.NodeType != XmlNodeType.EndElement)
                {
                    bool Leaders = false;
                    bool Cities = false;
                    int guilds = 0;
                    int blacks = 0;
                    int players = 3;

                    #pragma warning disable 0168
                    try
                    {
                        Leaders = xmlReader.GetAttribute("Leaders").Equals("1");
                    }
                    catch (SystemException e)
                    {
                        // Leaders expansion not mentioned
                    }

                    try
                    {
                        Cities = xmlReader.GetAttribute("Cities").Equals("1");
                    }
                    catch (SystemException e)
                    {
                        //Cities expansion not mentioned
                    }

                    try
                    {
                        guilds = Convert.ToInt32(xmlReader.GetAttribute("Guilds"));
                    }
                    catch (SystemException e)
                    {
                        //Guild cards not mentioned
                    }

                    try
                    {
                        blacks = Convert.ToInt32(xmlReader.GetAttribute("Black"));
                    }
                    catch (SystemException e)
                    {
                        //black cards not mentioned
                    }

                    try
                    {
                        players = Convert.ToInt32(xmlReader.GetAttribute("Players"));
                    }
                    catch (SystemException e){
                        //player minimum not mentioned
                    }
                    
                    if ((Leaders && config.isLeaders() || !Leaders) && (Cities && config.isCities() || !Cities) && players <= game.getnbPlayers())
                    {
                        int nb = Convert.ToInt32(xmlReader.GetAttribute("nb"));

                        for (int inc = 0; inc < nb; inc++)
                        {

                            do
                            {
                                xmlReader.Read();
                            } while (xmlReader.NodeType != XmlNodeType.Element);

                            cards.Add( prefix + xmlReader.GetAttribute("Name"));
                        }

                        //add guild cards

                        for (int inc = 0; inc < guilds; inc++)
                        {
                            cards.Add("Guilds\\" + Guilds.deal());
                        }

                        //add black cards

                        if (config.isCities())
                        {
                            for (int inc = 0; inc < blacks; inc++)
                            {
                                cards.Add("Black\\" + prefix + blackDeck.deal());
                            }
                        }
                    }
                }
                #pragma warning restore 0168
            }

            xmlReader.Close();

            Deck<String> deck = new Deck<String>(cards,rnd);
            deck.shuffle();

            return new Deck<String>(cards,rnd);
        }

        public Deck<String> getLeaderDeck(){
            return Leaders;
        }

        public Deck<String> getAgeIDeck()
        {
            return AgeI;
        }

        public Deck<String> getAgeIIDeck()
        {
            return AgeII;
        }

        public Deck<String> getAgeIIIDeck()
        {
            return AgeIII;
        }

        public Random getRandomGenerator()
        {
            return rnd;
        }
    }
}
