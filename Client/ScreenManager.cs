﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Client
{
    public class ScreenManager
    {
        [DllImport("user32.dll")]
        private static extern int FindWindow(string lpszClassName, string lpszWindowName);
        [DllImport("user32.dll")]
        private static extern int ShowWindow(int hWnd, int nCmdShow);
        private const int SW_HIDE = 0;
        private const int SW_SHOW = 1;

        Form form;

        bool fullScreen = false;
        bool enable = false;

        Timer maximizeWindow = new Timer();

        public event FullScreenHandler FullScreen;
        public event NormalScreenHandler NormalScreen;

        //First we have to define a delegate that acts as a signature for the
        //function that is ultimately called when the event is triggered.
        //You will notice that the second parameter is of MyEventArgs type.
        //This object will contain information about the triggered event.
        public delegate void NormalScreenHandler(object source, NormalScreenEvent e);

        //This is a class which describes the event to the class that recieves it.
        //An EventArgs class must always derive from System.EventArgs.
        public class NormalScreenEvent : EventArgs
        {
            private string EventInfo;
            public NormalScreenEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        //First we have to define a delegate that acts as a signature for the
        //function that is ultimately called when the event is triggered.
        //You will notice that the second parameter is of MyEventArgs type.
        //This object will contain information about the triggered event.
        public delegate void FullScreenHandler(object source, FullScreenEvent e);

        //This is a class which describes the event to the class that recieves it.
        //An EventArgs class must always derive from System.EventArgs.
        public class FullScreenEvent : EventArgs
        {
            private string EventInfo;
            public FullScreenEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public ScreenManager(Form form_)
        {
            form = form_;

            form.Deactivate += lostFocus;
            form.Activated += gainedFocus;

            maximizeWindow.Interval = 100;
            maximizeWindow.Tick += maximize;
        }

        public void normal_screen()
        {
            if (enable)
            {
                int hwnd = FindWindow("Shell_TrayWnd", "");
                ShowWindow(hwnd, SW_SHOW);

                form.WindowState = FormWindowState.Normal;
                form.FormBorderStyle = FormBorderStyle.Fixed3D;

                form.TopMost = false;

                if (NormalScreen != null)
                {
                    NormalScreen(this, new NormalScreenEvent("Screen changed to normal size."));
                    //timerStatusPoll.Tick += pollStatus;
                }
            }
        }
        public void full_screen()
        {
            if (enable)
            {
                // First, Hide the taskbar

                int hWnd = FindWindow("Shell_TrayWnd", "");
                ShowWindow(hWnd, SW_HIDE);

                // Then, format and size the window. 
                // Important: Borderstyle -must- be first, 
                // if placed after the sizing functions, 
                // it'll strangely firm up the taskbar distance.

                form.FormBorderStyle = FormBorderStyle.None;
                form.WindowState = FormWindowState.Maximized;

                form.TopMost = true;

                if (FullScreen != null)
                {
                    FullScreen(this, new FullScreenEvent("Screen changed to full size"));
                    //timerStatusPoll.Tick += pollStatus;
                }
            }
        }
        public void setFullScreen()
        {
            fullScreen = true;

            full_screen();
        }
        public void setNormalScreen()
        {
            fullScreen = false;

            normal_screen();
        }
        public void start()
        {
            if (fullScreen)
            {
                full_screen();
            }

            enable = true;
        }
        public void stop()
        {
            bool oldfullScreen = fullScreen;
            fullScreen = false;
            
            normal_screen();
            
            fullScreen = oldfullScreen;
            enable = false;
        }
        private void lostFocus(object sender, EventArgs e)
        {
            if (fullScreen)
            {
                int hwnd = FindWindow("Shell_TrayWnd", "");
                ShowWindow(hwnd, SW_SHOW);

                form.WindowState = FormWindowState.Minimized;
            }
        }
        private void gainedFocus(object sender, EventArgs e)
        {
            if (fullScreen)
            {
                int hWnd = FindWindow("Shell_TrayWnd", "");
                ShowWindow(hWnd, SW_HIDE);

                maximizeWindow.Start();
            }
        }
        private void maximize(object sender, EventArgs e)
        {
            form.WindowState = FormWindowState.Maximized;
            maximizeWindow.Stop();
        }
        public bool isFullScreen()
        {
            return fullScreen;
        }
    }
}
