﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Net;
using System.Threading;



namespace Client
{
    public class PNLServer : Panel
    {
        const String BackImage = @"Pictures\\Icons\\Server.png";
        GroupBox GBUsers = new GroupBox();
        PlayerIcon[] PIUsers = new PlayerIcon[Constants.MAX_PLAYERS_CITIES];
        const int GB_USER_SIZE_WIDTH = 820;//Constants.MAX_PLAYERS/2 * 100;
        const int GB_USER_SIZE_HEIGHT = 130;//Constants.MAX_PLAYERS / 4 * 100;
        Label LBLIPs = new Label();
        Label LBLPort = new Label();
        PopupMessage popUpMessage = new PopupMessage();
        Form1 form_;
        TranslucentTextBox LBLMSGBox = new TranslucentTextBox();
        TranslucentTextBox TXTMSG = new TranslucentTextBox();

        String publicIP = "";
        WonderChoices PNLWonder;

        TextButton TBBack = new TextButton("Back");
        TextButton TBReady = new TextButton("Ready");
        TextButton TBNotReady = new TextButton("Not Ready");
        TextButton TBWonders = new TextButton("Wonders");

        Point oldCursorPoint;
        bool toggleScroll = false;

        bool hasPublicIP = false;

        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

        TogglePictureGroupBox GBExpansions;
        TogglePicutreButton TPBLeaders = new TogglePicutreButton(Constants.LEADERS_ENABLED, Constants.LEADERS_DISABLED);
        TogglePicutreButton TPBCities = new TogglePicutreButton(Constants.CITIES_ENABLED, Constants.CITIES_DISABLED);
        //TogglePictureButton TPBBabel

        ToggleGroupBox TGBTime = new ToggleGroupBox();
        ToggleGroupBox TGBABChoice = new ToggleGroupBox();

        TimerPopUpMessage TPMStart = new TimerPopUpMessage("Game starts in: <TIME>");

        PNLGame PNLGame_;

        public event GameEventHandler Gamestart;

        //First we have to define a delegate that acts as a signature for the
        //function that is ultimately called when the event is triggered.
        //You will notice that the second parameter is of MyEventArgs type.
        //This object will contain information about the triggered event.
        public delegate void GameEventHandler(object source, GameStartEvent e);

        //This is a class which describes the event to the class that recieves it.
        //An EventArgs class must always derive from System.EventArgs.
        public class GameStartEvent : EventArgs
        {
            private string EventInfo;
            public GameStartEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

        public PNLServer(Form1 ans) {
            this.BackgroundImage = Image.FromFile(BackImage);
            this.BackgroundImageLayout = ImageLayout.Stretch;

            PNLGame_ = new PNLGame(ans);
            
            timer.Tick += showScrollBar;
            form_ = ans;

            //publicIP = GetPublicIP();
            LBLMSGBox.Location = new Point(42, 200);
            LBLMSGBox.setSize(600, 400);
            LBLMSGBox.setFontSize(Constants.FONT_SIZE);
            LBLMSGBox.BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
            LBLMSGBox.getTextBox().ReadOnly = true;
            LBLMSGBox.getTextBox().setSingleLine(false);
            LBLMSGBox.getTextBox().ScrollBars = RichTextBoxScrollBars.ForcedVertical;
            LBLMSGBox.getTextBox().ForeColor = Color.White;

            TXTMSG.setSize(600, 50);
            TXTMSG.Location = new Point(42, 610);
            TXTMSG.BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
            TXTMSG.getTextBox().ForeColor = Color.White;
            TXTMSG.getTextBox().MaxLength = 1006;
            TXTMSG.setFontSize(Constants.FONT_SIZE);

            TBWonders.setSize(100, 30);
            TBWonders.Location = new Point(675, 264);
            TBWonders.setFontSize(10);
            TBWonders.startHover();
            TBWonders.MouseClick += showWonderChoices;
            TBWonders.getLabel().MouseClick += showWonderChoices;

            TBBack.setSize(100, 30);
            TBBack.Location = new Point(675, 232);
            TBBack.setFontSize(10);
            TBBack.startHover();
            TBBack.onTextButtonClickEvent += back;
            
            TBReady.setSize(100, 30);
            TBReady.setFontSize(/*Constants.FONT_SIZE*/10);
            TBReady.Location = new Point(675, 200);
            TBReady.startHover();
            TBReady.MouseClick += ready;
            TBReady.getLabel().MouseClick += ready;

            TBNotReady.setSize(100, 30);
            TBNotReady.setFontSize(10);
            TBNotReady.Location = new Point(675, 200);
            TBNotReady.startHover();
            TBNotReady.MouseClick += notReady;
            TBNotReady.getLabel().MouseClick += notReady;
            TBNotReady.Visible = false;

            GBExpansions = new TogglePictureGroupBox();

            for (int count = 0; count < Constants.MAX_PLAYERS_CITIES; count++) { 
                PIUsers[count] = new PlayerIcon();
                PIUsers[count].Size = new Size(Constants.ICON_WIDTH,Constants.ICON_HEIGHT);
                PIUsers[count].Location = new Point(count * Constants.ICON_WIDTH + 10,20);
                PIUsers[count].Visible = false;
                PIUsers[count].onKickEvent += removeUsername;
                GBUsers.Controls.Add(PIUsers[count]);
            }

            GBUsers.Text = "Players";
            GBUsers.BackColor = Color.Transparent;
            GBUsers.Font = new Font(GBUsers.Font.FontFamily.Name, 10);
            GBUsers.Size = new Size(GB_USER_SIZE_WIDTH, GB_USER_SIZE_HEIGHT);
            GBUsers.Location = new Point(40, 55);

            Controls.Add(GBUsers);

            LBLIPs.Text = "Ip Address' :";
            LBLIPs.BackColor = Color.Transparent;
            LBLIPs.Font = new Font(LBLIPs.Font.FontFamily.Name, 10);
            LBLIPs.Location = new Point(48,15);
            LBLIPs.Size = new Size(800, 20);

            LBLPort.BackColor = Color.Transparent;
            LBLPort.Font = new Font(LBLPort.Font.FontFamily.Name, 10);
            LBLPort.Location = new Point(48, 30);
            LBLPort.Size = new Size(250, 20);
            

            popUpMessage.setSize(400, 200);
            popUpMessage.Location = new Point( 138, 280);
            popUpMessage.Visible = false;
            popUpMessage.setColorWhite();
            popUpMessage.setFontSize(Constants.FONT_SIZE);

            form_.Controls.Add(popUpMessage);
            Controls.Add(LBLIPs);
            Controls.Add(LBLPort);
            Controls.Add(LBLMSGBox);
            Controls.Add(TXTMSG);
            Controls.Add(TBBack);
            Controls.Add(TBReady);
            Controls.Add(TBNotReady);
            Controls.Add(TBWonders);

            LBLPort.BringToFront();
            timer.Interval = 100;
            Thread getExternalIPThread = new Thread(()=>getPublicIP());
            getExternalIPThread.Start();
            //timer.Tick += GetPublicIP;
            //timer.Stop();
            popUpMessage.getX().MouseClick += back;
            TXTMSG.EnterKeyPress += sendMessage;
            //popUpMessage.VisibleChanged += popupVisibleChanged;
            //timer.Tick += showScrollBar;

            GBExpansions.Text = "Expansions";
            GBExpansions.Font = new Font(GBExpansions.Font.FontFamily.Name, 10);
            GBExpansions.Size = new Size(140, 104);
            GBExpansions.Location = new Point(675, 500);
            GBExpansions.BackColor = Color.Transparent;

            GBExpansions.add(TPBLeaders);
            GBExpansions.add(TPBCities);

            if (form_.getConfig().isLeaders())
            {
                TPBLeaders.setActive(true);
            }

            if (form_.getConfig().isCities())
            {
                TPBCities.setActive(true);
            }

            /*
             * Check if babel is true in config
             */

            TPBLeaders.Location = new Point(10, 19);
            TPBLeaders.setSize(120, 35);
            TPBLeaders.valueChange += changeExpansionStatus;

            TPBCities.Location = new Point(10, 59);
            TPBCities.setSize(120, 35);
            TPBCities.valueChange += changeExpansionStatus;

            /*
             * Initialize TPBBabel
             */

            TGBTime.setButtonSize(40, 30);
            TGBTime.Size = new Size(140, 90);
            TGBTime.Location = new Point(675, 300);
            TGBTime.BackColor = Color.Transparent;
            TGBTime.Text = "Turn Time";
            TGBTime.Font = new Font(TGBTime.Font.FontFamily.Name, 10);
            TGBTime.valueChange += timeChange;

            TGBTime.addButton("30");
            TGBTime.addButton("45");
            TGBTime.addButton("60");
            TGBTime.addButton("75");
            TGBTime.addButton("90");
            TGBTime.addButton("120");
            TGBTime.selectButton("" + form_.getConfig().getTurnTime());

            TGBABChoice.setButtonSize(120, 30);
            TGBABChoice.Size = new Size(140, 90);
            TGBABChoice.Location = new Point(675, 399);
            TGBABChoice.BackColor = Color.Transparent;
            TGBABChoice.Text = "Side A or B";
            TGBABChoice.Font = new Font(TGBABChoice.Font.FontFamily.Name, 10);

            TGBABChoice.addButton("Random");
            TGBABChoice.addButton("Player's Choice");

            TGBABChoice.selectButton((form_.getConfig().getABPlayerChoice() ? 1 : 0));
            TGBABChoice.valueChange += changeChoiceAB;

            if (form_.getConfig().getABPlayerChoice()) {
                TGBABChoice.selectButton(1);
            }
            else
            {
                TGBABChoice.selectButton(0);
            }

            PNLWonder = new WonderChoices(form_);

            TPMStart.setSize(400, 200);
            TPMStart.setColorWhite();
            TPMStart.setTitle("All players ready");
            //TPMStart.setColorBlack();
            TPMStart.setFontSize(10);
            TPMStart.Location = new Point(138, 280);
            TPMStart.VisibleChanged += toggleEnableTimeout;
            TPMStart.TimeExpired += startGame;
            form_.Controls.Add(TPMStart);
            
            form_.Controls.Add(PNLWonder);
            form_.Controls.Add(PNLGame_);
            PNLWonder.Dock = DockStyle.Fill;


            this.Controls.Add(TGBABChoice);
            this.Controls.Add(TGBTime);
            this.Controls.Add(GBExpansions);
            //this.Controls.Add(TPBCities);
        }

        delegate void addGameUserName(String userName, String display);

        public void addUserName(String userName, String display)
        {
            if (this.GBUsers.InvokeRequired)
            {
                addGameUserName d = new addGameUserName(addUserName);
                this.Invoke(d, new object[] { userName,display });
            }
            else
            {
                for (int count = 0; count < Constants.MAX_PLAYERS_CITIES; count++){
                    if (PIUsers[count].getUsername().Length == 0) {
                        PIUsers[count].setDisplay(display);
                        PIUsers[count].setuserName(userName);
                        PIUsers[count].connected();
                        PIUsers[count].Visible = true;
                        //prevent the host from kicking himself out of the game
                        if (count > 0)
                        {
                            PIUsers[count].setKick(form_.getConfig().isHost());
                        }
                        break;
                    }
                }
            }
        }
        delegate void removeGameUsername(String username);

        public void removeUsername(String username) {

            if (this.GBUsers.InvokeRequired)
            {
                removeGameUsername d = new removeGameUsername(removeUsername);
                this.Invoke(d, new object[] { username });
            }
            else {
                for (int count = 0; count < Constants.MAX_PLAYERS_CITIES; count++)
                {
                    if (PIUsers[count].getUsername().Equals(username))
                    {
                        for (int count2 = count; count2 < Constants.MAX_PLAYERS_CITIES - 1; count2++)
                        {
                            PIUsers[count2].setDisplay(PIUsers[count2 + 1].getDisplay());
                            PIUsers[count2].setuserName(PIUsers[count2 + 1].getUsername());

                            if (PIUsers[count2].getUsername().Length == 0)
                            {
                                PIUsers[count2].Visible = false;
                            }
                        }
                        break;
                    }
                    
                }

                PIUsers[Constants.MAX_PLAYERS_CITIES - 1].Visible = false;
            }
        }
        delegate void BringToFrontD1();

        public void BringToFrontD()
        {
            if (this.InvokeRequired)
            {
                ////Console.WriteLine("Invoke Required");
                BringToFrontD1 d = new BringToFrontD1(BringToFrontD);
                this.Invoke(d, new object[] {  });
            }
            else
            {
                BringToFront();
                //TXTMSG.getTextBox().Text = "test.";
                //TXTMSG.getTextBox().Invalidate();
                //TXTMSG.getTextBox().Focus();
                //LBLMSGBox.getTextBox().Focus();
            }
        }

        public void setPlayerDisconnected(String username) {
            for (int count = 0; count < Constants.MAX_PLAYERS_CITIES; count++) {
                if (PIUsers[count].getUsername().Equals(username)) {
                    PIUsers[count].disconnected();
                }
            }
        }

        public void setPlayerReady(String username) {
            for (int count = 0; count < Constants.MAX_PLAYERS_CITIES; count++)
            {
                if (PIUsers[count].getUsername().Equals(username))
                {
                    PIUsers[count].ready();
                }
            }
        }

        public void setPlayerConnected(String username) {
            for (int count = 0; count < Constants.MAX_PLAYERS_CITIES; count++)
            {
                if (PIUsers[count].getUsername().Equals(username))
                {
                    PIUsers[count].connected();
                }
            }

            if (username.Equals(form_.getConfig().getUsername()))
            {
                notReady();
            }
        }
        
        delegate void popUpD(String title, String text);

        public void showPopUp(String title, String text)
        {
            if (this.InvokeRequired)
            {
                popUpD d = new popUpD(showPopUp);
                this.Invoke(d, new object[] { title, text });
            }
            else
            {
                popUpMessage.setTitle(title);
                popUpMessage.setText(text);
                popUpMessage.Visible = true;
                popUpMessage.BringToFront();
            }
        }
        delegate void hidepopUpD();

        public void hidePopUp()
        {
            if (this.InvokeRequired)
            {
                hidepopUpD d = new hidepopUpD(hidePopUp);
                this.Invoke(d, new object[] { });
            }
            else
            {
                popUpMessage.Visible = false;
                popUpMessage.BringToFront();
            }
        }
        delegate void updateNetworkInfoD();

        public void updateNetworkInfo() {
            
            if (this.InvokeRequired)
            {
                updateNetworkInfoD d = new updateNetworkInfoD(updateNetworkInfo);
                this.Invoke(d, new object[] { });
            }
            else
            {
                //TPMStart.start();
                clearText();
                /*
                 Options are not updated here because options are read from the initial connection and are updates then from the class Game.connect()
                 */
                LBLIPs.Text =  "IP Address' (or Hostname):";
                LBLPort.Text = "Port:                                    ";
                TBReady.Visible = true;
                TBNotReady.Visible = false;
                
                if (form_.getConfig().isHost())
                {
                    //enable options
                    PNLWonder.setEnable(true);
                    TGBABChoice.setEnable(true);
                    TGBTime.setEnable(true);
                    GBExpansions.setEnable(true);
                    //get external IP
                    if (hasPublicIP)
                    {
                        LBLIPs.Text += " " + publicIP + ",";
                    }
                    // Get host name
                    String strHostName = Dns.GetHostName();

                    // get all local IPAddress'
                    IPHostEntry iphostentry = Dns.GetHostByName(strHostName);
                    foreach (IPAddress ipaddress in iphostentry.AddressList)
                    {
                        ////Console.WriteLine(ipaddress);
                        LBLIPs.Text += " " + ipaddress + ",";
                        ////Console.WriteLine(LBLIPs.Text);
                    }
                    LBLIPs.Text = LBLIPs.Text.Remove(LBLIPs.Text.Length - 1);

                    LBLPort.Text += form_.getConfig().getPorth();
                }
                else
                {
                    //disable options
                    PNLWonder.setEnable(false);
                    TGBABChoice.setEnable(false);
                    TGBTime.setEnable(false);
                    GBExpansions.setEnable(false);

                    //this.Enabled = false;
                    LBLIPs.Text += " " + form_.getConfig().getIP();
                    LBLPort.Text += form_.getConfig().getPort();
                }
                //timer.Tick += showScrollBar;
                timer.Start();
            }
            
        }
        public void getPublicIP()
        {
            string url = "http://checkip.dyndns.org";
            try
            {
                System.Net.WebRequest req = System.Net.WebRequest.Create(url);
                System.Net.WebResponse resp = req.GetResponse();
                System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
                string response = sr.ReadToEnd().Trim();
                string[] a = response.Split(':');
                string a2 = a[1].Substring(1);
                string[] a3 = a2.Split('<');
                string a4 = a3[0];
                publicIP = a4;

                hasPublicIP = true;

                if (form_.getConfig().isHost())
                {
                    appendIP(publicIP);
                    //updateNetworkInfo();
                }

                //timer.Tick -= GetPublicIP;
            }
            #pragma warning disable 0168
            catch (System.Net.WebException ex)
            {
                return;
            }
            #pragma warning restore 0168

        }
        delegate void appendIPD(String IP);

        public void appendIP(String IP)
        {
            if (this.InvokeRequired)
            {
                appendIPD d = new appendIPD(appendIP);
                this.Invoke(d, new object[] { IP });
            }
            else
            {
                String beginning = LBLIPs.Text.Substring(0, LBLIPs.Text.IndexOf(':') + 1);
                String end = LBLIPs.Text.Substring(beginning.Length);
                LBLIPs.Text = beginning + " " + publicIP + "," + end;
            }

        }
        public void removeUsername(object sender, EventArgs e) {
            Server server = form_.getPNLMainContainer().getServer();
            PlayerIcon playerIcon = (PlayerIcon)sender;

            server.sendMessage("KICK" + Constants.DELIM_FIELD + playerIcon.getUsername());

            for (int index = 0; index < server.getPlayers().Count; index++)
            {
                if (server.getPlayers()[index].getUsername().Equals(playerIcon.getUsername()))
                {
                    server.getPlayers()[index].end();
                    server.getPlayers().RemoveAt(index);
                    break;
                }
            }

            
            //this.end();
        }
        private void back(object sender, EventArgs e) {
            bool ishost = form_.getConfig().isHost();
            
            if (ishost)
            {
                for (int index = 0; index < form_.getPNLMainContainer().getServer().getPlayers().Count; index++)
                {
                    if (form_.getPNLMainContainer().getServer().getPlayers()[index].getUsername().Equals(form_.getConfig().getUsername()))
                    {
                        Server.sendSingleMessage(form_.getPNLMainContainer().getServer().getPlayers()[index].getHandler(), "EXIT" + Constants.DELIM_FIELD);
                        
                        form_.getPNLMainContainer().getServer().getPlayers()[index].end();
                        
                        form_.getPNLMainContainer().getServer().getPlayers().RemoveAt(index);
                        break;
                    }
                }

                form_.getPNLMainContainer().getServer().stop();
                
            }
            else
            {
                form_.getGame().sendMessage("EXIT" + Constants.DELIM_FIELD);
            }

            form_.getGame().clearGame();
            form_.getPNLMainContainer().setConnectVisible();
            
            /*
            if (ishost) {
                form_.getPNLMainContainer().hidePopUp();
            }
            */
        }
        private void showScrollBar(object sender, EventArgs e) {
            if (!toggleScroll)
            {
                Point temp = form_.DesktopLocation;
                temp.X += 640;
                temp.Y += 260;
                oldCursorPoint = Cursor.Position;
                Cursor.Position = temp;
                toggleScroll = true;
            }
            else
            {
                Cursor.Position = oldCursorPoint;
                toggleScroll = false;
                TXTMSG.getTextBox().Focus();
                timer.Stop();
            }
            //scrollVisible = true;
            /*
            for (int count = 0; count < 20; count++)
            {
                LBLMSGBox.AppendText(count + "\n");
            }

            timer.Stop();

            LBLMSGBox.getTextBox().Text = "";
            TXTMSG.getTextBox().Focus();
             * */
            /*
            if (TXTMSG.getTextBox().ScrollBars == RichTextBoxScrollBars.Vertical)
            {
                TXTMSG.getTextBox().ScrollBars = RichTextBoxScrollBars.None;
            }
            else
            {
                TXTMSG.getTextBox().ScrollBars = RichTextBoxScrollBars.Vertical;
                TXTMSG.getTextBox().Focus();
                timer.Stop();
            }
             * */
        }
        private void sendMessage(object sender, EventArgs e){
            String msg = TXTMSG.getTextBox().Text;
            msg = msg.Trim();
            
            if (msg.Length > 0)
            {
                form_.getGame().sendMessage("MSG" + Constants.DELIM_FIELD + form_.getConfig().getUsername() + Constants.DELIM_FIELD + TXTMSG.getTextBox().Text);
            }

            TXTMSG.getTextBox().Text = "";
        }
        delegate void receiveMessageD(String user, String msg);

        public void receiveMessage(String user, String msg)
        {
            if (this.InvokeRequired)
            {
                receiveMessageD d = new receiveMessageD(receiveMessage);
                this.Invoke(d, new object[] {user,msg });
            }
            else
            {
                LBLMSGBox.AppendText(user + ": " + msg + "\n");
                LBLMSGBox.getTextBox().SelectionStart = LBLMSGBox.getTextBox().Text.Length;
                LBLMSGBox.getTextBox().ScrollToCaret();
            }

        }
        delegate void receiveSystemMessageD(String msg);

        public void receiveSystemMessage(String msg)
        {
            if (this.InvokeRequired)
            {
                receiveSystemMessageD d = new receiveSystemMessageD(receiveSystemMessage);
                this.Invoke(d, new object[] { msg });
            }
            else
            {
                LBLMSGBox.AppendText( "SYSTEM: " + msg + "\n",Color.DarkRed);
                LBLMSGBox.getTextBox().SelectionStart = LBLMSGBox.getTextBox().Text.Length;
                LBLMSGBox.getTextBox().ScrollToCaret();
            }

        }
        private void timeChange (object sender, EventArgs e)
        {
            ToggleGroupBox sender_ = (ToggleGroupBox)sender;

            String msg = "OPTIONS" + Constants.DELIM_FIELD + "TIME" + Constants.DELIM_FIELD + sender_.getSelectedValue();

            //update information in config, this function can only be entered if you are the host
            form_.getConfig().setTime(Convert.ToInt32(TGBTime.getSelectedValue()));

            form_.getPNLMainContainer().getServer().sendMessage(msg);
        }
        delegate void changeTimeD(int time);

        public void changeTime(int time)
        {
            if (this.InvokeRequired)
            {
                changeTimeD d = new changeTimeD(changeTime);
                this.Invoke(d, new object[] {time });
            }
            else
            {
                TGBTime.selectButton("" + time);
            }

        }
        private void changeChoiceAB(object sender, EventArgs e)
        {
            ToggleGroupBox sender_ = (ToggleGroupBox)sender;

            String msg = "OPTIONS" + Constants.DELIM_FIELD + "AB" + Constants.DELIM_FIELD + sender_.getSlectedIndex();

            //update information in config, this function can only be entered if you are the host

            form_.getConfig().setABPlayerChoice(sender_.getSlectedIndex() == 1);
            form_.getPNLMainContainer().getServer().sendMessage(msg);
        }
        delegate void changeChoiceABD(int index);

        public void changeChoiceAB(int index)
        {
            if (this.InvokeRequired)
            {
                changeChoiceABD d = new changeChoiceABD(changeChoiceAB);
                this.Invoke(d, new object[] { index });
            }
            else
            {
                ////Console.WriteLine("In changeChoiceAB with index: " + index);
                TGBABChoice.selectButton(index);
            }

        }
        private void changeExpansionStatus(object sender, EventArgs e)
        {
            TogglePicutreButton sender_ = (TogglePicutreButton)sender;
            String msg = "OPTIONS" + Constants.DELIM_FIELD + "EXP" + Constants.DELIM_FIELD + sender_.getID() + Constants.DELIM_FIELD + (sender_.isActive()?"1":"0");
            //update information in config, this function can only be entered if you are the host
            if (sender_.getID() == 0)
            {
                //leaders
                form_.getConfig().setLeaders(sender_.isActive());
            }
            else if (sender_.getID() == 1)
            {
                //cities
                form_.getConfig().setCities(sender_.isActive());
            }
            else if (sender_.getID() == 2)
            {
                //babel
                form_.getConfig().setBabel(sender_.isActive());
            }

            form_.getPNLMainContainer().getServer().sendMessage(msg);
        }
        delegate void changeExpansionStatusD(int index,bool status);

        public void changeExpansionStatus(int index,bool status)
        {
            if (this.InvokeRequired)
            {
                changeExpansionStatusD d = new changeExpansionStatusD(changeExpansionStatus);
                this.Invoke(d, new object[] { index,status });
            }
            else
            {
                ////Console.WriteLine("In changeChoiceAB with index: " + index);
                //TGBABChoice.selectButton(index);
                if (status)
                {
                    GBExpansions.activateIndex(index);
                }
                else
                {
                    GBExpansions.deactivateIndex(index);
                }
            }

        }

        private void showWonderChoices(object sender, EventArgs e)
        {
            PNLWonder.BringToFront();
        }

        public WonderChoices getPNLWonder()
        {
            return PNLWonder;
        }
        public void showScrollBar()
        {
            timer.Start();
        }
        private void ready(object sender, EventArgs e)
        {
                //Console.WriteLine("In ready function");
                form_.getGame().sendMessage("READY" + Constants.DELIM_FIELD + form_.getConfig().getUsername());
                TBReady.Visible = false;
                TBNotReady.Visible = true;

        }
        private void notReady(object sender, EventArgs e)
        {
                //the message is not sent in notReady() because otherwise it causes an infinite loop
                form_.getGame().sendMessage("NOTREADY" + Constants.DELIM_FIELD + form_.getConfig().getUsername());
                notReady();
        }
        delegate void notReadyD();

        public void notReady()
        {
            if (this.InvokeRequired)
            {
                notReadyD d = new notReadyD(notReady);
                this.Invoke(d, new object[] { });
            }
            else
            {
                TBReady.Visible = true;
                TBNotReady.Visible = false;
            }
        }
        public void clearText()
        {
            LBLMSGBox.getTextBox().Text = "";
        }
        public PlayerIcon[] getPlayerIcons()
        {
            return PIUsers;
        }

        delegate void startGameD();

        public void startGame()
        {
            if (this.InvokeRequired)
            {
                startGameD d = new startGameD(startGame);
                this.Invoke(d, new object[] { });
            }
            else
            {
                //PNLGame_.init();
                TPMStart.Visible = true;
                TPMStart.start();
            }
        }

        private void toggleEnableTimeout(object sender, EventArgs e)
        {
            if (form_.getConfig().isHost())
            {
                if (TPMStart.Visible)
                {
                    //TBWonders.
                    TGBTime.setEnable(false);
                    TGBABChoice.setEnable(false);
                    GBExpansions.setEnable(false);
                    PNLWonder.setEnable(false);
                }
                else
                {
                    TGBTime.setEnable(true);
                    TGBABChoice.setEnable(true);
                    GBExpansions.setEnable(true);
                    PNLWonder.setEnable(true);
                }
            }
        }
        delegate void stopGameD();

        public void stopGame()
        {
            if (this.InvokeRequired)
            {
                stopGameD d = new stopGameD(stopGame);
                this.Invoke(d, new object[] { });
            }
            else
            {
                TPMStart.Visible = false;
                TPMStart.stop();
            }
        }
        private void startGame(Object sender, EventArgs e){
            //PNLGame_.BringToFront();
            TPMStart.Visible = false;

            form_.getGame().setPlaying(true);

            PNLGame_.Size = this.Size;
            PNLGame_.Dock = DockStyle.Fill;

            if (form_.getConfig().isHost())
            {
                if (Gamestart != null)
                {
                    Gamestart(this, new GameStartEvent("Game starting"));
                }
            }
        }
        public PNLGame getPNLGame()
        {
            return PNLGame_;
        }
        //private void object sender, EventArgs e
    }
}
