﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client
{
    
    public class PlayerIcon : Panel
    {
        //const String testIcon = @"Pictures\\Icons\\Alexandria.png";
        //String display;
        const String LBLBackImage = Constants.WHITE_TRANSPARENT;//@"Pictures\\Icons\\WhiteTransparent.png";
        const String READY = Constants.Ready;//@"Pictures\\Icons\\Ready.PNG";
        const String CONNECTED = Constants.Connected;//@"Pictures\\Icons\\connected.png";
        const String DISCONNECTED = Constants.Disconnected;//@"Pictures\\Icons\\disconnected.png";
        PictureBox status = new PictureBox();
        Panel LBLBack = new Panel();
        Label LBLUser = new Label();
        Panel PNLPBBack = new Panel();
        TextButton TBKick = new TextButton("Kick");

        const int WIDTH = 100;
        const int HEIGHT = 100;
        String display;
        bool canKick = false;
        //PictureBox icon = new PictureBox();

        public event KickHandler onKickEvent;

        //First we have to define a delegate that acts as a signature for the
        //function that is ultimately called when the event is triggered.
        //You will notice that the second parameter is of MyEventArgs type.
        //This object will contain information about the triggered event.
        public delegate void KickHandler(object source, KickEvent e);

        //This is a class which describes the event to the class that recieves it.
        //An EventArgs class must always derive from System.EventArgs.
        public class KickEvent : EventArgs
        {
            private string EventInfo;
            public KickEvent(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }
        public PlayerIcon() {
            this.DoubleBuffered = true;
            LBLBack.BackgroundImage = Image.FromFile(LBLBackImage);
            LBLBack.BackColor = Color.Transparent;
            LBLBack.Size = new Size(WIDTH, LBLUser.Size.Height);
            //LBLUser.Text = "User";
            LBLUser.Location = new Point(0, 2);
            LBLUser.BackColor = Color.Transparent;
            Size = new Size(WIDTH, HEIGHT);
            LBLUser.Size = new Size(WIDTH + 10, 20);
            //this.BackgroundImage = Image.FromFile(display);
            status.BackgroundImageLayout = ImageLayout.Stretch;
            BackgroundImageLayout = ImageLayout.Stretch;

            status.BackColor = Color.Transparent;
            status.ImageLocation = CONNECTED;
            status.BackgroundImage = Image.FromFile(Constants.WHITE_TRANSPARENT);
            status.SizeMode = PictureBoxSizeMode.StretchImage;
            status.Size = new Size(15, 15);
            status.Location = new Point(3, 3);
            PNLPBBack.Size = new Size(21, 21);
            PNLPBBack.Location = new Point(this.Size.Width / 2 - 11, this.Size.Height - 26);
            PNLPBBack.BackColor = Color.Transparent;
            PNLPBBack.BackgroundImage = Image.FromFile(Constants.WHITE_TRANSPARENT);
            PNLPBBack.Controls.Add(status);
            //iconImageLocation = testIcon;
            //icon.la
            TBKick.setFontSize(Constants.FONT_SIZE);
            TBKick.Location = new Point(30, 40);
            TBKick.setSize(40, 30);
            TBKick.startHover();
            TBKick.Visible = false;
            this.Controls.Add(TBKick);
            this.Controls.Add(PNLPBBack);
            LBLBack.Controls.Add(LBLUser);
            //LBLBack.Controls.Add(status);
            status.BringToFront();
            //this.Controls.Add(LBLBack);
            this.Controls.Add(LBLBack);
            //LBLUser.Font = new Font(LBLUser.Font.FontFamily.Name, 9);
            //LBLUser.BringToFront();

            status.MouseEnter += showKick;
            this.MouseEnter += showKick;
            LBLBack.MouseEnter += showKick;
            LBLUser.MouseEnter += showKick;
            PNLPBBack.MouseEnter += showKick;
            TBKick.MouseEnter += showKick;
            TBKick.getLabel().MouseEnter += showKick;
            

            status.MouseLeave += hideKick;
            this.MouseLeave += hideKick;
            LBLBack.MouseLeave += hideKick;
            LBLUser.MouseLeave += hideKick;
            PNLPBBack.MouseLeave += hideKick;
            TBKick.MouseLeave += hideKick;
            TBKick.getLabel().MouseLeave += hideKick;

            TBKick.MouseClick += raiseKickEvent;
            TBKick.getLabel().MouseClick += raiseKickEvent;
            
        }
        public void setDisplay(String ans) {
            display = ans;
            if (ans == null || ans.Length == 0)
            {
                BackgroundImage = null;
            }
            else
            {
                BackgroundImage = Image.FromFile(ans);
            }
        }
        public void setuserName(String ans) {
            LBLUser.Text = ans;
        }
        public void ready() {
            status.ImageLocation = Constants.Ready;
        }
        public void connected() {
            status.ImageLocation = Constants.Connected;
        }
        public void disconnected() {
            status.ImageLocation = Constants.Disconnected;
        }
        public String getUsername() {
            return LBLUser.Text;
        }
        public String getDisplay() {
            return display;
        }
        private void showKick(object sender, EventArgs e){
            if (ClientRectangle.Contains(PointToClient(Cursor.Position)) && canKick)
            {
                TBKick.Visible = true;
            }
        }
        private void hideKick(object sender, EventArgs e) {
            ////Console.WriteLine("In hideKick");
            ////Console.WriteLine(Cursor.Position.Y);
            if (!ClientRectangle.Contains(PointToClient(Cursor.Position)) && canKick)
            {
                TBKick.Visible = false;
            }
        }
        public TextButton getKickButton() {
            return TBKick;
        }
        public void setKick(bool ans) {
            canKick = ans;
        }
        private void raiseKickEvent(object sender, EventArgs e) {
            if (onKickEvent != null)
            {
                onKickEvent(this, new KickEvent("Player is being kicked."));
            }
        }
        public bool isReady()
        {
            return status.ImageLocation.Equals(READY);
        }
        public bool isConnected()
        {
            return status.ImageLocation.Equals(CONNECTED);
        }
        public bool isDisconnected() {
            return status.ImageLocation.Equals(DISCONNECTED);
        }
    }
}
