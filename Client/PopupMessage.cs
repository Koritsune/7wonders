﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client
{
    class PopupMessage : Panel
    {
        Label LBLMessage = new Label();
        Label LBLBar = new Label();
        Label LBLX = new Label();
        public PopupMessage() {
            BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
            BackColor = Color.Transparent;
            LBLMessage.BackColor = Color.Transparent;
            //LBLBar.TextAlign = ContentAlignment.MiddleRight;
            LBLBar.ForeColor = Color.White;
            LBLBar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            LBLMessage.Text = "This is a PopUp";
            LBLX.Text = "X";
            LBLX.Size = new Size(18, 18);
            LBLX.ForeColor = Color.Black;
            LBLMessage.ForeColor = Color.White;
            LBLMessage.Location = new Point(0, 22);
            LBLX.Image = Image.FromFile(Constants.WHITE_TRANSPARENT);
            //LBLX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(LBLMessage);
            this.Controls.Add(LBLBar);
            this.Controls.Add(LBLX);
            LBLBar.BringToFront();
            LBLX.BringToFront();

            LBLX.MouseEnter += XHover;
            LBLX.MouseLeave += XLeave;
            LBLX.MouseClick += close;
            
        }
        public void setSize(int width, int height){
            this.Size = new Size(width, height);
            LBLBar.Size = new Size(width, 23);
            LBLMessage.Size = new Size(width, height - 20);
            LBLX.Location = new Point(width - 20, 2);
        }
        public void setFontSize(int size)
        {
            LBLX.Font = new Font(LBLX.Font.FontFamily.Name, size);
            LBLBar.Font = new Font(LBLBar.Font.FontFamily.Name, size);
            LBLMessage.Font = new Font(LBLMessage.Font.FontFamily.Name, size);
        }
        private void XHover(object sender, EventArgs e) {
            LBLX.ForeColor = Color.Gray;
        }
        private void XLeave(object sender, EventArgs e) {
            LBLX.ForeColor = Color.Black;
        }
        private void close(object sender, EventArgs e)
        {
            LBLX.ForeColor = Color.Black;
            this.Visible = false;
        }
        public void setText(String ans) {
            LBLMessage.Text = ans;
        }
        public void setTitle(String ans) {
            LBLBar.Text = ans;
        }
        public Label getX() {
            return LBLX;
        }
        public Label getLBLTitle() {
            return LBLBar;
        }
        public Label getLBLTest() {
            return LBLMessage;
        }
        public void setColorBlack(){
            BackgroundImage = Image.FromFile(Constants.BLACK_TRANSPARENT);
            LBLX.Image = Image.FromFile(Constants.WHITE_TRANSPARENT);
            LBLX.ForeColor = Color.White;
            LBLBar.ForeColor = Color.White;
            LBLMessage.ForeColor = Color.White;
        }
        public void setColorWhite(){
            BackgroundImage = Image.FromFile(Constants.WHITE_TRANSPARENT);
            LBLX.Image = Image.FromFile(Constants.BLACK_TRANSPARENT);
            LBLX.ForeColor = Color.Black;
            LBLBar.ForeColor = Color.Black;
            LBLMessage.ForeColor = Color.Black;
        }
    }
}
