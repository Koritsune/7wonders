﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client
{
    class WonderManagerMannekenPisA : WonderManager
    {
        WonderManager leftNeighborWonder;
        List<Panel> leftNeighborIndicators = new List<Panel>();

        WonderManager rightNeighborWonder;
        List<Panel> rightNeighborIndicators = new List<Panel>();

        List<int> wonderStagesBuilt = new List<int>();
        List<Card> wonderBuiltIndicator = new List<Card>();

        int currentWonder = 0;

        public WonderManagerMannekenPisA(Player player_) : base(player_)
        {
            Resources.Resource startingGold = new Resources.Resource();
            startingGold.gold = 4;

            player.addResource(startingGold);

            player.form_.getPNLServer().getPNLGame().NeighborsLinked += loadWonder;
        }
        private void loadWonder(object source, EventArgs e)
        {
            String wonderWithSideLeft = player.leftNeighbor.getWonder().getName() + player.leftNeighbor.getWonder().getSide();
            leftNeighborWonder = new WonderManager(wonderWithSideLeft,player.form_,player,true);
            leftNeighborWonder.setPlaceHolderPositionSize(0);
            leftNeighborWonder.resizeExternal = true;

            foreach (WonderStage item in leftNeighborWonder.getWonderStages())
            {
                item.neighborsLinkedEffects();

                if (item.priority == 3 || item.priority == 4)
                {
                    item.priority = 5;
                }

                if (item.moreInfoNeeded)
                {
                    item.DelayedBuildMessageSent += raiseDelayedMessageSent;
                }

                wonderStages.Add(item);
            }

            String wonderWithSideRight = player.rightNeighbor.getWonder().getName() + player.rightNeighbor.getWonder().getSide();
            rightNeighborWonder = new WonderManager(wonderWithSideRight, player.form_, player,true);
            rightNeighborWonder.setPlaceHolderPositionSize(1);
            rightNeighborWonder.resizeExternal = true;

            foreach (WonderStage item in rightNeighborWonder.getWonderStages())
            {
                item.neighborsLinkedEffects();

                if (item.priority == 3 || item.priority == 4)
                {
                    item.priority = 5;
                }

                if (item.moreInfoNeeded)
                {
                    item.DelayedBuildMessageSent += raiseDelayedMessageSent;
                }

                wonderStages.Add(item);
            }

            player.getWonder().Resize += resize;
            player.rightNeighbor.getWonder().Resize += resizeRightIndicators;
            player.leftNeighbor.getWonder().Resize += resizeLeftIndicators;
            addNeighborPlaceholders();
            getNextWonderStage();
            //leftNeighborWonder = new WonderManager()
        }
        public override string getDesc(int position = -1)
        {
            if (currentWonder == 0)
            {
                if (leftNeighborWonder.unordered)
                {
                    return leftNeighborWonder.getDesc();
                }

                return leftNeighborWonder.getDesc(currentWonder);
            }
            else if (currentWonder == 1)
            {
                if (rightNeighborWonder.unordered)
                {
                    return rightNeighborWonder.getDesc();
                }

                return rightNeighborWonder.getDesc(currentWonder);
            }
            else
            {
                if (leftNeighborWonder.unordered)
                {
                    return leftNeighborWonder.getDesc();
                }

                return leftNeighborWonder.getDesc(currentWonder);
            }
            //return base.getDesc();
        }
        public override WonderStage getNextWonderStage(int position = -1)
        {
            //remove background iamges on all neighbor wonderindicators

            foreach (Panel item in leftNeighborIndicators)
            {
                item.BackgroundImage = null;
            }

            foreach (Panel item in rightNeighborIndicators)
            {
                item.BackgroundImage = null;
            }

            WonderStage wonderStage;

            if (currentWonder == 0)
            {
                    wonderStage = leftNeighborWonder.getNextWonderStage();
                    leftNeighborIndicators[leftNeighborWonder.getWonderSpot(wonderStage)].BackgroundImage = Image.FromFile(Constants.WHITE_TRANSPARENT);
                    return wonderStage;
            }
            else if (currentWonder == 1)
            {
                if (rightNeighborWonder.unordered)
                {
                    wonderStage = rightNeighborWonder.getNextWonderStage();
                    rightNeighborIndicators[rightNeighborWonder.getWonderSpot(wonderStage)].BackgroundImage = Image.FromFile(Constants.WHITE_TRANSPARENT);
                    return wonderStage;
                }

                rightNeighborIndicators[1].BackgroundImage = Image.FromFile(Constants.WHITE_TRANSPARENT);
                return rightNeighborWonder.getNextWonderStage(currentWonder);
            }
            else
            {
                if (leftNeighborWonder.unordered)
                {
                    wonderStage = leftNeighborWonder.getNextWonderStage();
                    leftNeighborIndicators[leftNeighborWonder.getWonderSpot(wonderStage)].BackgroundImage = Image.FromFile(Constants.WHITE_TRANSPARENT);
                    return wonderStage;
                }

                leftNeighborIndicators[leftNeighborWonder.getNbWonderStages() - 1].BackgroundImage = Image.FromFile(Constants.WHITE_TRANSPARENT);
                return leftNeighborWonder.getNextWonderStage(leftNeighborWonder.getNbWonderStages() - 1);
            }
        }
        public override void sendBuildMessage(int position = -1)
        {
            if (currentWonder == 0)
            {
                leftNeighborWonder.sendBuildMessage();
            }
            else if (currentWonder == 1)
            {
                rightNeighborWonder.sendBuildMessage(currentWonder);
            }
            else
            {
                leftNeighborWonder.sendBuildMessage(leftNeighborWonder.getNbWonderStages() - 1);
            }
        }
        public override WonderStage getWonderStageToBuild(string buildData, int position = -1)
        {
            if (currentWonder == 0)
            {
                return leftNeighborWonder.getWonderStageToBuild(buildData);
            }
            else if (currentWonder == 1)
            {
                return rightNeighborWonder.getWonderStageToBuild(buildData,currentWonder);
            }
            else
            {
                return leftNeighborWonder.getWonderStageToBuild(buildData, leftNeighborWonder.getNbWonderStages() - 1);
            }
        }

        delegate void wonderStageBuiltD(WonderStage wonderBuilt);

        public override void wonderStageBuilt(WonderStage wonderBuilt)
        {
            if (player.InvokeRequired)
            {
                wonderStageBuiltD d = new wonderStageBuiltD(wonderStageBuilt);
                player.Invoke(d, new object[] { wonderBuilt });
            }
            else
            {
                Card cardBack = new Card(getCardBack(), player, player.getPlayerRotation());
                cardBack.play();
                cardBack.Location = getwonderbuiltIndicatorPoint(wonderBuiltIndicator.Count,player);
                cardBack.Size = getwonderbuiltIndicatorSize(wonderBuiltIndicator.Count, player);
                player.Controls.Add(cardBack);
                wonderBuiltIndicator.Add(cardBack);
                currentWonder++;

                leftNeighborWonder.markAsBuilt(wonderBuilt);
                rightNeighborWonder.markAsBuilt(wonderBuilt);

                if (currentWonder == 1 || currentWonder == 3)
                {
                    wonderStagesBuilt.Add(leftNeighborWonder.getWonderStagePosition(wonderBuilt));

                    if (currentWonder == 3)
                    {
                        List<Panel> leftNeighborPlaceholders = leftNeighborWonder.getwonderPlaceholder();

                        for (int count = 0; count < leftNeighborPlaceholders.Count; count++)
                        {
                            if (wonderStagesBuilt[0] != count && wonderStagesBuilt[2] != count)
                            {
                                player.getWonder().Controls.Remove(leftNeighborPlaceholders[count]);
                            }
                        }
                    }

                    resize();
                }
                else
                {
                    wonderStagesBuilt.Add(rightNeighborWonder.getWonderStagePosition(wonderBuilt));

                    List<Panel> rightNeighborPlaceholders = rightNeighborWonder.getwonderPlaceholder();

                    for (int count = 0; count < rightNeighborPlaceholders.Count; count++)
                    {
                        if (wonderStagesBuilt[1] != count)
                        {
                            player.getWonder().Controls.Remove(rightNeighborPlaceholders[count]);
                        }
                    }

                    resize();
                }

                getNextWonderStage();
                /*
                if (currentWonder == 1)
                {
                    leftNeighborWonder.setPlaceHolderPositionSize(2);
                }
                 */
            }
        }

        private delegate void addneighborPlaceholders();

        private void addNeighborPlaceholders()
        {
            if (player.InvokeRequired)
            {
                addneighborPlaceholders d = new addneighborPlaceholders(addNeighborPlaceholders);
                player.Invoke(d, new object[] { });
            }
            else
            {
                for (int count = 0; count < leftNeighborWonder.getNbWonderStages(); count++)
                {
                    Panel pnl = new Panel();
                    leftNeighborIndicators.Add(pnl);
                    
                    if (player == player.form_.getGame().getPlayers()[0])
                    {
                        player.leftNeighbor.getWonder().Controls.Add(pnl);
                        pnl.BackColor = Color.Transparent;
                    }
                    
                }

                for (int count = 0; count < rightNeighborWonder.getNbWonderStages(); count++)
                {
                    Panel pnl = new Panel();
                    rightNeighborIndicators.Add(pnl);
                    player.rightNeighbor.getWonder().Controls.Add(pnl);
                    pnl.BackColor = Color.Transparent;
                }

                resize();
                resizeRightIndicators();
                resizeLeftIndicators();
            }
        }
        public void resize(object source, EventArgs e)
        {
            resize();
        }
        private void resizeLeftIndicators(object source, EventArgs e)
        {
            resizeLeftIndicators();
        }
        private void resizeLeftIndicators()
        {
            //resize an reposition indicators on left neighbor, these are used to clearly indicate whcih wonder stage the user is building
            for (int count = 0; count < leftNeighborIndicators.Count; count++)
            {
                leftNeighborIndicators[count].Location = getwonderPlaceHolderPoint(count, player.leftNeighbor);
                leftNeighborIndicators[count].Size = getwonderPlaceHolderSize(count, player.leftNeighbor);
            }
        }
        private void resizeRightIndicators(object source, EventArgs e)
        {
            resizeRightIndicators();
        }
        private void resizeRightIndicators()
        {
            //resize an reposition indicators on right neighbor, these are used to clearly indicate whcih wonder stage the user is building
            for (int count = 0; count < rightNeighborIndicators.Count; count++)
            {
                rightNeighborIndicators[count].Location = getwonderPlaceHolderPoint(count, player.rightNeighbor);
                rightNeighborIndicators[count].Size = getwonderPlaceHolderSize(count, player.rightNeighbor);
            }
        }
        private void resize()
        {
            //resize and reposition wonderbuiltindicators
            for (int count = 0; count < wonderBuiltIndicator.Count; count++)
            {
                wonderBuiltIndicator[count].Location = getwonderbuiltIndicatorPoint(count, player);
                wonderBuiltIndicator[count].Size = getwonderbuiltIndicatorSize(count, player);
            }

            //resize and reposition own placeholders

            List<Panel> leftNeighborPlaceholders = leftNeighborWonder.getwonderPlaceholder();
            List<Panel> rightNeighborPlaceholders = rightNeighborWonder.getwonderPlaceholder();

            //start with the placeholders that are contained by the right neighbor as they do not change positions

            for (int count = 0; count < rightNeighborPlaceholders.Count; count++)
            {
                rightNeighborPlaceholders[count].Location = getwonderPlaceHolderPoint(1, player);
                rightNeighborPlaceholders[count].Size = getwonderPlaceHolderSize(1, player);
            }

            //if the first wonderstage has been built resize and reposition only the one the user has constructed to the first position (or position 0)

            if (wonderStagesBuilt.Count > 0)
            {
                leftNeighborPlaceholders[wonderStagesBuilt[0]].Location = getwonderPlaceHolderPoint(0, player);
                leftNeighborPlaceholders[wonderStagesBuilt[0]].Size = getwonderPlaceHolderSize(0, player);
            }

            //respotition and resize all remaining placeholders to the position in accordance to if we are waiting on the third stage or not

            int position = (wonderStagesBuilt.Count > 0 ? 2 : 0);

            for (int count = 0; count < leftNeighborPlaceholders.Count; count++)
            {
                if (wonderStagesBuilt.Count > 0)
                {
                    if (wonderStagesBuilt[0] == count)
                    {
                        continue;
                    }
                }

                leftNeighborPlaceholders[count].Location = getwonderPlaceHolderPoint(position, player);
                leftNeighborPlaceholders[count].Size = getwonderPlaceHolderSize(position, player);
            }
        }
        private Size getwonderPlaceHolderSize(int count, Player owner)
        {
            Size size = getwonderbuiltIndicatorSize(count, owner);

            switch (owner.getPlayerRotation())
            {
                case Player.PlayerRotation.None:
                    {
                        size.Height = Convert.ToInt32(owner.getWonder().Size.Height * 0.2);
                        break;
                    }
                case Player.PlayerRotation.Clockwise90:
                    {
                        size.Width = Convert.ToInt32(owner.getWonder().Size.Width * 0.2);
                        break;
                    }
                case Player.PlayerRotation.ClockWise270:
                    {
                        size.Width = Convert.ToInt32(owner.getWonder().Size.Width * 0.2);
                        break;
                    }
            }

            return size;
        }
        private Point getwonderPlaceHolderPoint(int count, Player owner)
        {
            Point point = getwonderbuiltIndicatorPoint(count, owner);

            switch (owner.getPlayerRotation())
            {
                case Player.PlayerRotation.None:
                    {
                        point.Y = Convert.ToInt32(owner.getWonder().Size.Height * 0.8);
                        break;
                    }
                case Player.PlayerRotation.Clockwise90:
                    {
                        point.X = 0;
                        point.Y -= owner.getWonder().Location.Y;
                        break;
                    }
                case Player.PlayerRotation.ClockWise270:
                    {
                        point.X = Convert.ToInt32(owner.getWonder().Size.Width * 0.8);
                        point.Y -= owner.getWonder().Location.Y;
                        break;
                    }
            }

            return point;
        }
        private Size getwonderbuiltIndicatorSize(int count, Player owner)
        {
            Size size = new Size(0, 0);
                switch (owner.getPlayerRotation())
                {
                    case Player.PlayerRotation.None:
                        {
                            size.Width = Convert.ToInt32(owner.getWonder().Size.Width * 0.25);
                         

                            size.Height = Convert.ToInt32(size.Width * 4 / 2.5);

                            break;
                        }
                    case Player.PlayerRotation.Clockwise90:
                        {
                            if (owner.getWonder().wonderManager.getNbWonderStages() == 1)
                            {
                                size.Height = Convert.ToInt32(owner.getWonder().Size.Height / 3);
                            }
                            else
                            {
                                size.Height = Convert.ToInt32(owner.getWonder().Size.Height * 0.25);
                            }

                            size.Width = Convert.ToInt32(size.Height * 4 / 2.5);

                            break;
                        }
                    case Player.PlayerRotation.ClockWise270:
                        {
                            if (owner.getWonder().wonderManager.getNbWonderStages() == 1)
                            {
                                size.Height = Convert.ToInt32(owner.getWonder().Size.Height / 3);
                            }
                            else
                            {
                                size.Height = Convert.ToInt32(owner.getWonder().Size.Height * 0.25);
                            }

                            size.Width = Convert.ToInt32(size.Height * 4 / 2.5);

                            break;
                        }
                    case Player.PlayerRotation.Clockwise180:
                        {

                            size.Width = Convert.ToInt32(owner.getWonder().Size.Width * 0.25);
                            size.Height = Convert.ToInt32(size.Width * 4 / 2.5);

                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            
            return size;
        }
        private Point getwonderbuiltIndicatorPoint(int count, Player owner)
        {
            Point point = new Point(0, 0);

            switch (owner.getPlayerRotation())
            {
                case Player.PlayerRotation.None:
                    {
                        point.Y = owner.getWonder().Location.Y + owner.getWonder().Size.Height;

                        if (owner.getWonder().wonderManager.getNbWonderStages() == 1)
                        {
                            point.X = Convert.ToInt32(owner.getWonder().Size.Width / 3);
                        }
                        else if (owner.getWonder().wonderManager.getNbWonderStages() == 2)
                        {
                            point.X = Convert.ToInt32(owner.getWonder().Size.Width / 4 * (count + 1));
                        }
                        else if (owner.getWonder().wonderManager.getNbWonderStages() == 3)
                        {
                            point.X = Convert.ToInt32(owner.getWonder().Size.Width / 3 * count);
                        }
                        else if (owner.getWonder().wonderManager.getNbWonderStages() == 4)
                        {
                            point.X = Convert.ToInt32(owner.getWonder().Size.Width / 4 * count);
                        }

                        break;
                    }
                case Player.PlayerRotation.Clockwise180:
                    {
                        if (owner.getWonder().wonderManager.getNbWonderStages() == 1)
                        {
                            point.X = Convert.ToInt32(owner.getWonder().Size.Width / 3);
                        }
                        else if (owner.getWonder().wonderManager.getNbWonderStages() == 2)
                        {
                            point.X = Convert.ToInt32(owner.getWonder().Size.Width / 4 * (2 - count));
                        }
                        else if (owner.getWonder().wonderManager.getNbWonderStages() == 3)
                        {
                            point.X = Convert.ToInt32(player.getWonder().Size.Width / 3 * (2 - count));
                        }
                        else if (owner.getWonder().wonderManager.getNbWonderStages() == 4)
                        {
                            point.X = Convert.ToInt32(owner.getWonder().Size.Width / 4 * (3 - count));
                        }

                        break;
                    }
                case Player.PlayerRotation.Clockwise90:
                    {
                        if (owner.getWonder().wonderManager.getNbWonderStages() == 1)
                        {
                            point.Y = Convert.ToInt32(owner.getWonder().Size.Height / 3 + owner.getWonder().Location.Y);
                        }
                        else if (owner.getWonder().wonderManager.getNbWonderStages() == 2)
                        {
                            point.Y = Convert.ToInt32(owner.getWonder().Size.Height / 4 * (count + 1) + owner.getWonder().Location.Y);
                        }
                        else if (owner.getWonder().wonderManager.getNbWonderStages() == 3)
                        {
                            point.Y = Convert.ToInt32(owner.getWonder().Size.Height / 3 * count + owner.getWonder().Location.Y);
                        }
                        else if (owner.getWonder().wonderManager.getNbWonderStages() == 4)
                        {
                            point.Y = Convert.ToInt32(owner.getWonder().Size.Height / 4 * count + owner.getWonder().Location.Y);
                        }

                        break;
                    }
                case Player.PlayerRotation.ClockWise270:
                    {
                        point.X = owner.getWonder().Location.X + player.getWonder().Size.Width;

                        if (owner.getWonder().wonderManager.getNbWonderStages() == 1)
                        {
                            point.Y = Convert.ToInt32(owner.getWonder().Size.Height * 2 / 3 + owner.getWonder().Location.Y);
                        }
                        else if (owner.getWonder().wonderManager.getNbWonderStages() == 2)
                        {
                            point.Y = Convert.ToInt32(owner.getWonder().Size.Height / 4 * (2 - count) + owner.getWonder().Location.Y);
                        }
                        else if (owner.getWonder().wonderManager.getNbWonderStages() == 3)
                        {
                            point.Y = Convert.ToInt32(owner.getWonder().Size.Height / 3 * (2 - count) + owner.getWonder().Location.Y);
                        }
                        else if (owner.getWonder().wonderManager.getNbWonderStages() == 4)
                        {
                            point.Y = Convert.ToInt32(owner.getWonder().Size.Height / 4 * (3 - count) + owner.getWonder().Location.Y);
                        }

                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            return point;
                
        }
        public override int getNbWonderStages()
        {
            return 3;
        }

        public override int getVictoryPoints()
        {
            int points = 0;

            points += leftNeighborWonder.getVictoryPoints();
            points += rightNeighborWonder.getVictoryPoints();

            return points;
        }

        public override int getNbofWondersBuilt()
        {
            int total = 0;

            total += leftNeighborWonder.getNbofWondersBuilt();
            total += rightNeighborWonder.getNbofWondersBuilt();

            return total;
        }
    }
}
