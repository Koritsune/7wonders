﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;

namespace Client
{
    public partial class Form1 : Form
    {
        Config config;
        Game game;
        ASynchronousSocketListener netListen = new ASynchronousSocketListener();
        PNLMainContainer PNLmaincontainer_;
        PNLServer PNLServer_;
        KickReasonGenerator kickReasonGenerator = new KickReasonGenerator();

        public Form1()
        {
            InitializeComponent();
           
            //DesktopLocation = new Point(0, 0);
            Application.ApplicationExit += closing;
            //PNLServer_ = new PNLServer();
        }
        public PNLServer getPNLServer()
        {
            return PNLServer_;
        }
        public PNLMainContainer getPNLMainContainer()
        {
            return PNLmaincontainer_;
        }
        public void Form1_Load(object sender, EventArgs e)
        {
            config = new Config(this);
            game = new Game(this);
            PNLmaincontainer_ = new PNLMainContainer(this);
            PNLServer_ = new PNLServer(this);
            Rectangle resolution = Screen.PrimaryScreen.Bounds;
            //resolution.

            if (resolution.Height <= 768) {
                SetDesktopLocation((resolution.Width - this.Width)/2, 0);
            }
            

            //GBUsers.BackColor = Color.Transparent;
            //PNLMAIN.Dock = DockStyle.Fill;
            //PNLMAIN.BringToFront();
            PBLogo.Load(@"Pictures\\Icons\\7w_logo.png");
            PBLogo.SizeMode = PictureBoxSizeMode.StretchImage;
            PBLogo.BackColor = Color.Transparent;
            
            //PlayerIcon test = new PlayerIcon();
            PNLServer_.Dock = DockStyle.Fill;
            
            Controls.Add(PNLServer_);

            PNLmaincontainer_.Size = new Size(410, 260);
            PNLmaincontainer_.Location = new Point(465, 160);
            //PNLmaincontainer_.Location = new Point(500, 160);
            PNLmaincontainer_.BackColor = Color.Transparent;
            PNLMAIN.Controls.Add(PNLmaincontainer_);
            PNLMAIN.Dock = DockStyle.Fill;

            //PopupMessage test = new PopupMessage();
            //PNLMAIN.Controls.Add(test);
            //test.BringToFront();
            //test.setSize(200, 80);
            //test.setFontSize(12);
            //DisplayPictureChoice test = new DisplayPictureChoice(config);
            //test.Size = new Size(400, 130);
            //test.Location = new Point(475, 160);
            
            //PNLMAIN.Controls.Add(test);

            //test.BringToFront();

        }
        public Game getGame() {
            return game;
        }
        public Config getConfig() {
            return config;
        }
        private void closing(Object sender, EventArgs e) {
            if (config.isHost()) {
                PNLmaincontainer_.getServer().stop();
            }
            game.endGame();
            config.saveToFile();
        }
        delegate void launchSocketKeepAlive(SocketKeepAlive ans);

        public void launchSocketKeepAliveD(SocketKeepAlive ans)
        {
            if (this.InvokeRequired)
            {
                launchSocketKeepAlive d = new launchSocketKeepAlive(launchSocketKeepAliveD);
                this.Invoke(d, new object[] { ans });
            }
            else
            {
                ans.start();
            }
        }

        delegate void launchTimer(Timer ans);

        public void launchTimerD(Timer ans)
        {
            if (this.InvokeRequired)
            {
                launchTimer d = new launchTimer(launchTimerD);
                this.Invoke(d, new object[] { ans });
            }
            else
            {
                ans.Start();
            }
        }

        public KickReasonGenerator getkickReasonGenerator() {
            return kickReasonGenerator;
        }
        
    }
}
