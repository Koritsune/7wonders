﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Collections;
using System.Net;
using System.Net.Sockets;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            

            /*
            XmlTextReader xmlReader = new XmlTextReader("Configuration\\Cities.xml");

            ArrayList list = new ArrayList();

            while (xmlReader.Read()) {
                //xmlReader.Name();
                //xmlReader.MoveToAttribute(0);
                if (xmlReader.Name == "City")
                {
                    Console.WriteLine(xmlReader.Name);
                    Console.WriteLine(xmlReader.GetAttribute("name"));
                    list.Add(xmlReader.GetAttribute("name"));
                    //label1.Text = label1.Text + "\n" + xmlReader.Value;
                }
                

            }
            Console.WriteLine("Image trial" + list[0]);
            String loc = "Pictures\\Wonders\\" + list[5] + ".PNG";
            Console.WriteLine(loc);
            //pictureBox1.ImageLocation = loc;
            Bitmap temp = (Bitmap)Image.FromFile(loc);
            //temp.RotateFlip(RotateFlipType.Rotate180FlipNone);
            //pictureBox1.Image = temp;
            //pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            //pictureBox1.r
            //pictureBox1.Refresh();
            //pictureBox1.Image = Image.FromFile("Pictures\\" + list[0] + ".PNG");
             * */
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String data;
            byte[] bytes = new Byte[1024];

            IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            Console.WriteLine(ipAddress);
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 6114);

            Socket listener = new Socket(AddressFamily.InterNetwork,
            SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and 
            // listen for incoming connections.
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(10);

                // Start listening for connections.
                while (true)
                {
                    Console.WriteLine("Waiting for a connection...");
                    // Program is suspended while waiting for an incoming connection.
                    Socket handler = listener.Accept();
                    data = null;

                    // An incoming connection needs to be processed.
                    while (true)
                    {
                        bytes = new byte[1024];
                        int bytesRec = handler.Receive(bytes);
                        data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        if (data.IndexOf("<EOF>") > -1)
                        {
                            break;
                        }
                    }

                    // Show the data on the console.
                    Console.WriteLine("Text received : {0}", data);

                    // Echo the data back to the client.
                    byte[] msg = Encoding.ASCII.GetBytes(data);

                    handler.Send(msg);
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }

            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.ToString());
            }
        }
    }
}
